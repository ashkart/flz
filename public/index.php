<?php

use Application\Application;

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the frontend root now.
 */
chdir(dirname(__DIR__));

date_default_timezone_set('Europe/Moscow');

define('ROOT_PATH', __DIR__ . '/..');
define('APP_PATH', __DIR__ . '/../Application');
define('VENDOR_PATH', __DIR__ . '/../vendor');

try {
    include 'init_autoloader.php';

    include __DIR__ . '/../Application/config/autoload/init_logger.php';
} catch (\Exception | Throwable $exception) {
    echo $exception;
    exit();
}

try {
    if (! class_exists(Application::class)) {
        throw new RuntimeException(
            "Unable to load application.\n"
            . "- Type `composer install` if you are developing locally.\n"
            . "- Type `vagrant ssh -c 'composer install'` if you are using Vagrant.\n"
            . "- Type `docker-compose run zf composer install` if you are using Docker.\n"
        );
    }

    // Retrieve configuration
    $appConfig = require __DIR__ . '/../Application/config/application.config.php';

    // Run the frontend!
    Application::init($appConfig)->run();
} catch (\Exception | Throwable $exception) {
    echo $exception;
    logError($exception, $appConfig);
}
