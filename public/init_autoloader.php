<?php

namespace Application;

include __DIR__ . '/../vendor/autoload.php';

require_once VENDOR_PATH . '/zendframework/zend-loader/src/AutoloaderFactory.php';

\Zend\Loader\AutoloaderFactory::factory([
    'Zend\Loader\StandardAutoloader' => [
        'namespaces' => [
            "Library\\" => "Library/"
        ]
    ],
    'Library\Master\Autoload\Autoloader' => [
        'namespaces' => [
            "Application\\"               => APP_PATH . "/",
            "Main\\"                      => APP_PATH . "/Module/Main/",
            "Frontend\\"                  => APP_PATH . "/Module/Frontend/",
            "Admin\\"                     => APP_PATH . "/Module/Admin/",
            "Console\\"                   => APP_PATH . "/Module/Console/",
        ]
    ]
]);