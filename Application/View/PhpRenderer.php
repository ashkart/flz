<?php

namespace Application\View;

use Application\Model\Domain\User;
use Main\View\Helper\Contact;
use Main\View\Helper\Form as FormHelper;
use Main\View\Helper\JData;
use Main\View\Helper\Plural;
use Main\View\Helper\Translator;
use Main\View\Helper\Url;
use Main\View\Helper\Version;
use Main\View\Helper\ViewMessage;
use Zend\Form\FormInterface;
use Zend\View\Helper as ZendHelper;
use Zend\View\Renderer\PhpRenderer as ZendPhpRenderer;

/**
 * @method Contact contact()
 * @method JData jData()
 * @method FormHelper form(FormInterface $form = null)
 * @method Plural plural(array $strings, int $number)
 * @method Translator translator()
 * @method Url        url($name = null, array $params = array(), $options = array(), $reuseMatchedParams = false)
 * @method ViewMessage viewMessage()
 * @method Version version()
 */
class PhpRenderer extends ZendPhpRenderer implements ViewInterface
{

    public function __call($name, $arguments)
    {
        return parent::__call($name, $arguments);
    }
}