<?php

namespace Application\View;

use Interop\Container\ContainerInterface;
use Main\Module;
use Zend\Mvc\View\Http;
use Zend\Mvc\Console;
use Zend\ServiceManager\Factory\FactoryInterface;

class ViewManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (Module::isCli()) {
            return new Console\View\ViewManager();
        }

        return new Http\ViewManager();
    }
}