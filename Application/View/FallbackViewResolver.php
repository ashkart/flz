<?php

namespace Application\View;

use Interop\Container\ContainerInterface;
use Zend\View\Renderer\RendererInterface as Renderer;
use Zend\View\Resolver\ResolverInterface;
use Zend\View\Resolver\TemplateMapResolver;

class FallbackViewResolver implements ResolverInterface
{
    /**
     * @var string
     */
    protected $_fallbackTemplate;

    /**
     * @var TemplateMapResolver
     */
    protected $_templateResolver;

    public function __construct(ContainerInterface $container, string $fallbackTemplate)
    {
        $this->_templateResolver = $container->get(TemplateMapResolver::class);
        $this->_fallbackTemplate = $fallbackTemplate;
    }

    public function resolve($name, Renderer $renderer = null)
    {
        return $this->_templateResolver->resolve($this->_fallbackTemplate, $renderer);
    }
}