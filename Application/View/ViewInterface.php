<?php

namespace Application\View;

use Application\Model\Domain\User;
use Main\View\Helper\ConfigData;
use Main\View\Helper\Contact;
use Main\View\Helper\JData;
use Main\View\Helper\Plural;
use Main\View\Helper\Translator;
use Main\View\Helper\Url;
use Main\View\Helper\Version;
use Main\View\Helper\ViewMessage;
use Zend\Form\FormInterface;
use Main\View\Helper\Form as FormHelper;
use Zend\View\Helper as ZendHelper;

/**
 * @method string                                              asset($asset)
 * @method string|null                                         basePath($file = null)
 * @method ConfigData                                          configData()
 * @method Contact                                             contact()
 * @method ZendHelper\Cycle                                    cycle(array $data = array(), $name = \Zend\View\Helper\Cycle::DEFAULT_NAME)
 * @method ZendHelper\DeclareVars                              declareVars()
 * @method ZendHelper\Doctype                                  doctype($doctype = null)
 * @method mixed                                               escapeCss($value, $recurse = \Zend\View\Helper\Escaper\AbstractHelper::RECURSE_NONE)
 * @method mixed                                               escapeHtml($value, $recurse = \Zend\View\Helper\Escaper\AbstractHelper::RECURSE_NONE)
 * @method mixed                                               escapeHtmlAttr($value, $recurse = \Zend\View\Helper\Escaper\AbstractHelper::RECURSE_NONE)
 * @method mixed                                               escapeJs($value, $recurse = \Zend\View\Helper\Escaper\AbstractHelper::RECURSE_NONE)
 * @method mixed                                               escapeUrl($value, $recurse = \Zend\View\Helper\Escaper\AbstractHelper::RECURSE_NONE)
 * @method FormHelper                                          form(FormInterface $form = null)
 * @method ZendHelper\Gravatar                                 gravatar($email = "", $options = array(), $attribs = array())
 * @method ZendHelper\HeadLink                                 headLink(array $attributes = null, $placement = \Zend\View\Helper\Placeholder\Container\AbstractContainer::APPEND)
 * @method ZendHelper\HeadMeta                                 headMeta($content = null, $keyValue = null, $keyType = 'name', $modifiers = array(), $placement = \Zend\View\Helper\Placeholder\Container\AbstractContainer::APPEND)
 * @method ZendHelper\HeadScript                               headScript($mode = \Zend\View\Helper\HeadScript::FILE, $spec = null, $placement = 'APPEND', array $attrs = array(), $type = 'text/javascript')
 * @method ZendHelper\HeadStyle                                headStyle($content = null, $placement = 'APPEND', $attributes = array())
 * @method ZendHelper\HeadTitle                                headTitle($title = null, $setType = null)
 * @method string                                              htmlFlash($data, array $attribs = array(), array $params = array(), $content = null)
 * @method string                                              htmlList(array $items, $ordered = false, $attribs = false, $escape = true)
 * @method string                                              htmlObject($data = null, $type = null, array $attribs = array(), array $params = array(), $content = null)
 * @method string                                              htmlPage($data, array $attribs = array(), array $params = array(), $content = null)
 * @method string                                              htmlQuicktime($data, array $attribs = array(), array $params = array(), $content = null)
 * @method User                                                identity()
 * @method ZendHelper\InlineScript                             inlineScript($mode = \Zend\View\Helper\HeadScript::FILE, $spec = null, $placement = 'APPEND', array $attrs = array(), $type = 'text/javascript')
 * @method JData                                               jData()
 * @method string|void                                         json($data, array $jsonOptions = array())
 * @method ZendHelper\Layout                                   layout($template = null)
 * @method ZendHelper\Navigation                               navigation($container = null)
 * @method string                                              paginationControl(\Zend\Paginator\Paginator $paginator = null, $scrollingStyle = null, $partial = null, $params = null)
 * @method string|ZendHelper\Partial                           partial($name = null, $values = null)
 * @method string                                              partialLoop($name = null, $values = null)
 * @method ZendHelper\Placeholder\Container\AbstractContainer  placeholder($name = null)
 * @method Plural                                              plural(array $strings, int $number)
 * @method string                                              renderChildModel($child)
 * @method void                                                renderToPlaceholder($script, $placeholder)
 * @method string                                              serverUrl($requestUri = null)
 * @method Translator                                          translator()
 * @method Url                                                 url($name = null, array $params = array(), $options = array(), $reuseMatchedParams = false)
 * @method Version                                             version()
 * @method ViewMessage                                         viewMessage()
 * @method ZendHelper\ViewModel                                viewModel()
 * @method ZendHelper\Navigation\Breadcrumbs                   breadCrumbs($container = null)
 * @method ZendHelper\Navigation\Links                         links($container = null)
 * @method ZendHelper\Navigation\Menu                          menu($container = null)
 * @method ZendHelper\Navigation\Sitemap                       sitemap($container = null)
 */
interface ViewInterface
{

}