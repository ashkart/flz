<?php

namespace Application\View\Factory;

use Application\View\PhpRenderer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class PhpRendererFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $renderer = new PhpRenderer();

        if (! $renderer instanceof $requestedName) {
            throw new \Exception('Wrong renderer class');
        }

        $renderer->setHelperPluginManager($container->get('ViewHelperManager'));
        $renderer->setResolver($container->get('ViewResolver'));

        return $renderer;
    }
}