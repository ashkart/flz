<?php


namespace Application\View\Factory;


use Application\Helper\ServiceManager\Factory;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\I18n\Translator\Loader\PhpArray;
use Zend\I18n\Translator\Resources;
use Zend\Mvc\I18n\Translator as MvcTranslator;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;

class TranslatorDelegator implements DelegatorFactoryInterface
{

    /**
     * A factory that creates delegates of a given service
     *
     * @param  ContainerInterface $container
     * @param  string $name
     * @param  callable $callback
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var \Zend\I18n\Translator\Translator $translator */
        $translator = $callback();

        $translator->addTranslationFilePattern(
            PhpArray::class,
            Resources::getBasePath(),
            Resources::getPatternForValidator()
        );

        /* Переопределение локализованных сообщений валидаторов */
        $config = $container->get(Factory::CONFIG);
        $validatorTranslatorConfig = $config['translator']['validator_translation_file_patterns']['default'];

        $translator->addTranslationFilePattern(
            $validatorTranslatorConfig['type'],
            $validatorTranslatorConfig['base_dir'],
            $validatorTranslatorConfig['pattern']
        );

        return new MvcTranslator($translator);
    }
}