<?php

return [
    'admin_panel' => 'Панель управления',
    'sign_in' => 'Войти',
    'logout' => 'Выйти',
    'not_registered' => 'Нет аккаунта?',
    'register' => 'Регистрация',
    'reset_password' => 'Сброс пароля',
    'job' => 'Работа',
    'forgot_password' => 'Забыли пароль?',
    'freelance' => 'Фриланс',
    'find_vacancies' => 'Искать вакансии',
    'find_resumes' => 'Искать сотрудников',
    'find_orders' => 'Искать заказы',
    'find_freelancers' => 'Искать исполнителя',
    'profile' => 'Профиль',
];