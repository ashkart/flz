<?php

return [
    'admin_panel' => 'Admin panel',
    'sign_in' => 'Sign in',
    'logout' => 'Logout',
    'not_registered' => 'Not registered',
    'register' => 'Register',
    'reset_password' => 'Reset password',
    'job' => 'Job',
    'forgot_password' => 'Forgot password?',
    'freelance' => 'Freelance',
    'find_vacancies' => 'Find vacancies',
    'find_resumes' => 'Find resumes',
    'find_orders' => 'Find orders',
    'find_freelancers' => 'Find freelancers',
    'profile' => 'Profile',
];