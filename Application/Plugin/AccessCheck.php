<?php

namespace Application\Plugin;
/**
 * @see http://www.youtube.com/watch?v=1xHuGqQ7jEQ
 */

use Application\Application;
use Application\Controller\HttpClientException;
use Application\Controller\HttpException;
use Application\Controller\HttpRedirectException;
use Application\Controller\HttpServerException;

use Application\Helper\RouteHelper;
use Library\Master\Controller\Plugin\AbstractPlugin;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain;
use Zend\Authentication\AuthenticationService;
use Zend;
use Library\Master;

class AccessCheck extends AbstractPlugin
{
    const SUPPORTED_METHODS = self::ALL_METHODS;

    /**
     * @var null | Master\Acl\Acl
     */
    private $_acl = NULL;

    /**
     * @var null | AuthenticationService
     */
    private $_auth = NULL;

    public function __construct(Zend\ServiceManager\ServiceLocatorInterface $sm)
    {
        parent::__construct($sm);
        $this->_acl  = $sm->get(Factory::ACL);
        $this->_auth = $sm->get(Factory::AUTH_SERVICE);
    }

    public function process()
    {
        $rm = $this->_event->getRouteMatch();
        $this->doAccessCheck(
            $rm->getParam('module') . ':' . $rm->getParam('controller'),
            $rm->getParam('action'),
            $this->_event
        );
    }

    /**
     * @param Zend\Mvc\MvcEvent | Zend\EventManager\EventInterface $e
     */
    private function doAccessCheck($resource, $privilege, $e)
    {
        $user = $this->_auth->getIdentity() ?? Domain\User::createEmpty();

        $role = $user->getRole();

        if (!$this->_acl->hasRole($role)) {
            throw new HttpServerException(HttpServerException::INTERNAL_SERVER_ERROR_500, "Невозможно определить ACL-роль $role");
        }

        if (!$this->_acl->hasResource($resource)) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        if ($this->_acl->isAllowed($role, $resource, $privilege)) {
            return;
        }

        $request = $this->_event->getRequest(); /** @var Zend\Http\PhpEnvironment\Request $request */

        if ($request->isXmlHttpRequest()) {
            throw new HttpClientException(HttpClientException::FORBIDDEN_403);
        }

        if ($user->isGuest()) {
            $rh = new RouteHelper($e->getRouter(), $e->getRouteMatch());
            $url = $rh->frontend('auth', 'index') . '?request=' . urlencode($request->getRequestUri());

            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $url);
        } else {
            throw new HttpClientException(HttpClientException::FORBIDDEN_403);
        }
    }
}
