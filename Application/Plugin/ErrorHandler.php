<?php

namespace Application\Plugin;

use Application\Controller\HttpClientException;
use Application\Controller\HttpRedirectException;
use Application\Controller\HttpServerException;
use Application\Helper\ServiceManager\Factory;

use Application\Model\Domain;

use Application\Controller\HttpException;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;
use Zend\Http\Header\Exception\InvalidArgumentException;

use Zend\Log;
use Zend\Mvc\Application;

use Library\Master;

/**
 * Class ErrorHandler
 * @package Application\Plugin
 * @see https://samsonasik.wordpress.com/2014/01/01/zend-framework-2-programmatically-handle-404-page/
 *
 */
class ErrorHandler extends Master\Controller\Plugin\AbstractPlugin
{
    public function process()
    {
        $event = $this->_event;

        $request    = $event->getRequest();  /* @var Request  $request */
        $response   = $event->getResponse(); /* @var Response $response */

        $log = true;

        switch ($event->getError()) {
            case Application::ERROR_CONTROLLER_NOT_FOUND:
            case Application::ERROR_CONTROLLER_INVALID:
            case Application::ERROR_ROUTER_NO_MATCH:
                $response->setStatusCode(HttpClientException::NOT_FOUND_404);
                $this->_setViewMessage('Запрошенная страница не найдена');
                $log = false;
                break;

            case Application::ERROR_EXCEPTION:
            default:

                $exception = $event->getResult()->exception;

                /** @var \Exception $exception */
                if (!$exception) {
                    return;
                }

                $code = $exception->getCode();

                if ($exception instanceof InvalidArgumentException) {
                    $response->setStatusCode(HttpClientException::BAD_REQUEST_400);
                    $log = false;
                } else if ($exception instanceof HttpException) {
                    /**
                     * @var HttpException | HttpRedirectException | HttpClientException | HttpServerException $exception
                     */

                    switch ($code) {
                        /* @see https://ru.wikipedia.org/wiki/Список_кодов_состояния_HTTP */

                        /* Redirects */

                        case HttpRedirectException::MOVED_PERMANENTLY_301:
                        case HttpRedirectException::FOUND_302:
                        case HttpRedirectException::SEE_OTHER_303:
                        case HttpRedirectException::TEMPORARY_REDIRECT_307:
                            $response->getHeaders()->addHeaderLine('Location', $exception->getUrl());
                            $response->setStatusCode($code);
                            $response->sendHeaders();
                            $log = false;
                            break;

                        /* Client errors */

                        case HttpClientException::BAD_REQUEST_400:
                        case HttpClientException::METHOD_NOT_ALLOWED_405:
                            $response->setStatusCode($code);
                            $log = false;
                            break;

                        case HttpClientException::FORBIDDEN_403:
                        case HttpClientException::NOT_FOUND_404:
                        case HttpClientException::GONE_410:
                        case HttpClientException::I_AM_A_TEAPOT_418:
                            $response->setStatusCode($code);
                            $this->_setViewMessage($exception->getMessage());
                            $log = false;
                            break;

                        case HttpClientException::LOCKED_423:
                        case HttpClientException::UPGRADE_REQUIRED_426:
                            $response->setStatusCode($code);
                            $response->getHeaders()->addHeaderLine('Message', $exception->getMessage());
                            $log = false;
                            break;

                        /* Server errors */

                        case HttpServerException::INTERNAL_SERVER_ERROR_500:
                            $response->setStatusCode($code);
                            $this->_setViewMessage($exception->getMessage());

                            break;

                        case HttpServerException::SERVICE_UNAVAILABLE_503:
                            $response->setStatusCode($code);
                            $response->getHeaders()->addHeaderLine('Retry-After', $exception->getRetryAfter() ?? 60);

                            // @TODO сообщение пользователю в отдельном шаблоне
                            break;

                        default:
                            $response->setStatusCode(HttpServerException::INTERNAL_SERVER_ERROR_500);
                    }

                    /**
                     * Здесь подменяем ViewModel, которую принесло исключение
                     */
                    if ($exception->getViewModel() !== null) {
                        $event->setViewModel($exception->getViewModel());
                        $event->stopPropagation();
                    }
                } else {
                    $response->setStatusCode(HttpServerException::INTERNAL_SERVER_ERROR_500);
                }
        }

        if ($log) {
            try {
                $this->_sm->get(Factory::ERROR_HANDLER)->logException($exception, $event->getRequest());
            } catch (Log\Exception\RuntimeException $e) {
                // Невозможно открыть или записать файл лога
                echo 'Возникла непредвиденная техническая ошибка. Пожалуйста, сообщите о ней администрации сайта. Невозможно открыть файл ошибок.';
                exit(1);
            }

            if ($response->getStatusCode() == Response::STATUS_CODE_500) {
                $this->logHeaders($request);
            }
        }


        if (
            $response->getStatusCode() !== Response::STATUS_CODE_200 &&
            ($request->isXmlHttpRequest() || !$this->_canRenderViewModel())
        ) {
            $response->send();
            exit();
        }
    }

    public function logHeaders(Request $request)
    {
        $logger = new Log\Logger();
        $writer = new Log\Writer\Stream($this->_sm->get(Factory::CONFIG)['logs']['headers']);
        $logger->addWriter($writer);

        $headers = [];
        foreach ($request->getHeaders()->toArray() as $name => $value) {
            if ($name === 'Cookie') {
                $sessionKey = Domain\ClientData::COOKIE_SESSION_ID;
                $value = preg_replace("~$sessionKey=[a-zA-Z0-9]+~", "$sessionKey=XXXХХХХХ", $value, 1);
            }

            $headers[] = "$name: $value";
        }

        $headers = "\n" . join("\n", $headers);

        $logger->debug($headers);
    }

    /**
     * @param string $message
     */
    protected function _setViewMessage(string $message)
    {
        $request = $this->_event->getRequest();  /** @var Request $request */

        $isXmlHttpRequest = $request->isXmlHttpRequest();

        if ($isXmlHttpRequest) {
            return;
        }

        $this->_event->getViewModel()->setVariable('i18n', $message);
    }

    protected function _canRenderViewModel() : bool
    {
        try {
            // получить view, в которое надо запихивать сообщение
            $viewModel = $this->_event->getViewModel();
            // попробовать отрисовать view
            $phpRenderer = $this->_sm->get(Factory::VIEW_RENDERER);
            $phpRenderer->render($viewModel);

            return true;

        } catch (\Exception $e) {
            // не получилось - что ж поделаешь
            return false;
        }
    }
}