<?php

namespace Application\Plugin;

use Application\Helper\DataSource\TraitIdentity;
use Application\Helper\RouteHelper;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain;
use Library\Master\Controller\Plugin\AbstractPlugin;
use Library;
use Zend\EventManager\EventInterface;

class ClientData extends AbstractPlugin
{
    use TraitIdentity;

    const SUPPORTED_METHODS   = self::COMMON_METHODS;
    const EXCLUDE_JS_REQUESTS = true;

    public function clean(EventInterface $e = null)
    {
        if ($e && !$this->_setup($e)) {
            return;
        }

        $authService = $this->_sm->get(Factory::AUTH_SERVICE);
        $clientData = new Domain\ClientData();

        if (!$authService->getIdentity() && !$clientData->hasMessages()) {
//            $clientData->getMessagesClean();
        }
    }

    public function send(EventInterface $e = null)
    {
        if ($e && !$this->_setup($e)) {
            return;
        }

        $router  = $this->_sm->get('Router');
        $request = $this->_sm->get('Request');
        $routeHelper = new RouteHelper($router, $router->match($request));

        /**
         * Данные в куках.
         */
        $user = self::_getUser();

        $clientData = new Domain\ClientData();
        $clientData->set($user);
    }

    public function process()
    {
        // Method Plugin\ClientData::process() is not callable;
    }

    protected function _getAccountUnconfirmedMessage(Domain\User $user, RouteHelper $routeHelper)
    {
        //@TODO
    }
}