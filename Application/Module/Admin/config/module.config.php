<?php

namespace Admin;

return [
    'controllers' => [
        'invokables' => [
            'city'            => Controller\City::class,
            'company'         => Controller\Company::class,
            'education'       => Controller\Education::class,
            'index'           => Controller\Index::class,
            'profarea'        => Controller\Profarea::class,
            'resume'          => Controller\Resume::class,
            'vacancy'         => Controller\Vacancy::class,
            'user'            => Controller\User::class,
            'work_experience' => Controller\WorkExperience::class
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'strategies' => [
            'ViewJsonStrategy',
        ],
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'frontend/index/index'    => __DIR__ . '/../view/frontend/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'logger' => [
        'error' => __DIR__ . '/var/logs/error.log'
    ]
];
