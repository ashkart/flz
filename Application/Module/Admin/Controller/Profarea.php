<?php

namespace Admin\Controller;

use Application\Model\Domain;
use Zend\View\Model\JsonModel;

class Profarea extends AbstractController
{
    public function indexAction()
    {
        $this->_checkMethodGetOrHead();

        $profarea = $this->_checkIdFetchModel(self::$ds->profareaMapper());

        return new JsonModel($profarea->getTableRow());
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $profareaRootItems = self::$ds->profareaMapper()->fetchRootWithCollections();

        $profareaList = [];

        foreach ($profareaRootItems as $profareaRootItem) {
            /** @var Domain\Profarea $profarea */
            foreach ($profareaRootItem->getChildren()->asArray() as $profarea) {
                $profareaList[$profarea->getId()] = $profarea->getTableRow();
            }
        }

        return new JsonModel($profareaList);
    }
}