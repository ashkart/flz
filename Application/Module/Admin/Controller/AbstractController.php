<?php

namespace Admin\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\AbstractModel;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Service\Filter\AbstractFilter;
use Zend\Http\PhpEnvironment\Request;
use Zend\View\Model\JsonModel;

class AbstractController extends \Main\Controller\AbstractController
{
    protected function _addFilterPagingAndOrder(AbstractFilter $filter)
    {
        $params = $this->_fromQuery();

        $itemsPerPage = $params[self::PARAM_PER_PAGE];
        $pageNumber   = $params[self::PARAM_PAGE];

        $sortBy = $params[self::PARAM_SORT_BY];
        $order  = $params[self::PARAM_ORDER];

        if ($itemsPerPage && $pageNumber) {
            $filter->setPaging($itemsPerPage, $pageNumber);
        }

        if ($sortBy && $order) {
            $filter->setOrder([$sortBy => $order]);
        }
    }

    public function _delete(AbstractMapper $mapper)
    {
        $this->_checkMethod([Request::METHOD_GET, Request::METHOD_POST]);

        if ($this->request->getMethod() === Request::METHOD_GET) {
            $ids = [$this->_fromRoute('id')];
        } else {
            $ids = $this->_fromPost('ids');
        }

        if (!$ids) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        $result = $mapper->deleteByIds($ids);

        return new JsonModel([
            self::STATE => (int) $result
        ]);
    }
}