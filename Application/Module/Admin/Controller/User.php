<?php

namespace Admin\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Service\Filter\UserFilter;
use Zend\View\Model\JsonModel;
use Main\View\Form;

class User extends AbstractController
{
    public function indexAction()
    {
        $this->_checkMethodGetOrHead();

        $user = $this->_checkIdFetchModel(self::$ds->userMapper());

        return new JsonModel($user->getTableRow());
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $params = $this->_fromQuery();

        $filters = $params[self::PARAM_FILTERS] ? json_decode($params[self::PARAM_FILTERS], JSON_OBJECT_AS_ARRAY) : null;

        $filter = new UserFilter();

        $this->_addFilterPagingAndOrder($filter);

        if ($filters) {
            //@TODO
        }

        $result = self::$ds->userService()->getList($filter, true);

        return new JsonModel($result);
    }

    public function saveAction()
    {
        $this->_checkMethodPost();

        $id   = $this->_fromRoute('id');
        $data = $this->_fromPost();

        $form = new Form\User();
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                'messages'   => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        if (!$id) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        $user = self::$ds->userMapper()->fetch($id);

        if (!$user) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        self::$ds->userService()->update($user, $formData);

        return new JsonModel($user->getTableRow());
    }
}