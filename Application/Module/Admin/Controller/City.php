<?php

namespace Admin\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Service\ElasticSearch;
use Zend\View\Model\JsonModel;

class City extends AbstractController
{
    public function indexAction()
    {
        $this->_checkMethodGetOrHead();

        $city = $this->_checkIdFetchModel(self::$ds->cityMapper());

        return new JsonModel($city->getTableRow());
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $params = $this->_fromQuery();

        $filter = json_decode($params[self::PARAM_FILTERS], JSON_OBJECT_AS_ARRAY);

        $queryStr = $filter['q'] ?? '';

        $result = [];

        if (strlen($queryStr) >= 2) {
            $cities = self::$ds->elasticSearchService()->queryMatch(ElasticSearch::INDEX_CITY, [
                'title' => $queryStr
            ]);

            foreach ($cities as $city) {
                $result[] = $city['_source'];
            }
        }

        return new JsonModel($result);
    }
}