<?php

namespace Admin\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Domain\Vacancy\VacancyMapper;
use Application\Model\Service\Filter\VacancyFilter;
use Main\View\Form;
use Zend\View\Model\JsonModel;

class Vacancy extends AbstractController
{
    public function indexAction()
    {
        $this->_checkMethodGetOrHead(true);

        /** @var \Application\Model\Domain\Vacancy $vacancy */
        $vacancy = $this->_checkIdFetchModel(self::$ds->vacancyMapper());

        $vacancyRow = $vacancy->getTableRow(true);
        $vacancyRow['locations']   = self::$ds->vacancyService()->getLocationsAsTags($vacancy);
        $vacancyRow['profarea_id'] = [$vacancyRow['profarea_id']];

        return new JsonModel($vacancyRow);
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $params = $this->_fromQuery();

        $filters = $params[self::PARAM_FILTERS] ? json_decode($params[self::PARAM_FILTERS], JSON_OBJECT_AS_ARRAY) : null;

        $filter = new VacancyFilter();

        $this->_addFilterPagingAndOrder($filter);

        if ($filters) {
            // @TODO
        }

        $vacancies = self::$ds->vacancyService()->getList($filter, true);

        return new JsonModel($vacancies);
    }

    public function saveAction()
    {
        $this->_checkMethodPost();

        $id   = $this->_fromRoute('id');
        $data = $this->_fromPost();

        $data['profarea_id'] = $data['profarea_id'][0];

        $form = new Form\Vacancy();
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE  => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        if (!$id) {
            $vacancy = self::$ds->vacancyService()->create($formData);
        } else {
            $vacancy = self::$ds->vacancyMapper()->fetch($id);

            if (!$vacancy) {
                throw new HttpClientException(HttpClientException::NOT_FOUND_404);
            }

            self::$ds->vacancyService()->update($vacancy, $formData);
        }

        return new JsonModel($vacancy->getTableRow());
    }

    public function deleteAction()
    {
        return $this->_delete(self::$ds->vacancyMapper());
    }

    public function deleteManyAction()
    {
        return $this->_delete(self::$ds->vacancyMapper());
    }
}