<?php

namespace Admin\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Domain\Education\EducationMapper;
use Application\Model\Domain\Resume\ResumeMapper;
use Application\Model\Service\Filter\ResumeFilter;
use Main\View\Form;
use Zend\View\Model\JsonModel;

class Resume extends AbstractController
{
    public function indexAction()
    {
        $this->_checkMethodGetOrHead();

        $resume = $this->_checkIdFetchModel(self::$ds->resumeMapper());

        return new JsonModel($resume->getTableRow());
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $params = $this->_fromQuery();

        $filters = $params[self::PARAM_FILTERS] ? json_decode($params[self::PARAM_FILTERS], JSON_OBJECT_AS_ARRAY) : null;

        $filter = new ResumeFilter();

        $this->_addFilterPagingAndOrder($filter);

        if ($filters) {
            //@TODO
        }

        $result = self::$ds->resumeService()->getList($filter, true);

        return new JsonModel($result);
    }

    public function saveAction()
    {
        $this->_checkMethodPost();

        $data = $this->_fromPost();

        $form = new Form\Resume();
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE  => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $id = $this->_fromRoute('id');

        $formData = $form->getData();

        if ($id) {
            $resume = self::$ds->resumeMapper()->fetch($id);

            if (!$resume) {
                throw new HttpClientException(HttpClientException::NOT_FOUND_404);
            }

            foreach ([&$formData[$form::EL_EDUCATION_COLLECTION], &$formData[$form::EL_WORK_EXPERIENCE_COLLECTION]] as &$collection) {
                foreach ($collection as &$row) {
                    $row[EducationMapper::COL_RESUME_ID] = $resume->getId();
                }
            }

            self::$ds->resumeService()->update($resume, $formData);
        } else {
            $resume = self::$ds->resumeService()->create($formData);
        }

        return new JsonModel($resume->getTableRow());
    }

    public function deleteAction()
    {
        return $this->_delete(self::$ds->resumeMapper());
    }

    public function deleteManyAction()
    {
        return $this->_delete(self::$ds->resumeMapper());
    }
}