<?php

namespace Admin\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Service\Filter\CompanyFilter;
use Zend\View\Model\JsonModel;
use Main\View\Form;

class Company extends AbstractController
{
    public function indexAction()
    {
        $this->_checkMethodGetOrHead();

        $company = $this->_checkIdFetchModel(self::$ds->companyMapper());

        return new JsonModel($company->getTableRow());
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $page = $this->_fromQuery(self::PARAM_PAGE);
        $perPage = $this->_fromQuery(self::PARAM_PER_PAGE);

        $filter = new CompanyFilter();

        if ($page !== null && $perPage) {
            $filter->setPaging($perPage, $page);
        }

        $companies = self::$ds->companyService()->getList($filter, true);

        return new JsonModel($companies);
    }

    public function saveAction()
    {
        $this->_checkMethodPost();

        $id   = $this->_fromRoute('id');
        $data = $this->_fromPost();

        $form = new Form\Company($id !== null);
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE  => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        if ($id) {
            $company = self::$ds->companyMapper()->fetch($id);

            if (!$company) {
                throw new HttpClientException(HttpClientException::NOT_FOUND_404);
            }

            self::$ds->companyService()->update($company, $formData);
        } else {
            $company = self::$ds->companyService()->create($formData[Form\Company::EL_OFFICIAL_NAME]);
            $company = self::$ds->companyService()->update($company, $formData);
        }

        return new JsonModel($company->getTableRow());
    }
}