<?php

namespace Admin;

use Application\Helper\ServiceManager\Factory;
use Library\Master\Session\SessionManager;
use Psr\Container\ContainerInterface;
use Zend\EventManager\EventInterface;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Mvc\Controller\PluginManager;
use Zend\Mvc\MvcEvent;
use Application\Plugin;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\View;

class Module
{
    const NAME = 'admin';

    /**
     * @param MvcEvent | EventInterface $e
     */
    public function onBootstrap(EventInterface $e)
    {
        $app = $e->getApplication();
        $app->getEventManager()->attach('render', [$this, 'registerJsonStrategy'], 100);
        $this->bootstrapPlugins($e);

        // старт сессии после установки статик-переменных в AbstractHavingStaticDataSource
        $this->bootstrapSession($e);
    }

    /**
     * @param  MvcEvent $e The MvcEvent instance
     * @return void
     */
    public function registerJsonStrategy(MvcEvent $e)
    {
        $app          = $e->getTarget();
        /** @var ServiceLocatorInterface $sm */
        $sm           = $app->getServiceManager();
        /** @var View $view */
        $view         = $sm->get('Zend\View\View');
        $jsonStrategy = $sm->get('ViewJsonStrategy');

        // Attach strategy, which is a listener aggregate, at high priority
        $jsonStrategy->attach($view->getEventManager(), 100);
    }

    /**
     * @param EventInterface | MvcEvent $e
     */
    public function bootstrapPlugins(EventInterface $e)
    {
        $sm = $e->getApplication()->getServiceManager();

        $em = $e->getApplication()->getEventManager();

        $request = $e->getRequest();

        /** @var PluginManager $cpm */
        $cpm = $sm->get(PluginManager::class);
        $cpm->setFactory(Params::class, function (ContainerInterface $sm) use ($request) {
            return new Plugin\Params($request);
        });

        /**
         * Проверка прав доступа на страницу
         * Приоритет должен быть больше 1, т.к. с приоритетом 1 выполняется action
         */
        $em->attach(MvcEvent::EVENT_DISPATCH, new Plugin\AccessCheck($sm), 2);

        /**
         * Данные пользователя на клиенте
         */
        $clientData = new Plugin\ClientData($sm);
        $em->attach(MvcEvent::EVENT_DISPATCH, [$clientData, 'clean'], -20);
        $em->attach(MvcEvent::EVENT_FINISH,   [$clientData, 'send'], -20);

        /**
         * Обработка ошибок
         */
        $em->attach(MvcEvent::EVENT_DISPATCH_ERROR, new Plugin\ErrorHandler($sm), -10);
        $em->attach(MvcEvent::EVENT_RENDER_ERROR,   new Plugin\ErrorHandler($sm), -10);
//
//        /**
//         * Этот плагин должен быть последним.
//         */
//        $em->attach(MvcEvent::EVENT_FINISH, new Plugin\ClientCache($sm), -30);
    }

    /**
     * Настраиваем сессию
     * @param EventInterface | MvcEvent $e
     */
    public function bootstrapSession(EventInterface $e)
    {
        /** @var SessionManager $session */
        $session = $e->getApplication()
            ->getServiceManager()
            ->get(Factory::SESSION_MANAGER);
        $session->start();
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}
