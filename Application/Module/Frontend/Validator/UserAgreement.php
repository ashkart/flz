<?php

namespace Frontend\Validator;


use Frontend\Form\Registration;
use Zend\Validator\AbstractValidator;

class UserAgreement extends AbstractValidator
{
    const MSG_KEY_INVALID = 'not_accepted';

    const MESSAGE_NOT_ACCEPTED = 'Для регистрации необходимо принять соглашение о пользовании услугами данного ресурса';

    protected $messageTemplates = [
        self::MSG_KEY_INVALID => self::MESSAGE_NOT_ACCEPTED,
    ];

    public function isValid($value)
    {
        if ($value !== true && $value !== Registration::CHECKED) {
            $this->error(self::MSG_KEY_INVALID);
            return false;
        }

        return true;
    }
}