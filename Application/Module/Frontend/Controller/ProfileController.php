<?php

namespace Frontend\Controller;

use Application\Cache\Slot\PasswordUpdate;
use Application\Cache\UserSession;
use Application\Controller\HttpClientException;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Callback;
use Application\Model\Domain\Callback\CallbackMapper;
use Application\Model\Domain\ClientData;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Domain\Resume;
use Application\Model\Domain\Resume\ResumeMapper;
use Application\Model\Domain\Vacancy;
use Application\Model\Domain\View;
use Application\Model\Service\AbstractService;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\ResumeFilter;
use Application\Model\Service\Filter\VacancyFilter;
use Application\Model\Service\RedisMQ\Job\PasswordConfirmation;
use Main\View\Helper\JData;
use Zend\View\Model\JsonModel;

class ProfileController extends AbstractController
{
    const PASSWORD_OLD    = 'password_old';
    const PASSWORD_NEW    = 'password_new';
    const EMAIL           = 'email';

    public function editAction()
    {

    }

    public function viewAction()
    {
        $this->_checkMethodGetOrHead();

        return new JsonModel(self::$ds->userService()->getProfileDataForView($this->_user()));
    }

    public function resumeListAction()
    {
        $this->_checkMethodGetOrHead();

        $userId = $this->_getUserIdNotGuest();

        $page = $this->_fromQuery(self::PARAM_PAGE) - 1;
        $perPage = $this->_fromQuery(self::PARAM_PER_PAGE);

        $filter = new ResumeFilter();
        $filter->setUserId($userId);

        $filter->setPublishPeriod(ResumeFilter::PUBLISH_ALL_TIME);

        $filter->setOrder([ResumeMapper::COL_UPDATE_DATE => AbstractFilter::ORDER_DESC]);
        $filter->setPaging($perPage, $page);

        $resumes = self::$ds->resumeService()->elasticDataToJsonModels(
            self::$ds->resumeService()->queryFromElasticsearch($filter),
            true
        );

        return new JsonModel($resumes);
    }

    public function resumesAction()
    {
        $this->_checkMethodGetOrHead();

        $userId = $this->_getUserIdNotGuest();

        $resumes = self::$ds->resumeMapper()->fetchByUserId($userId);

        $result = [];

        foreach ($resumes as $resume) {
            if (!$resume->isInvisible()) {
                $result[] = $resume->getTableRow();
            }
        }

        return new JsonModel($result);
    }

    public function vacanciesAction()
    {
        $this->_checkMethodGetOrHead();

        $userId = $this->_getUserIdNotGuest();

        $vacancies = self::$ds->vacancyMapper()->fetchByUserId($userId);

        $result = [];

        foreach ($vacancies as $vacancy) {
            if (!$vacancy->isInvisible()) {
                $result[] = self::$ds->vacancyService()->getVacancyRowsAsListItem($vacancy);
            }
        }

        return new JsonModel($result);
    }

    public function vacancyListAction()
    {
        $this->_checkMethodGetOrHead();

        $userId = $this->_getUserIdNotGuest();

        $page = $this->_fromQuery(self::PARAM_PAGE) - 1;
        $perPage = $this->_fromQuery(self::PARAM_PER_PAGE);

        $filter = new VacancyFilter();
        $filter->setUserId($userId);

        $filter->setPublishPeriod(VacancyFilter::PUBLISH_ALL_TIME);

        $filter->setOrder([ResumeMapper::COL_UPDATE_DATE => AbstractFilter::ORDER_DESC]);
        $filter->setPaging($perPage, $page);

        $vacancies = self::$ds->vacancyService()->elasticDataToJsonModels(
            self::$ds->vacancyService()->queryFromElasticsearch($filter),
            true
        );

        return new JsonModel($vacancies);
    }

    public function callbackListAction()
    {
        $this->_checkMethodGetOrHead();

        if ($this->_user()->isGuest()) {
            throw new HttpClientException(HttpClientException::FORBIDDEN_403);
        }

        $pageIndex = $this->_fromQuery(self::PARAM_PAGE) - 1;
        $perPage   = $this->_fromQuery(self::PARAM_PER_PAGE);

        $callbackService = self::$ds->callbackService();

        $getResultRow = function($id) {
            return self::$ds->callbackMapper()->fetch($id)->getTableRow();
        };

        $callbackListData = $callbackService->fetchPagedFromIds(
            $callbackService->selectIdsByUser($this->_user()->getId()),
            $getResultRow,
            $pageIndex,
            $perPage
        );

        foreach ($callbackListData as $key => &$callbackRow) {
            if ($key === AbstractService::KEY_TOTAL_ITEMS) {
                continue;
            }

            $callbackRow[$callbackRow[CallbackMapper::COL_ENTITY_TYPE]] = $callbackService->fetchCallbackEntityRow(
                $callbackRow[CallbackMapper::COL_ENTITY_TYPE],
                $callbackRow[CallbackMapper::COL_ENTITY_ID]
            );

            $callbackRow[$callbackRow[CallbackMapper::COL_FROM_ENTITY_TYPE]] = $callbackService->fetchCallbackEntityRow(
                $callbackRow[CallbackMapper::COL_FROM_ENTITY_TYPE],
                $callbackRow[CallbackMapper::COL_FROM_ENTITY_ID]
            );

            $callbackRow['views'] = self::$ds->viewService()->selectByEntity(
                $callbackRow[CallbackMapper::PRIMARY],
                View::ENTITY_TYPE_CALLBACK
            );
        }

        $callbackListData = self::$ds->callbackService()->normalizeDataRowsForView($callbackListData);

        return new JsonModel($callbackListData);
    }

    public function updateUsernameAction()
    {
        $this->_checkMethodPost();

        $userName = $this->_fromPost('user_name');

        if (!$userName) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => [
                    'user_name' => ['Имя пользователя не может быть пустым.']
                ]
            ]);
        }

        $user = $this->_user();
        $user->setUserName($userName);
        self::$ds->userMapper()->save($user);

        $clientData = new ClientData();
        $clientData->set($user);

        return new JsonModel(self::SUCCESS);
    }

    public function updateCityAction()
    {
        $this->_checkMethodPost();

        $cityId = $this->_fromPost('city_id');

        if (!$cityId || !($city = self::$ds->cityMapper()->fetch($cityId))) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => [
                    'city_id' => ['Город не выбран.']
                ]
            ]);
        }

        $user = $this->_user();

        if ($user->isGuest()) {
            return new JsonModel(self::SUCCESS);
        }

        $user->setCityId($cityId);

        self::$ds->userMapper()->save($user);

        (new ClientData())->setCookieCityId($cityId);

        return new JsonModel(self::SUCCESS);
    }

    public function updatePasswordAction()
    {
        $this->_checkMethodPost();

        $postData = $this->_fromPost();

        $passwordOld = $postData[self::PASSWORD_OLD];
        $passwordNew = $postData[self::PASSWORD_NEW];

        $errors = [];

        if (!$passwordOld) {
            $errors[self::PASSWORD_OLD][] = 'Поле не может быть пустым.';
        }

        if (!$passwordNew) {
            $errors[self::PASSWORD_NEW][] = 'Поле не может быть пустым.';
        }

        if (!self::$ds->authService()->verifyPassword($passwordOld, $this->_user()->getPassword())) {
            $errors[self::PASSWORD_OLD][] = 'Неверный пароль.';
        }

        if ($errors) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => $errors
            ]);
        }

        $newPasswordConfirmToken = self::$ds->userService()->getPasswordConfirmToken($this->_user());

        self::$ds->redisMQService()->enqueuePasswordChangeConfirmationMail(
            $this->_user()->getId(),
            $newPasswordConfirmToken
        );

        $slot = new PasswordUpdate($newPasswordConfirmToken);
        $slot->save([
            'user_id'     => $this->_user()->getId(),
            'new_password' => self::$ds->authService()->hashPasswordForSaving($passwordNew)
        ]);

        return new JsonModel(self::SUCCESS);
    }

    public function resetPasswordAction()
    {
        $this->_checkMethodPost();

        $props = $this->_fromPost();

        $email       = $props[self::EMAIL];
        $passwordNew = $props[self::PASSWORD_NEW];

        $errors = [];

        if (!$email) {
            $errors[self::EMAIL][] = 'Поле не может быть пустым.';
        }

        if (!$passwordNew) {
            $errors[self::PASSWORD_NEW][] = 'Поле не может быть пустым.';
        }

        if ($errors) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => $errors
            ]);
        }

        $userByEmail = self::$ds->userMapper()->fetchByEmail($email);

        if (!$userByEmail) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => [
                    self::EMAIL => [
                        'Пользователь с таким адресом электронной почты не зарегистрирован.'
                    ]
                ]
            ]);
        }

        $newPasswordConfirmToken = self::$ds->userService()->getPasswordConfirmToken($userByEmail);

        self::$ds->redisMQService()->enqueuePasswordChangeConfirmationMail(
            $userByEmail->getId(),
            $newPasswordConfirmToken
        );

        $slot = new PasswordUpdate($newPasswordConfirmToken);
        $slot->save([
            'user_id'      => $userByEmail->getId(),
            'new_password' => self::$ds->authService()->hashPasswordForSaving($passwordNew)
        ]);

        return new JsonModel(self::SUCCESS);
    }

    public function confirmNewPasswordAction()
    {
        $this->_checkMethodGetOrHead();

        $token = $this->_fromRoute('token');

        $slotPassUpdate = new PasswordUpdate($token);
        $data = $slotPassUpdate->load();

        $jdata = $this->_getRenderer()->jData();

        if (!$data) {
            $jdata->setData('messages', ['Ссылка устарела или не действительна.']);

            return [];
        }

        $userId      = $data['user_id'];
        $newPassword = $data['new_password'];

        $user = self::$ds->userMapper()->fetch($userId);

        $user->setPassword($newPassword);
        self::$ds->userMapper()->save($user);

        $slotPassUpdate->delete();

        self::$ds->authService()->dropAllUserSessions($user->getEmail());

        $jdata->setData('messages', ['Пароль успешно изменен.']);

        return [];
    }

    public function defaultCompanyAction()
    {
        $this->_checkMethodGetOrHead();

        if (!$this->_user()->getId()) {
            return new JsonModel([]);
        }

        $companies = $this->_user()->getCompanies();

        if ($companies->isEmpty()) {
            return new JsonModel([]);
        }

        return new JsonModel(
            self::$ds->companyService()->suggestionFromModel(
                $companies->asArray()[0]
            )
        );
    }

    protected function _getUserIdNotGuest() : int
    {
        $userId = $this->_user()->getId();

        if (!$userId) {
            throw new HttpClientException(HttpClientException::FORBIDDEN_403);
        }

        return $userId;
    }

    protected function _registerPasswordChangeRequest()
    {
        $newPasswordConfirmToken = self::$ds->userService()->getPasswordConfirmToken($userByEmail);

        self::$ds->redisMQService()->enqueuePasswordChangeConfirmationMail(
            $this->_user()->getId(),
            $newPasswordConfirmToken
        );

        $slot = new PasswordUpdate($newPasswordConfirmToken);
        $slot->save([
            'user_id'      => $userByEmail->getId(),
            'new_password' => self::$ds->authService()->hashPasswordForSaving($passwordNew)
        ]);
    }
}