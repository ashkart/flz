<?php

namespace Frontend\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Domain\City;
use Application\Model\Domain\ClientData;
use Zend\View\Model\JsonModel;
use Application\Model\Service\ElasticSearch as ESService;

class CityController extends AbstractController
{
    public function geoReverseAction()
    {
        $this->_checkMethodPost();

        $lat = $this->_fromPost('lat');
        $lng = $this->_fromPost('lng');

        if ($this->_user()->getCityId()) {
            $city = $this->_user()->getCity();
        } else {

            if (!$lat || !$lng) {
                throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
            }

            $city = self::$ds->cityService()->getFromCoords($lat, $lng);

            if (!$city) {
                $city = self::$ds->cityMapper()->fetch(City::ID_MOSCOW);
            }
        }

        return new JsonModel(self::$ds->cityService()->makeSuggestion($city));
    }
}