<?php

namespace Frontend\Controller;

use Application\Controller\HttpClientException;
use Application\Controller\HttpRedirectException;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\ClientData;
use Application\Model\Domain\ContactPerson;
use Application\Model\Domain\User;
use Application\Model\Exception\DuplicateEntryException;
use const Application\ROUTE_INDEX;
use function Couchbase\defaultDecoder;
use Frontend\Form\Auth;
use Frontend\Form\Registration;
use Frontend\Form\RegistrationWithCompany;
use Frontend\Module;
use Zend\Http\Request;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AuthController extends AbstractController
{
    const PARAM_LOGIN    = 'login';
    const PARAM_PASSWORD = 'password';
    const PARAM_TOKEN    = 'token';

    const PARAM_REG_TYPE = 'type';

    const REG_TYPE_APPLICANT = 'applicant';
    const REG_TYPE_COMPANY   = 'company';


    public function indexAction()
    {
        $this->_checkMethodGetOrHead();

        if (!$this->_user()->isGuest()) {
            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $this->_url()->frontend());
        }

        $form = new Auth();

        $this->layout()->setVariable('withNavbar', false);

        return [
            'form' => $form
        ];
    }

    public function loginAction()
    {
        $this->_checkMethodPost();

        $params = $this->_fromPost();

        $email    = $params[self::PARAM_LOGIN];
        $password = $params[self::PARAM_PASSWORD];

        $queryParams = $this->_fromQuery();

        if (!$this->_user()->isGuest()) {
            throw new HttpRedirectException(HttpRedirectException::FOUND_302, $this->_url()->frontend('index'));
        }

        if (!$email || !$password) {
            return new JsonModel([self::STATE => self::STATE_VIEW_FAILURE]);
        }

        if (self::$ds->authService()->auth($email, $password)) {
            $url = $this->_url(Module::NAME);

            if ($queryParams && isset($queryParams['request'])) {
                $path = $queryParams['request'];

                if ($queryParams['request'][0] === '/') {
                    $path = substr($path, 1, strlen($path));
                }

                $url .= $path;
            }

            $clientData = new ClientData();

            if ($this->_user()->getCityId()) {
                $clientData->setCookieCityId($this->_user()->getCityId());
            }

            return new JsonModel([
                self::STATE    => self::STATE_VIEW_SUCCESS,
                self::REDIRECT => $url
            ]);
        }

        return new JsonModel([self::STATE => self::STATE_VIEW_FAILURE]);
    }

    public function logoutAction()
    {
        $this->_checkMethodGetOrHead();

        if (!$this->_user()->isGuest()) {
            self::$ds->authService()->logout();
        }

        $url = $this->_url()->frontend();

        throw new HttpRedirectException(HttpRedirectException::FOUND_302, $url);
    }

    public function registerAction()
    {
        try {
            $this->_checkMethodPost();
        } catch (HttpClientException $exception) {
            $this->_checkMethodGetOrHead();
            // GET request - просто вернем layout
            return [];
        }

        if (!$this->_user()->isGuest()) {
            throw new HttpRedirectException(HttpRedirectException::FOUND_302, $this->_url()->frontend('profile', 'view'));
        }

        $data = $this->_fromPost();

        $regType = $data[self::PARAM_REG_TYPE];

        switch ($regType) {
            case self::REG_TYPE_APPLICANT:
                $form = new Registration();
                break;
            case self::REG_TYPE_COMPANY:
                $form = new RegistrationWithCompany();
                break;
            default:
                throw new HttpClientException(HttpClientException::BAD_REQUEST_400, 'Unknown registration type');
        }

        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        try {
            if ($regType === self::REG_TYPE_APPLICANT) {
                self::$ds->authService()->registerApplicant($formData);
            }

            if ($regType === self::REG_TYPE_COMPANY) {
                self::$ds->authService()->registerUserWithCompany($formData);
            }
        } catch (\Exception $exception) {
            $responseVars = self::FAILURE;

            if ($exception instanceof DuplicateEntryException) {
                $responseVars[self::ERRORS] = [
                    Registration::EL_LOGIN => [
                        'duplication' => 'Данный email уже зарегистрирован'
                    ]
                ];
            }

            return new JsonModel($responseVars);
        }

        if (self::$ds->authService()->auth($formData[Registration::EL_LOGIN], $formData[Registration::EL_PASSWORD])) {
            self::$ds->redisMQService()->enqueueUserConfirmation(
                $this->_user()->getId(),
                $formData[Registration::EL_PASSWORD]
            );

            throw new HttpRedirectException(HttpRedirectException::FOUND_302, $this->_url()->frontend());
        }

        return new JsonModel(self::FAILURE);
    }

    public function initFbAuthAction()
    {
        $fbConfig = self::$sm->get(Factory::CONFIG)['integrations']['facebookApi'];
        $redirectUri = urlencode($this->_url()->frontend('auth', 'fb-auth'));
        $url = "https://www.facebook.com/v3.3/dialog/oauth?client_id={$fbConfig['appId']}&redirect_uri=$redirectUri&state='{st=state123abc,ds=123456789}'&scope=public_profile,email";

        throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $url);
    }

    public function fbAuthAction()
    {
        $this->_checkMethodGetOrHead();

        $params = $this->_fromQuery();

        if (!$params['code']) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        $accessToken = self::$ds->socnetService()->fbCodeAccessTokenExchange($params['code'], $this->_url()->frontend('auth', 'fb-auth'));
        $userFbData  = self::$ds->socnetService()->fbGetUserInfo($accessToken);

        $fbUserId = $userFbData['id'];
        $name     = $userFbData['first_name'];
        $email    = $userFbData['email'];

        if (!$fbUserId || !$email) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        $user = self::$ds->userMapper()->fetchByFbId($fbUserId);

        if (!$user) {
            $user = self::$ds->userMapper()->fetchByEmail($email);
        }

        if (!$user) {
            $user = User::create($email, '', User::ROLE_EMPLOYEE, $name, null);
            $user
                ->setFbId($fbUserId)
                ->setConfirmed(true)
            ;

            self::$ds->userMapper()->save($user);
        } elseif (!$user->getFbId()) {
            $user->setFbId($fbUserId);

            self::$ds->userMapper()->save($user);
        }

        if (self::$ds->authService()->auth($user->getEmail(), $user->getPassword(), false)) {
            throw new HttpRedirectException(HttpRedirectException::FOUND_302, $this->_url()->frontend());
        }
    }

    public function initVkAuthAction()
    {
        $vkConfig = self::$sm->get(Factory::CONFIG)['integrations']['vkApi'];
        $redirectUri = urlencode($this->_url()->frontend('auth', 'index'));
        $url = "https://oauth.vk.com/authorize?client_id={$vkConfig['app_id']}&response_type=token&display=popup&scope=email&redirect_uri={$redirectUri}&v=5.95";

        throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $url);
    }

    public function vkAuthAction()
    {
        $this->_checkMethodGetOrHead();

        $params = $this->_fromQuery();

        $accessToken = $params['access_token'];
        $vkUserId    = $params['user_id'];
        $email       = $params['email'];

        if (!$vkUserId || !$email) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        $user = self::$ds->userMapper()->fetchByVkId($vkUserId);

        if (!$user) {
            $user = self::$ds->userMapper()->fetchByEmail($email);
        }

        if (!$user) {
            $userVkData = self::$ds->socnetService()->vkGetUserInfo($vkUserId, $accessToken);

            $user = User::create($email, '', User::ROLE_EMPLOYEE, $userVkData['first_name'], null);
            $user
                ->setVkId($vkUserId)
                ->setConfirmed(true)
            ;

            self::$ds->userMapper()->save($user);
        } elseif (!$user->getVkId()) {
            $user->setVkId($vkUserId);

            self::$ds->userMapper()->save($user);
        }

        if (self::$ds->authService()->auth($user->getEmail(), $user->getPassword(), false)) {
            throw new HttpRedirectException(HttpRedirectException::FOUND_302, $this->_url()->frontend());
        }
    }

    public function confirmAction()
    {
        $this->_checkMethodGetOrHead();

        $params = $this->_fromRoute();

        $userId = $params['id'];
        $token  = $params[self::PARAM_TOKEN];

        if (!$userId || !$token) {
            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $this->_url()->frontend('index', 'confirm', ['result' => 0]));
        }

        $user = self::$ds->userMapper()->fetch($userId);

        if (!$user) {
            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $this->_url()->frontend('index', 'confirm', ['result' => 0]));
        }

        if ($user->isConfirmed()) {
            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $this->_url()->frontend('index', 'confirm', ['result' => 0]));
        }

        $userToken = self::$ds->authService()->getConfirmToken($user);

        if ($userToken !== $token) {
            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $this->_url()->frontend('index', 'confirm', ['result' => 0]));
        }

        if (self::$ds->authService()->auth($user->getEmail(), $user->getPassword(), false)) {
            $user = self::$ds->userMapper()->fetch($userId);
            $user->setConfirmed(true);
            self::$ds->userMapper()->save($user);

            throw new HttpRedirectException(HttpRedirectException::FOUND_302, $this->_url()->frontend('index', 'confirm', ['result' => 1]));
        }

        throw new HttpClientException(HttpClientException::FORBIDDEN_403);
    }

    public function passwordChangeAction() {}
}