<?php

namespace Frontend\Controller;

use Application\Model\Domain\Resume\ResumeMapper;
use Application\Model\Domain\Vacancy\VacancyMapper;
use Application\Model\Service\ElasticSearch;
use Application\Model\Service\Filter\CompanyFilter;
use Zend\View\Model\JsonModel;

class SuggestController extends AbstractController
{
    public function localityAction()
    {
        $searchRes = $this->_suggestFromQuery('country,city', ['title*']);

        $result = self::$ds->suggestService()->createLocalitySuggestions($searchRes);

        return new JsonModel($result);
    }

    public function cityAction()
    {
        $searchRes = $this->_suggestFromQuery(ElasticSearch::INDEX_CITY, ['title*']);

        $result = self::$ds->suggestService()->createLocalitySuggestions($searchRes);

        return new JsonModel($result);
    }

    public function companyAction()
    {
        $this->_checkMethodGetOrHead();

        $filter = new CompanyFilter();
        $filter->setKeyword($this->_fromQuery('query'));

        if (!$this->_user()->isAdmin()) {
            $filter->setUserId($this->_user()->getId());
        }

        $searchRes = self::$ds->companyService()->queryFromElasticsearch($filter);

        $result = self::$ds->suggestService()->createCompanySuggestions($searchRes['hits']);

        return new JsonModel($result);
    }

    public function profareaAction()
    {
        $this->_checkMethodGetOrHead();

        $queryStr = $this->_fromQuery('query');

        if (!$queryStr) {
            return new JsonModel(self::$ds->profareaService()->buildViewTree());
        }

        $searchRes = self::$ds->elasticSearchService()->multiMatch(ElasticSearch::INDEX_PROFAREA, $queryStr, ['title']);

        $result = [];

        foreach ($searchRes as $index => $indexObject) {
            $result[] = $indexObject['_source'];
        }

        return new JsonModel(self::$ds->profareaService()->buildViewDataFromIndexed($searchRes));
    }

    public function resumeKeywordAction()
    {
        return $this->_queryKeyword(ElasticSearch::INDEX_RESUME, ResumeMapper::COL_POSITION_NAME);
    }

    public function vacancyKeywordAction()
    {
        return $this->_queryKeyword(ElasticSearch::INDEX_VACANCY, VacancyMapper::COL_POSITION_NAME);
    }

    public function tagAction()
    {
        $searchResult = $this->_suggestFromQuery(ElasticSearch::INDEX_TAG, ['name']);

        $result = self::$ds->suggestService()->createTagSuggestions($searchResult);

        return new JsonModel($result);
    }

    protected function _queryKeyword(string $indexName, string $keywordFieldName) : JsonModel
    {
        $this->_checkMethodGetOrHead();

        $queryStr = $this->_fromQuery('query');

        if (!$queryStr) {
            return new JsonModel(self::$ds->profareaService()->buildViewTree());
        }

        $searchRes = self::$ds->elasticSearchService()->queryDistinct(
            $indexName,
            $keywordFieldName,
            $queryStr,
            [$keywordFieldName]
        );

        $result = [];

        foreach ($searchRes as $index => $indexObject) {
            $result[] = [
                'title' => $indexObject['key']
            ];
        }

        return new JsonModel($result);
    }

    /**
     * @param string[] $fieldNames
     */
    protected function _suggestFromQuery(string $indexName, array $fieldNames) : array
    {
        $this->_checkMethodGetOrHead();

        $queryStr = $this->_fromQuery('query');

        return self::$ds->elasticSearchService()->multiMatch($indexName, $queryStr, $fieldNames);
    }
}