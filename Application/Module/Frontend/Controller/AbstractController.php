<?php

namespace Frontend\Controller;


use Application\Controller\HttpClientException;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Callback;
use Application\Model\Domain\City;
use Application\Model\Domain\ClientData;
use Application\Model\Domain\View;
use Application\View\FallbackViewResolver;
use Zend\View\Resolver\AggregateResolver;

abstract class AbstractController extends \Main\Controller\AbstractController
{
    public function __construct()
    {
        parent::__construct();

        /**
         * Фактически будет использоваться для выдачи базового layout во всех "пустых" экшенах.
         *
         * @var AggregateResolver $resolverAggregate
         */
        $resolverAggregate = self::$sm->get('ViewResolver');

        $config = self::$sm->get(Factory::CONFIG);
        $fallBackView = $config['fallback_view'];

        $fallbackResolver = new FallbackViewResolver(self::$sm, $fallBackView);

        $resolverAggregate->attach($fallbackResolver);
    }

    protected function _checkAccessByUserId(?int $entityUserId)
    {
        if (!$this->_user()->isAdmin() && $this->_user()->getId() !== $entityUserId) {
            throw new HttpClientException(HttpClientException::FORBIDDEN_403, "Document doesn't belong to user.");
        }
    }

    protected function _getUserCityId() : int
    {
        $cityId = $this->_user()->getCityId();

        if (!$cityId) {
            $clientData = new ClientData();
            $cityId     = $clientData->getCityId() ?? City::ID_MOSCOW;
        }

        return $cityId;
    }

    protected function _getEntityCallbacks(string $entityType) : array
    {
        $entityId = $this->_fromRoute('entity');
        $pageIndex = $this->_fromQuery(self::PARAM_PAGE) - 1;
        $perPage = $this->_fromQuery(self::PARAM_PER_PAGE);

        if (!$entityId) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        switch ($entityType) {
            case Callback::ENTITY_TYPE_RESUME:
                $resume = self::$ds->resumeMapper()->fetch($entityId);

                if (!$resume) {
                    throw new HttpClientException(HttpClientException::NOT_FOUND_404);
                }

                $this->_checkAccessByUserId($resume->getUserId());

                break;

            case Callback::ENTITY_TYPE_VACANCY:
                $vacancy = self::$ds->vacancyMapper()->fetch($entityId);

                if (!$vacancy) {
                    throw new HttpClientException(HttpClientException::NOT_FOUND_404);
                }

                $this->_checkAccessByUserId($vacancy->getUserId());

                break;
        }

        $callbacksDataRows = self::$ds->callbackService()->selectByEntityId(
            $entityId,
            $entityType,
            $perPage,
            $pageIndex
        );

        return self::$ds->callbackService()->normalizeDataRowsForView($callbacksDataRows);
    }

    protected function _checkAddCallbackView()
    {
        $id         = $this->_fromRoute('id');
        $callbackId = $this->_fromRoute('callbackId');

        /** @var Callback $callback */
        if (
            $id &&
            $callbackId &&
            !$this->_user()->isGuest() &&
            ($callback = self::$ds->callbackMapper()->fetch($callbackId))
        ) {
            $entityId = $callback->getEntityId();

            $userEntity = null;

            switch ($callback->getEntityType()) {
                case Callback::ENTITY_TYPE_VACANCY:
                    $userEntity = self::$ds->vacancyMapper()->fetch($entityId);
                    break;

                case Callback::ENTITY_TYPE_RESUME:
                    $userEntity = self::$ds->resumeMapper()->fetch($entityId);
                    break;
            }

            if (
                $userEntity &&
                $userEntity->getUserId() === $this->_user()->getId() &&
                (int) $id === $callback->getFromEntityId()
            ) {
                self::$ds->viewService()->add($callbackId, View::ENTITY_TYPE_CALLBACK);
            }
        }
    }
}