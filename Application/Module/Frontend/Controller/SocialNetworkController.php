<?php


namespace Frontend\Controller;


use Application\Controller\HttpClientException;
use Application\Controller\HttpRedirectException;
use Application\Helper\ServiceManager\Factory;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Zend\Config\Config;
use Zend\Config\Writer\PhpArray;

class SocialNetworkController extends AbstractController
{
    public function vkConfirmServerAction()
    {
        $this->_checkMethodPost();

        $data = $this->_fromPost();

        if ($data['type'] === 'confirmation' && $data['group_id'] === 180646941) {
            echo 'b6fdef12';
            exit();
        }

        return [];
    }

    public function vkCatchCodeAction()
    {
        $params = $this->_getAllParams();
        $code = $params['code'];

        $file = fopen('vk_code.txt', 'w');
        file_put_contents('vk_code.txt', $code);
        fclose($file);
    }

    public function fbCallbackAction()
    {
        $config = self::$sm->get(Factory::CONFIG)['integrations']['facebookApi'];

        $appId       = $config['appId'];
        $appSecret   = $config['appSecret'];

        $fb = new Facebook([
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v3.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();

            $ch = curl_init("https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=$appId&client_secret=$appSecret&fb_exchange_token=$accessToken");

            $options = [
                CURLOPT_CUSTOMREQUEST  => "GET",
                CURLOPT_RETURNTRANSFER => true,   // return web page
                CURLOPT_HEADER         => false,  // don't return headers
                CURLOPT_FOLLOWLOCATION => true,   // follow redirects
                CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
                CURLOPT_ENCODING       => "",     // handle compressed
                CURLOPT_USERAGENT      => "test", // name of client
                CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
                CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
                CURLOPT_TIMEOUT        => 120,    // time-out on response
            ];

            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);
            curl_close($ch);

            if (!$result) {
                throw new HttpClientException(HttpClientException::NOT_FOUND_404);
            }

            $result = json_decode($result, JSON_OBJECT_AS_ARRAY);
            $longAccessToken = $result['access_token'];

            if (!isset($longAccessToken) || !$longAccessToken) {
                throw new HttpClientException(HttpClientException::I_AM_A_TEAPOT_418);
            }

            // Create the config object
            $localConfigObj = new Config([], true);
            $localConfigObj->integrations = [
                'facebookApi' => [
                    'access_token_long_life' => $longAccessToken
                ]
            ];

            $writer = new PhpArray();
            $writer->toFile(__DIR__ . '/../../../config/autoload/temp.local.php', $localConfigObj);
        } catch(FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }

    public function fbLoginAction()
    {
        $this->_checkMethodGetOrHead();

        $config = self::$sm->get(Factory::CONFIG)['integrations']['facebookApi'];

        $appId       = $config['appId'];
        $appSecret   = $config['appSecret'];

        $fb = new Facebook([
            'app_id' => $appId, // Replace {app-id} with your app id
            'app_secret' => $appSecret,
            'default_graph_version' => 'v3.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['publish_to_groups', 'groups_access_member_info'];
        $loginUrl = $helper->getLoginUrl($this->_url()->frontend('socnet', 'fb-callback'), $permissions);

        echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
    }
}