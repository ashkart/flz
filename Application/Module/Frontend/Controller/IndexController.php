<?php

namespace Frontend\Controller;

use Main\View\Helper\ViewMessage;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractController
{
    public function indexAction()
    {

        return new ViewModel();
    }

    public function confirmAction()
    {
        $this->_checkMethodGetOrHead();

        $result = $this->_fromRoute('result');

        $viewMessage = $this->_getRenderer()->viewMessage();

        if ($result) {
            $viewMessage->add(ViewMessage::MSG_CONFIRMATION_SUCCESS);
        } else {
            $viewMessage->add(ViewMessage::MSG_CONFIRMATION_FAILURE);
        }

        return [];
    }
}
