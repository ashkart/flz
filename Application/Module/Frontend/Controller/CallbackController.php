<?php

namespace Frontend\Controller;

use Application\Controller\HttpClientException;
use Application\Model\Domain\Callback as CallbackModel;
use Application\Model\Domain\Conversation\ConversationMapper;
use Application\Model\Domain\ConversationMessage\ConversationMessageMapper;
use Application\Model\Service\AbstractService;
use Library\Master\Date\DateTime;
use Zend\View\Model\JsonModel;

class CallbackController extends AbstractController
{
    public function conversationAction()
    {
        $this->_checkMethodGetOrHead();

        $conversation = $this->_checkIdFetchModel(self::$ds->conversationMapper());

        $conversationRow = $conversation->getTableRow();

        foreach ($conversationRow[ConversationMapper::MESSAGES] as &$messageRow) {
            $messageRow[ConversationMessageMapper::COL_CREATE_DATE] = DateTime::reformat(
                $messageRow[ConversationMessageMapper::COL_CREATE_DATE],
                DateTime::DATE_FORMAT_RU
            );
        }

        return new JsonModel($conversationRow);
    }

    public function addMessageAction()
    {
        $this->_checkMethodPost();

        if ($this->_user()->isGuest()) {
            throw new HttpClientException(HttpClientException::FORBIDDEN_403);
        }

        $conversationId = $this->_fromRoute('conversation');
        $text = $this->_fromPost('text');

        if (!$conversationId) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        if (in_array($text, [null, ''])) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => ['text' => 'Введите текст сообщения для отправки.']
            ]);
        }

        $conversation = self::$ds->conversationMapper()->fetch($conversationId);

        if (!$conversation) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        self::$ds->conversationService()->addMessage($conversation, $this->_user(), $text);

        return new JsonModel(self::SUCCESS);
    }

    public function acceptAction()
    {
        $callback = $this->_updateCallbackState(CallbackModel::STATE_ACCEPTED);

        return new JsonModel($this->_getSingleCallbackInEntityView($callback));
    }

    public function rejectAction()
    {
        $callback = $this->_updateCallbackState(CallbackModel::STATE_REJECTED);

        return new JsonModel($this->_getSingleCallbackInEntityView($callback));
    }

    public function revokeAction()
    {
        $this->_updateCallbackState(CallbackModel::STATE_DELETED);

        return new JsonModel(self::SUCCESS);
    }

    protected function _updateCallbackState(string $newState) : CallbackModel
    {
        $this->_checkMethodGetOrHead();

        $callback = $this->_checkIdFetchModel(self::$ds->callbackMapper());

        /** @var CallbackModel $callback */
        $callback->setState($newState);
        $callback->setUpdateDate(new DateTime());

        self::$ds->callbackMapper()->save($callback);

        return $callback;
    }

    protected function _getSingleCallbackInEntityView(CallbackModel $callback) : array
    {
        $callbacksData = self::$ds->callbackService()->selectByEntityId(
            $callback->getEntityId(),
            $callback->getEntityType()
        );

        $result = [];

        foreach ($callbacksData as $row) {
            if ((int) $row[CallbackModel\CallbackMapper::PRIMARY] === $callback->getId()) {
                $result[] = array_merge($row, $callback->getTableRow());
                break;
            }
        }

        return self::$ds->callbackService()->normalizeDataRowsForView($result)[0] ?? [];
    }
}