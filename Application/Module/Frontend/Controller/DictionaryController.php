<?php

namespace Frontend\Controller;

use Application\Model\Domain\LanguageProficiency;
use Zend\View\Model\JsonModel;

class DictionaryController extends AbstractController
{
    public function currencyAction()
    {
        $this->_checkMethodGetOrHead();

        $currencies = self::$ds->currencyMapper()->fetchAll([], true);

        $result = [];

        foreach ($currencies as $currency) {
            $result[$currency->getId()] = $currency->getName();
        }

        return new JsonModel($result);
    }

    public function languageAction()
    {
        $this->_checkMethodGetOrHead();

        return new JsonModel(LanguageProficiency::LANGUAGES);
    }
}