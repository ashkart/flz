<?php

namespace Frontend\Controller;

use Application\Controller\HttpClientException;
use Application\Controller\HttpRedirectException;
use Application\Model\Domain\Callback;
use Application\Model\Domain\City;
use Application\Model\Domain\ClientData;
use Application\Model\Domain\Company;
use Application\Model\Domain\UserCompany;
use Application\Model\Domain\Vacancy;
use Application\Model\Domain\View;
use Application\Model\Interfaces\EntityRelationInterface;
use Application\Model\Service\ElasticSearch;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\VacancyFilter;
use Application\Model\Domain\Vacancy\VacancyMapper;
use Application\Model\Service\ElasticSearch as ESService;
use Application\Model\Service\RedisMQ;
use Main\View\Form;
use Zend\View\Model\JsonModel;

class VacancyController extends AbstractController
{
    public function indexAction() {}
    public function searchAction() {}

    public function editAction()
    {
        if (!$this->request->isXmlHttpRequest()) {
            return [];
        }

        $vacancyTableRow = $this->_getVacancyViewData();

        return new JsonModel($vacancyTableRow);
    }

    public function viewAction()
    {
        if (!$this->request->isXmlHttpRequest()) {
            return [];
        }

        $vacancyTableRow = $this->_getVacancyViewData(false);

        if (!$this->_user()->isGuest()) {
            $this->_checkAddCallbackView();

            self::$ds->viewService()->add($vacancyTableRow[VacancyMapper::PRIMARY], View::ENTITY_TYPE_VACANCY);
        }

        $vacancyTableRow['views'] = self::$ds->viewService()->getEntityViewsCount(
            $vacancyTableRow[VacancyMapper::PRIMARY],
            View::ENTITY_TYPE_VACANCY
        );

        return new JsonModel($vacancyTableRow);
    }

    public function saveAction()
    {
        $this->_checkMethodPost();

        $id   = $this->_fromRoute('id');
        $data = $this->_fromPost();

        if (
            $this->_user()->isAdmin() &&
            !$data[Form\Vacancy::EL_COMPANY] &&
            $data['company_id']
        ) {
            $company = new Company($data['company']);
            self::$ds->companyMapper()->save($company);

            self::$ds->userCompanyService()->createNewForCurrentUser($company);

            $data[Form\Vacancy::EL_COMPANY] = $company->getId();
        }

        $form = new Form\Vacancy($this->_user(), self::$ds->currencyMapper()->fetchAll([], true));
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE  => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        if (!$id) {
            $vacancy = self::$ds->vacancyService()->create($formData);

            $this->_checkAccessByUserId($vacancy->getUserId());

            self::$ds->elasticSearchService()->indexDocument(
                ESService::INDEX_VACANCY,
                self::$ds->vacancyService()->indexiseModel($vacancy)
            );

            $socnetService = self::$ds->socnetService();

            if ($socnetService->shouldPublishVacanciesVk() || $socnetService->shouldPublishVacanciesFb()) {
                self::$ds->redisMQService()->addJob(RedisMQ::QUEUE_SOCIAL_PUBLISHING, RedisMQ\Job\EntityPublication::class, [
                    RedisMQ\Job\EntityPublication::ARG_ENTITY_ID   => $vacancy->getId(),
                    RedisMQ\Job\EntityPublication::ARG_ENTITY_TYPE => EntityRelationInterface::ENTITY_TYPE_VACANCY
                ]);
            }
        } else {
            $vacancy = self::$ds->vacancyMapper()->fetch($id);

            if (!$vacancy) {
                throw new HttpClientException(HttpClientException::NOT_FOUND_404);
            }

            self::$ds->vacancyService()->update($vacancy, $formData);

            $vacancy = self::$ds->vacancyMapper()->fetch($vacancy->getId());

            self::$ds->elasticSearchService()->updateDoc(
                ESService::INDEX_VACANCY,
                self::$ds->vacancyService()->indexiseModel($vacancy)
            );
        }

        return new JsonModel([
            self::STATE => self::STATE_VIEW_SUCCESS,
            self::REDIRECT => '/vacancy/view/' . $vacancy->getId()
        ]);
    }

    public function recommendedListAction()
    {
        $this->_checkMethodGetOrHead();

        $cityId = $this->_getUserCityId();

        $filter = new VacancyFilter();
        $filter->setVisibility(Vacancy::VISIBILITY_ALL);
        $filter->setCityIds([$cityId]);
        $filter->setPublishPeriod(VacancyFilter::PUBLISH_ALL_TIME);

        $vacancyRows = self::$ds->vacancyService()->makeSimpleListJson(
            self::$ds->vacancyService()->queryFromElasticsearch($filter)
        );

        return new JsonModel($vacancyRows);
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $filters = $this->_fromQuery(self::PARAM_FILTERS);
        $filters = $filters ? json_decode($filters, JSON_OBJECT_AS_ARRAY) : [];

        $page = $this->_fromQuery(self::PARAM_PAGE) - 1;
        $perPage = $this->_fromQuery(self::PARAM_PER_PAGE);

        $cityIds       = null;
        $regionIds     = null;
        $countryIds    = null;
        $profareas     = $filters[self::FILTER_PROFAREA] ?? null;
        $keywords      = $filters[self::FILTER_KEYWORDS] ?? [];
        $paycheck      = $filters[self::FILTER_PAYCHECK] ?? null;
        $isRemote      = $filters[self::FILTER_IS_REMOTE] ?? null;
        $employments   = $filters[self::FILTER_EMPLOYMENT] ?? null;
        $experience    = $filters[self::FILTER_EXPERIENCE] ?? null;
        $publishPeriod = $filters[self::FILTER_PUBLISH_PERIOD] ?? null;

        if (isset($filters[self::FILTER_REGION])) {
            foreach ($filters[self::FILTER_REGION] as $filterItem) {
                switch ($filterItem[self::F_KEY_TYPE]) {
                    case self::F_REGION_TYPE_CITY:
                        $cityIds[] = $filterItem['id'];
                        break;
                    case self::F_REGION_TYPE_REGION:
                        $regionIds[] = $filterItem['id'];
                        break;
                    case self::F_REGION_TYPE_COUNTRY:
                        $countryIds[] = $filterItem['id'];
                }
            }
        } else {
            $cityIds = [$this->_getUserCityId()];
        }

        $filter = new VacancyFilter();

        $filter->setVisibility(Vacancy::VISIBILITY_ALL);

        if ($cityIds) {
            $filter->setCityIds($cityIds);
        }

        if ($countryIds) {
            $filter->setCountryIds($countryIds);
        }

        if ($regionIds) {
            $filter->setRegionIds($regionIds);
        }

        if ($profareas) {
            $filter->setProfareaIds($profareas);
        }

        if (
            isset($keywords[self::F_KEY_VALUES]) &&
            $keywords[self::F_KEY_VALUES]
        ) {
            $keywordsValues = array_values($keywords[self::F_KEY_VALUES]);

            if ($keywordsValues[0] && $keywordsValues[0]['title']) {
                $filter->setKeywords(self::$ds->elasticSearchService()->cleanUpReservedSymbols($keywordsValues[0]['title']));
            }
        }

        if (isset($keywords[self::F_KEY_CHECKED_OPTION]) && $keywords[self::F_KEY_CHECKED_OPTION] !== null) {
            $filter->setTitleOnly($keywords[self::F_KEY_CHECKED_OPTION]);
        }

        if ($paycheck) {
            $filter->setPaycheck($paycheck);
        }

        if ($isRemote) {
            /* Здесь $isRemote это bool, но обрабатываем только true для этого фильтра */
            $filter->setIsRemote($isRemote);
        }

        if ($employments) {
            $filter->setEmployments(array_values($employments));
        }

        if ($experience) {
            $filter->setExperience(array_values($experience));
        }

        if ($publishPeriod !== null) {
            $filter->setPublishPeriod($publishPeriod);
        }

        $filter->setOrder([VacancyMapper::COL_UPDATE_DATE => AbstractFilter::ORDER_DESC]);
        $filter->setPaging($perPage, $page);

        $vacancyRows = self::$ds->vacancyService()->elasticDataToJsonModels(
            self::$ds->vacancyService()->queryFromElasticsearch($filter),
            true
        );

        if (!isset($filters[self::FILTER_REGION]) && $cityIds) {
            // Если был неявно применен фильтр, нужно отобразить это на фронте
            $localityFilter = self::$ds->suggestService()->createLocalitySuggestions(
                self::$ds->elasticSearchService()->queryByIds(ElasticSearch::INDEX_CITY, $cityIds)
            );

            $vacancyRows[self::PARAM_UNEXPLICIT_FILTERS] = [self::FILTER_REGION => $localityFilter];
        }

        $vacancyRows = self::$ds->callbackService()->applyRowsUserCallbackData($vacancyRows, Callback::ENTITY_TYPE_VACANCY);

        return new JsonModel($vacancyRows);
    }

    public function addCallbackAction()
    {
        $this->_checkMethodPost();

        $data = $this->_fromPost();

        $form = new Form\Callback();
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        $resumeId    = $formData[Form\Callback::EL_FROM_ENTITY_ID];
        $coverLetter = $formData[Form\Callback::EL_COVER_LETTER];
        $vacancyId   = $formData[Form\Callback::EL_ENTITY_ID];

        if ($this->_user()->isGuest()) {
            $url = $this->_url()->frontend('auth', 'index') . '?request=' . urlencode("/vacancy/view/id-$vacancyId");

            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $url);
        }

        $vacancy = self::$ds->vacancyMapper()->fetch($vacancyId);

        if (!$vacancy) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        if ($vacancy->getUserId() === $this->_user()->getId()) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400, 'Cannot callback to owned document.');
        }

        $resume = self::$ds->resumeMapper()->fetch($resumeId);

        if (!$resume) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        self::$ds->callbackService()->add(
            $vacancyId,
            Callback::ENTITY_TYPE_VACANCY,
            $resumeId,
            Callback::ENTITY_TYPE_RESUME,
            $coverLetter
        );

        return new JsonModel([
            self::STATE => self::STATE_VIEW_SUCCESS
        ]);
    }

    public function callbacksAction()
    {
        $this->_checkMethodGetOrHead();

        return new JsonModel($this->_getEntityCallbacks(Callback::ENTITY_TYPE_VACANCY));
    }

    protected function _getVacancyViewData(bool $isEditMode = true) : array
    {
        $this->_checkMethodGetOrHead();

        /** @var Vacancy $vacancy */
        $vacancy = $this->_checkIdFetchModel(self::$ds->vacancyMapper());

        if ($isEditMode) {
            $this->_checkAccessByUserId($vacancy->getUserId());
        } else if ($vacancy->isInvisible() && $this->_user()->getId() !== $vacancy->getUserId()) {
            throw new HttpRedirectException(
                HttpRedirectException::SEE_OTHER_303,
                $this->_url()->frontend('error', 'not-found')
            );
        }

        $vacancyTableRow = $vacancy->getTableRow();

        $vacancyTableRow['locations'] = self::$ds->vacancyService()->getLocationsAsTags($vacancy);
        $vacancyTableRow['company']   = ($vacancy->getCompany()->getOpf()
                ? "{$vacancy->getCompany()->getOpf()} "
                : '')
            . $vacancy->getCompany()->getOfficialName()
        ;
        $vacancyTableRow['profarea']  = $vacancy->getProfarea()->getTitle();

        if ($vacancy->getPaycheckConst()) {
            $vacancyTableRow[VacancyMapper::COL_PAYCHECK_MIN] = $vacancy->getPaycheckConst();
            $vacancyTableRow[VacancyMapper::COL_PAYCHECK_MAX] = $vacancy->getPaycheckConst();
        }

        unset($vacancyTableRow[VacancyMapper::COL_PAYCHECK_CONST]);

        if (!$isEditMode) {
            $vacancyTableRow['currency'] = $vacancy->getCurrencyId() ? $vacancy->getCurrency()->getIsoCode() : null;

            unset($vacancyTableRow[VacancyMapper::COL_CURRENCY_ID]);
        }

        if (!$this->_user()->isGuest()) {
            $userCallbacks = self::$ds->callbackService()->getUserCreated(
                $this->_user()->getId(),
                Callback::ENTITY_TYPE_VACANCY
            );

            self::$ds->callbackService()->applyRowUserCallbackData($vacancyTableRow, $userCallbacks);
        }

        return $vacancyTableRow;
    }
}