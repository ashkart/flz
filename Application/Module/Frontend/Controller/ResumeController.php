<?php

namespace Frontend\Controller;

use Application\Controller\HttpClientException;
use Application\Controller\HttpRedirectException;
use Application\Model\Domain\Callback;
use Application\Model\Domain\Resume\ResumeMapper;
use Application\Model\Domain\View;
use Application\Model\Service\ElasticSearch;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\ResumeFilter;
use Application\Model\Service\ElasticSearch as ESService;
use Application\Plugin\ClientData;
use Main\View\Form\Resume;
use Main\View\Form;
use Application\Model\Domain;
use Zend\Uri\Http;
use Zend\View\Model\JsonModel;

class ResumeController extends AbstractController
{
    public function indexAction() {}

    public function viewAction() {
        if (!$this->request->isXmlHttpRequest()) {
            return [];
        }

        $resumeTableRow = $this->_getResumeTableRow(false);

        if (!$this->_user()->isGuest()) {
            $this->_checkAddCallbackView();
            self::$ds->viewService()->add($resumeTableRow[ResumeMapper::PRIMARY], View::ENTITY_TYPE_RESUME);
        }

        $resumeTableRow['views'] = self::$ds->viewService()->getEntityViewsCount(
            $resumeTableRow[ResumeMapper::PRIMARY],
            View::ENTITY_TYPE_RESUME
        );

        return new JsonModel($resumeTableRow);
    }

    public function editAction() {
        if (!$this->request->isXmlHttpRequest()) {
            return [];
        }

        $resumeTableRow = $this->_getResumeTableRow();

        return new JsonModel($resumeTableRow);
    }

    public function searchAction() {}

    public function saveAction()
    {
        $this->_checkMethodPost();

        $data = $this->_fromPost();

        $form = new Resume();

        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        self::$ds->resumeService()->normalizeFormData($formData);

        $id = $this->_fromRoute('id');

        $result = null;

        if ($id) {
            $resume = self::$ds->resumeMapper()->fetch($id);

            $this->_checkAccessByUserId($resume->getUserId());

            if (!$resume) {
                throw new HttpClientException(HttpClientException::NOT_FOUND_404);
            }

            $result = self::$ds->resumeService()->update($resume, $formData);
        } else {
            $resume = self::$ds->resumeService()->create($formData);
            $result = !!$resume;
        }

        if (!$result) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => ['error' => 'Ошибка при сохранении']
            ]);
        }

        if ($id) {
            self::$ds->elasticSearchService()->updateDoc(
                ESService::INDEX_RESUME,
                self::$ds->resumeService()->indexiseModel($resume)
            );
        } else {
            self::$ds->elasticSearchService()->indexDocument(
                ESService::INDEX_RESUME,
                self::$ds->resumeService()->indexiseModel($resume)
            );
        }

        return new JsonModel([
            self::STATE => self::STATE_VIEW_SUCCESS,
            self::REDIRECT => '/resume/view/' . $resume->getId()
        ]);
    }

    public function recommendedListAction()
    {
        $this->_checkMethodGetOrHead();

        $cityId = $this->_getUserCityId();

        $filter = new ResumeFilter();
        $filter->setVisibility(Domain\Resume::VISIBILITY_ALL);
        $filter->setCityIds([$cityId]);
        $filter->setPublishPeriod(ResumeFilter::PUBLISH_ALL_TIME);

        $resumeRows = self::$ds->resumeService()->makeSimpleListJson(
            self::$ds->resumeService()->queryFromElasticsearch($filter)
        );

        return new JsonModel($resumeRows);
    }

    public function listAction()
    {
        $this->_checkMethodGetOrHead();

        $filters = $this->_fromQuery(self::PARAM_FILTERS);
        $filters = $filters ? json_decode($filters, JSON_OBJECT_AS_ARRAY) : [];

        $page = $this->_fromQuery(self::PARAM_PAGE) - 1;
        $perPage = $this->_fromQuery(self::PARAM_PER_PAGE);

        $cityIds         = null;
        $regionIds       = null;
        $countryIds      = null;
        $keywords        = $filters[self::FILTER_KEYWORDS] ?? [];
        $paycheck        = $filters[self::FILTER_PAYCHECK] ?? null;
        $isRemote        = $filters[self::FILTER_IS_REMOTE] ?? null;
        $employments     = $filters[self::FILTER_EMPLOYMENT] ?? null;
        $experience      = $filters[self::FILTER_EXPERIENCE] ?? null;
        $age             = $filters[self::FILTER_AGE] ?? null;
        $publishPeriod   = $filters[self::FILTER_PUBLISH_PERIOD] ?? null;
        $gender          = $filters[self::FILTER_GENDER] ?? null;
        $isRelocateReady = $filters[self::FILTER_IS_RELOCATE_READY] ?? null;
        $educationLevels = $filters[self::FILTER_EDUCATION_LVL] ?? null;
        $skillTags       = $filters[self::FILTER_SKILL_TAGS] ?? null;

        if (isset($filters[self::FILTER_REGION])) {
            foreach ($filters[self::FILTER_REGION] as $filterItem) {
                switch ($filterItem[self::F_KEY_TYPE]) {
                    case self::F_REGION_TYPE_CITY:
                        $cityIds[] = $filterItem['id'];
                        break;
                    case self::F_REGION_TYPE_REGION:
                        $regionIds[] = $filterItem['id'];
                        break;
                    case self::F_REGION_TYPE_COUNTRY:
                        $countryIds[] = $filterItem['id'];
                }
            }
        } else {
            $cityIds = [$this->_getUserCityId()];
        }

        $filter = new ResumeFilter();

        $filter->setVisibility(Domain\Resume::VISIBILITY_ALL);

        if ($cityIds) {
            $filter->setCityIds($cityIds);
        }

        if ($countryIds) {
            $filter->setCountryIds($countryIds);
        }

        if ($regionIds) {
            $filter->setRegionIds($regionIds);
        }

        if (
            isset($keywords[self::F_KEY_VALUES]) &&
            $keywords[self::F_KEY_VALUES]
        ) {
            $keywordsValues = array_values($keywords[self::F_KEY_VALUES]);

            if ($keywordsValues[0] && $keywordsValues[0]['title']) {
                $filter->setKeywords(self::$ds->elasticSearchService()->cleanUpReservedSymbols($keywordsValues[0]['title']));
            }
        }

        if (isset($keywords[self::F_KEY_CHECKED_OPTION]) && $keywords[self::F_KEY_CHECKED_OPTION] !== null) {
            $filter->setTitleOnly($keywords[self::F_KEY_CHECKED_OPTION]);
        }

        if ($paycheck) {
            $filter->setPaycheckMin($paycheck[self::F_KEY_MIN] ?? null);
            $filter->setPaycheckMax($paycheck[self::F_KEY_MAX] ?? null);
        }

        if ($isRemote) {
            /* Здесь $isRemote это bool, но обрабатываем только true для этого фильтра */
            $filter->setIsRemote($isRemote);
        }

        if ($employments) {
            $filter->setEmployments(array_values($employments));
        }

        if ($experience) {
            $filter->setExperience($experience);
        }

        if ($age) {
            $filter->setAge(array_values($age));
        }

        if ($publishPeriod !== null) {
            $filter->setPublishPeriod($publishPeriod);
        }

        if ($gender) {
            $filter->setGender($gender);
        }

        if ($isRelocateReady) {
            /* Здесь $isRelocateReady это bool, но обрабатываем только true для этого фильтра */
            $filter->setRelocateReady($isRelocateReady);
        }

        if ($educationLevels) {
            $filter->setEducationLevels(array_values($educationLevels));
        }

        if ($skillTags) {
            $filter->setSkills(array_values($skillTags));
        }

        $filter->setOrder([ResumeMapper::COL_UPDATE_DATE => AbstractFilter::ORDER_DESC]);
        $filter->setPaging($perPage, $page);

        $resumeRows = self::$ds->resumeService()->elasticDataToJsonModels(
            self::$ds->resumeService()->queryFromElasticsearch($filter),
            true
        );

        if (!isset($filters[self::FILTER_REGION]) && $cityIds) {
            // Если был неявно применен фильтр, нужно отобразить это на фронте
            $localityFilter = self::$ds->suggestService()->createLocalitySuggestions(self::$ds->elasticSearchService()->queryByIds(ElasticSearch::INDEX_CITY, $cityIds));
            $resumeRows[self::PARAM_UNEXPLICIT_FILTERS] = [self::FILTER_REGION => $localityFilter];
        }

        $resumeRows = self::$ds->callbackService()->applyRowsUserCallbackData($resumeRows, Callback::ENTITY_TYPE_RESUME);

        return new JsonModel($resumeRows);
    }

    public function contextAction()
    {
        $this->_checkMethodGetOrHead();

        $paycheckRange = self::$ds->resumeService()->getPayCheckRange();

        $context = [
            'paycheckRange' => [
                self::F_KEY_MIN => $paycheckRange[self::F_KEY_MIN]['value'],
                self::F_KEY_MAX => $paycheckRange[self::F_KEY_MAX]['value']
            ]
        ];

        return new JsonModel($context);
    }

    public function addCallbackAction()
    {
        $this->_checkMethodPost();

        $data = $this->_fromPost();

        $form = new Form\Callback();
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                self::STATE => self::STATE_VIEW_FAILURE,
                self::ERRORS => $form->getMessages()
            ]);
        }

        $formData = $form->getData();

        $vacancyId   = $formData[Form\Callback::EL_FROM_ENTITY_ID];
        $coverLetter = $formData[Form\Callback::EL_COVER_LETTER];
        $resumeId    = $formData[Form\Callback::EL_ENTITY_ID];

        if ($this->_user()->isGuest()) {
            $url = $this->_url()->frontend('auth', 'index') . '?request=' . urlencode("resume/view/id-$resumeId");

            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $url);
        }

        $resume = self::$ds->resumeMapper()->fetch($resumeId);

        if (!$resume) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        if ($resume->getUserId() === $this->_user()->getId()) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400, 'Cannot callback to owned document.');
        }

        if (!$vacancyId) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        $vacancy = self::$ds->vacancyMapper()->fetch($vacancyId);

        if (!$vacancy) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        self::$ds->callbackService()->add(
            $resumeId,
            Callback::ENTITY_TYPE_RESUME,
            $vacancyId,
            Callback::ENTITY_TYPE_VACANCY,
            $coverLetter
        );

        return new JsonModel([
            self::STATE => self::STATE_VIEW_SUCCESS
        ]);
    }

    public function callbacksAction()
    {
        $this->_checkMethodGetOrHead();

        return new JsonModel($this->_getEntityCallbacks(Callback::ENTITY_TYPE_RESUME));
    }

    protected function _getResumeTableRow(bool $isEditMode = true) : array
    {
        $this->_checkMethodGetOrHead();

        /** @var Domain\Resume $resume */
        $resume = $this->_checkIdFetchModel(self::$ds->resumeMapper());

        if ($isEditMode) {
            $this->_checkAccessByUserId($resume->getUserId());
        } else if ($resume->isInvisible() && $this->_user()->getId() !== $resume->getUserId()) {
            throw new HttpRedirectException(HttpRedirectException::SEE_OTHER_303, $this->_url()->frontend('error', 'not-found'));
        }

        $resumeJsonRowNormalized = self::$ds->resumeService()->normalizeTableRowForForm($resume, $isEditMode);

        if (!$this->_user()->isGuest()) {
            $userCallbacks = self::$ds->callbackService()->getUserCreated(
                $this->_user()->getId(),
                Callback::ENTITY_TYPE_RESUME
            );

            self::$ds->callbackService()->applyRowUserCallbackData($resumeJsonRowNormalized, $userCallbacks);
        }

        return $resumeJsonRowNormalized;
    }
}