<?php

namespace Frontend;

return [
    'controllers' => [
        'invokables' => [
            'auth'       => Controller\AuthController::class,
            'city'       => Controller\CityController::class,
            'callback'   => Controller\CallbackController::class,
            'dictionary' => Controller\DictionaryController::class,
            'error'      => Controller\ErrorController::class,
            'index'      => Controller\IndexController::class,
            'profile'    => Controller\ProfileController::class,
            'resume'     => Controller\ResumeController::class,
            'rule'       => Controller\RuleController::class,
            'suggest'    => Controller\SuggestController::class,
            'vacancy'    => Controller\VacancyController::class,
            'socnet'     => Controller\SocialNetworkController::class
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'strategies' => [
            'ViewJsonStrategy',
        ],
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'frontend/index/index'    => __DIR__ . '/../view/frontend/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'view_helpers' => [
        'invokables' => [
//            'url' => Url::class
        ]
    ],

    'fallback_view' => 'frontend/index/index'
];
