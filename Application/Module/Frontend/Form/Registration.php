<?php

namespace Frontend\Form;

use Application\Model\Domain\AbstractMapper;
use Frontend\Validator\UserAgreement;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;
use Library\Master\Form\Form;
use Zend\Validator\EmailAddress;

class Registration extends Form implements InputFilterProviderInterface
{
    const EL_LOGIN     = 'login';
    const EL_USER_NAME = 'user_name';
    const EL_PASSWORD  = 'password';
    const EL_AGREE     = 'agree';

    public function __construct()
    {
        parent::__construct();

        $elLogin    = new Email(self::EL_LOGIN);
        $elUserName = new Text(self::EL_USER_NAME);
        $elPassword = new Password(self::EL_PASSWORD);
        $elAgree    = new Checkbox(self::EL_AGREE);

        $elAgree
            ->setCheckedValue(self::CHECKED)
            ->setUncheckedValue(self::UNCHECKED)
            ->setValue(self::UNCHECKED)
        ;

        $this
            ->add($elLogin)
            ->add($elUserName)
            ->add($elPassword)
            ->add($elAgree)
        ;
    }

    public function getInputFilterSpecification()
    {
        $emailSpec = array_merge_recursive(
            $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 4, true, true),
            [
                'validators' => [
                    [
                        'name' => EmailAddress::class
                    ]
                ]
            ]
        );

        return [
            self::EL_LOGIN => $emailSpec,
            self::EL_USER_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 1, true, true),
            self::EL_PASSWORD => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 8, true, false),
            self::EL_AGREE => [
                'required' => true,
                'validators' => [
                    [
                        'name' => UserAgreement::class
                    ]
                ]
            ]
        ];
    }
}