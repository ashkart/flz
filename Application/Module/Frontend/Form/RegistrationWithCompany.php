<?php

namespace Frontend\Form;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Company;
use Frontend\Form\Registration;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;

class RegistrationWithCompany extends Registration
{
    const EL_OPF                 = 'opf';
    const EL_COMPANY_NAME        = 'company';
    const EL_COMPANY_DESCRIPTION = 'company_description';
    const EL_CONTACT_POSITION    = 'position';
    const EL_CONTACT_LAST_NAME   = 'last_name';
    const EL_CONTACT_FIRST_NAME  = 'first_name';
    const EL_CONTACT_MIDDLE_NAME = 'middle_name';

    public function __construct()
    {
        parent::__construct();

        $elOpf = new Select(self::EL_OPF);
        $elOpf->setValueOptions(Company::OPFS);

        $elCompanyName       = new Text(self::EL_COMPANY_NAME);
        $elCompanyDesc       = new Text(self::EL_COMPANY_DESCRIPTION);
        $elContactPosition   = new Text(self::EL_COMPANY_DESCRIPTION);
        $elContactFirstName  = new Text(self::EL_CONTACT_FIRST_NAME);
        $elContactMiddleName = new Text(self::EL_CONTACT_MIDDLE_NAME);
        $elContactLastName   = new Text(self::EL_CONTACT_LAST_NAME);

        $this
            ->add($elOpf)
            ->add($elCompanyName)
            ->add($elCompanyDesc)
            ->add($elContactPosition)
            ->add($elContactFirstName)
            ->add($elContactMiddleName)
            ->add($elContactLastName)
        ;
    }

    public function getInputFilterSpecification()
    {
        return array_merge(
            parent::getInputFilterSpecification(),
            [
                self::EL_OPF => [
                    'required' => true
                ],
                self::EL_COMPANY_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 1, true),
                self::EL_COMPANY_DESCRIPTION => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 5, true, true),
                self::EL_CONTACT_POSITION => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 3, true, true),
                self::EL_CONTACT_LAST_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 2, true, true),
                self::EL_CONTACT_FIRST_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 2, true, true),
                self::EL_CONTACT_MIDDLE_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 2, false, true),
            ]
        );
    }
}