<?php

namespace Frontend\Form;

use Frontend\Validator\Password;
use Library\Master\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator as ZendValidator;

class Auth extends Form implements InputFilterProviderInterface
{
    const EL_LOGIN    = 'login';
    const EL_PASSWORD = 'password';

    public function __construct()
    {
        parent::__construct();
        
        $this->setAction('/auth/login');

        $elLogin    = new Element\Text(self::EL_LOGIN);
        $elPassword = new Element\Password(self::EL_PASSWORD);
        
        $elLogin->setAttribute('placeholder', 'Email');
        $elPassword->setAttribute('placeholder', 'Password');
        
        $this
            ->add($elLogin)
            ->add($elPassword)
        ;

        $this->addSubmit();
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_LOGIN => [
                'name' => ZendValidator\EmailAddress::class
            ],
            
            self::EL_PASSWORD => [
                'name' => Password::class
            ]
        ];
    }
}