<?php

namespace Main;

use Application\Application;
use Application\Helper\DataSource\TraitDataSource;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\AbstractModel;
use Zend\EventManager\EventInterface;
use Zend\I18n\Translator\Resources;
use Zend\Mvc\I18n\Translator;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Validator\AbstractValidator;

class Module
{
    const NAME = 'main';

    const ENV_DEVELOPMENT = 'dev';
    const ENV_TEST        = 'test';
    const ENV_PRODUCTION  = 'production';

    const ENVS = [
        self::ENV_DEVELOPMENT,
        self::ENV_TEST,
        self::ENV_PRODUCTION
    ];

    /**
     * @var string
     */
    protected static $_env;

    public static function getEnv() : string
    {
        return self::$_env;
    }

    public static function isDevelopment() : bool
    {
        return self::$_env === self::ENV_DEVELOPMENT;
    }

    public static function isTesting() : bool
    {
        return self::$_env === self::ENV_TEST;
    }

    public static function isProduction() : bool
    {
        return self::$_env === self::ENV_PRODUCTION;
    }

    public static function isCli() : bool
    {
        return PHP_SAPI === 'cli';
    }

    /**
     * @param EventInterface | MvcEvent $e
     */
    public function onBootstrap(EventInterface $e)
    {
        $this->bootstrapLocale($e);

        /** @var Application $application */
        $application = $e->getTarget();

        $config      = $application->getConfig();

        $envs = self::ENVS;

        if (in_array($config['env'], $envs)) {
            self::$_env = $config['env'];
        }

        TraitDataSource::setServiceManager($application->getServiceManager());
        AbstractModel::setServiceManager($application->getServiceManager());
        AbstractMapper::setServiceManager($application->getServiceManager());

        $mvcTranslator = $application->getServiceManager()->get('MvcTranslator');

        AbstractValidator::setDefaultTranslator($mvcTranslator);

        $eventManager = $e->getApplication()->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @param EventInterface | MvcEvent $e
     */
    private function bootstrapLocale(EventInterface $e)
    {
        setlocale(LC_ALL, 'ru_RU.utf8');
    }
}