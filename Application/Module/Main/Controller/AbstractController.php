<?php

namespace Main\Controller;

use Application\Controller\HttpClientException;
use Application\Controller\HttpServerException;
use Application\Helper\RouteHelper;
use Application\Helper\ServiceManager\Factory;
use Application\Helper\TraitViewRenderer;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Interfaces\DomainModelInterface;
use Library\Master\Session\SessionManager;
use Traversable;
use Zend\EventManager\EventInterface;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

/**
 * @property Request $request
 */
abstract class AbstractController extends AbstractActionController
{
    use TraitViewRenderer;

    const STATE = 'state';

    const STATE_VIEW_SUCCESS = 1;
    const STATE_VIEW_FAILURE = 0;

    const ERRORS = 'errors';

    const SUCCESS = [self::STATE => self::STATE_VIEW_SUCCESS];
    const FAILURE = [self::STATE => self::STATE_VIEW_FAILURE];

    const REDIRECT = 'redirect';

    const PARAM_FILTERS            = 'filter';
    const PARAM_PER_PAGE           = 'perPage';
    const PARAM_PAGE               = 'page';
    const PARAM_SORT_BY            = 'sortBy';
    const PARAM_ORDER              = 'order';
    const PARAM_UNEXPLICIT_FILTERS = 'unexplicitFilters';

    const FILTER_REGION            = 'region';
    const FILTER_PROFAREA          = 'profarea';
    const FILTER_KEYWORDS          = 'keywords';
    const FILTER_PAYCHECK          = 'paycheck';
    const FILTER_IS_REMOTE         = 'isRemote';
    const FILTER_EMPLOYMENT        = 'employment';
    const FILTER_EXPERIENCE        = 'experience';
    const FILTER_PUBLISH_PERIOD    = 'publishPeriod';
    const FILTER_AGE               = 'age';
    const FILTER_GENDER            = 'gender';
    const FILTER_IS_RELOCATE_READY = 'isRelocateReady';
    const FILTER_EDUCATION_LVL     = 'educationLvl';
    const FILTER_SKILL_TAGS        = 'skillTags';

    const F_KEY_VALUES          = 'values';
    const F_KEY_CHECKED_OPTION  = 'checkedOption';
    const F_KEY_TYPE            = 'type';

    const F_REGION_TYPE_CITY    = 'city';
    const F_REGION_TYPE_REGION  = 'region';
    const F_REGION_TYPE_COUNTRY = 'region';

    const F_KEY_MIN = 'min';
    const F_KEY_MAX = 'max';


    /**
     * @var RouteHelper | null
     */
    protected $routeHelper = null;

    public function __construct()
    {

    }

    /**
     * @param MvcEvent | EventInterface $e
     * @return bool
     */
    public function preDispatch($e)
    {
        return true;
    }

    public function onDispatch(MvcEvent $e)
    {
        $res = $this->preDispatch($e);

        if ($res === false) {
            throw new HttpServerException(HttpServerException::INTERNAL_SERVER_ERROR_500);
        }

        parent::onDispatch($e);

        $res = $this->postDispatch($e);

        if ($res === false) {
            throw new HttpServerException(HttpServerException::INTERNAL_SERVER_ERROR_500);
        }
    }

    /**
     * @param MvcEvent | EventInterface $e
     * @return bool
     */
    public function postDispatch($e)
    {
        return true;
    }

    /**
     * Получение параметров из маршрута
     */
    protected function _fromRoute(string $name = null, $defaultValue = null)
    {
        return $this->params()->fromRoute($name, $defaultValue);
    }

    protected function _fromPost($name = null, $default = null)
    {
        return $this->params()->fromPost($name, $default);
    }

    protected function _fromQuery($name = null, $default = null)
    {
        return $this->params()->fromQuery($name, $default);
    }

    protected function _getAllParams()
    {
        return array_merge(
            $this->params()->fromRoute(),
            $this->params()->fromQuery(),
            $this->params()->fromPost()
        );
    }

    protected function _checkMethodGetOrHead(bool $allowJsRequest = false)
    {
        $this->_checkMethod([Request::METHOD_GET, Request::METHOD_HEAD], $allowJsRequest);
    }

    protected function _checkMethodPost(bool $checkJsRequest = false)
    {
        $this->_checkMethod([Request::METHOD_POST], $checkJsRequest);
    }

    /**
     * @param string[] $methods
     */
    protected function _checkMethod(array $methods, bool $checkJsRequest = false)
    {
        if (!in_array($this->request->getMethod(), $methods)) {
            throw new HttpClientException(HttpClientException::METHOD_NOT_ALLOWED_405);
        }

        if ($checkJsRequest && !$this->request->isXmlHttpRequest()) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }
    }

    /**
     * @return SessionManager
     */
    protected function _getSessionManager()
    {
        return self::$sm->get(Factory::SESSION_MANAGER);
    }

    /**
     * @return RouteHelper
     */
    protected function _getRouteHelper()
    {
        if (!$this->routeHelper) {
            $this->routeHelper = new RouteHelper($this->getEvent()->getRouter(), $this->getEvent()->getRouteMatch());
        }

        return $this->routeHelper;
    }

    /**
     * @param  string               $name               Name of the route
     * @param  array                $params             Parameters for the link
     * @param  array|Traversable    $options            Options for the route
     * @param  bool                 $reuseMatchedParams Whether to reuse matched parameters
     *
     * @return RouteHelper | string
     */
    protected function _url($name = null, array $params = [], array $options = [], $reuseMatchedParams = false)
    {
        $routeHelper = $this->_getRouteHelper();

        if (!$name && !$params && !$options && !$reuseMatchedParams) {
            return $routeHelper;
        }

        return is_bool($options)
            ? $routeHelper->url($name, $params, $options)
            : $routeHelper->url($name, $params, $options, $reuseMatchedParams)
            ;
    }

    protected function _checkIdFetchModel(AbstractMapper $mapper) : DomainModelInterface
    {
        $id = $this->_fromRoute('id');

        if (!$id) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }

        $model = $mapper->fetch($id);

        if (!$model) {
            throw new HttpClientException(HttpClientException::NOT_FOUND_404);
        }

        return $model;
    }
}