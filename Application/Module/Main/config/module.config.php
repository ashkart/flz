<?php

namespace Main;

use Main\View\Helper\ConfigData;
use Main\View\Helper\Contact;
use Main\View\Helper\Factory\IdentityFactory;
use Main\View\Helper\Factory\TranslatorFactory;
use Main\View\Helper\Form;
use Main\View\Helper\JData;
use Main\View\Helper\Url;
use Main\View\Helper\Plural;
use Main\View\Helper\Version;
use Main\View\Helper\ViewMessage;

return [
    'controllers' => [
        'invokables' => [
        ]
    ],

    'view_helpers' => [
        'aliases' => [

        ],
        'factories' => [
            'translator'                => TranslatorFactory::class,
            'Zend\View\Helper\Identity' => IdentityFactory::class,
        ],

        'invokables' => [
            'configData'  => ConfigData::class,
            'contact'     => Contact::class,
            'form'        => Form::class,
            'jData'       => JData::class,
            'plural'      => Plural::class,
            'url'         => Url::class,
            'version'     => Version::class,
            'viewMessage' => ViewMessage::class,
        ]
    ]
];
