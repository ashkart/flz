<?php

namespace Main\View\Form;

use Application\Model\Domain\AbstractMapper;
use Library\Master\Form\Form;
use Main\View\Form\Collection\Location;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Number;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;
use Application\Model\Domain;

class Vacancy extends Form implements InputFilterProviderInterface
{
    const EL_COMPANY             = 'company_id';
    const EL_POSITION_NAME       = 'position_name';
    const EL_PROFAREA            = 'profarea_id';
    const EL_IS_REMOTE           = 'is_remote';
    const EL_REQUIRED_EXPERIENCE = 'required_experience';
    const EL_DESCRIPTION         = 'description';
    const EL_REQUIREMENTS        = 'requirements';
    const EL_LIABILITIES         = 'liabilities';
    const EL_CONDITIONS          = 'conditions';
    const EL_EMPLOYMENT          = 'employment';
    const EL_PAYCHECK_MIN        = 'paycheck_min';
    const EL_PAYCHECK_MAX        = 'paycheck_max';
    const EL_CURRENCY            = 'currency_id';
    const EL_VISIBILITY          = 'visibility';

    const EL_LOCATIONS_COLLECTION = 'locations';

    const EMPLOYMENTS = [
        Domain\Vacancy::EMPLOYMENT_REGULAR => 'Полная',
        Domain\Vacancy::EMPLOYMENT_PART_TIME => 'Частичная',
        Domain\Vacancy::EMPLOYMENT_SHIFT => 'Посменная',
        Domain\Vacancy::EMPLOYMENT_WATCH => 'Вахта',
    ];

    /**
     * @var bool
     */
    protected $_isAdmin = false;

    /**
     * @param Domain\Currency[] $currencies
     */
    public function __construct(Domain\User $user, array $currencies)
    {
        parent::__construct();

        $this->_isAdmin = $user->isAdmin();

        $userCompanies = [];

        /** @var Domain\Company $userCompany */
        foreach ($user->getCompanies()->asArray() as $userCompany) {
            $userCompanies[$userCompany->getId()] = $userCompany->getOfficialName();
        }

        $company = new Select(self::EL_COMPANY);
        $company->setValueOptions($userCompanies);

        if ($this->_isAdmin) {
            $company->setDisableInArrayValidator(true);
        }

        $position_name = new Text(self::EL_POSITION_NAME);
        $profarea      = new Number(self::EL_PROFAREA);
        $is_remote     = new Checkbox(self::EL_IS_REMOTE);

        $is_remote
            ->setCheckedValue(self::CHECKED)
            ->setUncheckedValue(self::UNCHECKED)
            ->setValue(self::UNCHECKED);

        $required_experience = new Select(self::EL_REQUIRED_EXPERIENCE);
        $required_experience->setValueOptions(array_flip(Domain\Vacancy::EXP_VALUES));

        $description  = new Text(self::EL_DESCRIPTION);
        $requirements = new Text(self::EL_REQUIREMENTS);
        $liabilities  = new Text(self::EL_LIABILITIES);
        $conditions   = new Text(self::EL_CONDITIONS);

        $employment = new Select(self::EL_EMPLOYMENT);
        $employment->setValueOptions(array_flip(Domain\Vacancy::EMPLOYMENTS));

        $elPaycheckMin = new Number(self::EL_PAYCHECK_MIN);
        $elPaycheckMax = new Number(self::EL_PAYCHECK_MAX);

        $elCurrency = new Select(self::EL_CURRENCY);

        $currencyHaystack = [];

        foreach ($currencies as $currency) {
            $currencyHaystack[$currency->getName()] = $currency->getId();
        }

        $elCurrency->setValueOptions($currencyHaystack);

        $elVisibility = new Select(self::EL_VISIBILITY);
        $elVisibility->setValueOptions([
            Domain\Resume::VISIBILITY_ALL  => Domain\Resume::VISIBILITY_ALL,
            Domain\Resume::VISIBILITY_NONE => Domain\Resume::VISIBILITY_NONE
        ]);

        $elLocations = new Location(self::EL_LOCATIONS_COLLECTION);

        $this
            ->add($company)
            ->add($position_name)
            ->add($profarea)
            ->add($is_remote)
            ->add($required_experience)
            ->add($description)
            ->add($requirements)
            ->add($liabilities)
            ->add($conditions)
            ->add($employment)
            ->add($elPaycheckMin)
            ->add($elPaycheckMax)
            ->add($elLocations)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_COMPANY => ['required' => true],
            self::EL_POSITION_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_LONG_CAPTION_MB, 3, true, true),
            self::EL_PROFAREA => ['required' => true],
            self::EL_IS_REMOTE => ['required' => false],
            self::EL_REQUIRED_EXPERIENCE => ['required' => true],
            self::EL_DESCRIPTION => $this->_standardTextFilter(AbstractMapper::LENGTH_TEXT_MB, 5, true),
            self::EL_REQUIREMENTS => $this->_standardTextFilter(AbstractMapper::LENGTH_TEXT_MB, 5, true),
            self::EL_LIABILITIES => $this->_standardTextFilter(AbstractMapper::LENGTH_TEXT_MB, 5, true),
            self::EL_CONDITIONS => $this->_standardTextFilter(AbstractMapper::LENGTH_TEXT_MB, 5, false),
            self::EL_EMPLOYMENT => ['required' => true],
            self::EL_PAYCHECK_MIN => ['required' => false],
            self::EL_PAYCHECK_MAX => ['required' => false],
            self::EL_CURRENCY => ['required' => true],
            self::EL_VISIBILITY => ['required' => true],
        ];
    }
}