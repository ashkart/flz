<?php

namespace Main\View\Form;

use Application\Model\Domain\AbstractMapper;
use Library\Master\Form\Form;
use Zend\Form\Element\Number;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;

class Callback extends Form implements InputFilterProviderInterface
{
    const EL_ENTITY_ID      = 'entity_id';
    const EL_FROM_ENTITY_ID = 'from_entity_id';
    const EL_COVER_LETTER   = 'cover_letter';

    public function __construct()
    {
        parent::__construct();

        $elEntityId = new Number(self::EL_ENTITY_ID);
        $elFromEntityId = new Number(self::EL_FROM_ENTITY_ID);
        $elCoverLetter = new Text(self::EL_COVER_LETTER);

        $this
            ->add($elEntityId)
            ->add($elFromEntityId)
            ->add($elCoverLetter)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_ENTITY_ID => [
                'required' => true
            ],
            self::EL_FROM_ENTITY_ID => [
                'required' => true
            ],
            self::EL_COVER_LETTER => $this->_standardTextFilter(AbstractMapper::MESSAGE_TEXT_MB, 0, false, true)
        ];
    }
}