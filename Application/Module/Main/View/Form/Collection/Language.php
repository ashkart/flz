<?php

namespace Main\View\Form\Collection;

use Main\View\Form\Fieldset\Language as LanguageFieldset;
use Library\Master\Form\Collection;

class Language extends Collection
{
    const DEFAULT_NAME = 'languages';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $fs = new LanguageFieldset();
        $this->setTargetElement($fs);
    }
}