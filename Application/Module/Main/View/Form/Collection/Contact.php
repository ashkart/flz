<?php

namespace Main\View\Form\Collection;

use Library\Master\Form\Collection;
use Main\View\Form\Fieldset\Contact as ContactFieldset;

class Contact extends Collection
{
    const DEFAULT_NAME = 'contacts';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $contactFieldset = new ContactFieldset();
        $this->setTargetElement($contactFieldset);
    }
}