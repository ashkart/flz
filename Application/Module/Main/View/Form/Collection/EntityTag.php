<?php

namespace Main\View\Form\Collection;

use Library\Master\Form\Collection;

class EntityTag extends Collection
{
    const DEFAULT_NAME = 'tags';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $fieldset = new \Main\View\Form\Fieldset\Tag();

        $this->setTargetElement($fieldset);
    }
}