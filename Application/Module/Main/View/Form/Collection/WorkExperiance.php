<?php

namespace Main\View\Form\Collection;

use Library\Master\Form\Collection;

class WorkExperiance extends Collection
{
    const DEFAULT_NAME = 'work_experiences';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);
        $fieldset = new \Main\View\Form\Fieldset\WorkExperience();
        $this->setTargetElement($fieldset);
    }
}