<?php


namespace Main\View\Form\Collection;


use Library\Master\Form\Collection;

class Location extends Collection
{
    const DEFAULT_NAME = 'locations';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $fieldset = new \Main\View\Form\Fieldset\Location(\Main\View\Form\Fieldset\Location::DEFAULT_NAME);

        $this->setTargetElement($fieldset);
    }
}