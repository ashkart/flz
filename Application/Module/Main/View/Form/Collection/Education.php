<?php

namespace Main\View\Form\Collection;

class Education extends \Library\Master\Form\Collection
{
    const DEFAULT_NAME = 'educations';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);
        $fieldset = new \Main\View\Form\Fieldset\Education();
        $this->setTargetElement($fieldset);
    }
}