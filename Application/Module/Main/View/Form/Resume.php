<?php

namespace Main\View\Form;


use Application\Model\Domain\AbstractMapper;
use Library\Master\Form\Form;
use Main\View\Form\Collection\Contact;
use Main\View\Form\Collection\Education;
use Main\View\Form\Collection\Language;
use Main\View\Form\Collection\EntityTag;
use Main\View\Form\Collection\Location;
use Main\View\Form\Collection\WorkExperiance;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Image;
use Zend\Form\Element\Number;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Application\Model\Domain;
use Zend\I18n\Validator\IsInt;
use Zend\InputFilter\InputFilterProviderInterface;

class Resume extends Form implements InputFilterProviderInterface
{
    const EL_POSITION_NAME     = 'position_name';
    const EL_CITY_ID           = 'city_id';
    const EL_FIRST_NAME        = 'first_name';
    const EL_MIDDLE_NAME       = 'middle_name';
    const EL_LAST_NAME         = 'last_name';
    const EL_BIRTH_DATE        = 'birth_date';
    const EL_GENDER            = 'gender';
    const EL_IS_RELOCATE_READY = 'is_relocate_ready';
    const EL_IS_REMOTE         = 'is_remote';
    const EL_EMPLOYMENT        = 'employment';
    const EL_TAGS              = 'tags';
    const EL_EDUCATION_LEVEL   = 'education_level';
    const EL_AUTODESCRIPTION   = 'autodescription';
    const EL_DESIRED_PAYCHECK  = 'desired_paycheck';
    const EL_CURRENCY          = 'currency_id';
    const EL_PHOTO             = 'photo';
    const EL_PHOTO_PATH        = 'photo_path';
    const EL_VISIBILITY        = 'visibility';

    const EL_CONTACT_COLLECTION         = 'contacts';
    const EL_EDUCATION_COLLECTION       = 'education';
    const EL_WORK_EXPERIENCE_COLLECTION = 'work_experience';
    const EL_LANGUAGES_COLLECTION       = 'languages';

    public function __construct()
    {
        parent::__construct();

        $elPositionName = new Text(self::EL_POSITION_NAME);
        $elFirstName    = new Text(self::EL_FIRST_NAME);
        $elMiddleName   = new Text(self::EL_MIDDLE_NAME);
        $elLastName     = new Text(self::EL_LAST_NAME);
        $elBirthDate    = new Text(self::EL_BIRTH_DATE);
        $elPhoto        = new Image(self::EL_PHOTO);
        $elPhotoPath    = new Text(self::EL_PHOTO_PATH);

        $elCityId       = new Number(self::EL_CITY_ID);

        $elGender = new Select(self::EL_GENDER);
        $elGender->setValueOptions([
            Domain\Resume::GENDER_MALE   => 'Мужской',
            Domain\Resume::GENDER_FEMALE => 'Женский'
        ]);

        $elIsRelocateReady = new Checkbox(self::EL_IS_RELOCATE_READY);
        $elIsRelocateReady
            ->setCheckedValue(self::CHECKED)
            ->setUncheckedValue(self::UNCHECKED)
        ;

        $elIsRemote = new Checkbox(self::EL_IS_REMOTE);
        $elIsRemote
            ->setCheckedValue(self::CHECKED)
            ->setUncheckedValue(self::UNCHECKED)
        ;

        $elEducationLevel  = new Select(self::EL_EDUCATION_LEVEL);
        $elEducationLevel->setValueOptions([
            Domain\Resume::EDUCATION_BASIC_GENERAL        => Domain\Resume::EDUCATION_BASIC_GENERAL,
            Domain\Resume::EDUCATION_SECONDARY_GENERAL    => Domain\Resume::EDUCATION_SECONDARY_GENERAL,
            Domain\Resume::EDUCATION_SECONDARY_VOCATIONAL => Domain\Resume::EDUCATION_SECONDARY_VOCATIONAL,
            Domain\Resume::EDUCATION_HIGHER_BACHELOR      => Domain\Resume::EDUCATION_HIGHER_BACHELOR,
            Domain\Resume::EDUCATION_HIGHER_MASTER        => Domain\Resume::EDUCATION_HIGHER_MASTER,
            Domain\Resume::EDUCATION_PHD                  => Domain\Resume::EDUCATION_PHD,
            Domain\Resume::EDUCATION_DOCTOR               => Domain\Resume::EDUCATION_DOCTOR,
        ]);

        $elAutodescription   = new Textarea(self::EL_AUTODESCRIPTION);
        $elDesiredPaycheck   = new Text(self::EL_DESIRED_PAYCHECK);
        $elCurrency          = new Text(self::EL_CURRENCY);
        $educationCollection = new Education(self::EL_EDUCATION_COLLECTION);
        $workExpCollection   = new WorkExperiance(self::EL_WORK_EXPERIENCE_COLLECTION);
        $contactCollection   = new Contact(self::EL_CONTACT_COLLECTION);
        $languageCollection  = new Language(self::EL_LANGUAGES_COLLECTION);
        $elTags              = new EntityTag(self::EL_TAGS);

        $elVisibility = new Select(self::EL_VISIBILITY);
        $elVisibility->setValueOptions([
            Domain\Resume::VISIBILITY_ALL  => Domain\Resume::VISIBILITY_ALL ,
            Domain\Resume::VISIBILITY_NONE => Domain\Resume::VISIBILITY_NONE,
            Domain\Resume::VISIBILITY_LINK => Domain\Resume::VISIBILITY_LINK
        ]);

        $this
            ->add($elPositionName)
            ->add($elCityId)
            ->add($elFirstName)
            ->add($elMiddleName)
            ->add($elLastName)
            ->add($elBirthDate)
            ->add($elPhoto)
            ->add($elPhotoPath)
            ->add($elGender)
            ->add($elIsRelocateReady)
            ->add($elIsRemote)
            ->add($elEducationLevel)
            ->add($elAutodescription)
            ->add($elDesiredPaycheck)
            ->add($elCurrency)
            ->add($educationCollection)
            ->add($workExpCollection)
            ->add($contactCollection)
            ->add($languageCollection)
            ->add($elTags)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_POSITION_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_LONG_CAPTION_MB, 2, true, true),
            self::EL_BIRTH_DATE => [
                'required' => true,
                'validators' => [
                    [
                        'name' => \Zend\Validator\Date::class,
                        'options' => [
                            'format' => \Library\Master\Date\Date::DATE_FORMAT_RU
                        ]
                    ]
                ]
            ],
            self::EL_PHOTO => [
                'required' => false
            ],
            self::EL_PHOTO_PATH => [
                'required' => false
            ],
            self::EL_EDUCATION_LEVEL => [
                'required' => false
            ],
            self::EL_AUTODESCRIPTION => [
                'required' => false
            ],
            self::EL_DESIRED_PAYCHECK => [
                'required' => false,
                'allow_empty' => true,
                'validators' => [
                    [
                        'name' => IsInt::class
                    ]
                ]
            ],
            self::EL_CURRENCY => [
                'required' => false,
                'allow_empty' => true,
                'validators' => [
                    [
                        'name' => IsInt::class
                    ]
                ]
            ],
            self::EL_CITY_ID => [
                'required' => true,
                'validators' => [
                    [
                        'name' => IsInt::class
                    ]
                ]
            ],
            self::EL_FIRST_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION, 1, true, true),
            self::EL_MIDDLE_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION, 1, false, true),
            self::EL_LAST_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION, 1, true, true),
            self::EL_GENDER => [
                'required' => true,
            ],
            self::EL_IS_RELOCATE_READY => [
                'required' => true,
            ],
            self::EL_IS_REMOTE => [
                'required' => true,
            ],
            self::EL_VISIBILITY => ['required' => true],
        ];
    }
}