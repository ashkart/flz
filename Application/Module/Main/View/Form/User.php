<?php

namespace Main\View\Form;

use Library\Master\Form\Form;
use Zend\Form\Element\Date;
use Zend\Form\Element\Email;
use Zend\Form\Element\Number;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Application\Model\Domain;
use Zend\InputFilter\InputFilterProviderInterface;

class User extends Form implements InputFilterProviderInterface
{
    const EL_EMAIL     = 'email';
    const EL_ROLE      = 'role';
    const EL_USER_NAME = 'user_name';
    const EL_PASSWORD  = 'password';
    const EL_CITY_ID   = 'city_id';

    /**
     * @var Domain\User
     */
    protected $_currentUser;

    public function __construct(Domain\User $currentUser)
    {
        parent::__construct();

        $this->_currentUser = $currentUser;

        if ($currentUser->isAdmin()) {
            $elRole = new Select(self::EL_ROLE);
            $elRole->setValueOptions([
                Domain\User::ROLE_GUEST     => 'Гость',
                Domain\User::ROLE_RECRUITER => 'Рекрутер',
                Domain\User::ROLE_EMPLOYEE  => 'Работник',
                Domain\User::ROLE_ADMIN     => 'Администратор',
            ]);

            $this->add($elRole);
        }

        $elEmail     = new Email(self::EL_EMAIL);
        $elFirstName = new Text(self::EL_USER_NAME);
        $elCityId    = new Number(self::EL_CITY_ID);

        $this
            ->add($elEmail)
            ->add($elFirstName)
            ->add($elCityId)
        ;

        $this->addSubmit();
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_EMAIL      => $this->_standardTextFilter(Domain\AbstractMapper::LENGTH_MEDIUM, 5, true, true),
            self::EL_USER_NAME  => $this->_standardTextFilter(Domain\AbstractMapper::LENGTH_MEDIUM, Domain\User::MIN_NAME_LENGTH, true, true),
            self::EL_PASSWORD   => $this->_standardTextFilter(Domain\AbstractMapper::LENGTH_MEDIUM, Domain\User::MIN_PASSWORD_LENGTH, false, false),
            self::EL_ROLE       => ['required' => false],
            self::EL_CITY_ID    => ['required' => true]
        ];
    }
}