<?php

namespace Main\View\Form;

use Application\Model\Domain\AbstractMapper;
use Library\Master\Form\Form;
use Zend\Form\Element\Number;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;

class Company extends Form implements InputFilterProviderInterface
{
    const EL_OPF           = 'opf';
    const EL_OFFICIAL_NAME = 'official_name';
    const EL_FULL_WITH_OPF = 'full_with_opf';
    const EL_INN           = 'inn';
    const EL_KPP           = 'kpp';
    const EL_OGRN          = 'ogrn';
    const EL_ADDRESS       = 'address';
    const EL_DESCRIPTION   = 'description';

    /**
     * @var bool
     */
    protected $_isCreateAction = false;

    public function __construct(bool $isCreateAction = false)
    {
        parent::__construct();

        $this->_isCreateAction = $isCreateAction;

        $elOpf        = new Text(self::EL_OPF);
        $officialName = new Text(self::EL_OFFICIAL_NAME);
        $fullWithOpf  = new Text(self::EL_FULL_WITH_OPF);
        $address      = new Text(self::EL_ADDRESS);
        $description      = new Text(self::EL_DESCRIPTION);

        $this
            ->add($elOpf)
            ->add($officialName)
            ->add($fullWithOpf)
            ->add($address)
            ->add($description)
        ;

        if ($isCreateAction) {
            $inn  = new Text(self::EL_INN);
            $kpp  = new Text(self::EL_KPP);
            $ogrn = new Text(self::EL_OGRN);

            $this
                ->add($inn)
                ->add($kpp)
                ->add($ogrn)
            ;
        }

        $this->addSubmit();
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_OPF => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION_MB, 2, true, true),
            self::EL_OFFICIAL_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 1, true, true),
            self::EL_FULL_WITH_OPF => $this->_standardTextFilter(AbstractMapper::LENGTH_MEDIUM, 4, true, true),
            self::EL_INN => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION_MB, 2, $this->_isCreateAction, true),
            self::EL_KPP => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION_MB, 2, $this->_isCreateAction, true),
            self::EL_OGRN => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION_MB, 2, $this->_isCreateAction, true),
            self::EL_ADDRESS => $this->_standardTextFilter(AbstractMapper::LENGTH_LONG_CAPTION_MB, 2, true, true),
            self::EL_ADDRESS => $this->_standardTextFilter(AbstractMapper::LENGTH_TEXT_MB, 5, true, true),
        ];
    }
}