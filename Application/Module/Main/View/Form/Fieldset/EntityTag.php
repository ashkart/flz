<?php


namespace Main\View\Form\Fieldset;

use Zend\Form\Element\Hidden;

class EntityTag extends Tag
{
    const EL_ENTITY_ID = 'entity_id';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $elEntityId = new Hidden(self::EL_ENTITY_ID);

        $this->add($elEntityId);
    }

    public function getInputFilterSpecification()
    {
        return array_merge(
            parent::getInputFilterSpecification(),
            [
                self::EL_ENTITY_ID => ['required' => false],
            ]
        );
    }
}