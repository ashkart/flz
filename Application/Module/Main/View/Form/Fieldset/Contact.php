<?php

namespace Main\View\Form\Fieldset;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Contact as ContactModel;
use Library\Master\Form\Fieldset;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;

class Contact extends Fieldset implements InputFilterProviderInterface
{
    const DEFAULT_NAME = 'contact';

    const EL_ID    = 'id';
    const EL_TYPE  = 'type';
    const EL_VALUE = 'value';

    public function __construct($name = null, array $options = [])
    {
        parent::__construct($name, $options);

        $id          = new Hidden(self::EL_ID);
        $contactType = new Text(self::EL_TYPE);

        $contactValue = new Text(self::EL_VALUE);

        $this
            ->add($id)
            ->add($contactType)
            ->add($contactValue)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_ID => [
                'required' => false
            ],
            self::EL_TYPE => [
                'required' => true
            ],

            self::EL_VALUE => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION, 3, true, true)
        ];
    }
}