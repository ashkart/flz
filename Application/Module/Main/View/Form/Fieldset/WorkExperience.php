<?php

namespace Main\View\Form\Fieldset;

use Application\Model\Domain\AbstractMapper;
use Library\Master\Form\Fieldset;
use Zend\Form\Element\Date;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;

class WorkExperience extends Fieldset implements InputFilterProviderInterface
{
    const DEFAULT_NAME = 'work_experience';

    const EL_ID            = 'id';
    const EL_POSITION_NAME = 'position_name';
    const EL_DESCRIPTION   = 'description';
    const EL_DATE_START    = 'date_start';
    const EL_DATE_END      = 'date_end';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $elId         = new Hidden(self::EL_ID);
        $positionName = new Text(self::EL_POSITION_NAME);
        $description  = new Text(self::EL_DESCRIPTION);
        $dateStart    = new Text(self::EL_DATE_START);
        $dateEnd      = new Text(self::EL_DATE_END);

        $this
            ->add($elId)
            ->add($positionName)
            ->add($description)
            ->add($dateStart)
            ->add($dateEnd)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_ID => [
                'required' => false
            ],
            self::EL_POSITION_NAME => $this->_standardTextFilter(AbstractMapper::LENGTH_LONG_CAPTION_MB, 2, true, true),
            self::EL_DESCRIPTION   => $this->_standardTextFilter(AbstractMapper::LENGTH_TEXT_MB, 2, true, true),
            self::EL_DATE_START => [
                'required' => true,
                'validators' => [
                    [
                        'name' => \Zend\Validator\Date::class,
                        'options' => [
                            'format' => \Library\Master\Date\Date::DATE_FORMAT_RU
                        ]
                    ]
                ]
            ],
            self::EL_DATE_END   => [
                'required' => false,
                'validators' => [
                    [
                        'name' => \Zend\Validator\Date::class,
                        'options' => [
                            'format' => \Library\Master\Date\Date::DATE_FORMAT_RU
                        ]
                    ]
                ]
            ],
        ];
    }
}