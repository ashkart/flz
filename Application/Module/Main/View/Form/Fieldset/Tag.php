<?php

namespace Main\View\Form\Fieldset;

use Application\Model\Domain\AbstractMapper;
use Library\Master\Form\Fieldset;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;

class Tag extends Fieldset implements InputFilterProviderInterface
{
    const DEFAULT_NAME = 'tag';

    const EL_ID        = 'id';
    const EL_TITLE     = 'title';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $elId       = new Hidden(self::EL_ID);
        $elTitle    = new Text(self::EL_TITLE);

        $this
            ->add($elId)
            ->add($elTitle)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_ID => ['required' => false],
            self::EL_TITLE => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION_MB, 1, true, true)
        ];
    }
}