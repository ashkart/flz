<?php

namespace Main\View\Form\Fieldset;


use Application\Model\Domain\LanguageProficiency;
use Library\Master\Form\Fieldset;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\InputFilter\InputFilterProviderInterface;

class Language extends Fieldset implements InputFilterProviderInterface
{
    const DEFAULT_NAME = 'language';

    const EL_ID    = 'id';
    const EL_CODE  = 'language_code';
    const EL_LEVEL = 'level';

    const LEVELS = [
        LanguageProficiency::LEVEL_BASE  => 'Базовый',
        LanguageProficiency::LEVEL_TECH  => 'Технический (чтение литературы)',
        LanguageProficiency::LEVEL_SPEAK => 'Разговорный',
        LanguageProficiency::LEVEL_FREE  => 'Свободное владение',
    ];

    public function __construct($name = null, array $options = [])
    {
        parent::__construct($name, $options);

        $id     = new Hidden(self::EL_ID);
        $elName = new Select(self::EL_CODE);
        $elName->setValueOptions(LanguageProficiency::LANGUAGES);

        $elLevel = new Select(self::EL_LEVEL);
        $elLevel->setValueOptions(self::LEVELS);

        $this
            ->add($id)
            ->add($elName)
            ->add($elLevel)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_ID => ['required' => false],
            self::EL_CODE => $this->_standardTextFilter(10, 1, true, true),
            self::EL_LEVEL => ['required' => true]
        ];
    }
}