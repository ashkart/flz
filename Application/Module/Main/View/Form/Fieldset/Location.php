<?php


namespace Main\View\Form\Fieldset;


use Application\Model\Domain\AbstractMapper;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;

class Location extends Tag
{
    const DEFAULT_NAME = 'location';

    const EL_TYPE        = 'type';
    const EL_LOCATION_ID = 'location_id';

    const EL_TYPE_CITY    = 'city';
    const EL_TYPE_COUNTRY = 'country';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $elType = new Select(self::EL_TYPE);
        $elType->setValueOptions([
            self::EL_TYPE_CITY => self::EL_TYPE_CITY,
            self::EL_TYPE_COUNTRY => self::EL_TYPE_COUNTRY
        ]);

        $elLocationId = new Hidden(self::EL_LOCATION_ID);

        $this
            ->add($elType)
            ->add($elLocationId)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_TITLE => $this->_standardTextFilter(AbstractMapper::LENGTH_SMALL_CAPTION_MB, 1, false, true),
            self::EL_TYPE => ['required' => true],
            self::EL_LOCATION_ID => ['required' => false]
        ];
    }
}