<?php

namespace Main\View\Form\Fieldset;

use Application\Model\Domain\AbstractMapper;
use Library\Master\Form\Fieldset;
use Zend\Form\Element\Date;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilterProviderInterface;

class Education extends Fieldset implements InputFilterProviderInterface
{
    const DEFAULT_NAME = 'education';

    const EL_ID                       = 'id';
    const EL_SPECIALTY                = 'speciality';
    const EL_EDUCATIONAL_ORGANIZATION = 'educational_organization';
    const EL_DATE_START               = 'date_start';
    const EL_DATE_END                 = 'date_end';

    public function __construct($name = self::DEFAULT_NAME, array $options = [])
    {
        parent::__construct($name, $options);

        $id             = new Hidden(self::EL_ID);
        $specialty      = new Text(self::EL_SPECIALTY);
        $educationalOrg = new Text(self::EL_EDUCATIONAL_ORGANIZATION);
        $dateStart      = new Text(self::EL_DATE_START);
        $dateEnd        = new Text(self::EL_DATE_END);

        $this
            ->add($id)
            ->add($specialty)
            ->add($educationalOrg)
            ->add($dateStart)
            ->add($dateEnd)
        ;
    }

    public function getInputFilterSpecification()
    {
        return [
            self::EL_ID => [
                'required' => false
            ],
            self::EL_SPECIALTY                => $this->_standardTextFilter(AbstractMapper::LENGTH_LONG_CAPTION_MB, 2, true, true),
            self::EL_EDUCATIONAL_ORGANIZATION => $this->_standardTextFilter(AbstractMapper::LENGTH_LONG_CAPTION_MB, 2, true, true),
            self::EL_DATE_START => [
                'required' => true,
                'validators' => [
                    [
                        'name' => \Zend\Validator\Date::class,
                        'options' => [
                            'format' => \Library\Master\Date\Date::DATE_FORMAT_RU
                        ]
                    ]
                ]
            ],
            self::EL_DATE_END   => [
                'required' => false,
                'validators' => [
                    [
                        'name' => \Zend\Validator\Date::class,
                        'options' => [
                            'format' => \Library\Master\Date\Date::DATE_FORMAT_RU
                        ]
                    ]
                ]
            ],
        ];
    }
}