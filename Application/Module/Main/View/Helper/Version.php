<?php


namespace Main\View\Helper;

use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\ServiceManager\Factory;
use Zend\View\Helper\AbstractHelper;

class Version extends AbstractHelper
{
    use TraitDataSource;

    const CONFIG_KEY_STATIC_VERSION = 'static_files_version';

    public function staticFiles() : string
    {
        $config = self::$sm->get(Factory::CONFIG);

        return $config[self::CONFIG_KEY_STATIC_VERSION];
    }
}