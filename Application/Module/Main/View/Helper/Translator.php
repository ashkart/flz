<?php

namespace Main\View\Helper;

use Zend\I18n\View\Helper\AbstractTranslatorHelper;

class Translator extends AbstractTranslatorHelper
{
    protected $locale = 'ru';

    public function __construct(\Zend\I18n\Translator\Translator $translator)
    {
        $this->setTranslator($translator);
        $this->setTranslatorEnabled(true);
    }

    public function translate(string $massage) : string
    {
        return $this->getTranslator()->translate($massage, $this->getTranslatorTextDomain());
    }
}