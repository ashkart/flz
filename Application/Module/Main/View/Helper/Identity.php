<?php

namespace Main\View\Helper;

use Application\Model\Domain\User;

class Identity extends \Zend\View\Helper\Identity
{
    public function __invoke()
    {
        $identity = $this->authenticationService->getIdentity();

        if ($identity) {
            return $identity;
        } else {
            static $emptyUser = null;

            if (!$emptyUser) {
                $emptyUser = User::createEmpty();
            }

            return $emptyUser;
        }
    }
}