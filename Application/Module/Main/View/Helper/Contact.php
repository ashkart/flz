<?php

namespace Main\View\Helper;

use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\ServiceManager\Factory;
use Zend\View\Helper\AbstractHelper;

class Contact extends AbstractHelper
{
    use TraitDataSource;

    /**
     * @var string
     */
    protected $_brandName;

    /**
     * @var string
     */
    protected $_supportEmail;

    public function __construct()
    {
        $config = self::$sm->get(Factory::CONFIG);
        $contactsConfig = $config['contacts'];

        $this->_brandName    = $contactsConfig['brand'];
        $this->_supportEmail = $contactsConfig['support_email'];
    }

    public function brandName() : string
    {
        return $this->_brandName;
    }

    public function supportEmail() : string
    {
        return $this->_supportEmail;
    }
}