<?php

namespace Main\View\Helper;

use Application\View\ViewInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * @method ViewInterface getView()
 */
class JData extends AbstractHelper
{
    const KEY_MESSAGES = 'messages';

    /**
     * @var array
     */
    protected $_data = [];

    public function __invoke()
    {
        static $instance = null;

        if (!$instance) {
            $instance = new self();
        }

        return $instance;
    }

    public function setData(string $key, array $data)
    {
        if (!isset($this->_data[$key]) || !is_array($this->_data[$key])){
            $this->_data[$key] = $data;
        } else {
            $this->_data[$key] = array_merge_recursive($this->_data[$key], $data);
        }
    }

    public function json() : string
    {
        $data = $this->_data;

        if (!$data) {
            $data = new \stdClass();
        }

        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}