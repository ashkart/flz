<?php

namespace Main\View\Helper;

use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\RouteHelper;
use Application\Helper\ServiceManager\Factory;
use Zend\View\Helper\AbstractHelper;

class Url extends AbstractHelper
{
    use TraitDataSource;

    /**
     * @var RouteHelper
     */
    protected $routeHelper;

    public function __invoke($name = null, $params = [], $options = [], $reuseMatchedParams = false)
    {
        $this->routeHelper = self::$sm->get(Factory::ROUTE_HELPER_HTTP);

        if ($name !== null) {
            return $this->_url($name, $params, $options, $reuseMatchedParams);
        }

        return $this;
    }

    public function frontend(string $controller, string $action, array $params = [], $options = []) : string
    {
        return $this->routeHelper->frontend($controller, $action, $params, $options);
    }

    public function admin(string $controller, string $action, array $params = [], $options = []) : string
    {
        return $this->routeHelper->admin($controller, $action, $params, $options);
    }

    protected function _url($name = null, $params = [], $options = [], $reuseMatchedParams = false) : string
    {
        return $this->routeHelper->url($name, $params, $options, $reuseMatchedParams);
    }
}