<?php

namespace Main\View\Helper;

class Plural extends \Zend\I18n\View\Helper\Plural
{
    const RUSSIAN_PLURAL_COUNT = 3;
    const PLURAL_RULE_RUS = 'n % 100 >= 5 && n % 100 <= 20 ? 2 : (n % 100 % 10 == 1 ? 0 : (n % 100 % 10 >= 2 && n % 100 % 10 <= 4 ? 1 : 2))';

    public function __construct()
    {
        parent::__construct();

        $nplurals   = self::RUSSIAN_PLURAL_COUNT;
        $pluralRule = self::PLURAL_RULE_RUS;

        $this->setPluralRule("nplurals=$nplurals; plural=($pluralRule)");
    }
}