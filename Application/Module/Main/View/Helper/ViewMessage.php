<?php


namespace Main\View\Helper;


use Application\View\ViewInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * @method ViewInterface getView()
 */
class ViewMessage extends AbstractHelper
{
    const MSG_CONFIRMATION_SUCCESS = 'Ваш аккаунт активирован.';
    const MSG_CONFIRMATION_FAILURE = 'Ссылка на активацию устарела или недействительна.';

    public function __invoke()
    {
        return $this;
    }

    public function add(string $message)
    {
        $this->getView()->jData()->setData(JData::KEY_MESSAGES, [$message]);
    }
}