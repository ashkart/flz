<?php


namespace Main\View\Helper;

use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\ServiceManager\Factory;
use Zend\View\Helper\AbstractHelper;

class ConfigData extends AbstractHelper
{
    use TraitDataSource;

    public function __invoke()
    {
        return $this;
    }

    public function getFbAppId() : string
    {
        $config = self::$sm->get(Factory::CONFIG);

        $vkConfig = $config['integrations']['facebookApi'];

        return $vkConfig['appId'];
    }
}