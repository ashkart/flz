<?php

namespace Main\View\Helper;

use Zend\Form\Element\Collection;
use Zend\Form\ElementInterface;
use Zend\Form\FormInterface;

class Form extends \Zend\Form\View\Helper\Form
{
    /**
     * @var \Library\Master\Form\Form
     */
    protected $_entity;

    public function __invoke(FormInterface $form = null)
    {
        $this->_entity = $form;

        return $this;
    }

    /**
     * @param string | ElementInterface $element
     */
    public function element($element) : string
    {
        if (
            is_string($element) &&
            (!$this->_entity || !($element = $this->_entity->get($element)))
        ) {
            throw new \Exception('Element not found');
        }

        return $this->renderElement($element);
    }

    public function renderElement(ElementInterface $element) : string
    {
        $tag  = null;

        switch ($element->getAttribute('type')) {
            case 'submit':
            case 'button':
                $tag = 'button ';
                break;

            default:
                $tag = 'input ';
                break;
        }

        $tagAttrs = '';

        foreach ($element->getAttributes() as $name => $value) {
            $tagAttrs .= " $name='$value'";
        }

        $renderedElement = "<{$tag}$tagAttrs>";

        if ($element instanceof Collection) {
            foreach ($element as $collectionItem) {
                $renderedElement .= $this->renderElement($collectionItem);
            }
        }

        if (in_array($element->getAttribute('type'), ['button', 'submit'])) {
            $renderedElement .= "{$element->getLabel()}</button>";
        }

        return $renderedElement;
    }

    public function openTag(FormInterface $form = null)
    {
        $form = $form ?? $this->_entity;

        return parent::openTag($form);
    }
}