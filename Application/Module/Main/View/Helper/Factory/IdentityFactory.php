<?php

namespace Main\View\Helper\Factory;

use Application\Helper\ServiceManager\Factory;
use Interop\Container\ContainerInterface;
use Main\View\Helper\Identity;
use Zend\ServiceManager\Factory\FactoryInterface;

class IdentityFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $identityHelperInstance = new Identity();

        $authService = $container->get(Factory::AUTH_SERVICE);

        $identityHelperInstance->setAuthenticationService($authService);

        return $identityHelperInstance;
    }
}