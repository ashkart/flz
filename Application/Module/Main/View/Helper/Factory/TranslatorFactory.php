<?php

namespace Main\View\Helper\Factory;

use Application\Model\Domain\ClientData;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class TranslatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $locale = (new ClientData())->getLocale();

        $translator = $container->get('translator');
        $translator->setLocale($locale);
        $translator->setFallbackLocale('en');

        return new \Main\View\Helper\Translator($translator);
    }
}