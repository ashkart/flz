<?php

namespace Console;

use Application\Helper\DataSource\DataSourceInterface;
use Application\Helper\ServiceManager\Factory;
use Console\Controller\AbstractController;
use Psr\Container\ContainerInterface;
use Zend\EventManager\EventInterface;
use Zend\Mvc\Console\ConfigProvider;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Mvc\Controller\PluginManager;
use Zend\Mvc\MvcEvent;
use Application\Plugin;

class Module
{
    const NAME = 'console';

    /**
     * @param MvcEvent | EventInterface $e
     */
    public function onBootstrap(EventInterface $e)
    {
        AbstractController::setServiceManager($e->getApplication()->getServiceManager());
        $this->bootstrapPlugins($e);
        $this->_bootstrapConsoleUser($e);
    }

    /**
     * @param EventInterface | MvcEvent $e
     */
    public function bootstrapPlugins(EventInterface $e)
    {
        $sm = $e->getApplication()->getServiceManager();

        $em = $e->getApplication()->getEventManager();

        $request = $e->getRequest();

        /** @var PluginManager $cpm */
        $cpm = $sm->get(PluginManager::class);
        $cpm->setFactory(Params::class, function (ContainerInterface $sm) use ($request) {
            return new Plugin\Params($request);
        });

        /**
         * Проверка прав доступа на страницу
         * Приоритет должен быть больше 1, т.к. с приоритетом 1 выполняется action
         */
        //$em->attach(MvcEvent::EVENT_DISPATCH, new Plugin\AccessCheck($sm), 2);
    }

    public function getConfig()
    {
        $configProvider = new ConfigProvider();
        $providerConfig = ['service_manager' => $configProvider->getDependencyConfig()];
        $moduleConfig = include __DIR__ . '/config/module.config.php';
        return array_merge_recursive($providerConfig, $moduleConfig);
    }

    /**
     * @param EventInterface | MvcEvent $e
     */
    protected function _bootstrapConsoleUser(EventInterface $e)
    {
        $sm = $e->getApplication()->getServiceManager();

        /** @var DataSourceInterface $ds */
        $ds = $sm->get(Factory::DATA_SOURCE);

        $config = $sm->get(Factory::CONFIG);
        $cliUserData = $config['cli_user'];

        $ds->authService()->auth($cliUserData['email'], $cliUserData['password']);
    }
}
