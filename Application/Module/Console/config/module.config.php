<?php

namespace Admin;

use Console\Controller\Console;
use Console\Controller\Cron;
use Console\Controller\ElasticSearch;
use Zend\Mvc\Console\Controller\Plugin\CreateConsoleNotFoundModel;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'invokables' => [
            'console'       => Console::class,
            'cron'          => Cron::class,
            'elasticsearch' => ElasticSearch::class,
        ],
    ],

    'controller_plugins' => [
        'aliases' => [
            'createConsoleNotFoundModel' => CreateConsoleNotFoundModel::class
        ],

        'factories' => [
            CreateConsoleNotFoundModel::class => InvokableFactory::class
        ]
    ],

    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
        'template_path_stack' => [
            __DIR__ . '/../../Main/View/view',
        ],
    ],

    'service_manager' => [

    ],

    'logs' => [
        'error' => __DIR__ . '/var/logs/cli.log'
    ]
];
