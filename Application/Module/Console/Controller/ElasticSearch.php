<?php

namespace Console\Controller;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\City\CityMapper;
use Application\Model\Service\Filter\CityFilter;
use Application\Model\Service\ElasticSearch as ESService;
use Application\Model\Domain\Vacancy\VacancyMapper;

class ElasticSearch extends AbstractController
{
    /**
     * Нужно не менее 10 гб (не точно, может и больше, в зависимости от объема данных) свободного места на харде,
     * иначе es автоматом блокирует индекс, переводя в режим "только чтение".
     */
    public function indexAllAction()
    {
        $this->countryIndexAction();
        $this->regionIndexAction();
        $this->cityIndexAction();
        $this->companyIndexAction();
        $this->tagIndexAction();
        $this->profareaIndexAction();
        $this->vacancyIndexAction();
        $this->resumeIndexAction();
        $this->viewIndexAction();
    }

    public function recreateIndexesAction()
    {
        $esService = self::$ds->elasticSearchService();

        foreach (ESService::INDEXES as $index) {
            if (self::$ds->elasticSearchService()->isIndexExists($index)) {
                $esService->deleteIndex($index);
            }

            $elasticSearchConfig = self::$sm->get(Factory::CONFIG)['elasticsearch'];
            $indexConfig         = $elasticSearchConfig['indexes'][$index];
            if (!isset($indexConfig['settings'])) {
                $indexConfig['settings'] = $elasticSearchConfig[ESService::INDEX_DEFAULT_SETTINGS];
            }

            $esService->createIndex($index, $indexConfig);
        }
    }

    public function countryIndexAction()
    {
        $countries = self::$ds->countryMapper()->fetchAll([]);

        $createBulkParams = function () use ($countries) {
            $params = [];

            foreach ($countries as $country) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_COUNTRY,
                        '_type' => ESService::INDEX_COUNTRY
                    ]
                ];

                $params['body'][] = [
                    'id' => $country->getId(),
                    'title_ru' => $country->getTitleRu(),
                    'title_en' => $country->getTitleEn()
                ];
            }

            return $params;
        };

        $bulkResult = self::$ds->elasticSearchService()->bulkIndexModels(
            ESService::INDEX_COUNTRY,
            $createBulkParams,
            true
        );

        printf("countries indexed: %d\n", count($bulkResult['items']));
    }

    public function regionIndexAction()
    {
        $regions = self::$ds->regionMapper()->fetchAll([]);

        $createBulkParams = function () use ($regions) {
            $params = [];

            foreach ($regions as $region) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_REGION,
                        '_type' => ESService::INDEX_REGION,
                    ]
                ];

                $params['body'][] = [
                    'id' => $region->getId(),
                    'country_id' => $region->getCountryId(),
                    'title' => $region->getTitle(),
                    'description' => $region->getCountry()->getTitleRu()
                ];
            }

            return $params;
        };

        $bulkResult = self::$ds->elasticSearchService()->bulkIndexModels(
            ESService::INDEX_REGION,
            $createBulkParams,
            true
        );

        printf("regions indexed: %d\n", count($bulkResult['items']));
    }

    public function cityIndexAction()
    {
        $countries = self::$ds->countryMapper()->fetchAll([]); // Russia, Ukraine, Belarus

        $counter = 0;

        foreach ($countries as $country) {
            $pageNumber = 1;

            printf("Current country: %s\n", $country->getTitleRu());

            while (true) {
                $filter = new CityFilter();
                $filter
                    ->setCountryId($country->getId())
                    ->setPaging(5000, $pageNumber++);

                $cities = self::$ds->cityService()->getList($filter);

                if (!$cities) {
                    break;
                }

                $createBulkParams = function () use ($cities) {
                    $params = [];

                    foreach ($cities as $city) {

                        $cityObj = self::$ds->cityMapper()->fetch($city[CityMapper::PRIMARY]);


                        $params['body'][] = [
                            'index' => [
                                '_index' => ESService::INDEX_CITY,
                                '_type' => ESService::INDEX_CITY
                            ]
                        ];

                        $params['body'][] = self::$ds->cityService()->indexiseModel($cityObj);
                    }

                    return $params;
                };

                $result = self::$ds->elasticSearchService()->bulkIndexModels(
                    ESService::INDEX_CITY,
                    $createBulkParams,
                    true
                );

                $counter += count($cities);
                printf("Cities indexed: %d\n", $counter);
            }
        }
    }

    public function companyIndexAction()
    {
        $userCompanies = self::$ds->userCompanyMapper()->fetchAll([]);

        $createBulkParams = function () use ($userCompanies) {
            $params = [];

            foreach ($userCompanies as $userCompany) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_COMPANY,
                        '_type' => ESService::INDEX_COMPANY,
                    ]
                ];

                $params['body'][] = self::$ds->userCompanyService()->indexiseModel($userCompany);
            }

            return $params;
        };

        $bulkResult = self::$ds->elasticSearchService()->bulkIndexModels(
            ESService::INDEX_COMPANY,
            $createBulkParams,
            true
        );

        printf("companies indexed: %d\n", count($bulkResult['items'] ?? []));
    }

    public function tagIndexAction()
    {
        $tags = self::$ds->tagMapper()->fetchAll([]);

        if (!$tags) {
            printf("No tags to index\n");
            return;
        }

        $bulkResult = self::$ds->tagService()->bulkIndex($tags, true);

        printf("tags indexed: %d\n", count($bulkResult['items'] ?? []));
    }

    public function profareaIndexAction()
    {
        $profareas = self::$ds->profareaMapper()->fetchAll([]);

        $createBulkParams = function () use ($profareas) {
            $params = [];

            foreach ($profareas as $profarea) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_PROFAREA,
                        '_type' => ESService::INDEX_PROFAREA
                    ]
                ];

                $params['body'][] = [
                    'id' => $profarea->getId(),
                    'parent_id' => $profarea->getParentId(),
                    'title' => $profarea->getTitle()
                ];
            }

            return $params;
        };

        $bulkResult = self::$ds->elasticSearchService()->bulkIndexModels(ESService::INDEX_PROFAREA, $createBulkParams, true);

        printf("profareas indexed: %d\n", count($bulkResult['items']));
    }
    
    public function vacancyIndexAction()
    {
        $vacancies = self::$ds->vacancyMapper()->fetchAll([VacancyMapper::COL_IS_DELETED => false], false, true);

        if (!$vacancies) {
            printf("No vacancies to index\n");
            return;
        }

        $bulkResult = self::$ds->vacancyService()->bulkIndex($vacancies, true);

        printf("vacancies indexed: %d\n", count($bulkResult['items'] ?? []));
    }

    public function resumeIndexAction()
    {
        $resumes = self::$ds->resumeMapper()->fetchAll([], false, true);

        if (!$resumes) {
            printf("No resumes to index\n");
            return;
        }

        $bulkResult = self::$ds->resumeService()->bulkIndex($resumes, true);

        printf("resumes indexed: %d\n", count($bulkResult['items'] ?? []));
    }

    public function viewIndexAction()
    {
        $views = self::$ds->viewMapper()->fetchAll([]);

        if (!$views) {
            $esService = self::$ds->elasticSearchService();

            if (!$esService->isIndexExists(ESService::INDEX_VIEW)) {

                $elasticSearchConfig = self::$sm->get(Factory::CONFIG)['elasticsearch'];
                $indexConfig         = $elasticSearchConfig['indexes'][ESService::INDEX_VIEW];
                if (!isset($indexConfig['settings'])) {
                    $indexConfig['settings'] = $elasticSearchConfig[ESService::INDEX_DEFAULT_SETTINGS];
                }

                $esService->createIndex(ESService::INDEX_VIEW, $indexConfig);
            }

            printf("No views to index\n");
            return;
        }

        $bulkResult = self::$ds->viewService()->bulkIndex($views, true);

        printf("views indexed: %d\n", count($bulkResult['items'] ?? []));
    }

    public function deleteIndexAction()
    {
        foreach (ESService::LOCALITY_INDEX_TYPES as $indexName) {
            self::$ds->elasticSearchService()->deleteIndex($indexName);
        }
    }
}