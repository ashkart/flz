<?php

namespace Console\Controller;

use Application\Helper\RouteHelper;
use Application\Helper\ServiceManager\Factory;
use Application\Helper\TraitViewRenderer;
use Traversable;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class AbstractController extends AbstractConsoleController
{
    use TraitViewRenderer;

    /**
     * @var RouteHelper | null
     */
    protected $routeHelper = null;

    public function __construct()
    {
        $robotUserData = self::$sm->get(Factory::CONFIG)['cli_user'];
        $authResult = self::$ds->authService()->auth($robotUserData['email'], $robotUserData['password']);

        if (!$authResult) {
            throw new \Exception('Cli user not authorized. Check config for cli user data.');
        }
    }


    /**
     * @return RouteHelper
     */
    protected function _getRouteHelper()
    {
        if (!$this->routeHelper) {
            $this->routeHelper = self::$sm->get(Factory::ROUTE_HELPER_HTTP);
        }

        return $this->routeHelper;
    }

    /**
     * @param  string               $name               Name of the route
     * @param  array                $params             Parameters for the link
     * @param  array|Traversable    $options            Options for the route
     * @param  bool                 $reuseMatchedParams Whether to reuse matched parameters
     *
     * @return RouteHelper | string
     */
    protected function _url($name = null, array $params = [], array $options = [], $reuseMatchedParams = false)
    {
        $routeHelper = $this->_getRouteHelper();

        if (!$name && !$params && !$options && !$reuseMatchedParams) {
            return $routeHelper;
        }

        return is_bool($options)
            ? $routeHelper->url($name, $params, $options)
            : $routeHelper->url($name, $params, $options, $reuseMatchedParams)
            ;
    }
}