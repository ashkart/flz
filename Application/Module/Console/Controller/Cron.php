<?php

namespace Console\Controller;

use Library\Master\Cron\CronManager;

class Cron extends AbstractController
{
    const APP_CLI_EXEC = ' php ' . APP_PATH . '/cli.php';

    const CRON_TABLE_LINES = [
        //'*/1 * * * *' . self::APP_CLI_EXEC . ' console test'
    ];

    public function updateCronTabAction()
    {
        CronManager::updateCronTable(self::CRON_TABLE_LINES, true);
    }
}