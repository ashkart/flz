<?php

namespace Application\Cache\Slot;

class PasswordUpdate extends Slot
{
    public function __construct(string $token, int $ttl = self::TTL_HOUR)
    {
        parent::__construct($token, self::class, $ttl);
    }
}