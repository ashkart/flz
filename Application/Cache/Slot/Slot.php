<?php

namespace Application\Cache\Slot;

use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\ServiceManager\Factory;
use Library\Master\Cache\PRedisClient as RedisBackend;

class Slot
{
    const TTL_HOUR = 3600;
    const TTL_DAY  = self::TTL_HOUR * 24;
    const TTL_WEEK = self::TTL_DAY * 7;

    const TTL_DEFAULT = self::TTL_HOUR;

    use TraitDataSource;

    /**
     * @var RedisBackend
     */
    protected $_backend;

    /**
     * @var string
     */
    protected $_key;

    /**
     * @var int
     */
    protected $_ttl;

    protected $_loadResult = null;

    public function __construct(string $key = null, string $keyNamespace = null, int $ttl = self::TTL_DEFAULT)
    {
        $this->_backend   = self::$sm->get(Factory::REDIS_CACHE);
        $this->_key       = $this->_createKey($key, $keyNamespace);
        $this->_ttl       = $ttl;

        if (PHP_SAPI === 'cli' && !$this->isConnected()) {
            $this->_backend->disconnect();
            $this->_backend->connect();
        }
    }

    public function save($value)
    {
        $serialized = igbinary_serialize($value);

        $this->_backend->set($this->_key, $serialized, 'EX', $this->_ttl);
    }

    public function saveMany(array $assoc)
    {
        $dictionary = [];

        foreach ($assoc as $key => $value) {
            $dictionary[$this->_createKey($key, $this->_key)] = igbinary_serialize($value);
        }

        $this->_backend->mset($dictionary);
    }

    /**
     * @param $key
     *
     * @return false | mixed
     */
    public function load()
    {
        $this->_loadResult = $this->_loadResult ?? $this->_backend->get($this->_key);

        if (!$this->_loadResult) {
            return false;
        }

        return igbinary_unserialize($this->_loadResult);
    }

    public function loadMany(array $keys) : array
    {
        $realKeys = $realKeys = $this->_createKeys($keys);

        $backendResult = $this->_backend->mget($realKeys);

        $unserializedResult = [];

        foreach ($backendResult as $index => $serialized) {
            $unserializedResult[$keys[$index]] = igbinary_unserialize($serialized);
        }

        return $unserializedResult;
    }

    public function deleteMany(array $keys) : int
    {
        $realKeys = $this->_createKeys($keys);

        return $this->_backend->del($realKeys);
    }

    public function delete() : int
    {
        return $this->_backend->del([$this->_key]);
    }

    public function setTtl(int $ttl)
    {
        $this->_backend->expire($this->_key, $ttl);
    }

    public function isConnected() : bool
    {
        try {
            $this->_loadResult = $this->load();
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    protected function _createKey(string $key = null, string $keyNamespace = null) : string
    {
        $keyNamespace = str_replace('\\', '_', $keyNamespace ?? get_class($this));

        if ($key === null || $key === '') {
            return $keyNamespace;
        }

        return "$keyNamespace:$key";
    }

    protected function _createKeys(array $keys) : array
    {
        $realKeys = [];

        foreach ($keys as $key) {
            $realKeys[] = $this->_createKey($key, $this->_key);
        }

        return $realKeys;
    }
}