<?php

namespace Application\Cache\Slot;

class UserCompanies extends Slot
{
    public function __construct(int $userId, int $ttl = self::TTL_DEFAULT)
    {
        parent::__construct($userId, self::class, $ttl);
    }
}