<?php

namespace Application\Cache\Slot;

class EntityCallbacks extends Slot
{
    public function __construct(int $entityId, string $entityType, int $ttl = self::TTL_DEFAULT)
    {
        parent::__construct($entityId, self::class . "_$entityType", $ttl);
    }
}