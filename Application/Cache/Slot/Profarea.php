<?php

namespace Application\Cache\Slot;

class Profarea extends Slot
{
    public function __construct(string $key = null, int $ttl = self::TTL_DEFAULT)
    {
        parent::__construct($key, self::class, $ttl);
    }
}