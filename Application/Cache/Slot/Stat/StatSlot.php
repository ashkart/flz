<?php

namespace Application\Cache\Slot\Stat;


use Application\Cache\Slot\Slot;
use Application\Helper\ServiceManager\Factory;

class StatSlot extends Slot
{
    public function __construct(string $key = null, string $keyNamespace = null, int $ttl = self::TTL_DEFAULT)
    {
        parent::__construct($key, $keyNamespace, $ttl);

        $this->_backend = self::$sm->get(Factory::REDIS_STAT);
    }
}