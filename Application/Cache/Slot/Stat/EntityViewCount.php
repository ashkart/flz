<?php

namespace Application\Cache\Slot\Stat;

class EntityViewCount extends StatSlot
{
    public function __construct(int $entityId, string $entityType, int $ttl = self::TTL_DEFAULT)
    {
        parent::__construct($entityId, self::class . "_$entityType", $ttl);
    }
}