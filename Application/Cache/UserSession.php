<?php

namespace Application\Cache;


use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\ServiceManager\Factory;
use Zend\Session\SaveHandler\Cache;

class UserSession
{
    use TraitDataSource;

    /**
     * @var Cache
     */
    protected $_backend;

    /**
     * @var string
     */
    protected $_key;

    public function __construct(string $key)
    {
        $this->_backend = self::$sm->get(Factory::REDIS_SESSION_CACHE);
        $this->_key = $this->_createKey(md5($key));
    }

    public function save($value)
    {
        $serialized = igbinary_serialize($value);

        $this->_backend->getCacheStorage()->setItem($this->_key, $serialized);
    }

    public function load()
    {
        $loadRes = $this->_backend->getCacheStorage()->getItem($this->_key);

        if (!$loadRes) {
            return false;
        }

        return igbinary_unserialize($loadRes);
    }

    public function destroy(string $id): int
    {
        $sessions = $this->load();

        if ($sessions) {
            $i = array_search($id, $sessions);

            if ($i !== false) {
                array_slice($sessions, $i, 1);
                $this->save($sessions);
            }
        }

        return $this->_backend->destroy($id);
    }

    public function destroyAll()
    {
        $sessions = $this->load();

        if ($sessions) {
            foreach ($sessions as $i => $session) {
                $this->_backend->destroy($session);
            }

            $this->_backend->getCacheStorage()->removeItem($this->_key);
        }
    }

    protected function _createKey(string $key = null) : string
    {
        $keyNamespace = str_replace('\\', '_', get_class($this));

        if ($key === null || $key === '') {
            return $keyNamespace;
        }

        return "$keyNamespace:$key";
    }
}