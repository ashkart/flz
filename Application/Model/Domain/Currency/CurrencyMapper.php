<?php
namespace Application\Model\Domain\Currency;

use Application\Cache\Slot\Slot;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Currency;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method Currency         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Currency[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Currency[]       fetchByIds(array $ids)
 * @method Currency | null  fetchOne(array $where)
 */
class CurrencyMapper extends AbstractMapper
{
    const COL_ISO_CODE = 'iso_code';
    const COL_NAME     = 'name';

    const COLS = [
        self::PRIMARY,
        self::COL_ISO_CODE,
        self::COL_NAME
    ];

    protected function fetchByIsoCode(string $isoCode, bool $useCache = true) : Currency
    {
        if ($useCache) {
            $slot = new Slot($isoCode, get_class($this));

            $cachedRes = $slot->load();

            if ($cachedRes) {
                return $cachedRes;
            }
        }

        $currency = $this->fetchOne([self::COL_ISO_CODE => $isoCode]);

        if ($useCache) {
            $slot->save($currency);
        }

        return $currency;
    }

    /**
     * @param DomainModelInterface | Currency $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_ISO_CODE => $obj->getIsoCode(),
            self::COL_NAME => $obj->getName()
        ];
    }

    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Currency($row[self::COL_ISO_CODE], $row[self::COL_NAME]))->setId($row[self::PRIMARY]);
    }
}
