<?php
namespace Application\Model\Domain\View;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\View;
use Application\Model\Interfaces\DomainModelInterface;
use Library\Master\Date\DateTime;

/**
 * @method View | null  fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method View[]       fetchByIds(array $ids)
 * @method View[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method View | null  fetchOne(array $where)
 */
class ViewMapper extends AbstractMapper
{
    const COL_ENTITY_ID   = 'entity_id';
    const COL_ENTITY_TYPE = 'entity_type';
    const COL_USER_ID     = 'user_id';
    const COL_COMPANY_ID  = 'company_id';
    const COL_DATE_TIME   = 'date_time';

    const COLS = [
        self::PRIMARY,
        self::COL_ENTITY_ID,
        self::COL_ENTITY_TYPE,
        self::COL_USER_ID,
        self::COL_COMPANY_ID,
        self::COL_DATE_TIME,
    ];

    const COLS_NOT_UPDATE = [
        self::COL_DATE_TIME
    ];

    public function getDataForSaving(View $obj) : array
    {
        return $this->_getDataForSaving($obj);
    }

    /**
     * @param DomainModelInterface | View $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_ENTITY_ID   => $obj->getEntityId(),
            self::COL_ENTITY_TYPE => $obj->getEntityType(),
            self::COL_USER_ID     => $obj->getUserId(),
            self::COL_COMPANY_ID  => $obj->getCompanyId()
        ];
    }

    /**
     * @param array $row
     * @return DomainModelInterface | View
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new View(
            $row[self::COL_ENTITY_ID],
            $row[self::COL_ENTITY_TYPE],
            $row[self::COL_USER_ID],
            $row[self::COL_COMPANY_ID],
            DateTime::createFromMysqlFormat($row[self::COL_DATE_TIME])
        ))
            ->setId($row[self::PRIMARY])
        ;
    }
}
