<?php
namespace Application\Model\Domain\View;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class ViewMapperEvents extends MapperEvents
{
}
