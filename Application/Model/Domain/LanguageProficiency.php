<?php

namespace Application\Model\Domain;

class LanguageProficiency extends AbstractModel
{
    const LEVEL_BASE  = 'base';
    const LEVEL_TECH  = 'tech';
    const LEVEL_SPEAK = 'speak';
    const LEVEL_FREE  = 'free';

    const LEVELS = [
        self::LEVEL_BASE,
        self::LEVEL_TECH,
        self::LEVEL_SPEAK,
        self::LEVEL_FREE
    ];

    const CODE_AB    = 'ab';
    const CODE_AZ    = 'az';
    const CODE_AY    = 'ay';
    const CODE_SQ    = 'sq';
    const CODE_EN    = 'en';
    const CODE_EN_US = 'en-us';
    const CODE_AR    = 'ar';
    const CODE_HY    = 'hy';
    const CODE_AS    = 'as';
    const CODE_AF    = 'af';
    const CODE_BA    = 'ba';
    const CODE_BE    = 'be';
    const CODE_BN    = 'bn';
    const CODE_BG    = 'bg';
    const CODE_BR    = 'br';
    const CODE_CY    = 'cy';
    const CODE_HU    = 'hu';
    const CODE_VI    = 'vi';
    const CODE_GL    = 'gl';
    const CODE_NL    = 'nl';
    const CODE_EL    = 'el';
    const CODE_KA    = 'ka';
    const CODE_GN    = 'gn';
    const CODE_DA    = 'da';
    const CODE_ZU    = 'zu';
    const CODE_IW    = 'iw';
    const CODE_JI    = 'ji';
    const CODE_IN    = 'in';
    const CODE_IA    = 'ia';
    const CODE_GA    = 'ga';
    const CODE_IS    = 'is';
    const CODE_ES    = 'es';
    const CODE_IT    = 'it';
    const CODE_KK    = 'kk';
    const CODE_KM    = 'km';
    const CODE_CA    = 'ca';
    const CODE_KS    = 'ks';
    const CODE_QU    = 'qu';
    const CODE_KY    = 'ky';
    const CODE_ZH    = 'zh';
    const CODE_KO    = 'ko';
    const CODE_CO    = 'co';
    const CODE_KU    = 'ku';
    const CODE_LO    = 'lo';
    const CODE_LV    = 'lv';
    const CODE_LA    = 'la';
    const CODE_LT    = 'lt';
    const CODE_MG    = 'mg';
    const CODE_MS    = 'ms';
    const CODE_MT    = 'mt';
    const CODE_MI    = 'mi';
    const CODE_MK    = 'mk';
    const CODE_MO    = 'mo';
    const CODE_MN    = 'mn';
    const CODE_NA    = 'na';
    const CODE_DE    = 'de';
    const CODE_NE    = 'ne';
    const CODE_NO    = 'no';
    const CODE_PA    = 'pa';
    const CODE_FA    = 'fa';
    const CODE_PL    = 'pl';
    const CODE_PT    = 'pt';
    const CODE_PS    = 'ps';
    const CODE_RM    = 'rm';
    const CODE_RO    = 'ro';
    const CODE_RU    = 'ru';
    const CODE_SM    = 'sm';
    const CODE_SA    = 'sa';
    const CODE_SR    = 'sr';
    const CODE_SK    = 'sk';
    const CODE_SL    = 'sl';
    const CODE_SO    = 'so';
    const CODE_SW    = 'sw';
    const CODE_SU    = 'su';
    const CODE_TL    = 'tl';
    const CODE_TG    = 'tg';
    const CODE_TH    = 'th';
    const CODE_TA    = 'ta';
    const CODE_TT    = 'tt';
    const CODE_BO    = 'bo';
    const CODE_TO    = 'to';
    const CODE_TR    = 'tr';
    const CODE_TK    = 'tk';
    const CODE_UZ    = 'uz';
    const CODE_UK    = 'uk';
    const CODE_UR    = 'ur';
    const CODE_FJ    = 'fj';
    const CODE_FI    = 'fi';
    const CODE_FR    = 'fr';
    const CODE_FY    = 'fy';
    const CODE_HA    = 'ha';
    const CODE_HI    = 'hi';
    const CODE_HR    = 'hr';
    const CODE_CS    = 'cs';
    const CODE_SV    = 'sv';
    const CODE_EO    = 'eo';
    const CODE_ET    = 'et';
    const CODE_JW    = 'jw';
    const CODE_JA    = 'ja';

    const NAME_AB    = 'Абхазский';
    const NAME_AZ    = 'Азербайджанский';
    const NAME_AY    = 'Аймарский';
    const NAME_SQ    = 'Албанский';
    const NAME_EN    = 'Английский';
    const NAME_EN_US = 'Американский английский';
    const NAME_AR    = 'Арабский';
    const NAME_HY    = 'Армянский';
    const NAME_AS    = 'Ассамский';
    const NAME_AF    = 'Африкаанс';
    const NAME_BA    = 'Башкирский';
    const NAME_BE    = 'Белорусский';
    const NAME_BN    = 'Бенгальский';
    const NAME_BG    = 'Болгарский';
    const NAME_BR    = 'Бретонский';
    const NAME_CY    = 'Валлийский';
    const NAME_HU    = 'Венгерский';
    const NAME_VI    = 'Вьетнамский';
    const NAME_GL    = 'Галисийский';
    const NAME_NL    = 'Голландский';
    const NAME_EL    = 'Греческий';
    const NAME_KA    = 'Грузинский';
    const NAME_GN    = 'Гуарани';
    const NAME_DA    = 'Датский';
    const NAME_ZU    = 'Зулу';
    const NAME_IW    = 'Иврит';
    const NAME_JI    = 'Идиш';
    const NAME_IN    = 'Индонезийский';
    const NAME_IA    = 'Интерлингва (искусственный язык)';
    const NAME_GA    = 'Ирландский';
    const NAME_IS    = 'Исландский';
    const NAME_ES    = 'Испанский';
    const NAME_IT    = 'Итальянский';
    const NAME_KK    = 'Казахский';
    const NAME_KM    = 'Камбоджийский';
    const NAME_CA    = 'Каталанский';
    const NAME_KS    = 'Кашмирский';
    const NAME_QU    = 'Кечуа';
    const NAME_KY    = 'Киргизский';
    const NAME_ZH    = 'Китайский';
    const NAME_KO    = 'Корейский';
    const NAME_CO    = 'Корсиканский';
    const NAME_KU    = 'Курдский';
    const NAME_LO    = 'Лаосский';
    const NAME_LV    = 'Латвийский, латышский';
    const NAME_LA    = 'Латынь';
    const NAME_LT    = 'Литовский';
    const NAME_MG    = 'Малагасийский';
    const NAME_MS    = 'Малайский';
    const NAME_MT    = 'Мальтийский';
    const NAME_MI    = 'Маори';
    const NAME_MK    = 'Македонский';
    const NAME_MO    = 'Молдавский';
    const NAME_MN    = 'Монгольский';
    const NAME_NA    = 'Науру';
    const NAME_DE    = 'Немецкий';
    const NAME_NE    = 'Непальский';
    const NAME_NO    = 'Норвежский';
    const NAME_PA    = 'Пенджаби';
    const NAME_FA    = 'Персидский';
    const NAME_PL    = 'Польский';
    const NAME_PT    = 'Португальский';
    const NAME_PS    = 'Пуштунский';
    const NAME_RM    = 'Ретороманский';
    const NAME_RO    = 'Румынский';
    const NAME_RU    = 'Русский';
    const NAME_SM    = 'Самоанский';
    const NAME_SA    = 'Санскрит';
    const NAME_SR    = 'Сербский';
    const NAME_SK    = 'Словацкий';
    const NAME_SL    = 'Словенский';
    const NAME_SO    = 'Сомали';
    const NAME_SW    = 'Суахили';
    const NAME_SU    = 'Суданский';
    const NAME_TL    = 'Тагальский';
    const NAME_TG    = 'Таджикский';
    const NAME_TH    = 'Тайский';
    const NAME_TA    = 'Тамильский';
    const NAME_TT    = 'Татарский';
    const NAME_BO    = 'Тибетский';
    const NAME_TO    = 'Тонга';
    const NAME_TR    = 'Турецкий';
    const NAME_TK    = 'Туркменский';
    const NAME_UZ    = 'Узбекский';
    const NAME_UK    = 'Украинский';
    const NAME_UR    = 'Урду';
    const NAME_FJ    = 'Фиджи';
    const NAME_FI    = 'Финский';
    const NAME_FR    = 'Французский';
    const NAME_FY    = 'Фризский';
    const NAME_HA    = 'Хауса';
    const NAME_HI    = 'Хинди';
    const NAME_HR    = 'Хорватский';
    const NAME_CS    = 'Чешский';
    const NAME_SV    = 'Шведский';
    const NAME_EO    = 'Эсперанто (искусственный язык)';
    const NAME_ET    = 'Эстонский';
    const NAME_JW    = 'Яванский';
    const NAME_JA    = 'Японский';

    const LANGUAGES = [
        self::CODE_EN    => self::NAME_EN,
        self::CODE_AB    => self::NAME_AB,
        self::CODE_AZ    => self::NAME_AZ,
        self::CODE_AY    => self::NAME_AY,
        self::CODE_SQ    => self::NAME_SQ,
        self::CODE_AR    => self::NAME_AR,
        self::CODE_HY    => self::NAME_HY,
        self::CODE_AS    => self::NAME_AS,
        self::CODE_AF    => self::NAME_AF,
        self::CODE_BA    => self::NAME_BA,
        self::CODE_BE    => self::NAME_BE,
        self::CODE_BN    => self::NAME_BN,
        self::CODE_BG    => self::NAME_BG,
        self::CODE_BR    => self::NAME_BR,
        self::CODE_CY    => self::NAME_CY,
        self::CODE_HU    => self::NAME_HU,
        self::CODE_VI    => self::NAME_VI,
        self::CODE_GL    => self::NAME_GL,
        self::CODE_NL    => self::NAME_NL,
        self::CODE_EL    => self::NAME_EL,
        self::CODE_KA    => self::NAME_KA,
        self::CODE_GN    => self::NAME_GN,
        self::CODE_DA    => self::NAME_DA,
        self::CODE_ZU    => self::NAME_ZU,
        self::CODE_IW    => self::NAME_IW,
        self::CODE_JI    => self::NAME_JI,
        self::CODE_IN    => self::NAME_IN,
        self::CODE_IA    => self::NAME_IA,
        self::CODE_GA    => self::NAME_GA,
        self::CODE_IS    => self::NAME_IS,
        self::CODE_ES    => self::NAME_ES,
        self::CODE_IT    => self::NAME_IT,
        self::CODE_KK    => self::NAME_KK,
        self::CODE_KM    => self::NAME_KM,
        self::CODE_CA    => self::NAME_CA,
        self::CODE_KS    => self::NAME_KS,
        self::CODE_QU    => self::NAME_QU,
        self::CODE_KY    => self::NAME_KY,
        self::CODE_ZH    => self::NAME_ZH,
        self::CODE_KO    => self::NAME_KO,
        self::CODE_CO    => self::NAME_CO,
        self::CODE_KU    => self::NAME_KU,
        self::CODE_LO    => self::NAME_LO,
        self::CODE_LV    => self::NAME_LV,
        self::CODE_LA    => self::NAME_LA,
        self::CODE_LT    => self::NAME_LT,
        self::CODE_MG    => self::NAME_MG,
        self::CODE_MS    => self::NAME_MS,
        self::CODE_MT    => self::NAME_MT,
        self::CODE_MI    => self::NAME_MI,
        self::CODE_MK    => self::NAME_MK,
        self::CODE_MO    => self::NAME_MO,
        self::CODE_MN    => self::NAME_MN,
        self::CODE_NA    => self::NAME_NA,
        self::CODE_DE    => self::NAME_DE,
        self::CODE_NE    => self::NAME_NE,
        self::CODE_NO    => self::NAME_NO,
        self::CODE_PA    => self::NAME_PA,
        self::CODE_FA    => self::NAME_FA,
        self::CODE_PL    => self::NAME_PL,
        self::CODE_PT    => self::NAME_PT,
        self::CODE_PS    => self::NAME_PS,
        self::CODE_RM    => self::NAME_RM,
        self::CODE_RO    => self::NAME_RO,
        self::CODE_RU    => self::NAME_RU,
        self::CODE_SM    => self::NAME_SM,
        self::CODE_SA    => self::NAME_SA,
        self::CODE_SR    => self::NAME_SR,
        self::CODE_SK    => self::NAME_SK,
        self::CODE_SL    => self::NAME_SL,
        self::CODE_SO    => self::NAME_SO,
        self::CODE_SW    => self::NAME_SW,
        self::CODE_SU    => self::NAME_SU,
        self::CODE_TL    => self::NAME_TL,
        self::CODE_TG    => self::NAME_TG,
        self::CODE_TH    => self::NAME_TH,
        self::CODE_TA    => self::NAME_TA,
        self::CODE_TT    => self::NAME_TT,
        self::CODE_BO    => self::NAME_BO,
        self::CODE_TO    => self::NAME_TO,
        self::CODE_TR    => self::NAME_TR,
        self::CODE_TK    => self::NAME_TK,
        self::CODE_UZ    => self::NAME_UZ,
        self::CODE_UK    => self::NAME_UK,
        self::CODE_UR    => self::NAME_UR,
        self::CODE_FJ    => self::NAME_FJ,
        self::CODE_FI    => self::NAME_FI,
        self::CODE_FR    => self::NAME_FR,
        self::CODE_FY    => self::NAME_FY,
        self::CODE_HA    => self::NAME_HA,
        self::CODE_HI    => self::NAME_HI,
        self::CODE_HR    => self::NAME_HR,
        self::CODE_CS    => self::NAME_CS,
        self::CODE_SV    => self::NAME_SV,
        self::CODE_EO    => self::NAME_EO,
        self::CODE_ET    => self::NAME_ET,
        self::CODE_JW    => self::NAME_JW,
        self::CODE_JA    => self::NAME_JA,
    ];

    /**
     * @var int
     */
    protected $_resumeId;

    /**
     * @var string
     */
    protected $_languageCode;

    /**
     * @var string
     */
    protected $_level;

    public function __construct(int $resumeId, string $languageCode, string $level)
    {
        $this->_resumeId     = $resumeId;
        $this->_languageCode = $languageCode;
        $this->_level        = $level;
    }

    public function getResumeId(): int
    {
        return $this->_resumeId;
    }

    public function getLanguageCode(): string
    {
        return $this->_languageCode;
    }

    public function getLevel(): string
    {
        return $this->_level;
    }

    public function setLanguageCode(int $languageCode): self
    {
        if (!array_key_exists($languageCode, self::LANGUAGES)) {
            throw new \Exception('Invalid language code given');
        }
        $this->_languageCode = $languageCode;
        return $this;
    }

    public function setLevel(string $level): self
    {
        if (!in_array($level, self::LEVELS)) {
            throw new \Exception('Invalid profiency level value given');
        }

        $this->_level = $level;
        return $this;
    }
}