<?php

namespace Application\Model\Domain;

use Library\Master\Date\DateTime;

class ConversationMessage extends AbstractModel
{
    /**
     * @var string
     */
    protected $_text;

    /**
     * @var int
     */
    protected $_conversationId;

    /**
     * @var int
     */
    protected $_fromUserId;

    /**
     * @var null | DateTime
     */
    protected $_createDateTime = null;

    public function __construct(int $conversationId, int $fromUserId, string $text)
    {
        $this->_conversationId = $conversationId;
        $this->_fromUserId     = $fromUserId;
        $this->_text           = $text;
    }

    public function getText(): string
    {
        return $this->_text;
    }

    public function getConversationId(): int
    {
        return $this->_conversationId;
    }

    public function getFromUserId(): int
    {
        return $this->_fromUserId;
    }

    public function getCreateDateTime(): ?DateTime
    {
        return $this->_createDateTime;
    }

    public function setCreateDateTime(?DateTime $createDateTime): self
    {
        $this->_createDateTime = $createDateTime;
        return $this;
    }


}