<?php

namespace Application\Model\Domain;


class Currency extends AbstractModel
{
    const RUB_ID = 1;

    /**
     * @var string
     */
    protected $_isoCode;

    /**
     * @var string
     */
    protected $_name;

    public function __construct(string $isoCode, string $name)
    {
        $this->_isoCode = $isoCode;
        $this->_name    = $name;
    }

    public function getIsoCode(): string
    {
        return $this->_isoCode;
    }

    public function getName(): string
    {
        return $this->_name;
    }
}