<?php
namespace Application\Model\Domain\ConversationMessage;

use Application\Model\Domain\ConversationMessage;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Interfaces\DomainModelInterface;
use Library\Master\Date\DateTime;

/**
 * @method ConversationMessage         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method ConversationMessage[]       fetchByIds(array $ids)
 * @method ConversationMessage[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method ConversationMessage | null  fetchOne(array $where)
 */
class ConversationMessageMapper extends AbstractMapper
{
    const COL_TEXT            = 'text';
    const COL_CONVERSATION_ID = 'conversation_id';
    const COL_FROM_USER_ID    = 'from_user_id';
    const COL_CREATE_DATE     = 'create_date';

    const COLS = [
        self::PRIMARY,
        self::COL_TEXT,
        self::COL_CONVERSATION_ID,
        self::COL_FROM_USER_ID,
        self::COL_CREATE_DATE
    ];

    const COLS_NOT_UPDATE = [
        self::COL_CREATE_DATE
    ];

    public function save(DomainModelInterface & $obj, array $cols = [], bool $useCache = false)
    {
        parent::save($obj, $cols, $useCache);
    }

    /**
     * @param DomainModelInterface | ConversationMessage $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_TEXT            => $obj->getText(),
            self::COL_CONVERSATION_ID => $obj->getConversationId(),
            self::COL_FROM_USER_ID    => $obj->getFromUserId()
        ];
    }

    /**
     * @param array $row
     * @return DomainModelInterface | ConversationMessage
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new ConversationMessage(
            $row[self::COL_CONVERSATION_ID],
            $row[self::COL_FROM_USER_ID],
            $row[self::COL_TEXT]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | ConversationMessage $obj
     * @return DomainModelInterface | ConversationMessage
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        return $obj->setCreateDateTime(DateTime::createFromMysqlFormat($row[self::COL_CREATE_DATE]));
    }
}
