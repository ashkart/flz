<?php
namespace Application\Model\Domain\ConversationMessage;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class ConversationMessageMapperEvents extends MapperEvents
{
}
