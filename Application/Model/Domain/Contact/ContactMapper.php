<?php
namespace Application\Model\Domain\Contact;

use Application\Model\Domain\Contact;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method Contact | null  fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Contact[]       fetchByIds(array $ids)
 * @method Contact[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false) : array
 * @method Contact | null  fetchOne(array $where)
 */
class ContactMapper extends AbstractMapper
{
    const COL_ENTITY_ID   = 'entity_id';
    const COL_ENTITY_TYPE = 'entity_type';
    const COL_TYPE        = 'type';
    const COL_VALUE       = 'value';

    const COLS = [
        self::PRIMARY,
        self::COL_ENTITY_ID,
        self::COL_ENTITY_TYPE,
        self::COL_TYPE,
        self::COL_VALUE,
    ];

    const COLS_NOT_UPDATE = [
        self::COL_ENTITY_ID,
        self::COL_ENTITY_TYPE
    ];

    /**
     * @param DomainModelInterface | Contact $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_ENTITY_ID => $obj->getEntityId(),
            self::COL_ENTITY_TYPE => $obj->getEntityType(),
            self::COL_TYPE => $obj->getType(),
            self::COL_VALUE => $obj->getValue(),
        ];
    }

    /**
     * @param array $row
     * @return DomainModelInterface | Contact
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Contact(
            $row[self::COL_ENTITY_ID],
            $row[self::COL_ENTITY_TYPE],
            $row[self::COL_TYPE],
            $row[self::COL_VALUE]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }
}
