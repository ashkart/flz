<?php
namespace Application\Model\Domain\Contact;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class ContactMapperEvents extends MapperEvents
{
}
