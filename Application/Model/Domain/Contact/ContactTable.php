<?php
/**
 * This file was generated by Atlas. Changes will be overwritten.
 */
namespace Application\Model\Domain\Contact;

use Atlas\Orm\Table\AbstractTable;

/**
 * @inheritdoc
 */
class ContactTable extends AbstractTable
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function getColNames()
    {
        return [
            'id',
            'entity_id',
            'entity_type',
            'type',
            'value',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCols()
    {
        return [
            'id' => (object) [
                'name' => 'id',
                'type' => 'int',
                'size' => 11,
                'scale' => null,
                'notnull' => true,
                'default' => null,
                'autoinc' => true,
                'primary' => true,
            ],
            'entity_id' => (object) [
                'name' => 'entity_id',
                'type' => 'int',
                'size' => 11,
                'scale' => null,
                'notnull' => false,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
            'entity_type' => (object) [
                'name' => 'entity_type',
                'type' => 'enum',
                'size' => 0,
                'scale' => 0,
                'notnull' => true,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
            'type' => (object) [
                'name' => 'type',
                'type' => 'varchar',
                'size' => 50,
                'scale' => null,
                'notnull' => true,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
            'value' => (object) [
                'name' => 'value',
                'type' => 'varchar',
                'size' => 512,
                'scale' => null,
                'notnull' => false,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryKey()
    {
        return [
            'id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAutoinc()
    {
        return 'id';
    }

    /**
     * @inheritdoc
     */
    public function getColDefaults()
    {
        return [
            'id' => null,
            'entity_id' => null,
            'entity_type' => null,
            'type' => null,
            'value' => null,
        ];
    }
}
