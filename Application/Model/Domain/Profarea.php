<?php

namespace Application\Model\Domain;

use Application\Model\Domain\Collection\DomainModelCollection;

class Profarea extends AbstractModel
{
    /**
     * @var null | int
     */
    protected $_parentId = null;

    /**
     * @var string
     */
    protected $_title;

    /**
     * @var DomainModelCollection
     */
    protected $_children;

    /**
     * Profarea constructor.
     * @param string $_title
     */
    public function __construct(string $_title)
    {
        $this->_title = $_title;
    }

    public function hasParentId() : bool
    {
        return $this->_parentId !== null;
    }
    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->_parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId(?int $parentId): self
    {
        $this->_parentId = $parentId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->_title;
    }

    public function getChildren(): DomainModelCollection
    {
        return $this->_children;
    }

    public function setChildren(DomainModelCollection $children): self
    {
        $this->_children = $children;
        return $this;
    }
}