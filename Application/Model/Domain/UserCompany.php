<?php

namespace Application\Model\Domain;

class UserCompany extends AbstractModel
{
    /**
     * @var int
     */
    protected $_userId;

    /**
     * @var int
     */
    protected $_companyId;

    public function __construct(int $userId, int $companyId)
    {
        $this->_userId    = $userId;
        $this->_companyId = $companyId;
    }

    public function getUserId(): int
    {
        return $this->_userId;
    }

    public function getCompanyId(): int
    {
        return $this->_companyId;
    }

    public function getUser() : User
    {
        return self::$ds->userMapper()->fetch($this->_userId);
    }

    public function getCompany() : Company
    {
        return self::$ds->companyMapper()->fetch($this->_companyId);
    }
}