<?php

namespace Application\Model\Domain\VacancyLocation;

use Application\Cache\Slot\Slot;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\VacancyLocation;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method VacancyLocation         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method VacancyLocation[]       fetchByIds(array $ids, bool $useCache = true, bool $withRelated = false)
 * @method VacancyLocation | null  fetchOne(array $where)
 * @method VacancyLocation[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 */
class VacancyLocationMapper extends AbstractMapper
{
    const COL_VACANCY_ID = 'vacancy_id';
    const COL_COUNTRY_ID = 'country_id';
    const COL_CITY_ID    = 'city_id';

    const COLS = [
        self::PRIMARY,
        self::COL_VACANCY_ID,
        self::COL_COUNTRY_ID,
        self::COL_CITY_ID,
    ];

    const COLS_NOT_UPDATE = [
        self::COL_VACANCY_ID
    ];

    /**
     * @param DomainModelInterface | VacancyLocation $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_VACANCY_ID => $obj->getVacancyId(),
            self::COL_COUNTRY_ID => $obj->getCountryId(),
            self::COL_CITY_ID    => $obj->getCityId(),
        ];
    }

    /**
     * @return DomainModelInterface | VacancyLocation $obj
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new VacancyLocation(
            $row[self::COL_VACANCY_ID],
            $row[self::COL_COUNTRY_ID],
            $row[self::COL_CITY_ID]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }

    protected function _getClearCacheDependencies(?DomainModelInterface $oldObj, ?DomainModelInterface $newObj): array
    {
        $dependencies = [];

        $dependencies[] = self::_createCacheDependency(
            $oldObj,
            $newObj,
            function (VacancyLocation $vacancyLocation) : ?string {
                return "{$vacancyLocation->getId()}_{$vacancyLocation->getCityId()}_{$vacancyLocation->getCountryId()}";
            },
            function (VacancyLocation $vacancyLocation) {
                return [
                    new Slot($vacancyLocation->getVacancyId(), get_class(self::$ds->vacancyMapper()))
                ];
            }
        );

        return $dependencies;
    }
}
