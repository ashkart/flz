<?php
/**
 * This file was generated by Atlas. Changes will be overwritten.
 */
namespace Application\Model\Domain\VacancyLocation;

use Atlas\Orm\Table\AbstractTable;

/**
 * @inheritdoc
 */
class VacancyLocationTable extends AbstractTable
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'vacancy_location';
    }

    /**
     * @inheritdoc
     */
    public function getColNames()
    {
        return [
            'id',
            'vacancy_id',
            'country_id',
            'city_id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCols()
    {
        return [
            'id' => (object) [
                'name' => 'id',
                'type' => 'int',
                'size' => 11,
                'scale' => null,
                'notnull' => true,
                'default' => null,
                'autoinc' => true,
                'primary' => true,
            ],
            'vacancy_id' => (object) [
                'name' => 'vacancy_id',
                'type' => 'int',
                'size' => 11,
                'scale' => null,
                'notnull' => true,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
            'country_id' => (object) [
                'name' => 'country_id',
                'type' => 'int',
                'size' => 11,
                'scale' => null,
                'notnull' => false,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
            'city_id' => (object) [
                'name' => 'city_id',
                'type' => 'int',
                'size' => 11,
                'scale' => null,
                'notnull' => false,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryKey()
    {
        return [
            'id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAutoinc()
    {
        return 'id';
    }

    /**
     * @inheritdoc
     */
    public function getColDefaults()
    {
        return [
            'id' => null,
            'vacancy_id' => null,
            'country_id' => null,
            'city_id' => null,
        ];
    }
}
