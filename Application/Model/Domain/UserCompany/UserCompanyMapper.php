<?php
namespace Application\Model\Domain\UserCompany;

use Application\Cache\Slot\UserCompanies;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Domain\Company;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\User;
use Application\Model\Domain\UserCompany;

/**
 * @method UserCompany | null  fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method UserCompany[]       fetchByIds(array $ids)
 * @method UserCompany[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method UserCompany | null  fetchOne(array $where)
 */
class UserCompanyMapper extends AbstractMapper
{
    const COL_USER_ID = 'user_id';
    const COL_COMPANY_ID = 'company_id';

    const COLS = [
        self::PRIMARY,
        self::COL_USER_ID,
        self::COL_COMPANY_ID
    ];

    /**
     * @return UserCompany[]
     */
    public function fetchByUserId(int $userId, bool $useCache = true) : array
    {
        if ($useCache) {
            $slot = new UserCompanies($userId);

            $cachedRes = $slot->load();

            if ($cachedRes) {
                return $cachedRes;
            }
        }

        $companies = $this->fetchAll([self::COL_USER_ID => $userId]);

        if ($useCache) {
            $slot->save($companies);
        }

        return $companies;
    }

    /**
     * @param DomainModelInterface | UserCompany $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_COMPANY_ID => $obj->getCompanyId(),
            self::COL_USER_ID => $obj->getUserId()
        ];
    }

    /**
     * @param array $row
     * @return DomainModelInterface | UserCompany
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new UserCompany($row[self::COL_USER_ID], $row[self::COL_COMPANY_ID]))
            ->setId($row[self::PRIMARY])
        ;
    }

    protected function _getClearCacheDependencies(?DomainModelInterface $oldObj, ?DomainModelInterface $newObj): array
    {
        $dependencies = [];

        $dependencies[] = self::_createCacheDependency(
            $oldObj,
            $newObj,
            function (UserCompany $userCompany) : array {
                $user = $userCompany->getUser();

                $companyIds = [];

                /** @var Company $company */
                foreach ($user->getCompanies()->asArray() as  $company) {
                    $companyIds[] = $company->getId();
                }

                return $companyIds;
            },
            function (UserCompany $userCompany) {
                return [
                    new UserCompanies($userCompany->getUserId())
                ];
            }
        );

        return $dependencies;
    }
}
