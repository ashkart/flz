<?php

namespace Application\Model\Domain;

class Region extends AbstractModel
{
    /**
     * @var int
     */
    protected $_countryId;

    /**
     * @var string
     */
    protected $_title;

    public function __construct(int $countryId, string $title)
    {
        $this->_countryId = $countryId;
        $this->_title     = $title;
    }

    public function getCountryId(): int
    {
        return $this->_countryId;
    }

    public function getCountry() : Country
    {
        return self::$ds->countryMapper()->fetch($this->_countryId);
    }

    public function getTitle(): string
    {
        return $this->_title;
    }

    public function setTitle(string $title): self
    {
        $this->_title = $title;
        return $this;
    }
}