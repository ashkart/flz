<?php

namespace Application\Model\Domain;

class City extends AbstractModel
{
    const ID_MOSCOW = 1;

    /**
     * @var int
     */
    protected $_countryId;

    /**
     * @var int | null
     */
    protected $_regionId = null;

    /**
     * @var bool
     */
    protected $_isImportant = false;

    /**
     * @var string | null
     */
    protected $_titleEn = null;

    /**
     * @var string | null
     */
    protected $_titleRu = null;

    /**
     * @var string | null
     */
    protected $_area = null;

    public function __construct(int $countryId, ?int $regionId)
    {
        $this->_countryId   = $countryId;
        $this->_regionId    = $regionId;
    }

    public function getCountryId(): int
    {
        return $this->_countryId;
    }

    public function getRegionId(): ?int
    {
        return $this->_regionId;
    }

    public function getTitleEn(): ?string
    {
        return $this->_titleEn;
    }

    public function getArea(): ?string
    {
        return $this->_area;
    }

    public function setTitleEn(?string $title): self
    {
        $this->_titleEn = $title;
        return $this;
    }

    public function setArea(?string $area): self
    {
        $this->_area = $area;
        return $this;
    }

    public function getRegion() : Region
    {
        return self::$ds->regionMapper()->fetch($this->_regionId);
    }

    public function getCountry() : Country
    {
        return self::$ds->countryMapper()->fetch($this->_countryId);
    }

    public function isImportant(): bool
    {
        return $this->_isImportant;
    }

    public function setIsImportant(bool $isImportant): self
    {
        $this->_isImportant = $isImportant;
        return $this;
    }

    public function getTitleRu(): ?string
    {
        return $this->_titleRu;
    }

    public function setTitleRu(?string $titleRu): self
    {
        $this->_titleRu = $titleRu;
        return $this;
    }
}