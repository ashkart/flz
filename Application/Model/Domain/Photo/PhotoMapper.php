<?php
namespace Application\Model\Domain\Photo;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Photo;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method Photo         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Photo[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Photo[]       fetchByIds(array $ids)
 * @method Photo | null  fetchOne(array $where)
 */
class PhotoMapper extends AbstractMapper
{
    const COL_RESUME_ID = 'resume_id';
    const COL_PATH      = 'path';

    const COLS = [
        self::PRIMARY,
        self::COL_RESUME_ID,
        self::COL_PATH
    ];

    /**
     * @param DomainModelInterface | Photo $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_RESUME_ID => $obj->getResumeId(),
            self::COL_PATH => $obj->getPath()
        ];
    }

    /**
     * @return DomainModelInterface | Photo
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Photo($row[self::COL_RESUME_ID], self::COL_PATH))->setId($row[self::PRIMARY]);
    }
}
