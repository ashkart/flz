<?php

namespace Application\Model\Domain\Profarea;

use Application\Cache\Slot\Profarea as ProfareaSlot;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\Profarea;
use Atlas\Orm\Table\Row;

/**
 * @method Profarea         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Profarea[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Profarea[]       fetchByIds(array $ids)
 * @method Profarea | null  fetchOne(array $where)
 */
class ProfareaMapper extends AbstractMapper
{
    const COL_PARENT_ID = 'parent_id';
    const COL_TITLE     = 'title';

    const COLS = [
        self::PRIMARY,
        self::COL_PARENT_ID,
        self::COL_TITLE
    ];

    const CHILDREN_COLLECTION = 'children';

    const COLLECTIONS = [
        self::CHILDREN_COLLECTION
    ];

    /**
     * @return Profarea[]
     */
    public function fetchRootWithCollections(bool $useCache = true) : array
    {
        if ($useCache) {
            $slot = new ProfareaSlot();
            $cachedResult = $slot->load();

            if ($cachedResult) {
                return $cachedResult;
            }
        }

        $recordSet = $this->fetchRecordSetBy([self::COL_PARENT_ID => null], self::COLLECTIONS);

        $models = [];

        foreach ($recordSet as $record) {
            $models[] = $this->createModel($record);
        }

        if ($useCache) {
            $slot = new ProfareaSlot();

            $slot->save($models);
        }

        return $models;
    }

    /**
     * @param DomainModelInterface | Profarea $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_PARENT_ID => $obj->getParentId(),
            self::COL_TITLE => $obj->getTitle()
        ];
    }

    /**
     * @param array $row
     * @return DomainModelInterface | Profarea
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Profarea($row[self::COL_TITLE]))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | Profarea $obj
     * @return DomainModelInterface | Profarea
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        $children = $row[self::CHILDREN_COLLECTION] ?? [];

        $childs = [];

        foreach ($children as $child) {
            $childs[] = $this->createModel($this->turnRowIntoRecord(new Row($child)));
        }

        $childrenCollection = new DomainModelCollection($childs);

        return $obj
            ->setParentId($row[self::COL_PARENT_ID])
            ->setChildren($childrenCollection)
        ;
    }

    protected function setRelated()
    {
        $this->oneToMany(self::CHILDREN_COLLECTION, ProfareaMapper::class)
            ->on([self::PRIMARY => self::COL_PARENT_ID])
        ;
    }
}
