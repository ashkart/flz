<?php
namespace Application\Model\Domain\Country;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Country;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method Country         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Country[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Country[]       fetchByIds(array $ids)
 * @method Country | null  fetchOne(array $where)
 */
class CountryMapper extends AbstractMapper
{
    const COL_TITLE_RU = 'title_ru';
    const COL_TITLE_EN = 'title_en';
    const COL_ISO2     = 'iso2';

    const COLS = [
        self::PRIMARY,
        self::COL_TITLE_RU,
        self::COL_TITLE_EN,
        self::COL_ISO2
    ];

    /**
     * @param DomainModelInterface | Country $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_TITLE_RU => $obj->getTitleRu(),
            self::COL_TITLE_EN => $obj->getTitleEn(),
            self::COL_ISO2     => $obj->getIso()
        ];
    }

    /**
     * @return DomainModelInterface | Country
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Country($row[self::COL_TITLE_RU], $row[self::COL_TITLE_EN]))
            ->setIso($row[self::COL_ISO2])
            ->setId($row[self::PRIMARY])
        ;
    }
}
