<?php
namespace Application\Model\Domain\EntityTag;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\EntityTag;
use Application\Model\Domain\Tag\TagMapper;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method EntityTag         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method EntityTag[]       fetchByIds(array $ids)
 * @method EntityTag[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method EntityTag | null  fetchOne(array $where)
 */
class EntityTagMapper extends AbstractMapper
{
    const COL_TAG_ID      = 'tag_id';
    const COL_ENTITY_ID   = 'entity_id';
    const COL_ENTITY_TYPE = 'entity_type';

    const COLS = [
        self::PRIMARY,
        self::COL_TAG_ID,
        self::COL_ENTITY_ID,
        self::COL_ENTITY_TYPE
    ];

    const RELATED_TAG = 'tag';

    const COLLECTIONS = [
        self::RELATED_TAG
    ];

    /**
     * @param DomainModelInterface | EntityTag $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_ENTITY_TYPE => $obj->getEntityType(),
            self::COL_ENTITY_ID   => $obj->getEntityId(),
            self::COL_TAG_ID      => $obj->getTagId()
        ];
    }

    /**
     * @return DomainModelInterface | EntityTag
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new EntityTag(
            $row[self::COL_TAG_ID],
            $row[self::COL_ENTITY_ID],
            $row[self::COL_ENTITY_TYPE]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | EntityTag $obj
     * @return DomainModelInterface | EntityTag
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        if ($row[self::RELATED_TAG] !== null) {
            $obj->setTagName($row[self::RELATED_TAG][TagMapper::COL_NAME]);
        }

        return $obj;
    }

    protected function setRelated()
    {
        $this
            ->oneToOne(self::RELATED_TAG, TagMapper::class)
            ->on([
                self::COL_TAG_ID => TagMapper::PRIMARY
            ])
        ;
    }
}
