<?php
namespace Application\Model\Domain\EntityTag;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class EntityTagMapperEvents extends MapperEvents
{
}
