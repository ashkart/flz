<?php

namespace Application\Model\Domain;

use Library\Master\Date\Date;
use Library\Master\Date\DateRange;

class Education extends AbstractModel
{
    /**
     * @var int
     */
    protected $_resumeId;
    
    /**
     * @var string
     */
    protected $_specialty;
    
    /**
     * @var string
     */
    protected $_educationalOrganization;
    
    /**
     * @var DateRange
     */
    protected $_period;

    public function __construct(
        int $resumeId, 
        string $specialty, 
        string $educationalOrganization, 
        DateRange $period
    )
    {
        $this->_resumeId                = $resumeId;
        $this->_specialty               = $specialty;
        $this->_educationalOrganization = $educationalOrganization;
        $this->_period                  = $period;
    }

    public function getResumeId(): int
    {
        return $this->_resumeId;
    }

    public function getResume() : Resume
    {
        return self::$ds->resumeMapper()->fetch($this->getResumeId());
    }

    public function getSpecialty(): string
    {
        return $this->_specialty;
    }

    public function setSpecialty(string $specialty): self
    {
        $this->_specialty = $specialty;
        return $this;
    }

    public function getEducationalOrganization(): string
    {
        return $this->_educationalOrganization;
    }

    public function setEducationalOrganization(string $educationalOrganization): self
    {
        $this->_educationalOrganization = $educationalOrganization;
        return $this;
    }

    public function getPeriod(): DateRange
    {
        return $this->_period;
    }

    public function setPeriod(DateRange $period): self
    {
        $this->_period = $period;
        return $this;
    }
}