<?php

namespace Application\Model\Domain;

use Application\Model\Interfaces\EntityRelationInterface;
use Library\Master\Date\DateTime;

class View extends AbstractModel implements EntityRelationInterface
{
    /**
     * @var int
     */
    protected $_entityId;

    /**
     * @var string
     */
    protected $_entityType;

    /**
     * @var int | null
     */
    protected $_userId = null;

    /**
     * @var int | null
     */
    protected $_companyId = null;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * View constructor.
     * @param int $_entityId
     * @param string $_entityType
     * @param int|null $_userId
     * @param int|null $_companyId
     * @param DateTime|null $_dateTime
     */
    public function __construct(
        int $entityId,
        string $entityType,
        ?int $userId,
        int $companyId = null,
        DateTime $dateTime = null
    )
    {
        if (!in_array($entityType, self::ENTITY_TYPES)) {
            throw new \Exception('Invalid entity type given');
        }

        $this->_entityId   = $entityId;
        $this->_entityType = $entityType;
        $this->_userId     = $userId;
        $this->_companyId  = $companyId;
        $this->_dateTime   = $dateTime ?? new DateTime();
    }

    public function getEntityId(): int
    {
        return $this->_entityId;
    }

    public function getEntityType(): string
    {
        return $this->_entityType;
    }

    public function getUserId(): ?int
    {
        return $this->_userId;
    }

    public function getCompanyId(): ?int
    {
        return $this->_companyId;
    }

    public function getDateTime(): DateTime
    {
        return $this->_dateTime;
    }
}