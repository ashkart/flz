<?php

namespace Application\Model\Domain;

class Photo extends AbstractModel
{
    /**
     * @var int
     */
    protected $_resumeId;

    /**
     * @var string
     */
    protected $_path;

    public function __construct(int $resumeId, string $path)
    {
        $this->_resumeId = $resumeId;
        $this->_path     = $path;
    }

    public function getResumeId(): int
    {
        return $this->_resumeId;
    }

    public function getPath(): string
    {
        return $this->_path;
    }
}