<?php

namespace Application\Model\Domain;

use Application\Helper\DataSource\TraitDataSource;
use Application\Model\Interfaces\DomainModelInterface;
use Atlas\Orm\Mapper;

abstract class AbstractModel implements DomainModelInterface
{
    use TraitDataSource;

    /**
     * @var int
     */
    protected $_id = null;

    /**
     * @var array
     */
    private $_tableRow = [];

    public function getId() : ? int
    {
        return $this->_id;
    }

    public function setId(? int $id) : self
    {
        $this->_id = $id;
        return $this;
    }

    public function getTableRow(bool $unquoteBooleans = false, array $cols = null): array
    {
        static $unquotedBoolsRow = null;

        $row = $this->_tableRow;

        if ($cols) {
            $row = array_intersect_key($row, array_flip($cols));
        }

        if ($unquoteBooleans) {
            if ($unquotedBoolsRow === null) {
                $unquotedBoolsRow = [];

                foreach ($row as $colName => $value) {
                    $val = $value;

                    if (in_array($value, ["0", "1"], true)) {
                        $val = (int) $value;
                    }

                    $unquotedBoolsRow[$colName] = $val;
                }
            }

            return $unquotedBoolsRow;
        }

        return $row;
    }

    public function setTableRow(array $record): self
    {
        $this->_tableRow = $record;
        return $this;
    }
}