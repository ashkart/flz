<?php

namespace Application\Model\Domain;

class Country extends AbstractModel
{
    /**
     * @var string
     */
    protected $_titleRu;

    /**
     * @var string
     */
    protected $_titleEn;

    /**
     * @var string
     */
    protected $_iso;

    public function __construct(string $titleRu, string $titleEn)
    {
        $this->_titleRu = $titleRu;
        $this->_titleEn = $titleEn;
    }

    public function getTitleRu(): string
    {
        return $this->_titleRu;
    }

    public function getTitleEn(): string
    {
        return $this->_titleEn;
    }

    public function getIso(): string
    {
        return $this->_iso;
    }

    public function setIso(string $iso): self
    {
        $this->_iso = $iso;
        return $this;
    }
}