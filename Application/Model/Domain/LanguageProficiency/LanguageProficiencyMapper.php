<?php
namespace Application\Model\Domain\LanguageProficiency;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\LanguageProficiency;
use Application\Model\Domain\Resume\ResumeMapper;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method LanguageProficiency         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method LanguageProficiency[]       fetchByIds(array $ids)
 * @method LanguageProficiency[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method LanguageProficiency | null  fetchOne(array $where)
 */
class LanguageProficiencyMapper extends AbstractMapper
{
    const COL_RESUME_ID     = 'resume_id';
    const COL_LANGUAGE_CODE = 'language_code';
    const COL_LEVEL         = 'level';

    const COLS = [
        self::PRIMARY,
        self::COL_RESUME_ID,
        self::COL_LANGUAGE_CODE,
        self::COL_LEVEL
    ];

    const COLS_NOT_UPDATE = [
        self::COL_RESUME_ID
    ];

    const RELATION_TO_RESUME = 'resume';

    /**
     * @param DomainModelInterface | LanguageProficiency $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_RESUME_ID => $obj->getResumeId(),
            self::COL_LANGUAGE_CODE => $obj->getLanguageCode(),
            self::COL_LEVEL => $obj->getLevel()
        ];
    }

    /**
     * @param array $row
     * @return DomainModelInterface | LanguageProficiency
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new LanguageProficiency(
            $row[self::COL_RESUME_ID],
            $row[self::COL_LANGUAGE_CODE],
            $row[self::COL_LEVEL]
        ))
            ->setId($row[self::PRIMARY])
            ;
    }

    protected function setRelated()
    {
        $this->manyToOne(self::RELATION_TO_RESUME, ResumeMapper::class);
    }
}
