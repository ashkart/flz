<?php

namespace Application\Model\Domain;

use Couchbase\Exception;

class VacancyLocation extends AbstractModel
{
    /**
     * @var int
     */
    protected $_vacancyId;

    /**
     * @var int | null
     */
    protected $_countryId = null;

    /**
     * @var int | null
     */
    protected $_cityId = null;

    public function __construct(int $vacancyId, ?int $countryId, ?int $cityId)
    {
        if (!$countryId && !$cityId) {
            throw new \Exception('Both countryId and cityId cannot be null');
        }

        $this->_vacancyId = $vacancyId;
        $this->_countryId = $countryId;
        $this->_cityId    = $cityId;
    }

    public function getVacancyId(): int
    {
        return $this->_vacancyId;
    }

    public function getCountryId(): ?int
    {
        return $this->_countryId;
    }

    public function getCityId(): ?int
    {
        return $this->_cityId;
    }

    public function getCity(): ?City
    {
        return self::$ds->cityMapper()->fetch($this->_cityId);
    }

    public function getCountry(): ?Country
    {
        return self::$ds->countryMapper()->fetch($this->_countryId);
    }

    public function setCountryId(?int $countryId): self
    {
        $this->_countryId = $countryId;
        return $this;
    }

    public function setCityId(?int $cityId): self
    {
        $this->_cityId = $cityId;
        return $this;
    }
}