<?php

namespace Application\Model\Domain;

use Library\Master\Date\Date;
use Library\Master\Date\DateRange;

class WorkExperience extends AbstractModel
{
    /**
     * @var int
     */
    protected $_resumeId;
    /**
     * @var string
     */
    protected $_positionName;
    /**
     * @var string
     */
    protected $_description;
    /**
     * @var DateRange
     */
    protected $_period;

    /**
     * WorkExperience constructor.
     * @param int $_resumeId
     * @param string $_positionName
     * @param string $_description
     * @param Date $_dateStart
     * @param Date $_dateEnd
     */
    public function __construct(int $resumeId, string $positionName, string $description, DateRange $period)
    {
        $this->_resumeId     = $resumeId;
        $this->_positionName = $positionName;
        $this->_description  = $description;
        $this->_period       = $period;
    }

    public function getResumeId(): int
    {
        return $this->_resumeId;
    }

    public function getPositionName(): string
    {
        return $this->_positionName;
    }

    public function setPositionName(string $positionName): self
    {
        $this->_positionName = $positionName;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->_description;
    }

    public function setDescription(string $description): self
    {
        $this->_description = $description;
        return $this;
    }

    public function getPeriod(): DateRange
    {
        return $this->_period;
    }

    public function setPeriod(DateRange $period): self
    {
        $this->_period = $period;
        return $this;
    }
}