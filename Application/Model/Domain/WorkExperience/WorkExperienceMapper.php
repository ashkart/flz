<?php

namespace Application\Model\Domain\WorkExperience;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\WorkExperience;
use Library\Master\Date\Date;
use Library\Master\Date\DateRange;

/**
 * @method WorkExperience         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method WorkExperience[]       fetchByIds(array $ids)
 * @method WorkExperience[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method WorkExperience | null  fetchOne(array $where)
 */
class WorkExperienceMapper extends AbstractMapper
{
    const COL_RESUME_ID     = 'resume_id';
    const COL_POSITION_NAME = 'position_name';
    const COL_DESCRIPTION   = 'description';
    const COL_DATE_START    = 'date_start';
    const COL_DATE_END      = 'date_end';

    const COLS = [
        self::PRIMARY,
        self::COL_RESUME_ID,
        self::COL_POSITION_NAME,
        self::COL_DESCRIPTION,
        self::COL_DATE_START,
        self::COL_DATE_END,
    ];

    /**
     * @param DomainModelInterface | WorkExperience $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        $dateStart = $obj->getPeriod()->getDateStart();
        $dateEnd = $obj->getPeriod()->getDateEnd();

        return [
            self::COL_RESUME_ID     => $obj->getResumeId(),
            self::COL_POSITION_NAME => $obj->getPositionName(),
            self::COL_DESCRIPTION   => $obj->getDescription(),
            self::COL_DATE_START    => $dateStart->mysqlFormat(),
            self::COL_DATE_END      => $dateEnd ? $dateEnd->mysqlFormat() : null,
        ];
    }

    /**
     * @return DomainModelInterface | WorkExperience
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        $dateStart = Date::createFromNormalFormats($row[self::COL_DATE_START]);
        $dateEnd   = $row[self::COL_DATE_END] ? Date::createFromNormalFormats($row[self::COL_DATE_END]) : null;

        return (new WorkExperience(
            $row[self::COL_RESUME_ID],
            $row[self::COL_POSITION_NAME],
            $row[self::COL_DESCRIPTION],
            new DateRange($dateStart, $dateEnd)
        ))
            ->setId($row[self::PRIMARY])
        ;
    }
}
