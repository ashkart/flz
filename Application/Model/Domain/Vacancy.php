<?php

namespace Application\Model\Domain;


use Application\Model\Domain\Collection\DomainModelCollection;
use Library\Master\Date\DateTime;

class Vacancy extends AbstractModel
{
    const EXP_NOT_REQUIRED = '0';
    const EXP_LTE_1_YEAR   = '1';
    const EXP_LTE_2_YEARS  = '2';
    const EXP_LTE_3_YEARS  = '3';
    const EXP_LTE_5_YEARS  = '5';
    const EXP_GT_5_YEARS   = '6';

    const EXP_VALUES = [
        self::EXP_NOT_REQUIRED,
        self::EXP_LTE_1_YEAR,
        self::EXP_LTE_2_YEARS,
        self::EXP_LTE_3_YEARS,
        self::EXP_LTE_5_YEARS,
        self::EXP_GT_5_YEARS,
    ];

    const EMPLOYMENT_REGULAR   = 'full_day';
    const EMPLOYMENT_PART_TIME = 'part_time';
    const EMPLOYMENT_SHIFT     = 'shift';
    const EMPLOYMENT_WATCH     = 'watch';

    const EMPLOYMENTS = [
        self::EMPLOYMENT_REGULAR,
        self::EMPLOYMENT_PART_TIME,
        self::EMPLOYMENT_SHIFT,
        self::EMPLOYMENT_WATCH,
    ];

    const VISIBILITY_ALL  = 'all';
    const VISIBILITY_NONE = 'none';

    const VISIBILITY_VALUES = [
        self::VISIBILITY_ALL,
        self::VISIBILITY_NONE,
    ];

    /**
     * @var int
     */
    protected $_userId;

    /**
     * @var int
     */
    protected $_companyId;

    /**
     * @var string
     */
    protected $_positionName;

    /**
     * @var int
     */
    protected $_profareaId;

    /**
     * @var string
     */
    protected $_requirements;

    /**
     * @var string
     */
    protected $_liabilities;

    /**
     * @var null | string
     */
    protected $_requiredExperience = self::EXP_NOT_REQUIRED;

    /**
     * @var string | null
     */
    protected $_conditions = null;

    /**
     * @var string | null
     */
    protected $_description = null;

    /**
     * @var null | string
     */
    protected $_employment = null;

    /**
     * @var bool
     */
    protected $_isRemote = false;

    /**
     * @var int | null
     */
    protected $_paycheckMin = null;

    /**
     * @var int | null
     */
    protected $_paycheckMax = null;

    /**
     * @var int | null
     */
    protected $_paycheckConst = null;

    /**
     * @var string
     */
    protected $_visibility = self::VISIBILITY_ALL;

    /**
     * @var bool
     */
    protected $_isDeleted = false;

    /**
     * @var DateTime | null
     */
    protected $_createDate = null;

    /**
     * @var DateTime | null
     */
    protected $_updateDate = null;

    /**
     * @var null | int
     */
    protected $_currencyId = null;

    /**
     * @var DomainModelCollection
     */
    protected $_callbacks = null;

    /**
     * @var null | DomainModelCollection
     */
    protected $_locations = null;

    /**
     * VacancyListItem constructor.
     * @param int $_userId
     * @param string $_positionName
     * @param string $requirements
     * @param string $liabilities
     * @param string $conditions
     */
    public function __construct(
        int    $userId,
        int    $companyId,
        string $positionName,
        string $requirements,
        string $liabilities
    )
    {
        $this->_userId       = $userId;
        $this->_companyId    = $companyId;
        $this->_positionName = $positionName;
        $this->_requirements = $requirements;
        $this->_liabilities  = $liabilities;
    }

    public function getUserId(): int
    {
        return $this->_userId;
    }

    public function getCompanyId(): int
    {
        return $this->_companyId;
    }

    public function setCompanyId(int $companyId): self
    {
        $this->_companyId = $companyId;
        return $this;
    }

    public function getCompany() : Company
    {
        return self::$ds->companyMapper()->fetch($this->_companyId);
    }

    public function getPositionName(): string
    {
        return $this->_positionName;
    }

    public function getRequirements(): string
    {
        return $this->_requirements;
    }

    public function getLiabilities(): string
    {
        return $this->_liabilities;
    }

    public function getConditions(): string
    {
        return $this->_conditions;
    }

    public function getDescription(): ? string
    {
        return $this->_description;
    }

    public function getPaycheckMin(): ? int
    {
        return $this->_paycheckMin;
    }

    public function getPaycheckMax(): ? int
    {
        return $this->_paycheckMax;
    }

    public function getPaycheckConst(): ? int
    {
        return $this->_paycheckConst;
    }

    public function setPositionName(string $positionName) : self
    {
        $this->_positionName = $positionName;
        return $this;
    }

    public function setRequirements(string $requirements) : self
    {
        $this->_requirements = $requirements;
        return $this;
    }

    public function setLiabilities(string $liabilities) : self
    {
        $this->_liabilities = $liabilities;
        return $this;
    }

    public function setConditions(string $conditions) : self
    {
        $this->_conditions = $conditions;
        return $this;
    }

    public function setDescription(?string $description) : self
    {
        $this->_description = $description;
        return $this;
    }

    public function setPaycheckMin(?int $paycheckMin) : self
    {
        $this->_paycheckMin = $paycheckMin;
        return $this;
    }

    public function setPaycheckMax(?int $paycheckMax) : self
    {
        $this->_paycheckMax = $paycheckMax;
        return $this;
    }

    public function setPaycheckConst(?int $paycheckConst) : self
    {
        $this->_paycheckConst = $paycheckConst;
        return $this;
    }

    public function getVisibility(): string
    {
        return $this->_visibility;
    }

    public function isVisible() : bool
    {
        return $this->_visibility === self::VISIBILITY_ALL;
    }

    public function isInvisible() : bool
    {
        return $this->_visibility === self::VISIBILITY_NONE;
    }

    public function setVisibility(string $visibility): self
    {
        if (!in_array($visibility, self::VISIBILITY_VALUES)) {
            throw new \Exception('Invalid visibility value.');
        }

        $this->_visibility = $visibility;
        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->_isDeleted;
    }

    public function setDeleted(bool $isDeleted): self
    {
        $this->_isDeleted = $isDeleted;
        return $this;
    }

    public function getCreateDate(): ?DateTime
    {
        return $this->_createDate;
    }

    public function setCreateDate(DateTime $createDate): self
    {
        $this->_createDate = $createDate;
        return $this;
    }

    public function getUpdateDate(): ?DateTime
    {
        return $this->_updateDate;
    }

    public function setUpdateDate(?DateTime $updateDate): self
    {
        $this->_updateDate = $updateDate;
        return $this;
    }

    public function getProfarea() : Profarea
    {
        return self::$ds->profareaMapper()->fetch($this->_profareaId);
    }

    public function getProfareaId(): int
    {
        return $this->_profareaId;
    }

    public function setProfareaId(int $profareaId): self
    {
        $this->_profareaId = $profareaId;
        return $this;
    }

    public function getRequiredExperience(): ?string
    {
        return $this->_requiredExperience;
    }

    public function setRequiredExperience(?string $requiredExperience): self
    {
        if ($requiredExperience && !in_array($requiredExperience, self::EXP_VALUES)) {
            throw new \Exception('Invalid required experience value');
        }

        $this->_requiredExperience = $requiredExperience;
        return $this;
    }

    public function getEmployment(): ?string
    {
        return $this->_employment;
    }

    public function setEmployment(?string $scheduleType): self
    {
        if ($scheduleType && !in_array($scheduleType, self::EMPLOYMENTS)) {
            throw new \Exception('Invalid employment value');
        }

        $this->_employment = $scheduleType;
        return $this;
    }

    public function isRemote(): bool
    {
        return $this->_isRemote;
    }

    public function setIsRemote(bool $isRemote): self
    {
        $this->_isRemote = $isRemote;
        return $this;
    }

    public function getCurrencyId() : ?int
    {
        return $this->_currencyId;
    }

    public function getCurrency() : ?Currency
    {
        if (!$this->_currencyId) {
            return null;
        }

        return self::$ds->currencyMapper()->fetch($this->_currencyId);
    }

    public function setCurrencyId(?int $currencyId): self
    {
        $this->_currencyId = $currencyId;
        return $this;
    }

    public function getCallbacks(): DomainModelCollection
    {
        if (!$this->_callbacks) {
            $callbacks = self::$ds->callbackMapper()->fetchByEntityId($this->_id, Callback::ENTITY_TYPE_VACANCY);
            $this->_callbacks = new DomainModelCollection($callbacks);
        }

        return $this->_callbacks;
    }

    public function setCallbacks(DomainModelCollection $callbacks): self
    {
        $this->_callbacks = $callbacks;
        return $this;
    }

    public function getLocations(): ? DomainModelCollection
    {
        return $this->_locations;
    }

    public function setLocations(DomainModelCollection $locations): self
    {
        $this->_locations = $locations;
        return $this;
    }
}