<?php
namespace Application\Model\Domain\ConversationUser;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\ConversationUser;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method ConversationUser         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method ConversationUser[]       fetchByIds(array $ids)
 * @method ConversationUser[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method ConversationUser | null  fetchOne(array $where)
 */
class ConversationUserMapper extends AbstractMapper
{
    const COL_USER_ID         = 'user_id';
    const COL_CONVERSATION_ID = 'conversation_id';

    const COLS = [
        self::PRIMARY,
        self::COL_CONVERSATION_ID,
        self::COL_USER_ID
    ];

    /**
     * @param DomainModelInterface | ConversationUser $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_CONVERSATION_ID => $obj->getConversationId(),
            self::COL_USER_ID => $obj->getUserId()
        ];
    }

    /**
     * @return DomainModelInterface | ConversationUser
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new ConversationUser(
            $row[self::COL_USER_ID],
            $row[self::COL_CONVERSATION_ID]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }
}
