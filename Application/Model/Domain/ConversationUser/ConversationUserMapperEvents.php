<?php
namespace Application\Model\Domain\ConversationUser;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class ConversationUserMapperEvents extends MapperEvents
{
}
