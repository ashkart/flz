<?php
namespace Application\Model\Domain\Education;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Education;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\Resume\ResumeMapper;
use Library\Master\Date\Date;
use Library\Master\Date\DateRange;

/**
 * @method Education         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Education[]       fetchByIds(array $ids)
 * @method Education[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Education | null  fetchOne(array $where)
 */
class EducationMapper extends AbstractMapper
{
    const COL_RESUME_ID                = 'resume_id';
    const COL_SPECIALITY               = 'speciality';
    const COL_EDUCATIONAL_ORGANIZATION = 'educational_organization';
    const COL_DATE_START               = 'date_start';
    const COL_DATE_END                 = 'date_end';

    const COLS = [
        self::PRIMARY,
        self::COL_RESUME_ID,
        self::COL_SPECIALITY,
        self::COL_EDUCATIONAL_ORGANIZATION,
        self::COL_DATE_START,
        self::COL_DATE_END,
    ];

    const RELATION_TO_RESUME = 'resume';

    /**
     * @param DomainModelInterface | Education $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        $dateEnd = $obj->getPeriod()->getDateEnd();

        return [
            self::COL_RESUME_ID                => $obj->getResumeId(),
            self::COL_SPECIALITY               => $obj->getSpecialty(),
            self::COL_EDUCATIONAL_ORGANIZATION => $obj->getEducationalOrganization(),
            self::COL_DATE_START               => $obj->getPeriod()->getDateStart()->mysqlFormat(),
            self::COL_DATE_END                 => $dateEnd ? $dateEnd->mysqlFormat() : null,
        ];
    }

    /**
     * @return DomainModelInterface | Education
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        $dateStart = Date::createFromNormalFormats($row[self::COL_DATE_START]);
        $dateEnd   = $row[self::COL_DATE_END] ? Date::createFromNormalFormats($row[self::COL_DATE_END]) : null;

        $period = new DateRange($dateStart, $dateEnd);

        return (new Education(
            $row[self::COL_RESUME_ID],
            $row[self::COL_SPECIALITY],
            $row[self::COL_EDUCATIONAL_ORGANIZATION],
            $period
        ))
            ->setId($row[self::PRIMARY])
        ;
    }

    protected function setRelated()
    {
        $this->manyToOne(self::RELATION_TO_RESUME, ResumeMapper::class);
    }
}
