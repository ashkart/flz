<?php

namespace Application\Model\Domain;

use Application\Helper\RouteHelper;
use Application\Helper\ServiceManager\Factory;
use Library\Master\Session\SessionManager;
use Main\View\Helper\Translator;
use Zend\Http\Header\Cookie;

class ClientData extends AbstractModel
{
    const COOKIE_SESSION_ID = 'staffz';
    const COOKIE_LOCALE     = 'locale';
    const COOKIE_ROLE       = 'role';
    const COOKIE_USERNAME   = 'username';
    const COOKIE_USER_ID    = 'userId';
    const COOKIE_CITY_ID    = 'cityId';
    const COOKIE_CP_URL     = 'cpURL';

    const TTL_HOUR = 3600;
    const TTL_DAY = self::TTL_HOUR * 24;
    const TTL_YEAR = self::TTL_DAY * 365;

    //@TODO

    /**
     * @var User
     */
    protected $_user;

    /**
     * @var string[]
     */
    protected $_messages = [];

    /**
     * То, что пришло в качестве идентификатора сессии с клиента
     *
     * @var string
     */
    protected $_sessionId;

    public function __construct()
    {
        $this->_sessionId = $_COOKIE[self::COOKIE_SESSION_ID];
    }

    public function set(User $user)
    {
        $this->_user = $user;

        setcookie(self::COOKIE_LOCALE, 'ru', 0, '/');
        setcookie(self::COOKIE_ROLE, $user->getRole(), 0, '/');
        setcookie(self::COOKIE_USERNAME, $user->getUserName(), 0, '/');
        setcookie(self::COOKIE_USER_ID, $user->getId(), 0, '/');

        if ($user->isAdmin()) {
            /** @var RouteHelper $rh */
            $rh = self::$sm->get(Factory::ROUTE_HELPER);
            setcookie(self::COOKIE_CP_URL, $rh->admin('index', 'index'), 0, '/');
        }
    }

    public function getLocale() :? string
    {
        return $_COOKIE[self::COOKIE_LOCALE];
    }

    public function getCityId() :? string
    {
        return $_COOKIE[self::COOKIE_CITY_ID];
    }

    public function setCookieCityId(int $cityId)
    {
        setcookie(self::COOKIE_CITY_ID, $cityId, time() + self::TTL_YEAR, '/');
    }

    public function addMessage(string $message)
    {
        $this->_messages[] = $message;
    }

    public function hasMessages() : bool
    {
        return !!$this->_messages;
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return $this->_messages;
    }

    public function getSessionId(): string
    {
        return $this->_sessionId;
    }
}