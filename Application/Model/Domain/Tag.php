<?php

namespace Application\Model\Domain;

class Tag extends AbstractModel
{
    /**
     * @var string
     */
    protected $_name;

    public function __construct(string $name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_name;
    }
}