<?php

namespace Application\Model\Domain\Collection;

use Application\Model\Domain\AbstractModel;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @property AbstractModel[] $_entities
 *
 * @method AbstractModel|null       last()
 * @method DomainModelInterface[]   asArray()
 */
class DomainModelCollection
{
    use TraitCollection;

    /**
     * @param AbstractModel[] $models
     */
    public function __construct(array $models = [])
    {
        foreach ($models as $model) {
            if (isset($prevClass) && !$model instanceof $prevClass) {
                throw new \Exception('Invalid typeset');
            }

            $prevClass = get_class($model);
        }

        $this->_entities = $models;
    }

    public function asArrayRecords() : array
    {
        $records = [];

        foreach ($this->_entities as $model) {
            $records[] = $model->getTableRow();
        }

        return $records;
    }

    public function isEmpty() : bool
    {
        return (bool) !$this->_entities;
    }

    public function getModelById(int $id) : ? AbstractModel
    {
        foreach ($this->_entities as $entity) {
            if ($id === $entity->getId()) {
                return $entity;
            }
        }

        return null;
    }

    /**
     * Вернет коллекцию из элементов, отличных при равном id,
     * и элементов, присутствующих в $this, но отсутствующих в $other.
     *
     * @param DomainModelCollection $otherCollection
     * @return DomainModelCollection
     * @throws \Exception
     */
    public function diff(DomainModelCollection $otherCollection) : DomainModelCollection
    {
        $this->_checkWithOtherCollection($otherCollection);

        if ($this->isEmpty() || $otherCollection->isEmpty()) {
            return $this;
        }

        /**
         * @var $entities      AbstractModel[]
         * @var $otherEntities AbstractModel[]
         */
        $entities      = array_values($this->_entities);
        $otherEntities = array_values($otherCollection->asArray());

        $diff = [];

        foreach ($entities as $key => $entity) {
            foreach ($otherEntities as $otherKey => $otherEntity) {
                if ($entity->getTableRow() == $otherEntity->getTableRow()) {
                    unset($entities[$key]);
                    unset($otherEntities[$otherKey]);
                    continue 2;
                }

                if (
                    $entity->getId() &&
                    $entity->getId() === $otherEntity->getId()
                ) {
                    $diff[] = $entity;

                    unset($entities[$key]);
                    unset($otherEntities[$otherKey]);
                    continue 2;
                }
            }

            $diff[] = $entity;
        }

        return new DomainModelCollection($diff);
    }

    /**
     * Вернет коллекцию из элементов, присутствующих в $this, но отсутствующих в $other.
     *
     * @param DomainModelCollection $otherCollection
     * @return DomainModelCollection
     * @throws \Exception
     */
    public function getAbsentIn(DomainModelCollection $otherCollection) : DomainModelCollection
    {
        $this->_checkWithOtherCollection($otherCollection);

        if ($this->isEmpty() || $otherCollection->isEmpty()) {
            return $this;
        }

        /**
         * @var $entities      AbstractModel[]
         * @var $otherEntities AbstractModel[]
         */
        $entities      = $this->_entities;
        $otherEntities = $otherCollection->asArray();

        $diff = [];

        foreach ($entities as $key => $entity) {
            foreach ($otherEntities as $otherKey => $otherEntity) {
                if ($entity->getId() === $otherEntity->getId()) {
                    unset($entities[$key]);
                    unset($otherEntities[$otherKey]);
                    continue 2;
                }
            }

            $diff[] = $entities[$key];
        }

        return new DomainModelCollection($diff);
    }

    public function getClass() : ? string
    {
        if ($this->_entities) {
            return get_class(array_values($this->_entities)[0]);
        }

        return null;
    }

    public function push(DomainModelInterface $entity)
    {
        if (!$this->isEmpty() && get_class($entity) !== $this->getClass()) {
            throw new \Exception('Invalid given entity class (current collection: class violation)');
        }

        $this->_entities[] = $entity;
    }

    protected function _checkWithOtherCollection(DomainModelCollection $otherCollection)
    {
        if (
            !$this->isEmpty() &&
            !$otherCollection->isEmpty() &&
            $this->getClass() !== $otherCollection->getClass()
        ) {
            throw new \Exception('Incompatible collections');
        }
    }
}