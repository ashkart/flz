<?php

namespace Application\Model\Domain\Collection;


trait TraitCollection
{
    protected $_entities = [];

    public function asArray() : array
    {
        return $this->_entities;
    }

    public function last()
    {
        return $this->_entities ? end($this->_entities) : null;
    }
}