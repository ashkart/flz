<?php

namespace Application\Model\Domain;

class ContactPerson extends AbstractModel
{
    /**
     * @var int
     */
    protected $_companyId;

    /**
     * @var string
     */
    protected $_firstName;

    /**
     * @var string
     */
    protected $_lastName;

    /**
     * @var string | null
     */
    protected $_middleName;

    /**
     * @var string
     */
    protected $_position;

    public function __construct(
        int $companyId,
        string $position,
        string $firstName,
        string $lastName,
        string $middleName = null
    )
    {
        $this->_companyId  = $companyId;
        $this->_position   = $position;
        $this->_firstName  = $firstName;
        $this->_lastName   = $lastName;
        $this->_middleName = $middleName;
    }

    public function getCompanyId(): int
    {
        return $this->_companyId;
    }

    public function getFirstName(): string
    {
        return $this->_firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->_firstName = $firstName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->_lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->_lastName = $lastName;
        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->_middleName;
    }

    public function setMiddleName(?string $middleName): self
    {
        $this->_middleName = $middleName;
        return $this;
    }

    public function getPosition(): string
    {
        return $this->_position;
    }

    public function setPosition(string $position): self
    {
        $this->_position = $position;
        return $this;
    }
}