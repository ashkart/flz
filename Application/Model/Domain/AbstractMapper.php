<?php

namespace Application\Model\Domain;

use Application\Cache\Slot\Slot;
use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\Logger;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Exception\DuplicateEntryException;
use Application\Model\Interfaces\DomainModelInterface;
use Atlas\Orm\Mapper\MapperSelect;
use Atlas\Orm\Mapper\RecordInterface;
use Aura\Sql\ExtendedPdo;
use Library\Master\Db\Mapper\AbstractTable;
use Atlas\Orm\Table\Row;
use Aura\SqlQuery\Mysql\Select;

/**
 * @property AbstractTable     table
 * @method Select|MapperSelect select(array $whereEquals = [])
 */
abstract class AbstractMapper extends \Atlas\Orm\Mapper\AbstractMapper
{
    use TraitDataSource;

    const EXCEPTION_CODE_DUPLICATE = '23000';

    const EXCEPTION_CODE_MYSQL_GONE_AWAY = 'HY000';

    /**
     * in mysql utf8 consumes up to 3 bytes.
     */
    const LENGTH_SMALL_CAPTION    = 128; // 128b
    const LENGTH_SMALL_CAPTION_MB = 31;  // 128b
    const LENGTH_MEDIUM           = 170; // 512b
    const LENGTH_LONG_CAPTION_MB  = 341; // 1024b
    const MESSAGE_TEXT_MB         = 1365; // 4096b
    const LENGTH_TEXT_MB          = 21845; // 64kb TEXT type

    const PRIMARY = 'id';

    const COLS = [
        self::PRIMARY
    ];

    const COLLECTIONS = [];

    const COLS_NOT_UPDATE = [];

    /**
     * @var null | string
     */
    protected static $_transactionName = null;

    protected static function _createCacheDependency(
        ?DomainModelInterface $oldObj,
        ?DomainModelInterface $newObj,
        \Closure $getProps,
        \Closure $collectDependencies
    ) : array
    {
        if (
            !$oldObj ||
            !$newObj ||
            $getProps($oldObj) !== $getProps($newObj)
        ) {
            return $collectDependencies($oldObj ?? $newObj);
        }

        return [];
    }

    public function fetch(int $id, bool $useCache = true, bool $withRelated = true) : ? DomainModelInterface
    {
        if ($useCache) {
            $slot = new Slot($id, get_class($this));
            $cachedResult = $slot->load();

            if ($cachedResult) {
                return $cachedResult;
            }
        }

        $record = $this->fetchRecord($id, $withRelated ? static::COLLECTIONS : []);

        if (!$record) {
            return null;
        }

        $model = $this->createModel($record);

        if ($useCache) {
            $slot = new Slot($model->getId(), get_class($this));
            $slot->save($model);
        }

        return $model;
    }

    /**
     * @param int[] $ids
     * @return DomainModelInterface[]
     */
    public function fetchByIds(array $ids, bool $useCache = true, bool $withRelated = false) : array
    {
        if (!$ids) {
            return [];
        }

        if ($useCache) {
            $slot = new Slot(null, get_class($this));

            $cachedResult = array_filter($slot->loadMany($ids)); // уберет из результата null элементы

            if ($cachedResult) {
                $notFoundKeys = array_diff($ids, array_keys($cachedResult));

                if ($notFoundKeys) {
                    $models = array_merge($this->fetchByIds($notFoundKeys), $cachedResult);
                    return $models;
                }
            }
        }

        $records = $this->fetchRecordSet($ids, $withRelated ? static::COLLECTIONS : []);

        $models = [];

        /** @var RecordInterface $record */
        foreach ($records as $record) {
            $model = $this->createModel($record);

            $models[$model->getId()] = $model;
        }

        if ($useCache) {
            $slot = new Slot(null, get_class($this));
            $slot->saveMany($models);
        }

        return $models;
    }

    public function fetchOne(array $where) : ? DomainModelInterface
    {
        $record = $this->fetchRecordBy($where, static::COLLECTIONS);

        $object = null;

        if ($record) {
            $object = $this->createModel($record);
        }

        return $object;
    }

    /**
     * @return DomainModelInterface[]
     */
    public function fetchAll(array $where, bool $useCache = false, bool $withRelated = false) : array
    {
        if (!$where && $useCache) {
            $slot = new Slot(null, get_class($this));

            $objects = $slot->load();

            if ($objects) {
                return $objects;
            }
        }

        $related = [];

        if ($withRelated) {
            $related = static::COLLECTIONS;
        }

        $records = $this->fetchRecordSetBy($where, $related);

        $objects = [];

        /** @var RecordInterface $record */
        foreach ($records as $record) {
            $objects[] = $this->createModel($record);
        }

        if (!$where && $useCache) {
            $slot->save($objects);
        }

        return $objects;
    }

    public function save(DomainModelInterface & $obj, array $cols = [], bool $useCache = true)
    {
        $oldObj = $obj->getId() ? $this->fetch($obj->getId()) : null;

        $dataForSaving = $this->_getDataForSaving($obj);

        if ($cols) {
            $dataForSaving = array_intersect_key($dataForSaving, array_flip($cols));
        }

        if ($obj->getId() !== null) {
            $dataForSaving = array_diff_key($dataForSaving, array_flip(static::COLS_NOT_UPDATE));
        }

        $dataForSaving[static::PRIMARY] = $obj->getId();

        if ($obj->getId() !== null) {
            $row = $this->table->getIdentityMap()->getRow([static::PRIMARY => $obj->getId()]);

            if (!$row) {
                $row = $this->_createNewRowFromData($obj->getTableRow());

                $this->table->getIdentityMap()->setRow(
                    $row,
                    [static::PRIMARY => $obj->getId()],
                    [static::PRIMARY]
                );
            }

            $record = $this->turnRowIntoRecord($row);
            $record->set($dataForSaving);
            $record->getRow()->setStatus(Row::MODIFIED);
        } else {
            $rowObj = new Row($dataForSaving);
            $record = $this->turnRowIntoRecord($rowObj);
        }

        $id = $this->_save($obj, $record);

        $newObj = $this->fetch($id, false);

        $this->_invalidateCacheDependencies($oldObj, $newObj);

        $obj = $newObj;

        if ($useCache) {
            $slot = new Slot($obj->getId(), get_class($this));
            $slot->save($obj);
        }
    }

    public function deleteByIds(array $ids) : bool
    {
        try {
            $this->beginTransaction();

            $result = true;

            foreach ($ids as $id) {
                $deleteQuery = $this->getWriteConnection()->prepare(
                    "DELETE FROM {$this->getTable()->getName()} WHERE id = :id"
                );

                $result &= $deleteQuery->execute([
                    'id' => $id
                ]);
            }

            $this->commit();

            $slot = new Slot(null, get_class($this));
            $slot->deleteMany($ids);

            return $result;
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }
    }

    public function createModel(RecordInterface $record) : DomainModelInterface
    {
        $row = $record->getArrayCopy();

        $obj = $this->_getInstance($row);
        $obj->setTableRow($record->jsonSerialize());

        return $this->_loadLine($obj, $row);
    }

    public function beginTransaction(string $name = null)
    {
        static $isRetry = false;

        try {
            if (!$this->getWriteConnection()->inTransaction()) {
                AbstractMapper::$_transactionName = $name;
                $this->getWriteConnection()->beginTransaction();
                $isRetry = false;
            }
        } catch (\PDOException $ex) {
            if ($ex->getCode() === self::EXCEPTION_CODE_MYSQL_GONE_AWAY && !$isRetry) {
                // Endless recursion cycle call is prevented.

                $writeConnection = $this->getWriteConnection();

                if ($writeConnection instanceof ExtendedPdo) {
                    $writeConnection->disconnect();
                    $writeConnection->connect();

                    $isRetry = true;
                    $this->beginTransaction($name);
                    return;
                }
            }

            throw $ex;
        }
    }

    public function commit(string $name = null)
    {
        if ($this->getWriteConnection()->inTransaction() && $name === AbstractMapper::$_transactionName) {
            $this->getWriteConnection()->commit();

            AbstractMapper::$_transactionName = null;
        }
    }

    public function rollBack()
    {
        if ($this->getWriteConnection()->inTransaction()) {
            $this->getWriteConnection()->rollBack();

            AbstractMapper::$_transactionName = null;
        }
    }

    protected function _save(DomainModelInterface &$obj, RecordInterface $record) : int
    {
        try {
            $this->beginTransaction();

            $this->persist($record);

            if ($record->getRow()->getStatus() === Row::INSERTED) {
                $this->table->getIdentityMap()->deleteRow($record->getRow(), [static::PRIMARY]);
            }

            $lastInsertId = $this->getWriteConnection()->lastInsertId();

            $this->commit();

            return $lastInsertId ?: $obj->getId();
        } catch (\Exception $exception) {
            $this->rollBack();

            if (
                $exception instanceof \PDOException &&
                $exception->getCode() === self::EXCEPTION_CODE_DUPLICATE &&
                strpos($exception->getMessage(), 'Duplicate') !== false
            ) {
                $exception = new DuplicateEntryException(
                    $exception->getMessage(),
                    $exception->getCode(),
                    $exception->getPrevious()
                );
            }

            /** @var $logger Logger */
            $logger = self::$sm->get(Factory::LOGGER)->getLogger();
            $logger->err($exception, $record->getArrayCopy());

            throw $exception;
        }
    }

    protected function _invalidateCacheDependencies(?DomainModelInterface $oldObj, ?DomainModelInterface $newObj)
    {
        foreach ($this->_getClearCacheDependencies($oldObj, $newObj) as $cacheDependencies) {
            /** @var Slot $slot */
            foreach ($cacheDependencies as $slot) {
                $slot->delete();
            }
        }
    }

    protected function _getClearCacheDependencies(?DomainModelInterface $oldObj, ?DomainModelInterface $newObj) : array
    {
        return [];
    }

    protected function _loadLine(DomainModelInterface $obj, array $row) : DomainModelInterface
    {
        return $obj;
    }

    protected function _createRelatedCollection(AbstractMapper $mapper, array $relations) : DomainModelCollection
    {
        $models = [];
        /* @TODO: не работает для многих-ко-многим, когда есть связующая модель */
        foreach ($relations as $relationRow) {
            $models[] = $mapper->createModel(
                $mapper->turnRowIntoRecord(new Row($relationRow))
            );
        }

        return new DomainModelCollection($models);
    }

    protected function _createNewRowFromData(array $data) : Row
    {
        foreach (static::COLLECTIONS as $collection) {
            unset($data[$collection]);
        }

        return new Row($data);
    }

    protected abstract function _getDataForSaving(DomainModelInterface $obj) : array;

    protected abstract function _getInstance(array $row) : DomainModelInterface;
}