<?php

namespace Application\Model\Domain\Company;

use Application\Cache\Slot\Slot;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Company;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Service\Auth;
use Application\Model\Domain\UserCompany;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\AuthenticationServiceInterface;

/**
 * @method Company | null  fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Company[]       fetchByIds(array $ids)
 * @method Company[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Company | null  fetchOne(array $where)
 */
class CompanyMapper extends AbstractMapper
{
    const COL_OPF           = 'opf';
    const COL_OFFICIAL_NAME = 'official_name';
    const COL_INN           = 'inn';
    const COL_KPP           = 'kpp';
    const COL_OGRN          = 'ogrn';
    const COL_ADDRESS       = 'address';
    const COL_DESCRIPTION   = 'description';

    const COLS = [
        self::PRIMARY,
        self::COL_OPF,
        self::COL_OFFICIAL_NAME,
        self::COL_INN,
        self::COL_KPP,
        self::COL_OGRN,
        self::COL_ADDRESS,
        self::COL_DESCRIPTION,
    ];

    public function fetchByInn(int $inn, bool $useCache) : Company
    {
        if ($useCache) {
            $slot = new Slot("inn_$inn", get_class($this));

            $cachedRes = $slot->load();

            if ($cachedRes) {
                return $cachedRes;
            }
        }

        $company = $this->fetchOne([self::COL_INN => $inn]);

        if ($useCache) {
            $slot->save($company);
        }

        return $company;
    }

    /**
     * @param DomainModelInterface | Company $obj
     */
    public function save(DomainModelInterface & $obj, array $cols = [], bool $useCache = true)
    {
        parent::save($obj, $cols, $useCache);
    }

    /**
     * @param DomainModelInterface | Company $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_OPF => $obj->getOpf(),
            self::COL_OFFICIAL_NAME => $obj->getOfficialName(),
            self::COL_INN => $obj->getInn(),
            self::COL_KPP => $obj->getKpp(),
            self::COL_OGRN => $obj->getOgrn(),
            self::COL_ADDRESS => $obj->getAddress(),
            self::COL_DESCRIPTION => $obj->getDescription(),
        ];
    }

    /**
     * @return DomainModelInterface | Company
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Company($row[self::COL_OFFICIAL_NAME]))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | Company $obj
     * @return DomainModelInterface | Company
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        return $obj
            ->setAddress($row[self::COL_ADDRESS])
            ->setInn($row[self::COL_INN])
            ->setOpf($row[self::COL_OPF])
            ->setKpp($row[self::COL_KPP])
            ->setOgrn($row[self::COL_OGRN])
            ->setDescription($row[self::COL_DESCRIPTION])
        ;
    }
}
