<?php
namespace Application\Model\Domain\ContactPerson;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\ContactPerson;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method ContactPerson | null  fetch(int $id, bool $useCache = true)
 * @method ContactPerson[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method ContactPerson[]       fetchByIds(array $ids)
 * @method ContactPerson|null    fetchOne(array $ids)
 */
class ContactPersonMapper extends AbstractMapper
{
    const COL_COMPANY_ID  = 'company_id';
    const COL_FIRST_NAME  = 'first_name';
    const COL_LAST_NAME   = 'last_name';
    const COL_MIDDLE_NAME = 'middle_name';
    const COL_POSITION    = 'position';

    const COLS = [
        self::PRIMARY,
        self::COL_COMPANY_ID,
        self::COL_FIRST_NAME,
        self::COL_LAST_NAME,
        self::COL_MIDDLE_NAME,
        self::COL_POSITION,
    ];

    /**
     * @param DomainModelInterface | ContactPerson $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_COMPANY_ID => $obj->getCompanyId(),
            self::COL_FIRST_NAME => $obj->getFirstName(),
            self::COL_LAST_NAME => $obj->getLastName(),
            self::COL_MIDDLE_NAME => $obj->getMiddleName(),
            self::COL_POSITION => $obj->getPosition(),
        ];
    }

    /**
     * @return DomainModelInterface | ContactPerson
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new ContactPerson(
            $row[self::COL_COMPANY_ID],
            $row[self::COL_POSITION],
            $row[self::COL_FIRST_NAME],
            $row[self::COL_LAST_NAME],
            $row[self::COL_MIDDLE_NAME]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }
}
