<?php
namespace Application\Model\Domain\ContactPerson;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class ContactPersonMapperEvents extends MapperEvents
{
}
