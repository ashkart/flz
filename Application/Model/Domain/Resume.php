<?php

namespace Application\Model\Domain;

use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Domain\Education\EducationMapper;
use Library\Master\Date\Date;
use Library\Master\Date\DateRange;
use Library\Master\Date\DateTime;

class Resume extends AbstractModel
{
    const EDUCATION_BASIC_GENERAL        = 'basic_general';
    const EDUCATION_SECONDARY_GENERAL    = 'secondary_general';
    const EDUCATION_SECONDARY_VOCATIONAL = 'secondary_vocational';
    const EDUCATION_HIGHER_BACHELOR      = 'higher_bachelor';
    const EDUCATION_HIGHER_MASTER        = 'higher_master';
    const EDUCATION_PHD                  = 'phd';               // кандидатская степень
    const EDUCATION_DOCTOR               = 'doctor';            // докторская степень

    const EDUCATION_LEVELS = [
        self::EDUCATION_BASIC_GENERAL,
        self::EDUCATION_SECONDARY_GENERAL,
        self::EDUCATION_SECONDARY_VOCATIONAL,
        self::EDUCATION_HIGHER_BACHELOR,
        self::EDUCATION_HIGHER_MASTER,
        self::EDUCATION_PHD,
        self::EDUCATION_DOCTOR
    ];

    const GENDER_MALE   = 'male';
    const GENDER_FEMALE = 'female';

    const GENDERS = [
        self::GENDER_MALE,
        self::GENDER_FEMALE
    ];

    const EMPLOYMENT_REGULAR   = Vacancy::EMPLOYMENT_REGULAR;
    const EMPLOYMENT_PART_TIME = Vacancy::EMPLOYMENT_PART_TIME;
    const EMPLOYMENT_SHIFT     = Vacancy::EMPLOYMENT_SHIFT;
    const EMPLOYMENT_WATCH     = Vacancy::EMPLOYMENT_WATCH;

    const EMPLOYMENTS = Vacancy::EMPLOYMENTS;

    const VISIBILITY_ALL  = 'all';
    const VISIBILITY_NONE = 'none';
    const VISIBILITY_LINK = 'link';

    const VISIBILITY_VALUES = [
        self::VISIBILITY_ALL,
        self::VISIBILITY_NONE,
        self::VISIBILITY_LINK
    ];

    /**
     * @var int
     */
    protected $_userId;

    /**
     * @var int
     */
    protected $_cityId;

    /**
     * @var string
     */
    protected $_firstName;

    /**
     * @var string | null
     */
    protected $_middleName = null;

    /**
     * @var string
     */
    protected $_lastName;

    /**
     * @var null | string
     */
    protected $_photoPath = null;

    /**
     * @var Date
     */
    protected $_birthDate;

    /**
     * @var string
     */
    protected $_gender;

    /**
     * @var string
     */
    protected $_positionName;

    /**
     * @var null | int
     */
    protected $_profareaId = null;

    /**
     * @var bool
     */
    protected $_isRelocateReady = false;

    /**
     * @var bool
     */
    protected $_isRemote = false;

    /**
     * @var null | string
     */
    protected $_employment = null;

    /**
     * @var string | null
     */
    protected $_educationLevel = null;

    /**
     * @var string | null
     */
    protected $_autodescription = null;

    /**
     * @var int | null
     */
    protected $_desiredPaycheck = null;

    /**
     * @var int
     */
    protected $_currencyId = Currency::RUB_ID;

    /**
     * @var DomainModelCollection
     */
    protected $_educations;

    /**
     * @var DomainModelCollection
     */
    protected $_workExperiences;

    /**
     * @var DomainModelCollection
     */
    protected $_contacts;

    /**
     * @var DomainModelCollection
     */
    protected $_languages;

    /**
     * @var DomainModelCollection
     */
    protected $_tags;

    /**
     * @var string
     */
    protected $_visibility = self::VISIBILITY_ALL;

    /**
     * @var bool
     */
    protected $_isDeleted = false;

    /**
     * @var DateTime | null
     */
    protected $_createDate = null;

    /**
     * @var DateTime | null
     */
    protected $_updateDate = null;

    /**
     * @var null | int
     */
    protected $_age = null;

    /**
     * @var DomainModelCollection
     */
    protected $_callbacks;

    public function __construct(int $userId, int $cityId, string $positionName, string $firstName, string $lastName, Date $birthDate)
    {
        $this->_userId       = $userId;
        $this->_cityId       = $cityId;
        $this->_positionName = $positionName;
        $this->_firstName    = $firstName;
        $this->_lastName     = $lastName;
        $this->_birthDate    = $birthDate;

        $this->_educations      = new DomainModelCollection();
        $this->_workExperiences = new DomainModelCollection();
        $this->_contacts        = new DomainModelCollection();
        $this->_languages       = new DomainModelCollection();
        $this->_tags            = new DomainModelCollection();
    }

    public function getCityId(): int
    {
        return $this->_cityId;
    }

    public function setCityId(int $cityId): self
    {
        $this->_cityId = $cityId;
        return $this;
    }

    public function getCity() : City
    {
        return self::$ds->cityMapper()->fetch($this->_cityId);
    }

    public function getFirstName(): string
    {
        return $this->_firstName;
    }

    public function getLastName(): string
    {
        return $this->_lastName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->_firstName = $firstName;
        return $this;
    }

    public function setLastName(string $lastName): self
    {
        $this->_lastName = $lastName;
        return $this;
    }

    public function getPhotoPath(): ?string
    {
        return $this->_photoPath;
    }

    public function setPhotoPath(?string $path): self
    {
        $this->_photoPath = $path;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->_userId;
    }

    public function getUser() : User
    {
        return self::$ds->userMapper()->fetch($this->_userId);
    }

    public function getPositionName(): string
    {
        return $this->_positionName;
    }

    public function setPositionName(string $positionName): self
    {
        $this->_positionName = $positionName;
        return $this;
    }

    public function getEducationLevel(): ?string
    {
        return $this->_educationLevel;
    }

    public function setEducationLevel(?string $educationLevel): self
    {
        if (!in_array($educationLevel, self::EDUCATION_LEVELS)) {
            throw new \Exception('Invalid education level value');
        }

        $this->_educationLevel = $educationLevel;
        return $this;
    }

    public function getAutodescription(): ?string
    {
        return $this->_autodescription;
    }

    public function setAutodescription(?string $autodescription): self
    {
        $this->_autodescription = $autodescription;
        return $this;
    }

    public function getDesiredPaycheck(): ?int
    {
        return $this->_desiredPaycheck;
    }

    public function setDesiredPaycheck(?int $desiredPaycheck): self
    {
        $this->_desiredPaycheck = $desiredPaycheck;
        return $this;
    }

    public function getCurrencyId(): int
    {
        return $this->_currencyId;
    }

    public function setCurrencyId(int $currencyId): self
    {
        $this->_currencyId = $currencyId;
        return $this;
    }

    public function getCurrency()
    {
        return self::$ds->currencyMapper()->fetch($this->_currencyId);
    }

    public function getEducations(): DomainModelCollection
    {
        return $this->_educations;
    }

    public function setEducations(DomainModelCollection $educations): self
    {
        $this->_educations = $educations;
        return $this;
    }

    public function getWorkExperiences(): DomainModelCollection
    {
        return $this->_workExperiences;
    }

    public function setWorkExperiences(DomainModelCollection $workExperiences): self
    {
        $this->_workExperiences = $workExperiences;
        return $this;
    }

    public function getContacts(): DomainModelCollection
    {
        return $this->_contacts;
    }

    public function setContacts(DomainModelCollection $contacts): self
    {
        $this->_contacts = $contacts;
        return $this;
    }

    public function getLanguages(): DomainModelCollection
    {
        return $this->_languages;
    }

    public function setLanguages(DomainModelCollection $languages): self
    {
        $this->_languages = $languages;
        return $this;
    }

    public function getVisibility(): string
    {
        return $this->_visibility;
    }

    public function isVisible() : bool
    {
        return $this->_visibility === self::VISIBILITY_ALL;
    }

    public function isInvisible() : bool
    {
        return $this->_visibility === self::VISIBILITY_NONE;
    }

    public function setVisibility(string $visibility): self
    {
        if (!in_array($visibility, self::VISIBILITY_VALUES)) {
            throw new \Exception('Invalid visibility value.');
        }

        $this->_visibility = $visibility;
        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->_isDeleted;
    }

    public function setDeleted(bool $isDeleted): self
    {
        $this->_isDeleted = $isDeleted;
        return $this;
    }

    public function getCreateDate(): ?DateTime
    {
        return $this->_createDate;
    }

    public function setCreateDate(DateTime $createDate): self
    {
        $this->_createDate = $createDate;
        return $this;
    }

    public function getUpdateDate(): ?DateTime
    {
        return $this->_updateDate;
    }

    public function setUpdateDate(?DateTime $updateDate): self
    {
        $this->_updateDate = $updateDate;
        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->_middleName;
    }

    public function setMiddleName(?string $middleName): self
    {
        $this->_middleName = $middleName;
        return $this;
    }

    public function getAge() : int
    {
        if ($this->_age === null) {
            $this->_age = (new DateRange($this->_birthDate, null))->getFullYears();
        }

        return $this->_age;
    }

    public function getBirthDate(): Date
    {
        return $this->_birthDate;
    }

    public function setBirthDate(Date $birthDate): self
    {
        $this->_birthDate = $birthDate;
        return $this;
    }

    public function getGender(): string
    {
        return $this->_gender;
    }

    public function setGender(string $gender): self
    {
        $this->_gender = $gender;
        return $this;
    }

    public function isRelocateReady(): bool
    {
        return $this->_isRelocateReady;
    }

    public function setRelocateReady(bool $isRelocateReady): self
    {
        $this->_isRelocateReady = $isRelocateReady;
        return $this;
    }

    public function getTags(): DomainModelCollection
    {
        return $this->_tags;
    }

    public function setTags(DomainModelCollection $tags): self
    {
        $this->_tags = $tags;
        return $this;
    }

    public function getEmployment(): ?string
    {
        return $this->_employment;
    }

    public function setEmployment(?string $scheduleType): self
    {
        if ($scheduleType && !in_array($scheduleType, self::EMPLOYMENTS)) {
            throw new \Exception('Invalid employment value');
        }

        $this->_employment = $scheduleType;
        return $this;
    }

    public function isRemote(): bool
    {
        return $this->_isRemote;
    }

    public function setRemote(bool $isRemote): self
    {
        $this->_isRemote = $isRemote;
        return $this;
    }

    public function getProfareaId(): ?int
    {
        return $this->_profareaId;
    }

    public function setProfareaId(?int $profareaId): self
    {
        $this->_profareaId = $profareaId;
        return $this;
    }

    public function getCallbacks(): DomainModelCollection
    {
        if (!$this->_callbacks) {
            $callbacks = self::$ds->callbackMapper()->fetchByEntityId($this->_id, Callback::ENTITY_TYPE_RESUME);
            $this->_callbacks = new DomainModelCollection($callbacks);
        }

        return $this->_callbacks;
    }

    public function setCallbacks(DomainModelCollection $callbacks): self
    {
        $this->_callbacks = $callbacks;
        return $this;
    }
}