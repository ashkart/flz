<?php

namespace Application\Model\Domain;

use Application\Model\Interfaces\EntityRelationInterface;
use Library\Master\Date\DateTime;

class Callback extends AbstractModel implements EntityRelationInterface
{
    const STATE_NEW      = 'new';
    const STATE_WATCHED  = 'watched';
    const STATE_ACCEPTED = 'accepted';
    const STATE_REJECTED = 'rejected';
    const STATE_DELETED  = 'deleted';

    const STATES = [
        self::STATE_NEW,
        self::STATE_WATCHED,
        self::STATE_ACCEPTED,
        self::STATE_REJECTED,
        self::STATE_DELETED,
    ];

    const STATES_EXISTING = [
        self::STATE_NEW,
        self::STATE_WATCHED,
        self::STATE_ACCEPTED
    ];

    const STATES_NOT_DELETED = [
        self::STATE_NEW,
        self::STATE_WATCHED,
        self::STATE_ACCEPTED,
        self::STATE_REJECTED
    ];

    /**
     * @var int
     */
    protected $_userId;

    /**
     * @var string
     */
    protected $_entityType;

    /**
     * @var int
     */
    protected $_entityId;

    /**
     * @var string
     */
    protected $_fromEntityType;

    /**
     * @var int
     */
    protected $_fromEntityId;

    /**
     * @var int
     */
    protected $_conversationId;

    /**
     * @var string
     */
    protected $_state = self::STATE_NEW;

    /**
     * @var null | DateTime
     */
    protected $_updateDate = null;

    /**
     * @var DateTime
     */
    protected $_createDateTime = null;

    public function __construct(
        int $userId,
        string $entityType,
        int $entityId,
        string $fromEntityType,
        int $fromEntityId,
        int $conversationId
    )
    {
        $this->_userId         = $userId;
        $this->_entityType     = $entityType;
        $this->_entityId       = $entityId;
        $this->_fromEntityType = $fromEntityType;
        $this->_fromEntityId   = $fromEntityId;
        $this->_conversationId = $conversationId;
    }

    public function getUserId(): int
    {
        return $this->_userId;
    }

    public function getEntityType(): string
    {
        return $this->_entityType;
    }

    public function getEntityId(): int
    {
        return $this->_entityId;
    }

    public function getFromEntityType(): string
    {
        return $this->_fromEntityType;
    }

    public function getFromEntityId(): int
    {
        return $this->_fromEntityId;
    }

    public function getConversationId(): int
    {
        return $this->_conversationId;
    }

    public function getCreateDateTime(): DateTime
    {
        return $this->_createDateTime;
    }

    public function setCreateDateTime(DateTime $createDateTime): self
    {
        $this->_createDateTime = $createDateTime;
        return $this;
    }

    public function getState(): string
    {
        return $this->_state;
    }

    public function setState(string $state): self
    {
        if (!in_array($state, self::STATES)) {
            throw new \Exception('Invalid state given');
        }

        $this->_state = $state;
        return $this;
    }

    public function getUpdateDate(): ?DateTime
    {
        return $this->_updateDate;
    }

    public function setUpdateDate(?DateTime $updateDate): self
    {
        $this->_updateDate = $updateDate;
        return $this;
    }
}