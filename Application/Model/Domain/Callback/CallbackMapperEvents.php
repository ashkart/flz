<?php
namespace Application\Model\Domain\Callback;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class CallbackMapperEvents extends MapperEvents
{
}
