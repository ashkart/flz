<?php
namespace Application\Model\Domain\Callback;

use Application\Cache\Slot\UserCallbacks;
use Application\Cache\Slot\EntityCallbacks;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Callback;
use Application\Model\Interfaces\DomainModelInterface;
use Library\Master\Date\DateTime;

/**
 * @method Callback[]       fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Callback[]       fetchByIds(array $ids)
 * @method Callback[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Callback | null  fetchOne(array $where)
 */
class CallbackMapper extends AbstractMapper
{
    const COL_USER_ID          = 'user_id';
    const COL_ENTITY_TYPE      = 'entity_type';
    const COL_ENTITY_ID        = 'entity_id';
    const COL_FROM_ENTITY_TYPE = 'from_entity_type';
    const COL_FROM_ENTITY_ID   = 'from_entity_id';
    const COL_CONVERSATION_ID  = 'conversation_id';
    const COL_STATE            = 'state';
    const COL_UPDATE_DATE      = 'update_date';
    const COL_CREATE_DATE      = 'create_date';

    const COLS = [
        self::PRIMARY,
        self::COL_USER_ID,
        self::COL_ENTITY_TYPE,
        self::COL_ENTITY_ID,
        self::COL_FROM_ENTITY_TYPE,
        self::COL_FROM_ENTITY_ID,
        self::COL_CONVERSATION_ID,
        self::COL_STATE,
        self::COL_UPDATE_DATE,
        self::COL_CREATE_DATE,
    ];

    const COLS_NOT_UPDATE = [
        self::COL_CREATE_DATE
    ];

    /**
     * @return Callback[]
     */
    public function fetchByEntityId(int $entityId, string $entityType, bool $useCache = true) : array
    {
        if ($useCache) {
            $callbacks = self::$ds->callbackService()->selectByEntityId($entityId, $entityType);

            $result = [];

            foreach ($callbacks as $callback) {
                $result[] = $this->createModel($callback);
            }

            return $result;
        }

        return $this->fetchAll(
            [
                self::COL_ENTITY_ID => $entityId,
                self::COL_ENTITY_TYPE => $entityType
            ],
            $useCache
        );
    }

    /**
     * @param DomainModelInterface | Callback $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        $updateDate = $obj->getUpdateDate();

        return [
            self::COL_USER_ID          => $obj->getUserId(),
            self::COL_ENTITY_TYPE      => $obj->getEntityType(),
            self::COL_ENTITY_ID        => $obj->getEntityId(),
            self::COL_FROM_ENTITY_TYPE => $obj->getFromEntityType(),
            self::COL_FROM_ENTITY_ID   => $obj->getFromEntityId(),
            self::COL_CONVERSATION_ID  => $obj->getConversationId(),
            self::COL_STATE            => $obj->getState(),
            self::COL_UPDATE_DATE      => $updateDate ? $updateDate->mysqlFormat() : null ,
        ];
    }

    /**
     * @return DomainModelInterface | Callback
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Callback(
            $row[self::COL_USER_ID],
            $row[self::COL_ENTITY_TYPE],
            $row[self::COL_ENTITY_ID],
            $row[self::COL_FROM_ENTITY_TYPE],
            $row[self::COL_FROM_ENTITY_ID],
            $row[self::COL_CONVERSATION_ID]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | Callback $obj
     * @return DomainModelInterface | Callback
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        return $obj
            ->setState($row[self::COL_STATE])
            ->setCreateDateTime(DateTime::createFromMysqlFormat($row[self::COL_CREATE_DATE]))
            ->setUpdateDate(
                $row[self::COL_UPDATE_DATE]
                    ? DateTime::createFromMysqlFormat($row[self::COL_UPDATE_DATE])
                    : null
            )
        ;
    }

    protected function _getClearCacheDependencies(?DomainModelInterface $oldObj, ?DomainModelInterface $newObj): array
    {
        $dependencies = [];

        $dependencies[] = self::_createCacheDependency(
            $oldObj,
            $newObj,
            function (Callback $callback) : bool {
                return $callback->getId() && in_array($callback->getState(), Callback::STATES_NOT_DELETED);
            },
            function (Callback $callback) {
                return [
                    new UserCallbacks($callback->getUserId()),
                    new EntityCallbacks($callback->getEntityId(), $callback->getEntityType())
                ];
            }
        );

        return $dependencies;
    }
}
