<?php

namespace Application\Model\Domain;


class Contact extends AbstractModel
{
    const ENTITY_TYPE_USER           = 'user';
    const ENTITY_TYPE_RESUME         = 'resume';
    const ENTITY_TYPE_VACANCY        = 'vacancy';
    const ENTITY_TYPE_CONTACT_PERSON = 'contact_person';

    const ENTITY_TYPES = [
        self::ENTITY_TYPE_USER,
        self::ENTITY_TYPE_RESUME,
        self::ENTITY_TYPE_VACANCY,
        self::ENTITY_TYPE_CONTACT_PERSON
    ];

    /**
     * @var int
     */
    protected $_entityId;

    /**
     * @var string
     */
    protected $_entityType;

    /**
     * @var string
     */
    protected $_type;

    /**
     * @var string
     */
    protected $_value;

    public function __construct(
        int $entityId,
        string $entityType,
        string $type,
        string $value
    )
    {
        if (!in_array($entityType, self::ENTITY_TYPES)) {
            throw new \Exception('Invalid entity type given');
        }

        $this->_entityId   = $entityId;
        $this->_entityType = $entityType;
        $this->_type       = $type;
        $this->_value      = $value;
    }

    public function getEntityId(): int
    {
        return $this->_entityId;
    }

    public function getEntityType(): string
    {
        return $this->_entityType;
    }

    public function getType(): string
    {
        return $this->_type;
    }

    public function setType(string $type): self
    {
        $this->_type = $type;
        return $this;
    }

    public function getValue(): string
    {
        return $this->_value;
    }

    public function setValue(string $value): self
    {
        $this->_value = $value;
        return $this;
    }
}