<?php
namespace Application\Model\Domain\Region;

use Application\Cache\Slot\Slot;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\Region;

/**
 * @method Region         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Region[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Region[]       fetchByIds(array $ids)
 * @method Region | null  fetchOne(array $where)
 */
class RegionMapper extends AbstractMapper
{
    const COL_COUNTRY_ID = 'country_id';
    const COL_TITLE      = 'title';

    const COLS = [
        self::PRIMARY,
        self::COL_COUNTRY_ID,
        self::COL_TITLE
    ];

    /**
     * @return Region[]
     */
    public function fetchByCountryId(int $countryId, bool $useCache = true) : array
    {
        if ($useCache) {
            $slot = new Slot("country_$countryId", get_class($this));

            $regions = $slot->load();

            if ($regions) {
                return $regions;
            }
        }

        $regions = $this->fetchAll([
            self::COL_COUNTRY_ID => $countryId
        ]);

        if ($useCache) {
            $slot->save($regions);
        }

        return $regions;
    }

    /**
     * @param DomainModelInterface | Region $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_COUNTRY_ID => $obj->getCountryId(),
            self::COL_TITLE      => $obj->getTitle()
        ];
    }

    /**
     * @return DomainModelInterface | Region
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Region($row[self::COL_COUNTRY_ID], $row[self::COL_TITLE]))
            ->setId($row[self::PRIMARY])
        ;
    }
}
