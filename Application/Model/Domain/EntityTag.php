<?php

namespace Application\Model\Domain;

use Application\Model\Interfaces\EntityRelationInterface;

class EntityTag extends AbstractModel implements EntityRelationInterface
{
    /**
     * @var string
     */
    protected $_entityType;

    /**
     * @var int
     */
    protected $_entityId;

    /**
     * @var int
     */
    protected $_tagId;

    /**
     * @var string
     */
    protected $_tagName;

    public function __construct(int $tagId, int $entityId, string $entityType)
    {
        if (!in_array($entityType, self::ENTITY_TYPES)) {
            throw new \Exception('Invalid tag entity type given');
        }

        $this->_tagId      = $tagId;
        $this->_entityId   = $entityId;
        $this->_entityType = $entityType;
    }

    public function getEntityType(): string
    {
        return $this->_entityType;
    }

    public function getEntityId(): int
    {
        return $this->_entityId;
    }

    public function getTagId(): int
    {
        return $this->_tagId;
    }

    public function getTagName(): string
    {
        return $this->_tagName;
    }

    public function setTagName($tagName): self
    {
        $this->_tagName = $tagName;
        return $this;
    }
}