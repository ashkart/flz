<?php

namespace Application\Model\Domain;

class Company extends AbstractModel
{
    const OPF_OOO = 'ООО';
    const OPF_OAO = 'ОАО';
    const OPF_ZAO = 'ЗАО';
    const OPF_PAO = 'ПАО';
    const OPF_AO  = 'АО';
    const OPF_UP  = 'УП';
    const OPF_GU  = 'ГУ';
    const OPF_TOO = 'ТОО';
    const OPF_IP  = 'ИП';
    const OPF_NO  = 'НО';

    const OPFS = [
        self::OPF_OOO => self::OPF_OOO,
        self::OPF_OAO => self::OPF_OAO,
        self::OPF_ZAO => self::OPF_ZAO,
        self::OPF_PAO => self::OPF_PAO,
        self::OPF_AO => self::OPF_AO,
        self::OPF_UP => self::OPF_UP,
        self::OPF_GU => self::OPF_GU,
        self::OPF_TOO => self::OPF_TOO,
        self::OPF_IP => self::OPF_IP,
        self::OPF_NO => self::OPF_NO,
    ];

    /**
     * @var string | null
     */
    protected $_opf = null;

    /**
     * @var string
     */
    protected $_officialName;

    /**
     * @var string | null
     */
    protected $_description = null;

    /**
     * @var int | null
     */
    protected $_inn = null;

    /**
     * @var int | null
     */
    protected $_kpp = null;

    /**
     * @var int | null
     */
    protected $_ogrn = null;

    /**
     * @var string | null
     */
    protected $_address = null;

    public function __construct(string $officialName)
    {
        $this->_officialName = $officialName;
    }

    public function getOpf(): ?string
    {
        return $this->_opf;
    }

    public function getOfficialName(): string
    {
        return $this->_officialName;
    }

    public function getInn(): ?int
    {
        return $this->_inn;
    }

    public function getKpp(): ?int
    {
        return $this->_kpp;
    }

    public function getOgrn(): ?int
    {
        return $this->_ogrn;
    }

    public function getAddress(): ?string
    {
        return $this->_address;
    }

    public function setOpf(?string $opf): self
    {
        $this->_opf = $opf;
        return $this;
    }

    public function setOfficialName(string $officialName): self
    {
        $this->_officialName = $officialName;
        return $this;
    }

    public function setAddress(?string $address): self
    {
        $this->_address = $address;
        return $this;
    }

    public function setInn(?int $inn): self
    {
        $this->_inn = $inn;
        return $this;
    }

    public function setKpp(?int $kpp): self
    {
        $this->_kpp = $kpp;
        return $this;
    }

    public function setOgrn(?int $ogrn): self
    {
        $this->_ogrn = $ogrn;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->_description;
    }

    public function setDescription(?string $description): self
    {
        $this->_description = $description;
        return $this;
    }
}