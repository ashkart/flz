<?php

namespace Application\Model\Domain\Resume;

use Application\Cache\Slot\UserResumeIds;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Callback;
use Application\Model\Domain\Callback\CallbackMapper;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Domain\Contact;
use Application\Model\Domain\Contact\ContactMapper;
use Application\Model\Domain\Education\EducationMapper;
use Application\Model\Domain\EntityTag;
use Application\Model\Domain\EntityTag\EntityTagMapper;
use Application\Model\Domain\LanguageProficiency\LanguageProficiencyMapper;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\Resume;
use Application\Model\Domain\WorkExperience\WorkExperienceMapper;
use Library\Master\Date\Date;
use Library\Master\Date\DateTime;
use Atlas\Orm\Table\Row;

/**
 * @method Resume         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Resume[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Resume[]       fetchByIds(array $ids, bool $useCache = true, bool $withRelated = false)
 * @method Resume | null  fetchOne(array $where)
 */
class ResumeMapper extends AbstractMapper
{
    const COL_USER_ID           = 'user_id';
    const COL_CITY_ID           = 'city_id';
    const COL_PHOTO_PATH        = 'photo_path';
    const COL_POSITION_NAME     = 'position_name';
    const COL_PROFAREA_ID       = 'profarea_id';
    const COL_FIRST_NAME        = 'first_name';
    const COL_MIDDLE_NAME       = 'middle_name';
    const COL_LAST_NAME         = 'last_name';
    const COL_BIRTH_DATE        = 'birth_date';
    const COL_GENDER            = 'gender';
    const COL_IS_RELOCATE_READY = 'is_relocate_ready';
    const COL_IS_REMOTE         = 'is_remote';
    const COL_EMPLOYMENT        = 'employment';
    const COL_EDUCATION_LEVEL   = 'education_level';
    const COL_AUTODESCRIPTION   = 'autodescription';
    const COL_DESIRED_PAYCHECK  = 'desired_paycheck';
    const COL_CURRENCY_ID       = 'currency_id';
    const COL_VISIBILITY        = 'visibility';
    const COL_IS_DELETED        = 'is_deleted';
    const COL_CREATE_DATE       = 'create_date';
    const COL_UPDATE_DATE       = 'update_date';

    const COLLECTION_OF_WORK_EXPERIENCE = 'work_experience';
    const COLLECTION_OF_EDUCATION       = 'education';
    const COLLECTION_OF_LANGUAGES       = 'languages';
    const COLLECTION_OF_CONTACTS        = 'contacts';
    const COLLECTION_OF_TAGS            = 'tags';
    const COLLECTION_OF_PHOTOS          = 'photos';

    const CALLBACKS = 'callbacks';

    const COLLECTIONS = [
        self::COLLECTION_OF_EDUCATION,
        self::COLLECTION_OF_WORK_EXPERIENCE,
        self::COLLECTION_OF_LANGUAGES,
        self::COLLECTION_OF_CONTACTS,
        self::COLLECTION_OF_TAGS
    ];

    const COLS = [
        self::PRIMARY,
        self::COL_USER_ID,
        self::COL_CITY_ID,
        self::COL_PHOTO_PATH,
        self::COL_POSITION_NAME,
        self::COL_FIRST_NAME,
        self::COL_MIDDLE_NAME,
        self::COL_LAST_NAME,
        self::COL_BIRTH_DATE,
        self::COL_GENDER,
        self::COL_IS_RELOCATE_READY,
        self::COL_EDUCATION_LEVEL,
        self::COL_AUTODESCRIPTION,
        self::COL_DESIRED_PAYCHECK,
        self::COL_CURRENCY_ID,
        self::COL_VISIBILITY,
        self::COL_IS_DELETED,
        self::COL_UPDATE_DATE,
        self::COL_CREATE_DATE
    ];

    const COLS_NOT_UPDATE = [
        self::COL_CREATE_DATE
    ];

    public function fetchByUserId(int $userId, bool $useCache = true, bool $withRelated = true)
    {
        if ($useCache) {
            $slot = new UserResumeIds($userId);

            $userResumeIds = $slot->load();

            if ($userResumeIds) {
                return $this->fetchByIds($userResumeIds, true, true);
            }
        }

        $rows = $this->select([self::COL_USER_ID => $userId])->cols([self::PRIMARY])->fetchAll();

        $userResumeIds = [];

        foreach ($rows as $row) {
            $userResumeIds[] = $row['id'];
        }

        if ($useCache) {
            $slot = new UserResumeIds($userId);
            $slot->save($userResumeIds);
        }

        return $this->fetchByIds($userResumeIds, $useCache, $withRelated);
    }

    /**
     * @param DomainModelInterface | Resume $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_USER_ID           => $obj->getUserId(),
            self::COL_CITY_ID           => $obj->getCityId(),
            self::COL_PHOTO_PATH        => $obj->getPhotoPath(),
            self::COL_POSITION_NAME     => $obj->getPositionName(),
            self::COL_PROFAREA_ID       => $obj->getProfareaId(),
            self::COL_FIRST_NAME        => $obj->getFirstName(),
            self::COL_MIDDLE_NAME       => $obj->getMiddleName(),
            self::COL_LAST_NAME         => $obj->getLastName(),
            self::COL_BIRTH_DATE        => $obj->getBirthDate()->mysqlFormat(),
            self::COL_GENDER            => $obj->getGender(),
            self::COL_IS_RELOCATE_READY => (int) $obj->isRelocateReady(),
            self::COL_IS_REMOTE         => (int) $obj->isRemote(),
            self::COL_EMPLOYMENT        => $obj->getEmployment(),
            self::COL_EDUCATION_LEVEL   => $obj->getEducationLevel(),
            self::COL_AUTODESCRIPTION   => $obj->getAutodescription(),
            self::COL_DESIRED_PAYCHECK  => $obj->getDesiredPaycheck(),
            self::COL_CURRENCY_ID       => $obj->getCurrencyId(),
            self::COL_VISIBILITY        => $obj->getVisibility(),
            self::COL_IS_DELETED        => (int) $obj->isDeleted(),
            self::COL_UPDATE_DATE       => (new DateTime())->mysqlFormat()
        ];
    }

    /**
     * @return DomainModelInterface | Resume
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        $birthDate = Date::createFromMysqlFormat($row[self::COL_BIRTH_DATE]);

        return (new Resume(
            $row[self::COL_USER_ID],
            $row[self::COL_CITY_ID],
            $row[self::COL_POSITION_NAME],
            $row[self::COL_FIRST_NAME],
            $row[self::COL_LAST_NAME],
            $birthDate
        ))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | Resume $obj
     *
     * @return DomainModelInterface | Resume
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        $educations      = $this->_createRelatedCollection(self::$ds->educationMapper(), $row[self::COLLECTION_OF_EDUCATION] ?? []);
        $workExperiences = $this->_createRelatedCollection(self::$ds->workExperienceMapper(), $row[self::COLLECTION_OF_WORK_EXPERIENCE] ?? []);
        $contacts        = $this->_createRelatedCollection(self::$ds->contactMapper(), $row[self::COLLECTION_OF_CONTACTS] ?? []);
        $languages       = $this->_createRelatedCollection(self::$ds->langProficiencyMapper(), $row[self::COLLECTION_OF_LANGUAGES] ?? []);;

        if ($row[self::COLLECTION_OF_TAGS] !== null) {
            $tagIds       = [];
            $tagRelations = [];

            $entityTagMapper = self::$ds->entityTagMapper();

            foreach ($row[self::COLLECTION_OF_TAGS] as $entityTagRow) {
                $tagIds[] = $entityTagRow[EntityTagMapper::COL_TAG_ID];

                /** @var EntityTag $entityTag */
                $entityTag = $entityTagMapper->createModel($entityTagMapper->newRecord($entityTagRow));

                $tagRelations[$entityTag->getTagId()] = $entityTag;
            }

            $tags = self::$ds->tagMapper()->fetchByIds($tagIds);

            /** @var EntityTag[] $tagRelations */
            foreach ($tags as $tag) {
                $tagRelations[$tag->getId()]->setTagName($tag->getName());
            }
        }

        $tagCollection = new DomainModelCollection($tagRelations ?? []);

        $createDate = DateTime::createFromMysqlFormat($row[self::COL_CREATE_DATE]);

        return $obj
            ->setCityId($row[self::COL_CITY_ID])
            ->setPhotoPath($row[self::COL_PHOTO_PATH])
            ->setAutodescription($row[self::COL_AUTODESCRIPTION])
            ->setDesiredPaycheck($row[self::COL_DESIRED_PAYCHECK])
            ->setCurrencyId($row[self::COL_CURRENCY_ID])
            ->setEducationLevel($row[self::COL_EDUCATION_LEVEL])
            ->setMiddleName($row[self::COL_MIDDLE_NAME])
            ->setGender($row[self::COL_GENDER])
            ->setProfareaId($row[self::COL_PROFAREA_ID])
            ->setRelocateReady($row[self::COL_IS_RELOCATE_READY])
            ->setRemote($row[self::COL_IS_REMOTE])
            ->setEmployment($row[self::COL_EMPLOYMENT])
            ->setEducations($educations)
            ->setWorkExperiences($workExperiences)
            ->setContacts($contacts)
            ->setLanguages($languages)
            ->setTags($tagCollection)
            ->setVisibility($row[self::COL_VISIBILITY])
            ->setDeleted((int) $row[self::COL_IS_DELETED])
            ->setUpdateDate(DateTime::createFromMysqlFormat($row[self::COL_UPDATE_DATE]))
            ->setCreateDate($createDate)
        ;
    }

    protected function _createNewRowFromData(array $data): Row
    {
        unset($data[self::CALLBACKS]);

        return parent::_createNewRowFromData($data);
    }

    protected function setRelated()
    {
        $this
            ->oneToMany(self::COLLECTION_OF_WORK_EXPERIENCE, WorkExperienceMapper::class)
            ->on([self::PRIMARY => WorkExperienceMapper::COL_RESUME_ID])
        ;
        $this
            ->oneToMany(self::COLLECTION_OF_EDUCATION, EducationMapper::class)
            ->on([self::PRIMARY => EducationMapper::COL_RESUME_ID])
        ;
        $this
            ->oneToMany(self::COLLECTION_OF_LANGUAGES, LanguageProficiencyMapper::class)
            ->on([self::PRIMARY => LanguageProficiencyMapper::COL_RESUME_ID])
        ;
        $this
            ->oneToMany(self::COLLECTION_OF_CONTACTS, ContactMapper::class)
            ->on([self::PRIMARY => ContactMapper::COL_ENTITY_ID])
            ->where(
                ContactMapper::COL_ENTITY_TYPE . " = ?",
                Contact::ENTITY_TYPE_RESUME
            )
        ;
        $this
            ->oneToMany(self::COLLECTION_OF_TAGS, EntityTagMapper::class)
            ->on([self::PRIMARY => EntityTagMapper::COL_ENTITY_ID])
            ->where(
                EntityTagMapper::COL_ENTITY_TYPE . " = ? ",
                EntityTag::ENTITY_TYPE_RESUME
            )
        ;
        $this->oneToMany(self::CALLBACKS, CallbackMapper::class)
            ->on([self::PRIMARY => CallbackMapper::COL_ENTITY_ID])
            ->where(CallbackMapper::COL_ENTITY_TYPE . ' = ?', Callback::ENTITY_TYPE_RESUME)
        ;
    }

    protected function _getClearCacheDependencies(?DomainModelInterface $oldObj, ?DomainModelInterface $newObj): array
    {
        $dependencies = [];

        $dependencies[] = self::_createCacheDependency(
            $oldObj,
            $newObj,
            function (Resume $resume) : ?int {
                return $resume->getId();
            },
            function (Resume $resume) {
                return [
                    new UserResumeIds($resume->getUserId())
                ];
            }
        );

        return $dependencies;
    }
}
