<?php
namespace Application\Model\Domain\City;

use Application\Cache\Slot\Slot;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\City;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method City         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method City[]       fetchByIds(array $ids)
 * @method City[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method City | null  fetchOne(array $where)
 */
class CityMapper extends AbstractMapper
{
    public const COL_COUNTRY_ID   = 'country_id';
    public const COL_REGION_ID    = 'region_id';
    public const COL_IS_IMPORTANT = 'is_important';
    public const COL_TITLE_EN     = 'title_en';
    public const COL_TITLE_RU     = 'title_ru';
    public const COL_AREA         = 'area';

    public const COLS = [
        self::PRIMARY,
        self::COL_COUNTRY_ID,
        self::COL_REGION_ID,
        self::COL_IS_IMPORTANT,
        self::COL_TITLE_EN,
        self::COL_AREA,
    ];

    /**
     * @return City[]
     */
    public function fetchByRegionId(int $regionId, bool $useCache = true) : array
    {
        if ($useCache) {
            $slot = new Slot("region_$regionId", get_class($this));

            $cities = $slot->load();

            if ($cities) {
                return $cities;
            }
        }

        $cities = $this->fetchAll([
            self::COL_REGION_ID => $regionId
        ]);

        if ($useCache) {
            $slot->save($cities);
        }

        return $cities;
    }

    /**
     * @param DomainModelInterface | City $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_COUNTRY_ID   => $obj->getCountryId(),
            self::COL_REGION_ID    => $obj->getRegionId(),
            self::COL_IS_IMPORTANT => (int) $obj->isImportant(),
            self::COL_TITLE_EN     => $obj->getTitleEn(),
            self::COL_TITLE_RU     => $obj->getTitleRu(),
            self::COL_AREA         => $obj->getArea(),
        ];
    }

    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new City($row[self::COL_COUNTRY_ID], $row[self::COL_REGION_ID]))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | City $obj
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        return $obj
            ->setArea($row[self::COL_AREA])
            ->setTitleEn($row[self::COL_TITLE_EN])
            ->setTitleRu($row[self::COL_TITLE_RU])
            ->setIsImportant($row[self::COL_IS_IMPORTANT])
        ;
    }
}
