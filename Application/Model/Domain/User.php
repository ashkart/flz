<?php

namespace Application\Model\Domain;

use Application\Model\Domain\Collection\DomainModelCollection;
use Library\Master\Date\Date;
use Library\Master\Date\DateTime;

class User extends AbstractModel
{
    const PASSWORD_SALT  = '$2y$07$iZkzkuIxnLtTSjRIydL2yf$';

    const ROLE_GUEST     = 'guest';
    const ROLE_EMPLOYEE  = 'employee';
    const ROLE_RECRUITER = 'recruiter';
    const ROLE_ADMIN     = 'admin';

    const ROLES = [
        self::ROLE_GUEST,
        self::ROLE_EMPLOYEE,
        self::ROLE_RECRUITER,
        self::ROLE_ADMIN,
    ];

    const LOCALE_EN = 'en';
    const LOCALE_RU = 'ru';

    const LOCALES = [
        self::LOCALE_EN,
        self::LOCALE_RU
    ];

    const MIN_NAME_LENGTH     = 2;
    const MIN_PASSWORD_LENGTH = 6;


    /**
     * @var null | int
     */
    protected $_vkId = null;

    /**
     * @var null | string
     */
    protected $_fbId = null;

    /**
     * @var string | null
     */
    protected $_email = null;

    /**
     * @var string | null
     */
    protected $_password = null;

    /**
     * @var string
     */
    protected $_role = self::ROLE_GUEST;

    /**
     * @var string | null
     */
    protected $_userName = null;

    /**
     * @var bool
     */
    protected $_isConfirmed = false;

    /**
     * @var int | null
     */
    protected $_cityId = null;

    /**
     * @var string
     */
    protected $_locale = self::LOCALE_RU;

    /**
     * @var DomainModelCollection
     */
    protected $_companyCollection;

    /**
     * @var DateTime | null
     */
    protected $_lastVisitDate = null;

    /**
     * @var DateTime | null
     */
    protected $_createDate = null;

    public static function createEmpty()
    {
        return new self();
    }

    public static function create(
        string $email,
        string $password,
        string $role,
        string $userName,
        int    $cityId = null
    ) : self
    {
        if ($role === self::ROLE_GUEST || !in_array($role, self::ROLES)) {
            throw new \Exception('Bad role value');
        }

        $user = new self();

        return $user
            ->setEmail($email)
            ->setPassword($password)
            ->setRole($role)
            ->setUserName($userName)
            ->setCityId($cityId)
        ;
    }

    protected function __construct()
    {
        $this->_companyCollection = new DomainModelCollection([]);
    }

    public function getVkId(): ?int
    {
        return $this->_vkId;
    }

    public function setVkId(?int $vkId): self
    {
        $this->_vkId = $vkId;
        return $this;
    }

    public function getFbId(): ?string
    {
        return $this->_fbId;
    }

    public function setFbId(?int $fbId): self
    {
        $this->_fbId = $fbId;
        return $this;
    }

    public function getEmail() : ? string
    {
        return $this->_email;
    }

    public function getPassword() : ? string
    {
        return $this->_password;
    }

    public function setEmail(? string $email) : self
    {
        $this->_email = $email;
        return $this;
    }

    public function setPassword(? string $password) : self
    {
        $this->_password = $password;
        return $this;
    }

    public function setUserName(? string $userName) : self
    {
        $this->_userName = $userName;
        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->_userName;
    }

    public function isConfirmed(): bool
    {
        return $this->_isConfirmed;
    }

    public function setConfirmed(bool $isConfirmed): self
    {
        $this->_isConfirmed = $isConfirmed;
        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->_cityId;
    }

    public function getCity() : ? City
    {
        return $this->_cityId ? self::$ds->cityMapper()->fetch($this->_cityId) : null;
    }

    public function setCityId(?int $cityId): self
    {
        $this->_cityId = $cityId;
        return $this;
    }

    public function getRole() : string
    {
        return $this->_role;
    }

    public function setRole(string $role) : self
    {
        if (!in_array($role, self::ROLES)) {
            throw new \Exception('Invalid method parameter');
        }

        $this->_role = $role;
        return $this;
    }

    public function getLastVisitDate() : ? DateTime
    {
        return $this->_lastVisitDate;
    }

    public function setLastVisitDate(? DateTime $lastVisitDate) : self
    {
        $this->_lastVisitDate = $lastVisitDate;
        return $this;
    }

    public function getCreateDate(): ?DateTime
    {
        return $this->_createDate;
    }

    public function setCreateDate(DateTime $createDate): self
    {
        $this->_createDate = $createDate;
        return $this;
    }

    public function isAdmin() : bool
    {
        return $this->_role === self::ROLE_ADMIN;
    }

    public function isEmployee() : bool
    {
        return $this->_role === self::ROLE_EMPLOYEE;
    }

    public function isRecruiter() : bool
    {
        return $this->_role === self::ROLE_RECRUITER;
    }

    public function isGuest() : bool
    {
        return $this->_role === self::ROLE_GUEST;
    }

    public function getLocale(): string
    {
        return $this->_locale;
    }

    public function setLocale(string $locale): self
    {
        if (!in_array($locale, self::LOCALES)) {
            throw new \Exception('Unknown locale name');
        }

        $this->_locale = $locale;
        return $this;
    }

    public function setCompanies(DomainModelCollection $collection) : self
    {
        $this->_companyCollection = $collection;
        return $this;
    }

    public function getCompanies() : DomainModelCollection
    {
        return $this->_companyCollection;
    }
}