<?php
/**
 * This file was generated by Atlas. Changes will be overwritten.
 */
namespace Application\Model\Domain\Tag;

use Atlas\Orm\Mapper\Record;

/**
 * @property mixed $id INT(11) NOT NULL
 * @property mixed $name VARCHAR(128) NOT NULL
 */
class TagFields extends Record
{
}
