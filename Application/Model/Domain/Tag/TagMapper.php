<?php
namespace Application\Model\Domain\Tag;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Tag;
use Application\Model\Interfaces\DomainModelInterface;

/**
 * @method Tag         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Tag[]       fetchByIds(array $ids)
 * @method Tag[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Tag | null  fetchOne(array $where)
 */
class TagMapper extends AbstractMapper
{
    const COL_NAME = 'name';

    const COLS = [
        self::PRIMARY,
        self::COL_NAME
    ];

    /**
     * @param DomainModelInterface | Tag $obj
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_NAME => $obj->getName()
        ];
    }

    /**
     * @return DomainModelInterface | Tag
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Tag($row[self::COL_NAME]))->setId($row[self::PRIMARY]);
    }
}
