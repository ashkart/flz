<?php
namespace Application\Model\Domain\Tag;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class TagMapperEvents extends MapperEvents
{
}
