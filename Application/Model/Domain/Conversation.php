<?php

namespace Application\Model\Domain;

use Application\Model\Domain\Collection\DomainModelCollection;
use Library\Master\Date\DateTime;

class Conversation extends AbstractModel
{
    /**
     * @var DateTime
     */
    protected $_createDate;

    /**
     * @var int
     */
    protected $_creatorUserId;

    /**
     * @var DomainModelCollection
     */
    protected $_messages;

    public function __construct(int $creatorUserId)
    {
        $this->_creatorUserId = $creatorUserId;
    }

    public function getCreateDate(): DateTime
    {
        return $this->_createDate;
    }

    public function setCreateDate(DateTime $createDate): self
    {
        $this->_createDate = $createDate;
        return $this;
    }

    public function getCreatorUserId(): int
    {
        return $this->_creatorUserId;
    }

    public function getMessages(): DomainModelCollection
    {
        return $this->_messages;
    }

    public function setMessages(DomainModelCollection $messages): self
    {
        $this->_messages = $messages;
        return $this;
    }
}