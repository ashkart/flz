<?php

namespace Application\Model\Domain;

class ConversationUser extends AbstractModel
{
    /**
     * @var int
     */
    protected $_userId;

    /**
     * @var int
     */
    protected $_conversationId;

    public function __construct(int $userId, int $conversationId)
    {
        $this->_userId         = $userId;
        $this->_conversationId = $conversationId;
    }

    public function getUserId(): int
    {
        return $this->_userId;
    }

    public function getConversationId(): int
    {
        return $this->_conversationId;
    }
}