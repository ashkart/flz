<?php
namespace Application\Model\Domain\Conversation;

use Atlas\Orm\Table\RowInterface;
use Atlas\Orm\Table\TableEvents;
use Atlas\Orm\Table\TableInterface;
use Aura\SqlQuery\Common\Delete;
use Aura\SqlQuery\Common\Insert;
use Aura\SqlQuery\Common\Update;
use PDOStatement;

/**
 * @inheritdoc
 */
class ConversationTableEvents extends TableEvents
{
}
