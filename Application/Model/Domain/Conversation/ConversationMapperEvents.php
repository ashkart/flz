<?php
namespace Application\Model\Domain\Conversation;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class ConversationMapperEvents extends MapperEvents
{
}
