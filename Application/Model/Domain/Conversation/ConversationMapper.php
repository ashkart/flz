<?php
namespace Application\Model\Domain\Conversation;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Domain\Conversation;
use Application\Model\Domain\ConversationMessage\ConversationMessageMapper;
use Application\Model\Interfaces\DomainModelInterface;
use Atlas\Orm\Mapper\Record;
use Library\Master\Date\DateTime;

/**
 * @method Conversation         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Conversation[]       fetchByIds(array $ids)
 * @method Conversation[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method Conversation | null  fetchOne(array $where)
 */
class ConversationMapper extends AbstractMapper
{
    const COL_CREATOR_USER_ID = 'creator_user_id';
    const COL_CREATE_DATE     = 'create_date';

    const MESSAGES = 'messages';

    const COLS = [
        self::PRIMARY,
        self::COL_CREATOR_USER_ID
    ];

    const COLS_NOT_UPDATE = [
        self::COL_CREATE_DATE
    ];

    const COLLECTIONS = [
        self::MESSAGES
    ];

    /**
     * @param DomainModelInterface | Conversation $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_CREATE_DATE     => DateTime::createFromTimestamp(time())->mysqlFormat(),
            self::COL_CREATOR_USER_ID => $obj->getCreatorUserId()
        ];
    }

    /**
     * @param array $row
     * @return DomainModelInterface | Conversation
     */
    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Conversation($row[self::COL_CREATOR_USER_ID]))
            ->setCreateDate(DateTime::createFromMysqlFormat($row[self::COL_CREATE_DATE]))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | Conversation $obj
     * @return DomainModelInterface | Conversation
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        $messageModels = [];
        $messageRows   = $row[self::MESSAGES] ?? [];

        $messageMapper = self::$ds->conversationMessageMapper();

        foreach ($messageRows as $messageRow) {
            $messageModels[] = $messageMapper->createModel($messageMapper->newRecord($messageRow));
        }

        $messages = new DomainModelCollection($messageModels);

        return $obj->setMessages($messages);
    }

    protected function setRelated()
    {
        $this->oneToMany(self::MESSAGES, ConversationMessageMapper::class)
            ->on([self::PRIMARY => ConversationMessageMapper::COL_CONVERSATION_ID])
        ;
    }
}
