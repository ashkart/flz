<?php

namespace Application\Model\Domain\Vacancy;

use Application\Cache\Slot\UserVacancyIds;
use Application\Model\Domain\Callback;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Domain\VacancyLocation\VacancyLocationMapper;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Vacancy;
use Atlas\Orm\Table\Row;
use Library\Master\Date\DateTime;

/**
 * @method Vacancy         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method Vacancy[]       fetchByIds(array $ids, bool $useCache = true, bool $withRelated = false)
 * @method Vacancy | null  fetchOne(array $where)
 * @method Vacancy[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 */
class VacancyMapper extends AbstractMapper
{
    const COL_USER_ID             = 'user_id';
    const COL_COMPANY_ID          = 'company_id';
    const COL_POSITION_NAME       = 'position_name';
    const COL_PROFAREA_ID         = 'profarea_id';
    const COL_REQUIRED_EXPERIENCE = 'required_experience';
    const COL_REQUIREMENTS        = 'requirements';
    const COL_LIABILITIES         = 'liabilities';
    const COL_CONDITIONS          = 'conditions';
    const COL_DESCRIPTION         = 'description';
    const COL_EMPLOYMENT          = 'employment';
    const COL_IS_REMOTE           = 'is_remote';
    const COL_PAYCHECK_MIN        = 'paycheck_min';
    const COL_PAYCHECK_MAX        = 'paycheck_max';
    const COL_PAYCHECK_CONST      = 'paycheck_const';
    const COL_CURRENCY_ID         = 'currency_id';
    const COL_VISIBILITY          = 'visibility';
    const COL_IS_DELETED          = 'is_deleted';
    const COL_CREATE_DATE         = 'create_date';
    const COL_UPDATE_DATE         = 'update_date';

    const COLS = [
        self::PRIMARY,
        self::COL_USER_ID,
        self::COL_COMPANY_ID,
        self::COL_POSITION_NAME,
        self::COL_PROFAREA_ID,
        self::COL_REQUIRED_EXPERIENCE,
        self::COL_REQUIREMENTS,
        self::COL_LIABILITIES,
        self::COL_CONDITIONS,
        self::COL_DESCRIPTION,
        self::COL_EMPLOYMENT,
        self::COL_IS_REMOTE,
        self::COL_PAYCHECK_MIN,
        self::COL_PAYCHECK_MAX,
        self::COL_PAYCHECK_CONST,
        self::COL_CURRENCY_ID,
        self::COL_VISIBILITY,
        self::COL_IS_DELETED,
        self::COL_UPDATE_DATE,
        self::COL_CREATE_DATE
    ];

    const COLS_NOT_UPDATE = [
        self::COL_CREATE_DATE
    ];

    const CALLBACKS = 'callbacks';
    const LOCATIONS = 'locations';

    const COLLECTIONS = [
        self::LOCATIONS
    ];

    public function fetchByUserId(int $userId, bool $useCache = true, bool $withRelated = true)
    {
        if ($useCache) {
            $slot = new UserVacancyIds($userId);

            $userVacancyIds = $slot->load();

            if ($userVacancyIds) {
                return $this->fetchByIds($userVacancyIds, true, true);
            }
        }

        $rows = $this->select([self::COL_USER_ID => $userId])->cols([self::PRIMARY])->fetchAll();

        $userVacancyIds = [];

        foreach ($rows as $row) {
            $userVacancyIds[] = $row['id'];
        }

        if ($useCache) {
            $slot = new UserVacancyIds($userId);
            $slot->save($userVacancyIds);
        }

        return $this->fetchByIds($userVacancyIds, $useCache, $withRelated);
    }

    /**
     * @param DomainModelInterface | Vacancy $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        return [
            self::COL_USER_ID             => $obj->getUserId(),
            self::COL_COMPANY_ID          => $obj->getCompanyId(),
            self::COL_POSITION_NAME       => $obj->getPositionName(),
            self::COL_PROFAREA_ID         => $obj->getProfareaId(),
            self::COL_REQUIRED_EXPERIENCE => $obj->getRequiredExperience(),
            self::COL_REQUIREMENTS        => $obj->getRequirements(),
            self::COL_LIABILITIES         => $obj->getLiabilities(),
            self::COL_CONDITIONS          => $obj->getConditions(),
            self::COL_DESCRIPTION         => $obj->getDescription(),
            self::COL_EMPLOYMENT          => $obj->getEmployment(),
            self::COL_IS_REMOTE           => (int) $obj->isRemote(),
            self::COL_PAYCHECK_MIN        => $obj->getPaycheckMin(),
            self::COL_PAYCHECK_MAX        => $obj->getPaycheckMax(),
            self::COL_PAYCHECK_CONST      => $obj->getPaycheckConst(),
            self::COL_CURRENCY_ID         => $obj->getCurrencyId(),
            self::COL_VISIBILITY          => $obj->getVisibility(),
            self::COL_IS_DELETED          => (int) $obj->isDeleted(),
            self::COL_UPDATE_DATE         => (new DateTime())->mysqlFormat(),
        ];
    }

    protected function _getInstance(array $row): DomainModelInterface
    {
        return (new Vacancy(
            $row[self::COL_USER_ID],
            $row[self::COL_COMPANY_ID],
            $row[self::COL_POSITION_NAME],
            $row[self::COL_REQUIREMENTS],
            $row[self::COL_LIABILITIES]
        ))->setId($row[self::PRIMARY]);
    }

    /**
     * @param DomainModelInterface | Vacancy $obj
     *
     * @return DomainModelInterface | Vacancy
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        $updateDate = DateTime::createFromMysqlFormat($row[self::COL_UPDATE_DATE]);
        $createDate = DateTime::createFromMysqlFormat($row[self::COL_CREATE_DATE]);

        $locations = [];

        foreach ($row[self::LOCATIONS] as $vacancyLocationRow) {
            $locations[] = self::$ds->vacancyLocationMapper()->createModel(
                self::$ds->vacancyLocationMapper()->turnRowIntoRecord(new Row($vacancyLocationRow))
            );
        }

        return $obj
            ->setProfareaId($row[self::COL_PROFAREA_ID])
            ->setEmployment($row[self::COL_EMPLOYMENT])
            ->setConditions($row[self::COL_CONDITIONS])
            ->setDescription($row[self::COL_DESCRIPTION])
            ->setPaycheckMin($row[self::COL_PAYCHECK_MIN])
            ->setPaycheckMax($row[self::COL_PAYCHECK_MAX])
            ->setPaycheckConst($row[self::COL_PAYCHECK_CONST])
            ->setCurrencyId($row[self::COL_CURRENCY_ID])
            ->setRequiredExperience($row[self::COL_REQUIRED_EXPERIENCE])
            ->setVisibility($row[self::COL_VISIBILITY])
            ->setDeleted((int) $row[self::COL_IS_DELETED])
            ->setUpdateDate($updateDate)
            ->setCreateDate($createDate)
            ->setIsRemote($row[self::COL_IS_REMOTE])
            ->setLocations(new DomainModelCollection($locations))
        ;
    }

    protected function _createNewRowFromData(array $data): Row
    {
        unset($data[self::CALLBACKS]);

        return parent::_createNewRowFromData($data);
    }

    protected function setRelated()
    {
        $this->oneToMany(self::CALLBACKS, Callback\CallbackMapper::class)
            ->on([self::PRIMARY => Callback\CallbackMapper::COL_ENTITY_ID])
            ->where(Callback\CallbackMapper::COL_ENTITY_TYPE . ' = ?', Callback::ENTITY_TYPE_VACANCY)
        ;

        $this->oneToMany(self::LOCATIONS, VacancyLocationMapper::class)
            ->on([self::PRIMARY => VacancyLocationMapper::COL_VACANCY_ID])
        ;
    }

    protected function _getClearCacheDependencies(?DomainModelInterface $oldObj, ?DomainModelInterface $newObj): array
    {
        $dependencies = [];

        $dependencies[] = self::_createCacheDependency(
            $oldObj,
            $newObj,
            function (Vacancy $vacancy) : ?int {
                return $vacancy->getId();
            },
            function (Vacancy $vacancy) {
                return [
                    new UserVacancyIds($vacancy->getUserId())
                ];
            }
        );

        return $dependencies;
    }
}
