<?php

namespace Application\Model\Domain\User;

use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Domain\User;
use Application\Model\Domain\UserCompany\UserCompanyMapper;
use Library\Master\Date\Date;
use Library\Master\Date\DateTime;

/**
 * @method User         fetch(int $id, bool $useCache = true, bool $withRelated = true)
 * @method User[]       fetchByIds(array $ids)
 * @method User[]       fetchAll(array $where, bool $useCache = false, bool $withRelated = false)
 * @method User | null  fetchOne(array $where)
 */
class UserMapper extends AbstractMapper
{
    const COL_VK_ID           = 'vk_id';
    const COL_FB_ID           = 'fb_id';
    const COL_EMAIL           = 'email';
    const COL_PASSWORD        = 'password';
    const COL_ROLE            = 'role';
    const COL_USER_NAME       = 'user_name';
    const COL_IS_CONFIRMED    = 'is_confirmed';
    const COL_CITY_ID         = 'city_id';
    const COL_LOCALE          = 'locale';
    const COL_LAST_VISIT_DATE = 'last_visit_date';
    const COL_CREATE_DATE     = 'create_date';

    const COLS = [
        self::PRIMARY,
        self::COL_VK_ID,
        self::COL_FB_ID,
        self::COL_EMAIL,
        self::COL_PASSWORD,
        self::COL_ROLE,
        self::COL_USER_NAME,
        self::COL_IS_CONFIRMED,
        self::COL_CITY_ID,
        self::COL_LOCALE,
        self::COL_LAST_VISIT_DATE,
        self::COL_CREATE_DATE
    ];

    const COLS_NOT_UPDATE = [
        self::COL_CREATE_DATE
    ];

    const COLLECTION_COMPANIES = 'companies';

    const COLLECTIONS = [
        self::COLLECTION_COMPANIES
    ];

    public function fetchByEmail(string $email) : ? User
    {
        return $this->fetchOne([
            self::COL_EMAIL => $email
        ]);
    }

    public function fetchByVkId(int $vkId) : ? User
    {
        return $this->fetchOne([
            self::COL_VK_ID => $vkId
        ]);
    }

    public function fetchByFbId(int $fbId) : ? User
    {
        return $this->fetchOne([
            self::COL_FB_ID => $fbId
        ]);
    }

    /**
     * @param array $row
     * @return DomainModelInterface | User
     */
    protected function _getInstance(array $row) : DomainModelInterface
    {
        return (User::create(
            $row[self::COL_EMAIL],
            $row[self::COL_PASSWORD],
            $row[self::COL_ROLE],
            $row[self::COL_USER_NAME],
            $row[self::COL_CITY_ID]
        ))
            ->setId($row[self::PRIMARY])
        ;
    }

    /**
     * @param DomainModelInterface | User $obj
     * @param array $row
     */
    protected function _loadLine(DomainModelInterface $obj, array $row): DomainModelInterface
    {
        if ($row[self::COL_LAST_VISIT_DATE] !== null) {
            $obj->setLastVisitDate(DateTime::createFromMysqlFormat($row[self::COL_LAST_VISIT_DATE]));
        }

        $companies = [];

        foreach ($row[self::COLLECTION_COMPANIES] as $userCompany) {
            $companies[] = self::$ds->companyMapper()->fetch($userCompany[UserCompanyMapper::COL_COMPANY_ID]);
        }

        $obj
            ->setConfirmed($row[self::COL_IS_CONFIRMED])
            ->setCompanies(new DomainModelCollection($companies))
            ->setLocale($row[self::COL_LOCALE])
            ->setVkId($row[self::COL_VK_ID])
            ->setFbId($row[self::COL_FB_ID])
        ;

        return $obj;
    }

    /**
     * @param DomainModelInterface | User $obj
     * @return array
     */
    protected function _getDataForSaving(DomainModelInterface $obj): array
    {
        if ($obj->getRole() === User::ROLE_GUEST) {
            throw new \Exception('Registered user role can not be "guest"');
        }

        $lastVisitDate = $obj->getLastVisitDate() ? $obj->getLastVisitDate()->mysqlFormat() : null;

        return [
            self::COL_VK_ID           => $obj->getVkId(),
            self::COL_FB_ID           => $obj->getFbId(),
            self::COL_EMAIL           => $obj->getEmail(),
            self::COL_PASSWORD        => $obj->getPassword(),
            self::COL_ROLE            => $obj->getRole(),
            self::COL_USER_NAME       => $obj->getUserName(),
            self::COL_IS_CONFIRMED    => (int) $obj->isConfirmed(),
            self::COL_CITY_ID         => $obj->getCityId(),
            self::COL_LOCALE          => $obj->getLocale(),
            self::COL_LAST_VISIT_DATE => $lastVisitDate
        ];
    }

    protected function setRelated()
    {
        $this->oneToMany(self::COLLECTION_COMPANIES, UserCompanyMapper::class)->on([
            self::PRIMARY => UserCompanyMapper::COL_USER_ID
        ]);
    }
}
