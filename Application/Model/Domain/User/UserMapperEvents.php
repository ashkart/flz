<?php
namespace Application\Model\Domain\User;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class UserMapperEvents extends MapperEvents
{
}
