<?php

namespace Application\Model\Service;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\Contact;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;

class Mail extends AbstractService
{
    const SUBJECT_PASSWORD_CHANGE_CONFIRM = \Main\View\Helper\Contact::BRAND_NAME . ' Подтверждение смены пароля';

    /**
     * @var Smtp
     */
    protected $_mailTransport;

    /**
     * @var string
     */
    protected $_defaultFromMail;

    /**
     * @var string
     */
    protected $_defaultFromName;

    /**
     * @var bool
     */
    protected $_sendToUsers = false;

    /**
     * @var string | null
     */
    protected $_developmentMail = null;

    public function __construct()
    {
        parent::__construct();

        $this->_mailTransport = self::$sm->get(Factory::MAIL);

        $mailConfig = self::$sm->get(Factory::CONFIG)['mail'];

        $this->_sendToUsers     = $mailConfig['sendToUsers'];
        $this->_developmentMail = $mailConfig['developmentMail'];

        $this->_defaultFromMail = self::$sm->get(Factory::CONFIG)['mail']['fromMail'];
        $this->_defaultFromName = self::$sm->get(Factory::CONFIG)['mail']['fromName'];
    }

    public function sendEmail(Message $message)
    {
        $this->_mailTransport->send($message);
    }

    public function createMessage(string $subject, string $to) : Message
    {
        $message = new Message();

        if (!$this->_sendToUsers) {
            if ($this->_developmentMail) {
                $message->addTo($this->_developmentMail);
            }
        } else {
            $message->addTo($to);
        }

        return $message
            ->addFrom($this->_defaultFromMail, $this->_defaultFromName)
            ->setSubject($subject)
        ;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}