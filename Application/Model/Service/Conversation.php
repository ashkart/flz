<?php

namespace Application\Model\Service;

use Application\Cache\Slot\Slot;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain;

class Conversation extends AbstractService
{
    public function addMessage(Domain\Conversation $conversation, Domain\User $fromUser, string $text)
    {
        $message = new Domain\ConversationMessage($conversation->getId(), $fromUser->getId(), $text);

        self::$ds->conversationMessageMapper()->save($message);

        $messages = $conversation->getMessages();
        $messages->push($message);

        /*
         * Сброс кеша беседы, чтобы при следующем обращении к ней подцепились новые сообщения.
         */
        $conversationSlot = new Slot($conversation->getId(), get_class(self::$ds->conversationMapper()));

        if ($conversationSlot->load()) {
            $conversationSlot->delete();
        }
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}