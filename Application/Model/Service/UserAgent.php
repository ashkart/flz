<?php

namespace Application\Model\Service;

use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Library\Master\CrawlerDetector\CrawlerDetector;
use Zend\Http\PhpEnvironment\RemoteAddress;

class UserAgent extends AbstractService
{
    const KEY_HTTP_USER_AGENT = 'HTTP_USER_AGENT';

    /**
     * @var CrawlerDetector
     */
    protected $_crawlerDetector;

    public function __construct()
    {
        parent::__construct();

        $this->_crawlerDetector = new CrawlerDetector();
    }

    public function getIp(): ?string
    {
        $remoteAddr = new RemoteAddress();

        return $remoteAddr->getIpAddress();
    }

    public function isRobot()
    {
        return $this->_crawlerDetector->isCrawler($_SERVER[self::KEY_HTTP_USER_AGENT]);
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        // Не нужно в этом сервисе
        throw new \Exception('Not implemented');
    }
}