<?php

namespace Application\Model\Service;

use Application\Helper\DataSource\TraitIdentity;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\AbstractModel;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Atlas;
use Atlas\Orm\Mapper\MapperSelect;
use Atlas\Orm\Table\Row;
use Library\Master\Date\Date;
use Main\Module;

abstract class AbstractService
{
    const KEY_TOTAL_ITEMS = 'totalItems';

    protected const MIN_SHORTEN_LENGTH = 300;
    protected const MAX_SHORTEN_LENGTH = 400;

    use TraitIdentity;

    /**
     * @var Atlas | null
     */
    protected $_db = null;

    public function __construct()
    {
        $this->_db = self::$sm->get(Factory::DB_ADAPTER);
    }

    public function getList(AbstractFilter $filter, bool $withCount = false)
    {
        $select = $this->_prepareSelect($filter);

        $result = $this->_query($select);

        if ($withCount) {
            $count = $this->count($select);

            $result[self::KEY_TOTAL_ITEMS] = $count;
        }

        return $result;
    }

    public function count(MapperSelect $select) : int
    {
        return $select->fetchCount();
    }

    public function purgeHtml(string $source) : string
    {
        return preg_replace("/(\<\/?\w+(\s\w+\=(\".+\"))*\>)*(\&nbsp\;)*/", '', $source);
    }

    protected function _shortenLongText(
        string $source,
        int $minLength = self::MIN_SHORTEN_LENGTH,
        int $maxLength = self::MAX_SHORTEN_LENGTH
    ) : string
    {
        if (mb_strlen($source) <= $maxLength) {
            return $source;
        }

        $statements = explode("\n", $source);

        $result = '';

        foreach ($statements as $statement) {
            $result = $result ? "$result $statement" : $statement;

            $currentLength = mb_strlen($result);

            if ($currentLength < $minLength) {
                continue;
            }

            if ($currentLength <= $maxLength) {
                break;
            } else {
                $result = mb_substr($result, 0, $minLength - 3);
                $result = "$result...";
                break;
            }
        }

        return $result;
    }

    protected function _setEntityId(
        array $collectionsRows,
        int $id,
        string $idKey,
        string $entityTypeKey = null,
        string $entityType = null
    ) : array
    {
        foreach ($collectionsRows as & $row) {
            $row[$idKey] = $id;

            if ($entityTypeKey) {
                $row[$entityTypeKey] = $entityType;
            }
        }

        return $collectionsRows;
    }

    protected function _buildKeywordQueryString(string $filterKeywords) : string
    {
        $keywordsNoExtraWS = str_replace(', ', ',', $filterKeywords);
        $keywordsStrictGroups = explode(' ', $keywordsNoExtraWS);

        $keywordsStmtTotal = '';

        foreach ($keywordsStrictGroups as $keywordsStrictGrp) {
            $keywordStmtOneOf = '';

            foreach (explode(',', $keywordsStrictGrp) as $keywordOneOf) {
                $keywordStmtOneOf = $this->_concatenateQueryParts($keywordStmtOneOf, $keywordOneOf, 'OR');
            }

            if ($keywordStmtOneOf) {
                $keywordStmtOneOf = "($keywordStmtOneOf)";
            }

            $keywordsStmtTotal = $this->_concatenateQueryParts($keywordsStmtTotal, ($keywordStmtOneOf ?: $keywordsStrictGrp), 'AND');
        }

        return $keywordsStmtTotal;
    }

    protected function _concatenateQueryParts(string $leftPart, string $rightPart, string $orAndCondition = '') : string
    {
        return $leftPart . ($leftPart ? " $orAndCondition " : '') . $rightPart;
    }

    protected function _updateCollection(DomainModelCollection $currentCollection, DomainModelCollection $newCollection, AbstractMapper $mapper)
    {
        $collToSave = $newCollection->diff($currentCollection);
        $collToDelete = $currentCollection->getAbsentIn($newCollection);

        /** @var AbstractModel $model */
        foreach ($collToDelete->asArray() as $model) {
            $mapper->deleteByIds([$model->getId()]);
        }

        foreach ($collToSave->asArray() as $model) {
            $mapper->save($model);
        }
    }

    protected function _createNewCollectionFromDataRows(array $newCollectionRows, AbstractMapper $mapper) : DomainModelCollection
    {
        $newCollModels = [];

        foreach ($newCollectionRows as $fieldsRow) {
            $newCollModels[] = $mapper->createModel($mapper->newRecord($fieldsRow));
        }

        return new DomainModelCollection($newCollModels);
    }

    protected function _reformatRowsDateFields(array & $rows, array $dateFields, string $outFormat)
    {
        foreach ($rows as & $row) {
            foreach ($dateFields as $dateField) {
                $row[$dateField] = $row[$dateField] ? Date::reformat($row[$dateField], $outFormat) : $row[$dateField];
            }
        }
    }

    protected function _logException(\Exception $e)
    {
        if (Module::isCli()) {
            /* @var \Zend\Log\Logger $logger */
            $logger = self::$sm->get(Factory::LOGGER)->getLogger();
            $logger->err((string) $e);
        } else {
            self::$sm->get(Factory::ERROR_HANDLER)->logException($e, self::$sm->get(Factory::REQUEST));
        }
    }

    protected function _setPaging(AbstractFilter $filter, MapperSelect $select) : MapperSelect
    {
        $select
            ->setPaging($filter->getItemsPerPage() ?? $filter::PER_PAGE_DEFAULT)
            ->page($filter->getPageNumber() ?? $filter::PAGE_DEFAULT)
        ;

        return $select;
    }

    protected function _query(MapperSelect $select) : array
    {
        return $select->fetchRecordSet()->getArrayCopy();
    }

    abstract protected function _prepareSelect(AbstractFilter $filter) : MapperSelect;
}