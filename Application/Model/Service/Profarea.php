<?php

namespace Application\Model\Service;

use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain;

class Profarea extends AbstractService
{
    protected const KEY_ID        = 'id';
    protected const KEY_PARENT_ID = 'parent_id';
    protected const KEY_LABEL     = 'label';
    protected const KEY_CHILDREN  = 'children';

    public function buildViewDataFromIndexed(array $indexData) : array
    {
        $result = [];

        foreach ($indexData as $profareaDocument) {
            $docSource = $profareaDocument['_source'];
            $docId = $docSource['id'];

            $result[$docId] = [
                'id' => $docId,
                'label' => $docSource['title'],
                'parent_id' => $docSource['parent_id'],
            ];
        }

        return $result;
    }

    public function buildViewTree() : array
    {
        $profareas = self::$ds->profareaMapper()->fetchRootWithCollections();

        $profareaTree = [];

        foreach ($profareas as $profarea) {
            $profareaTree[$profarea->getId()] = $this->_buildViewTree($profarea);
        }

        return $profareaTree;
    }

    protected function _buildViewTree(Domain\Profarea $profarea) : array
    {
        $childNodes = [];

        /**
         * @var Domain\Profarea $profareaChild
         */
        foreach ($profarea->getChildren()->asArray() as $profareaChild) {
            $childNodes[$profareaChild->getId()] = $this->_buildViewTree($profareaChild);
        }

        $node = [
            self::KEY_ID => $profarea->getId(),
            self::KEY_PARENT_ID => $profarea->getParentId(),
            self::KEY_LABEL => $profarea->getTitle()
        ];

        if ($childNodes) {
            $node[self::KEY_CHILDREN] = $childNodes;
        }

        return $node;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}