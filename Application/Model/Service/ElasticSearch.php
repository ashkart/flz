<?php

namespace Application\Model\Service;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;

class ElasticSearch extends AbstractService
{
    const RESERVED_SYMBOLS      = '+ - = & | > < ! ( ) { } [ ] ^ " ~ * ? : \ /';

    const INDEX_COUNTRY         = 'country';
    const INDEX_REGION          = 'region';
    const INDEX_CITY            = 'city';
    const INDEX_COMPANY         = 'company';
    const INDEX_PROFAREA        = 'profarea';
    const INDEX_VACANCY         = 'vacancy';
    const INDEX_RESUME          = 'resume';
    const INDEX_TAG             = 'tag';
    const INDEX_VIEW            = 'view';

    const INDEXES = [
        self::INDEX_COUNTRY,
        self::INDEX_REGION,
        self::INDEX_CITY,
        self::INDEX_COMPANY,
        self::INDEX_PROFAREA,
        self::INDEX_VACANCY,
        self::INDEX_RESUME,
        self::INDEX_TAG,
        self::INDEX_VIEW,
    ];

    const LOCALITY_INDEX_TYPES = [
        self::INDEX_COUNTRY,
        self::INDEX_REGION,
        self::INDEX_CITY,
    ];

    const DEFAULT_FROM_PAGE = 0;
    const DEFAULT_SIZE      = 10;

    const INDEX_DEFAULT_SETTINGS = 'default_settings';

    /**
     * @var \Elasticsearch\Client
     */
    protected $_esClient;

    public function __construct()
    {
        parent::__construct();

        $this->_esClient = self::$sm->get(Factory::ELASTICSEARCH_ADAPTER);
    }

    public function createIndex(string $indexName, array $settingsAndMappings = []) : array
    {
        $params = [
            'index' => $indexName,
            'body'  => $settingsAndMappings,
        ];

        $result = $this->_esClient->indices()->create($params);

        return $result;
    }

    public function checkIndexExists(string $indexName) : bool
    {
        return $this->_esClient->indices()->exists(['index' => $indexName]);
    }

    public function indexDocument(string $indexName, array $data) : array
    {
        $params = [
            'index' => $indexName,
            'type'  => $indexName,
            'body'  => $data
        ];

        return $this->_esClient->index($params);
    }

    public function updateDoc(string $indexName, array $data)
    {
        $currentIndexed = $this->queryByIds($indexName, [$data['id']]);

        $esId = $currentIndexed[0]['_id'];

        $updatedDoc = $this->_esClient->update([
            'index' => $indexName,
            'type'  => $indexName,
            'id'    => $esId,
            'body'  => [
                'doc' => $data
            ]
        ]);

        return $updatedDoc;
    }

    public function deleteDocument(string $indexName, string $id)
    {
        $currentIndexed = $this->queryByIds($indexName, [$id]);

        $esId = $currentIndexed[0]['_id'];

        $this->_esClient->delete([
            'index' => $indexName,
            'type'  => $indexName,
            'id'    => $esId,
        ]);
    }

    public function bulkIndexDocuments(array $params)
    {
        return $this->_esClient->bulk($params);
    }

    public function bulkIndexModels(
        string $indexName,
        \Closure $createBulkParams,
        bool $deleteIfExists = false
    ) : array
    {
        static $index = null;

        if ($deleteIfExists && $index !== $indexName && $this->isIndexExists($indexName)) {
            $this->deleteIndex($indexName);
        }

        if (!$this->isIndexExists($indexName) || $deleteIfExists && $index !== $indexName) {
            $index = $indexName;
            $elasticSearchConfig = self::$sm->get(Factory::CONFIG)['elasticsearch'];
            $indexConfig         = $elasticSearchConfig['indexes'][$indexName];
            if (!isset($indexConfig['settings'])) {
                $indexConfig['settings'] = $elasticSearchConfig[self::INDEX_DEFAULT_SETTINGS];
            }

            $this->createIndex($indexName, $indexConfig);
        }

        $params = $createBulkParams();

        if (!$params) {
            return [];
        }

        return $this->bulkIndexDocuments($params);
    }

    public function analyze(string $indexName, string $text, array $options = []) : array
    {
        $params = [
            'index' => $indexName,
            'body' => array_merge_recursive(['text' => $text], $options)
        ];

        return $this->_esClient->indices()->analyze($params);
    }

    public function queryMatch(string $indexName, array $queryMatch) : array
    {
        $params = [
            'index' => $indexName,
            'type'  => $indexName,
            'body'  => [
                'query' => [
                    'match' => $queryMatch
                ],
            ]
        ];

        $results = $this->_esClient->search($params);

        return $results['hits']['hits'] ?? [];
    }

    public function multiMatch(string $indexName, string $queryString, array $fields) : array
    {
        $params = [
            'index' => $indexName,
            'type'  => $indexName,
            'body'  => [
                'query' => [
                    'multi_match' => [
                        'query' => $queryString,
                        'fields' => $fields
                    ]
                ],
            ]
        ];

        $searchResult = $this->_esClient->search($params);

        return $searchResult['hits']['hits'] ?? [];
    }

    /**
     * @return [['key' => field value, doc_count => (int) count]]
     */
    public function queryDistinct(string $indexName, string $distinctField, string $queryString, array $fields) : array
    {
        $params = [
            'index' => $indexName,
            'type'  => $indexName,
            'body'  => [
                'query' => [
                    'multi_match' => [
                        'query' => $queryString,
                        'fields' => $fields
                    ]
                ],
                'aggs' => [
                    $distinctField => [
                        'significant_text' => [
                            'field' => "$distinctField.keyword",
                            "min_doc_count" => 1
                        ]
                    ]
                ]
            ]
        ];

        $searchResult = $this->_esClient->search($params);

        return $searchResult['aggregations'][$distinctField]['buckets'];
    }

    public function query(
        string $indexName,
        array $query,
        int $fromPageNumber = null,
        int $size = null,
        array $sort = []
    )
    {
        $params = [
            'index' => $indexName,
            'type'  => $indexName,
            'body'  => [
                'query' => $query
            ]
        ];

        if ($size) {
            $params['body']['size'] = $size;
            $params['body']['from'] = $fromPageNumber * $size;
        }

        if ($sort) {
            $params['body']['sort'] = $sort;
        }

        $results = $this->_esClient->search($params);

        return $results['hits'] ?? [];
    }

    public function customQuery(string $indexName, array $body)
    {
        $params = [
            'index' => $indexName,
            'type'  => $indexName,
            'body'  => $body
        ];

        return $this->_esClient->search($params);
    }

    public function queryByIds(string $indexName, array $ids) : array
    {
        $query = [
            'terms' => [
                'id' => $ids
            ]
        ];

        return $this->query($indexName, $query)['hits'] ?? [];
    }

    public function getIndex(string $name) : array
    {
        $params = [
            'index' => $name
        ];

        $response = $this->_esClient->indices()->get($params);

        return $response;
    }

    public function isIndexExists(string $indexName) : bool
    {
        return $this->_esClient->indices()->exists(['index' => $indexName]);
    }

    public function deleteIndex(string $name) : array
    {
        $params = [
            'index' => $name
        ];

        $response = $this->_esClient->indices()->delete($params);

        return $response;
    }

    public function putSettings(string $indexName, array $settings) : array
    {
        $params = [
            'index' => $indexName,
            'body' => [
                'settings' => $settings
            ]
        ];

        $index = ['index' => $indexName];

        $this->_esClient->indices()->close($index);
        $putResult = $this->_esClient->indices()->putSettings($params);
        $this->_esClient->indices()->open($index);

        return $putResult;
    }

    public function reindex(string $indexFrom, string $indexTo, array $options = []) : array
    {
        $params = [
            'source' => [
                'index' => $indexFrom
            ],
            'dest' => [
                'index' => $indexTo
            ]
        ];

        return $this->_esClient->reindex(array_merge_recursive($params, $options));
    }

    public function cleanUpReservedSymbols(string $queryString) : string
    {
        $symbols = join('\\', explode(' ', self::RESERVED_SYMBOLS));

        $result = preg_replace("[\\$symbols]", ' ', $queryString);

        return $result;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        // не нужно в этом сервисе
        throw new \Exception('Not implemented');
    }
}