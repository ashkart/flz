<?php


namespace Application\Model\Service;


use Application\Helper\ServiceManager\Factory;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Library\Master\SocialNet\Vk\ApiClient as VkApiClient;
use Library\Master\SocialNet\Facebook\ApiClient as FbApiClient;

class SocialNetwork extends AbstractService
{
    /**
     * @var VkApiClient
     */
    protected $_vkApiClient;

    /**
     * @var FbApiClient
     */
    protected $_fbApiClient;

    /**
     * @var bool
     */
    protected $_publishVacanciesVk = false;

    /**
     * @var bool
     */
    protected $_publishVacanciesFb = false;

    public function __construct()
    {
        parent::__construct();

        $integrationsConfig = self::$sm->get(Factory::CONFIG)['integrations'];
        $vkApiConfig        = $integrationsConfig['vkApi'];
        $fbApiConfig        = $integrationsConfig['facebookApi'];

        $this->_vkApiClient = new VkApiClient(
            $vkApiConfig['owner_id'],
            $vkApiConfig['access_token'],
            $vkApiConfig['language']
        );

        $this->_publishVacanciesVk = $vkApiConfig['publishVacancies'];

        $this->_fbApiClient = new FbApiClient(
            $fbApiConfig['appId'],
            $fbApiConfig['groupId'],
            $fbApiConfig['appSecret'],
            $fbApiConfig['access_token_long_life'] ?? $fbApiConfig['access_token']
        );

        $this->_publishVacanciesFb = $fbApiConfig['publishVacancies'];
    }

    public function shouldPublishVacanciesVk() : bool
    {
        return $this->_publishVacanciesVk;
    }

    public function shouldPublishVacanciesFb() : bool
    {
        return $this->_publishVacanciesFb;
    }

    public function postWallMessageVk(string $message, string $snippetUrl = null)
    {
        $this->_vkApiClient->wallPost($message, $snippetUrl);
    }

    public function postWallMessageFb(string $message, string $snippetUrl = null)
    {
        $this->_fbApiClient->wallPost($message, $snippetUrl);
    }

    public function postWallMessages(string $message, string $snippetUrl = null)
    {
        if ($this->_publishVacanciesVk) {
            $this->postWallMessageVk($message, $snippetUrl);
        }

        if ($this->_publishVacanciesFb) {
            $this->postWallMessageFb($message, $snippetUrl);
        }
    }

    public function vkGetUserInfo(int $userVkId, string $userAccessToken)
    {
        return $this->_vkApiClient->getUsersInfo([$userVkId], $userAccessToken)[0];
    }

    public function fbCodeAccessTokenExchange(string $code, string $redirectUri) : string
    {
        return $this->_fbApiClient->codeAccessTokenExchange($code, $redirectUri);
    }

    public function fbGetUserInfo(string $userAccessToken)
    {
        return $this->_fbApiClient->getUserInfo($userAccessToken);
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}