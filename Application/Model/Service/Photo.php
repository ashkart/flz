<?php

namespace Application\Model\Service;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;

class Photo extends AbstractService
{
    protected const DIR_IMG = 'img';

    protected const DIR_MINOR_PHOTO = 'photo';

    protected const DIR_MAJOR_PROTECTED = 'protected';
    protected const DIR_MAJOR_PUBLIC    = 'public';

    protected const SUBDIR_LEN = 2;

    protected const ACCESS_FULL = 0777;

    /**
     * @var string
     */
    protected $_defaultFormat;

    /**
     * @var array
     */
    protected $_optionalFormats = [];

    /**
     * @var int
     */
    protected $_quality;

    /**
     * @var int
     */
    protected $_width;

    /**
     * @var int
     */
    protected $_height;

    public function __construct()
    {
        parent::__construct();

        $config      = self::$sm->get(Factory::CONFIG);
        $photoConfig = $config['photo'];

        $this->_defaultFormat   = $photoConfig['formats']['default'] ?? '.jpg';
        $this->_optionalFormats = $photoConfig['formats']['optional'] ?? [];
        $this->_width           = $photoConfig['file_size']['width'];
        $this->_height          = $photoConfig['file_size']['height'];
        $this->_quality         = $photoConfig['quality'];
    }

    public function generateFullPath(string $base64Image) : string
    {
        $name = md5($base64Image) . $this->_defaultFormat;
        $dir  = substr($name, 0, self::SUBDIR_LEN) . '/' . substr($name, self::SUBDIR_LEN, self::SUBDIR_LEN);

        $dir = '/'
            . self::DIR_IMG
            . '/'
            . self::DIR_MINOR_PHOTO
            . '/'
            . $dir
            . '/'
            . $name
        ;

        return $dir;
    }

    public function saveFile(string $imageDataB64, string $relativeFileName, bool $isProtected = false)
    {
        $gmagick = new \Gmagick();
        $blob = base64_decode(explode(',', $imageDataB64)[1]);
        $gmagick->readimageblob($blob);

        $width = $gmagick->getimagewidth();
        $height = $gmagick->getimageheight();

        if ($width >= $height) {
            $width  = $this->_width;
            $height = round($width / $gmagick->getimagewidth() * $gmagick->getimageheight(), 0, PHP_ROUND_HALF_UP);
        } else {
            $height = $this->_height;
            $width  = round($height / $gmagick->getimageheight() * $gmagick->getimagewidth(), 0, PHP_ROUND_HALF_UP);
        }

        $gmagick->resampleimage(
            round(100 / $gmagick->getimagewidth() * $width, 0, PHP_ROUND_HALF_UP),
            round(100 / $gmagick->getimageheight() * $height, 0, PHP_ROUND_HALF_UP),
            0,
            1
        );

        $gmagick->setCompressionQuality($this->_quality);
        $gmagick->resizeimage($width, $height, \Gmagick::FILTER_UNDEFINED, 1);
        $gmagick->stripimage();

        if ($isProtected) {
            $parentFolder = self::DIR_MAJOR_PROTECTED;
        } else {
            $parentFolder = self::DIR_MAJOR_PUBLIC;
        }

        $filename = ROOT_PATH . "/{$parentFolder}{$relativeFileName}";

        $dirName = pathinfo($filename)['dirname'];

        if (!is_dir($dirName)) {
            mkdir($dirName, self::ACCESS_FULL, true);
        }

        $gmagick->write($filename);

        foreach ($this->_optionalFormats as $format) {
            $gmagick->write($filename . $format);
        }

        $gmagick->destroy();
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}