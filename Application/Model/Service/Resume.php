<?php

namespace Application\Model\Service;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\Education\EducationMapper;
use Application\Model\Domain\Resume\ResumeMapper;
use Application\Model\Domain\WorkExperience;
use Application\Model\Domain\WorkExperience\WorkExperienceMapper;
use Application\Model\Domain\LanguageProficiency\LanguageProficiencyMapper;
use Application\Model\Domain\EntityTag\EntityTagMapper;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\ResumeFilter;
use Application\Model\Service\ElasticSearch as ESService;
use Application\Model\Domain;
use Application\View\PhpRenderer;
use Atlas\Orm\Mapper\MapperSelect;

use Library\Master\Date\Date;
use Main\View\Form;
use Main\View\Helper\Plural;
use Zend\Mvc\View\Http\ViewManager;

class Resume extends AbstractService
{
    protected const MIN_DAYS_IN_MONTH = 28;
    protected const MONTHS_PER_YEAR   = 12;

    public function create(array $formData): ? Domain\Resume
    {
        $cityId       = $formData[Form\Resume::EL_CITY_ID];
        $positionName = $formData[Form\Resume::EL_POSITION_NAME];
        $firstName    = $formData[Form\Resume::EL_FIRST_NAME];
        $lastName     = $formData[Form\Resume::EL_LAST_NAME];
        $birthDate    = Date::createFromFormat(Date::MYSQL_FORMAT, $formData[Form\Resume::EL_BIRTH_DATE]);

        $resume = new Domain\Resume($this->_user()->getId(), $cityId, $positionName, $firstName, $lastName, $birthDate);
        $this->_setResumeFields($resume, $formData);

        self::$ds->resumeMapper()->save($resume);

        if (!$this->update($resume, $formData, true)) {
            return null;
        }

        return $resume;
    }

    public function update(Domain\Resume $resume, array $formData, bool $collectionsOnly = false) : bool
    {
        $oldEducations = $resume->getEducations();
        $oldWorkExps   = $resume->getWorkExperiences();
        $oldLanguages  = $resume->getLanguages();
        $oldContacts   = $resume->getContacts();
        $oldTags       = $resume->getTags();

        $oldPhotoPath = $resume->getPhotoPath();

        if (!$collectionsOnly) {
            $this->_setResumeFields($resume, $formData);
        }

        $resumeMapper = self::$ds->resumeMapper();

        $educations = $this->_setEntityId(
            $formData[Form\Resume::EL_EDUCATION_COLLECTION],
            $resume->getId(),
            EducationMapper::COL_RESUME_ID
        );

        $workExperiences = $this->_setEntityId(
            $formData[Form\Resume::EL_WORK_EXPERIENCE_COLLECTION],
            $resume->getId(),
            WorkExperienceMapper::COL_RESUME_ID
        );

        $languages = $this->_setEntityId(
            $formData[Form\Resume::EL_LANGUAGES_COLLECTION],
            $resume->getId(),
            LanguageProficiencyMapper::COL_RESUME_ID
        );

        $tagsData = $this->_setEntityId(
            $formData[Form\Resume::EL_TAGS],
            $resume->getId(),
            EntityTagMapper::COL_ENTITY_ID,
            EntityTagMapper::COL_ENTITY_TYPE,
            'resume'
        );

        $contacts = $this->_makeContactsRows($formData[Form\Resume::EL_CONTACT_COLLECTION], $resume->getId());

        $newEducations = $this->_createNewCollectionFromDataRows($educations, self::$ds->educationMapper());
        $newWorkExps   = $this->_createNewCollectionFromDataRows($workExperiences, self::$ds->workExperienceMapper());
        $newLanguages  = $this->_createNewCollectionFromDataRows($languages, self::$ds->langProficiencyMapper());
        $newContacts   = $this->_createNewCollectionFromDataRows($contacts, self::$ds->contactMapper());

        try {
            $transactionName = 'resumeSave_' . microtime();

            $resumeMapper->beginTransaction($transactionName);

            $newTags = self::$ds->entityTagService()->createTagCollection($tagsData);

            $this->_updateCollection($oldEducations, $newEducations, self::$ds->educationMapper());
            $this->_updateCollection($oldWorkExps, $newWorkExps, self::$ds->workExperienceMapper());
            $this->_updateCollection($oldLanguages, $newLanguages, self::$ds->langProficiencyMapper());
            $this->_updateCollection($oldContacts, $newContacts, self::$ds->contactMapper());
            $this->_updateCollection($oldTags, $newTags, self::$ds->entityTagMapper());

            if ($resume->getPhotoPath() && $oldPhotoPath !== $resume->getPhotoPath()) {
                self::$ds->photoService()->saveFile($formData[Form\Resume::EL_PHOTO], $resume->getPhotoPath());
            }

            $resumeMapper->save($resume);

            $resumeMapper->commit($transactionName);
        } catch (\Exception $exception) {
            $resumeMapper->rollBack();

            $this->_logException($exception);

            return false;
        }

        return true;
    }

    /**
     * @param Domain\Resume[] $resumes
     */
    public function bulkIndex(array $resumes, bool $recreateIndex = false) : array
    {
        $createBulkParams = function () use ($resumes) {
            $params = [];

            foreach ($resumes as $resume) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_RESUME,
                        '_type' => ESService::INDEX_RESUME
                    ]
                ];

                $params['body'][] = $this->indexiseModel($resume);
            }

            return $params;
        };

        return self::$ds->elasticSearchService()->bulkIndexModels(
            ESService::INDEX_RESUME,
            $createBulkParams,
            $recreateIndex
        );
    }

    public function indexiseModel(Domain\Resume $resume) : array
    {
        /** @var WorkExperience $lastWorkPlace */
        $lastWorkPlace = $resume->getWorkExperiences()->last();
        $lastWorkEndDate = null;

        if ($lastWorkPlace) {
            $lastWorkEndDate = $lastWorkPlace->getPeriod()->getDateEnd();
            $lastWorkEndDate = $lastWorkEndDate ? $lastWorkEndDate->mysqlFormat() : null;
        }

        $totalWorkInterval = self::$ds->resumeService()->summarizeWorkingInterval($resume);

        $plural = new Plural();
        if ($totalWorkInterval->y) {
            $years = $plural(['год', 'года', 'лет'], $totalWorkInterval->y);
        }

        if ($totalWorkInterval->m) {
            $months = $plural(['месяц', 'месяца', 'месяцев'], $totalWorkInterval->m);
        }

        $workExpString = $totalWorkInterval->y ? "$totalWorkInterval->y $years" : '';
        $workExpString .= ($workExpString ? " " : '')
            . ($totalWorkInterval->m ? "$totalWorkInterval->m $months" : '')
        ;

        return [
            ResumeMapper::PRIMARY               => $resume->getId(),
            ResumeMapper::COL_USER_ID           => $resume->getUserId(),
            ResumeMapper::COL_CITY_ID           => $resume->getCityId(),
            'city'                              => $resume->getCity()->getTitleEn(),
            'region_id'                         => $resume->getCity()->getRegionId(),
            'country_id'                        => $resume->getCity()->getCountryId(),
            ResumeMapper::COL_POSITION_NAME     => $resume->getPositionName(),
            ResumeMapper::COL_DESIRED_PAYCHECK  => $resume->getDesiredPaycheck(),
            ResumeMapper::COL_CURRENCY_ID       => $resume->getCurrencyId(),
            ResumeMapper::COL_GENDER            => $resume->getGender(),
            'age'                               => $resume->getAge(),
            ResumeMapper::COL_EDUCATION_LEVEL   => $resume->getEducationLevel(),
            ResumeMapper::COL_IS_RELOCATE_READY => $resume->isRelocateReady(),
            ResumeMapper::COL_IS_REMOTE         => $resume->isRemote(),
            ResumeMapper::COL_EMPLOYMENT        => $resume->getEmployment(),
            ResumeMapper::COL_VISIBILITY        => $resume->getVisibility(),
            ResumeMapper::COL_UPDATE_DATE       => $resume->getUpdateDate()->mysqlFormat(),
            'last_work_position'                => $lastWorkPlace ? $lastWorkPlace->getPositionName() : '',
            'last_work_date_from'               => $lastWorkPlace ? $lastWorkPlace->getPeriod()->getDateStart()->mysqlFormat() : null,
            'last_work_date_to'                 => $lastWorkEndDate,
            'work_exp_years'                    => $totalWorkInterval->y + ($totalWorkInterval->m / 12),
            'work_exp_string'                   => $workExpString
        ];
    }

    public function summarizeWorkingInterval(Domain\Resume $resume) : \DateInterval
    {
        /** @var Domain\WorkExperience[] $workExps */
        $workExps = $resume->getWorkExperiences()->asArray();

        $totalYears  = 0;
        $totalMonths = 0;
        $days        = 0;

        foreach ($workExps as $workExp) {
            $interval = $workExp->getPeriod()->interval();

            $totalYears  += $interval->y;
            $totalMonths += $interval->m;
            $days        += $interval->days;
        }

        if ($days >= self::MIN_DAYS_IN_MONTH) {
            $totalMonths++;
        }

        if ($totalMonths >= self::MONTHS_PER_YEAR) {
            $totalYears += $totalMonths / self::MONTHS_PER_YEAR;
            $totalMonths = $totalMonths % self::MONTHS_PER_YEAR;
        }

        $now = new \DateTime('now');
        $resultInterval = $now->diff($now);
        $resultInterval->y = $totalYears;
        $resultInterval->m = $totalMonths;

        return $resultInterval;
    }

    public function getPayCheckRange() : array
    {
        $result = self::$ds->elasticSearchService()->customQuery(ElasticSearch::INDEX_RESUME, [
            "aggs" => [
                "max" => [
                    "max" => [
                        "field" => "desired_paycheck"
                    ]
                ],
                "min" => [
                    "min" => [
                        "field" => "desired_paycheck"
                    ]
                ],
            ]
        ]);

        return $result['aggregations'];
    }

    public function normalizeTableRowForForm(Domain\Resume $resume, bool $isEditForm = true) : array
    {
        $resumeTableRow = $resume->getTableRow(true);

        $resumeTableRow['city'] = $resume->getCity()->getTitleEn();
        $resumeTableRow[ResumeMapper::COL_BIRTH_DATE] = $resume->getBirthDate()->format(Date::DATE_FORMAT_RU);

        $this->_reformatRowsDateFields(
            $resumeTableRow[ResumeMapper::COLLECTION_OF_EDUCATION],
            [EducationMapper::COL_DATE_START, EducationMapper::COL_DATE_END],
            Date::DATE_FORMAT_RU
        );

        $this->_reformatRowsDateFields(
            $resumeTableRow[ResumeMapper::COLLECTION_OF_WORK_EXPERIENCE],
            [WorkExperienceMapper::COL_DATE_START, WorkExperienceMapper::COL_DATE_END],
            Date::DATE_FORMAT_RU
        );

        $tags = $resume->getTags();

        $tagsObject = new \stdClass();

        foreach ($resumeTableRow[ResumeMapper::COLLECTION_OF_TAGS] as $key => $tagRow) {
            /** @var Domain\EntityTag $entityTag */
            $entityTag = $tags->getModelById($tagRow[EntityTagMapper::PRIMARY]);
            $tagRow['title'] = $entityTag->getTagName();

            unset($tagRow[EntityTagMapper::RELATED_TAG]);
            unset($tagRow[EntityTagMapper::COL_ENTITY_TYPE]);
            unset($tagRow[EntityTagMapper::COL_ENTITY_ID]);

            $tagsObject->$key = $tagRow;
        }

        $resumeTableRow[ResumeMapper::COLLECTION_OF_TAGS] = $tagsObject;

        if (!$isEditForm) {
            foreach ($resumeTableRow[ResumeMapper::COLLECTION_OF_LANGUAGES] as &$lang) {
                $lang['language_name'] = Domain\LanguageProficiency::LANGUAGES[$lang[LanguageProficiencyMapper::COL_LANGUAGE_CODE]];
                unset($lang[LanguageProficiencyMapper::COL_LANGUAGE_CODE]);

                $lang['language_level'] = Form\Fieldset\Language::LEVELS[$lang[LanguageProficiencyMapper::COL_LEVEL]];
                unset($lang[LanguageProficiencyMapper::COL_LEVEL]);
            }

            /** @var PhpRenderer $viewManager */
            $viewRenderer = self::$sm->get(Factory::VIEW_RENDERER);

            $resumeTableRow['age'] = "{$resume->getAge()} {$viewRenderer->plural(['год', 'года', 'лет'], $resume->getAge())}";
            unset($resumeTableRow[ResumeMapper::COL_BIRTH_DATE]);

            $resumeTableRow[ResumeMapper::COL_GENDER] = $resume->getGender() === Domain\Resume::GENDER_MALE
                ? 'Мужчина'
                : 'Женщина'
            ;

            $resumeTableRow['city'] = $resume->getCity()->getTitleEn();
            unset($resumeTableRow[ResumeMapper::COL_CITY_ID]);

            $resumeTableRow['currency'] = $resume->getCurrency()->getIsoCode();
            unset($resumeTableRow[ResumeMapper::COL_CURRENCY_ID]);
        }

        return $resumeTableRow;
    }

    public function queryFromElasticsearch(ResumeFilter $filter) : array
    {
        $esService = self::$ds->elasticSearchService();

        $must = [];

        if ($filter->getKeywords() !== null) {
            $keywords = $this->_buildKeywordQueryString($filter->getKeywords());

            $keywordFields = [
                'position_name^2',
            ];

            if ($filter->getTitleOnly() !== true) {
                $keywordFields[] = 'full_description';
            }

            $must[] = [
                "query_string" => [
                    'fields' => $keywordFields,
                    'query' => $keywords,
                    'default_operator' => 'AND'
                ]
            ];
        } else {
            $must[]['match_all'] = new \stdClass();
        }

        if ($filter->getUserId() !== null) {
            $must[] = [
                'term' => [
                    ResumeMapper::COL_USER_ID => $filter->getUserId()
                ]
            ];
        }

        if ($filter->getCityIds() !== null || $filter->getCountryIds() !== null || $filter->getRegionIds() !== null) {
            $terms = [];

            if ($filter->getCityIds() !== null) {
                $terms[]['terms'] = ['city_id' => $filter->getCityIds()];
            }

            if ($filter->getRegionIds() !== null) {
                $terms[]['terms'] = ['region_id' => $filter->getRegionIds()];
            }

            if ($filter->getCountryIds() !== null) {
                $terms[]['terms'] = ['country_id' => $filter->getCountryIds()];
            }

            $must = array_merge($must, $terms);
        }

        if ($filter->getPaycheckMin() !== null && $filter->getPaycheckMax() !== null) {
            $must[] = [
                'bool' => [
                    'should' => [
                        [
                            'range' => [
                                'desired_paycheck' => [
                                    'gte' => $filter->getPaycheckMin(),
                                    'lte' => $filter->getPaycheckMax()
                                ]
                            ]
                        ],
                        [
                            "bool" => [
                                "must_not" => [
                                    [
                                        'exists' => [
                                            'field' => 'desired_paycheck'
                                        ]
                                    ]
                                ],
                            ]
                        ]
                    ],
                    "minimum_should_match" => 1
                ]
            ];
        }

        if ($filter->isRemote() !== null) {
            $must[] = [
                'term' => [
                    'is_remote' => $filter->isRemote()
                ]
            ];
        }

        if ($filter->getEmployments() !== null) {
            $must[] = [
                'terms' => [
                    'employment' => $filter->getEmployments()
                ]
            ];
        }

        if ($filter->getAge() !== null) {
            $ageRangeConditions = [];

            foreach ($filter->getAge() as $ageValue) {
                switch ($ageValue) {
                    case ResumeFilter::AGE_14_18:
                        $ageRangeConditions[] = [
                            'range' => [
                                'age' => [
                                    'gte' => ResumeFilter::AGE_14,
                                    'lte' => ResumeFilter::AGE_18,
                                ]
                            ]
                        ];
                        break;

                    case ResumeFilter::AGE_18_30:
                        $ageRangeConditions[] = [
                            'range' => [
                                'age' => [
                                    'gte' => ResumeFilter::AGE_18,
                                    'lte' => ResumeFilter::AGE_30,
                                ]
                            ]
                        ];
                        break;
                    case ResumeFilter::AGE_30_40:
                        $ageRangeConditions[] = [
                            'range' => [
                                'age' => [
                                    'gte' => ResumeFilter::AGE_30,
                                    'lte' => ResumeFilter::AGE_40,
                                ]
                            ]
                        ];
                        break;
                    case ResumeFilter::AGE_40_50:
                        $ageRangeConditions[] = [
                            'range' => [
                                'age' => [
                                    'gte' => ResumeFilter::AGE_40,
                                    'lte' => ResumeFilter::AGE_50,
                                ]
                            ]
                        ];
                        break;
                    case ResumeFilter::AGE_50_60:
                        $ageRangeConditions[] = [
                            'range' => [
                                'age' => [
                                    'gte' => ResumeFilter::AGE_50,
                                    'lte' => ResumeFilter::AGE_60,
                                ]
                            ]
                        ];
                        break;
                    case ResumeFilter::AGE_FROM_60:
                        $ageRangeConditions[] = [
                            'range' => [
                                'age' => [
                                    'gte' => ResumeFilter::AGE_60,
                                ]
                            ]
                        ];
                        break;
                }
            }

            $must[] = [
                'bool' => [
                    'should' => $ageRangeConditions,
                    'minimum_should_match' => 1
                ]
            ];
        }

        if ($filter->getExperience() !== null) {
            $rangeConditions = [];

            foreach ($filter->getExperience() as $expValue) {

                switch ($expValue) {
                    case Domain\Vacancy::EXP_NOT_REQUIRED:
                        $rangeConditions[] = [
                            'term' => [
                                'required_experience' => ResumeFilter::EXP_NONE
                            ]
                        ];
                        break;

                    case Domain\Vacancy::EXP_LTE_1_YEAR:
                        $rangeConditions[] = [
                            'range' => [
                                'work_exp_years' => [
                                    'gt' => ResumeFilter::EXP_NONE,
                                    'lte' => ResumeFilter::EXP_1_YR
                                ]
                            ]
                        ];
                        break;

                    case Domain\Vacancy::EXP_LTE_2_YEARS:
                        $rangeConditions[] = [
                            'range' => [
                                'work_exp_years' => [
                                    'gte' => ResumeFilter::EXP_1_YR,
                                    'lte' => ResumeFilter::EXP_2_YRS
                                ]
                            ]
                        ];
                        break;

                    case Domain\Vacancy::EXP_LTE_3_YEARS:
                        $rangeConditions[] = [
                            'range' => [
                                'work_exp_years' => [
                                    'gte' => ResumeFilter::EXP_2_YRS,
                                    'lte' => ResumeFilter::EXP_3_YRS
                                ]
                            ]
                        ];
                        break;

                    case Domain\Vacancy::EXP_LTE_5_YEARS:
                        $rangeConditions[] = [
                            'range' => [
                                'work_exp_years' => [
                                    'gte' => ResumeFilter::EXP_3_YRS,
                                    'lte' => ResumeFilter::EXP_5_YRS
                                ]
                            ]
                        ];
                        break;

                    case Domain\Vacancy::EXP_GT_5_YEARS:
                        $rangeConditions[] = [
                            'range' => [
                                'work_exp_years' => [
                                    'gte' => ResumeFilter::EXP_5_YRS,
                                ]
                            ]
                        ];
                        break;
                }
            }

            $must[] = [
                'bool' => [
                    'should' => $rangeConditions,
                    'minimum_should_match' => 1
                ]
            ];
        }

        if (in_array($filter->getGender(), Domain\Resume::GENDERS)) {
            $must[] = [
                'term' => [
                    ResumeMapper::COL_GENDER => $filter->getGender()
                ]
            ];
        }

        if ($filter->getPublishPeriod() !== ResumeFilter::PUBLISH_ALL_TIME) {
            $rangeProps = [];

            switch($filter->getPublishPeriod()) {
                case ResumeFilter::PUBLISH_PER_DAY:
                    $rangeProps = [
                        'gte' => 'now-1d'
                    ];
                    break;
                case ResumeFilter::PUBLISH_PER_WEEK:
                    $rangeProps = [
                        'gte' => 'now-1w'
                    ];
                    break;
                case ResumeFilter::PUBLISH_PER_2_WEEKS:
                    $rangeProps = [
                        'gte' => 'now-2w'
                    ];
                    break;
                case ResumeFilter::PUBLISH_PER_MONTH:
                    $rangeProps = [
                        'gte' => 'now-1M'
                    ];
                    break;
            }

            $must[] = [
                'range' => [
                    'update_date' => $rangeProps
                ]
            ];
        }

        if ($filter->getRelocateReady() !== null) {
            $must[] = [
                'term' => [
                    'is_relocate_ready' => $filter->getRelocateReady()
                ]
            ];
        }

        if ($filter->getEducationLevels() !== null) {
            $must[] = [
                'terms' => [
                    'education_level' => $filter->getEducationLevels()
                ]
            ];
        }

        if ($filter->getVisibility() !== null) {
            $must[] = [
                'term' => [
                    'visibility' => $filter->getVisibility()
                ]
            ];
        }

        $query = [
            'bool' => [
                'must' => $must
            ]
        ];

        return $esService->query(
            ElasticSearch::INDEX_RESUME,
            $query,
            $filter->getPageNumber(),
            $filter->getItemsPerPage(),
            $filter->getOrder() ?? []
        );
    }

    public function elasticDataToJsonModels(array $elasticSearchDocuments, bool $withCount = false) : array
    {
        $resumeArrayRecords = [];

        $resumeMapper = self::$ds->resumeMapper();

        foreach ($elasticSearchDocuments['hits'] as $document) {
            $resume = $resumeMapper->fetch($document['_source']['id']);

            if (!$resume) {
                continue;
            }

            $resumeRecord                        = $resume->getTableRow();
            $resumeRecord['city']                = $resume->getCity()->getTitleEn();
            $resumeRecord['currency_code']       = $resume->getCurrency()->getIsoCode();
            $resumeRecord['currency_name']       = $resume->getCurrency()->getName();
            $resumeRecord['age']                 = $document['_source']['age'] ?? null;
            $resumeRecord['last_work_position']  = $document['_source']['last_work_position'] ?? null;
            $resumeRecord['last_work_date_from'] = $document['_source']['last_work_date_from'] ?? null;
            $resumeRecord['last_work_date_to']   = $document['_source']['last_work_date_to'] ?? null;
            $resumeRecord['work_exp_string']     = $document['_source']['work_exp_string'] ?? null;

            $resumeRecord['views'] = self::$ds->viewService()->getEntityViewsCount($resume->getId(), Domain\View::ENTITY_TYPE_RESUME);

            $resumeArrayRecords[] = $resumeRecord;
        }

        if ($withCount) {
            $resumeArrayRecords[self::KEY_TOTAL_ITEMS] = $elasticSearchDocuments['total'];
        }

        return $resumeArrayRecords;
    }

    public function makeSimpleListJson(array $elasticSearchResult) : array
    {
        $result = [];

        foreach ($elasticSearchResult['hits'] as $document) {
            $resumeData = [];

            $elasticRow = $document['_source'];

            $currency = self::$ds->currencyMapper()->fetch($elasticRow[ResumeMapper::COL_CURRENCY_ID]);

            $resumeData[ResumeMapper::PRIMARY]           = $elasticRow[ResumeMapper::PRIMARY];
            $resumeData[ResumeMapper::COL_POSITION_NAME] = $elasticRow[ResumeMapper::COL_POSITION_NAME];
            $paycheck = $resumeData[ResumeMapper::COL_DESIRED_PAYCHECK] ?? null;
            $currency = $currency->getIsoCode();

            $resumeData['paycheck'] = $paycheck
                ? sprintf("%s %s", number_format($paycheck, 0, '.', ' '), $currency)
                : null
            ;

            $result[] = $resumeData;
        }

        return $result;
    }

    public function normalizeFormData(array & $formData)
    {
        $formData[Form\Resume::EL_BIRTH_DATE] = Date::reformat($formData[Form\Resume::EL_BIRTH_DATE], Date::MYSQL_FORMAT);

        $this->_reformatRowsDateFields(
            $formData[Form\Resume::EL_EDUCATION_COLLECTION],
            [Form\Fieldset\Education::EL_DATE_START, Form\Fieldset\Education::EL_DATE_END],
            Date::MYSQL_FORMAT
        );

        $this->_reformatRowsDateFields(
            $formData[ResumeMapper::COLLECTION_OF_WORK_EXPERIENCE],
            [Form\Fieldset\WorkExperience::EL_DATE_START, Form\Fieldset\WorkExperience::EL_DATE_END],
            Date::MYSQL_FORMAT
        );
    }

    protected function _setResumeFields(Domain\Resume $resume, array $formData)
    {
        $positionName    = $formData[Form\Resume::EL_POSITION_NAME];
        $cityId          = $formData[Form\Resume::EL_CITY_ID];
        $isRemote        = $formData[Form\Resume::EL_IS_REMOTE];
        $firstName       = $formData[Form\Resume::EL_FIRST_NAME];
        $middleName      = $formData[Form\Resume::EL_MIDDLE_NAME];
        $lastName        = $formData[Form\Resume::EL_LAST_NAME];
        $birthDate       = $formData[Form\Resume::EL_BIRTH_DATE]
            ? Date::createFromMysqlFormat($formData[Form\Resume::EL_BIRTH_DATE])
            : null
        ;
        $gender          = $formData[Form\Resume::EL_GENDER];
        $isRelocateReady = $formData[Form\Resume::EL_IS_RELOCATE_READY];
        $currencyId      = $formData[Form\Resume::EL_CURRENCY];
        $educationLevel  = $formData[Form\Resume::EL_EDUCATION_LEVEL];
        $desiredPaycheck = $formData[Form\Resume::EL_DESIRED_PAYCHECK]
            ? (int) $formData[Form\Resume::EL_DESIRED_PAYCHECK]
            : null
        ;
        $autodescription = $formData[Form\Resume::EL_AUTODESCRIPTION];
        $photoPath       = $resume->getPhotoPath();
        $visibility      = $formData[Form\Resume::EL_VISIBILITY];

        if ($formData[Form\Resume::EL_PHOTO]) {
            $photoPath = self::$ds->photoService()->generateFullPath($formData[Form\Resume::EL_PHOTO]);
        } elseif (!$formData[Form\Resume::EL_PHOTO_PATH]) {
            $photoPath = null;
        }

        $resume
            ->setPositionName($positionName)
            ->setCityId($cityId)
            ->setFirstName($firstName)
            ->setMiddleName($middleName)
            ->setLastName($lastName)
            ->setBirthDate($birthDate)
            ->setGender($gender)
            ->setRelocateReady($isRelocateReady)
            ->setCurrencyId($currencyId)
            ->setEducationLevel($educationLevel)
            ->setDesiredPaycheck($desiredPaycheck)
            ->setAutodescription($autodescription)
            ->setPhotoPath($photoPath)
            ->setRemote($isRemote)
            ->setVisibility($visibility)
        ;
    }

    protected function _makeContactsRows(array $formDataContacts, int $resumeId) : array
    {
        $contacts = [];

        foreach ($formDataContacts as $contactRow) {
            $contacts[] = [
                Domain\Contact\ContactMapper::PRIMARY => $contactRow[Form\Fieldset\Contact::EL_ID],
                Domain\Contact\ContactMapper::COL_ENTITY_ID => $resumeId,
                Domain\Contact\ContactMapper::COL_ENTITY_TYPE => Domain\Contact::ENTITY_TYPE_RESUME,
                Domain\Contact\ContactMapper::COL_TYPE => $contactRow[Form\Fieldset\Contact::EL_TYPE],
                Domain\Contact\ContactMapper::COL_VALUE => $contactRow[Form\Fieldset\Contact::EL_VALUE]
            ];
        }

        return $contacts;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        if (!$filter instanceof ResumeFilter) {
            throw new \Exception('Invalid sideFilter type');
        }

        $where = [];

        $select = self::$ds->resumeMapper()
            ->select($where)
            ->with(ResumeMapper::COLLECTIONS)
        ;

        return $this->_setPaging($filter, $select);
    }
}