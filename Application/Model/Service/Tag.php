<?php

namespace Application\Model\Service;

use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\ElasticSearch as ESService;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain;

class Tag extends AbstractService
{
    /**
     * @param Domain\Tag[] $tags
     */
    public function bulkIndex(array $tags, bool $preDeleteIndex = false) : array
    {
        $createBulkParams = function () use ($tags) {
            $params = [];

            foreach ($tags as $tag) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_TAG,
                        '_type' => ESService::INDEX_TAG
                    ]
                ];

                $params['body'][] = $this->indexiseModel($tag);
            }

            return $params;
        };

        return self::$ds->elasticSearchService()->bulkIndexModels(
            ESService::INDEX_TAG,
            $createBulkParams,
            $preDeleteIndex
        );
    }

    public function indexiseModel(Domain\Tag $tag) : array
    {
        return [
            Domain\Tag\TagMapper::PRIMARY  => $tag->getId(),
            Domain\Tag\TagMapper::COL_NAME => $tag->getName()
        ];
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}