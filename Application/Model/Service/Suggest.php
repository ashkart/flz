<?php

namespace Application\Model\Service;

use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain;

class Suggest extends AbstractService
{
    public function createLocalitySuggestions(array $elasticSearchResult) : array
    {
        $result = [];

        foreach ($elasticSearchResult as $indexObject) {
            $resultItem = [];

            $resultItem['title'] = $indexObject['_source']['title']
                ?? $indexObject['_source']['title_ru']
                ?? $indexObject['_source']['title_en']
            ;


            if ($indexObject['_index'] !== ElasticSearch::INDEX_COUNTRY) {
                $resultItem['description'] = $indexObject['_source']['description'];
            }

            $resultItem['id'] = $indexObject['_source']['id'];
            $resultItem['type'] = $indexObject['_index'];

            $result[] = $resultItem;
        }

        return $result;
    }

    public function createTagSuggestions(array $elasticSearchResult)
    {
        $result = [];

        foreach ($elasticSearchResult as $indexObject) {
            $resultItem = [];

            $resultItem['tag_id'] = $indexObject['_source']['id'];
            $resultItem['type']   = $indexObject['_index'];
            $resultItem['title']  = $indexObject['_source']['name'];

            $result[] = $resultItem;
        }

        return $result;
    }

    public function createCompanySuggestions(array $elasticSearchResult)
    {
        $result = [];

        foreach ($elasticSearchResult as $indexObject) {
            $resultItem = [];

            $resultItem['id']    = $indexObject['_source']['id'];
            $resultItem['title'] = ($indexObject['_source']['opf']
                    ? "{$indexObject['_source']['opf']} "
                    : '')
                . $indexObject['_source']['official_name']
            ;

            $result[] = $resultItem;
        }

        return $result;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}