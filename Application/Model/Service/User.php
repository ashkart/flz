<?php

namespace Application\Model\Service;

use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\UserFilter;
use Application\Model\Domain\User\UserMapper;
use Atlas\Orm\Mapper\MapperSelect;
use Frontend\Form\Registration;
use Main\View\Form;
use Application\Model\Domain;

class User extends AbstractService
{
    public function update(Domain\User $user, array $formData)
    {
        $email    = $formData[Form\User::EL_EMAIL];
        $role     = $formData[Form\User::EL_ROLE];
        $userName = $formData[Form\User::EL_USER_NAME];

        $user
            ->setEmail($email)
            ->setRole($role)
            ->setUserName($userName)
        ;

        self::$ds->userMapper()->save($user);
    }

    public function create(string $role, array $formData) : Domain\User
    {
        $email    = $formData[Registration::EL_LOGIN];
        $password = $formData[Registration::EL_PASSWORD];
        $userName = $formData[Registration::EL_USER_NAME];

        /* @TODO location detection */
        $user = Domain\User::create(
            $email,
            self::$ds->authService()->hashPasswordForSaving($password),
            $role,
            $userName,
            Domain\City::ID_MOSCOW
        );

        self::$ds->userMapper()->save($user);

        return $user;
    }

    public function getProfileDataForView(Domain\User $user) : array
    {
        if ($user->isGuest()) {
            return [];
        }

        $result = [];

        $email = $user->getEmail();

        $firstEmailChar = $email[0];
        $emailParts     = explode('@', $email);
        $lastEmailChar  = $emailParts[0][strlen($emailParts[0]) - 1];

        $hiddenUserNameEmail = sprintf("%s***%s@%s", $firstEmailChar, $lastEmailChar, $emailParts[1]);

        $result[Form\User::EL_EMAIL] = $hiddenUserNameEmail;

        $city     = $user->getCity();
        $region   = $city && $city->getRegionId() ? $city->getRegion()->getTitle() : null;
        $country  = $city ? $city->getCountry()->getTitleRu() : null;

        $result['geo_location'] = [
            'city'    => $city ? self::$ds->cityService()->makeSuggestion($city) : null,
            'region'  => $region,
            'country' => $country
        ];

        $result[Form\User::EL_USER_NAME] = $user->getUserName();

        return $result;
    }

    public function getPasswordConfirmToken(Domain\User $user) : string
    {
        return sha1(self::$ds->authService()->hashPasswordForSaving(time() . $user->getId() .  $user->getPassword() . Auth::CONFIRM_TOKEN_SALT));
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        if (!$filter instanceof UserFilter) {
            throw new \Exception('Invalid sideFilter type');
        }

        $where = [];

        if ($filter->getId() !== null) {
            $where[UserMapper::PRIMARY] = $filter->getId();
        }

        $select = self::$ds->userMapper()->select($where);

        return $this->_setPaging($filter, $select);
    }
}