<?php

namespace Application\Model\Service;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\RedisMQ\Job\PasswordConfirmation;
use Application\Model\Service\RedisMQ\Job\UserConfirmation;
use Application\Model\Service\RedisMQ\Job\ViewStat;
use Application\Model\Service\RedisMQ\Worker\ViewStatWorker;
use Atlas\Orm\Mapper\MapperSelect;
use Library\Master\QueueManager\Resque;
use Application\Model\Domain;

class RedisMQ extends AbstractService
{
    const QUEUE_USER_CONFIRMATION            = 'user_confirmation';
    const QUEUE_PASSWORD_CHANGE_CONFIRMATION = 'password_change_confirmation';
    const QUEUE_VIEW_STAT_PROCESSING         = 'view_stat_processing';
    const QUEUE_SOCIAL_PUBLISHING            = 'social_publishing';

    const MAIL_QUEUES = [
        self::QUEUE_USER_CONFIRMATION,
        self::QUEUE_PASSWORD_CHANGE_CONFIRMATION
    ];

    /**
     * @var Resque
     */
    protected $_resque;

    public function __construct()
    {
        parent::__construct();

        $this->_resque = self::$sm->get(Factory::REDIS_MQ);
    }

    public function enqueueUserConfirmation(int $userId, string $password) : string
    {
        return $this->addJob(
            self::QUEUE_USER_CONFIRMATION,
            UserConfirmation::class,
            ['userId' => $userId, 'password' => $password]
        );
    }

    public function enqueuePasswordChangeConfirmationMail(int $userId, string $token) : string
    {
        return $this->addJob(
            self::QUEUE_PASSWORD_CHANGE_CONFIRMATION,
            PasswordConfirmation::class,
            [
                PasswordConfirmation::ARG_USER_ID => $userId,
                PasswordConfirmation::ARG_TOKEN   => $token
            ]
        );
    }

    public function enqueueView(Domain\View $view) : string
    {
        return $this->addJob(
            self::QUEUE_VIEW_STAT_PROCESSING,
            ViewStat::class,
            self::$ds->viewService()->indexiseModel($view)
        );
    }

    public function addJob(string $queueName, string $jobClass, array $args = []) : string
    {
        return $this->_resque->enqueue($queueName, $jobClass, $args);
    }

    public function spawnMailWorker()
    {
        $this->_resque->runNewWorker(self::MAIL_QUEUES, self::$sm->get(Factory::LOGGER));
    }

    public function spawnSocialPublishWorker()
    {
        $this->_resque->runNewWorker([self::QUEUE_SOCIAL_PUBLISHING], self::$sm->get(Factory::LOGGER));
    }

    public function spawnViewStatWorker()
    {
        $config       = self::$sm->get(Factory::CONFIG);
        $workerConfig = $config['staffzredismq']['view_stat'];
        $interval     = $workerConfig['interval'];
        $maxBunch     = $workerConfig['max_bunch'];

        $worker = new ViewStatWorker([self::QUEUE_VIEW_STAT_PROCESSING], self::$ds->viewService(), $maxBunch);
        $this->_resque->runCustomWorker($worker, self::$sm->get(Factory::LOGGER), $interval);
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('not implemented');
    }
}