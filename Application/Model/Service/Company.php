<?php

namespace Application\Model\Service;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\CompanyFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain\Company\CompanyMapper;
use Application\Model\Domain;
use Library\Master\DaData;

class Company extends AbstractService
{
    public function importFromDaData(int $inn) : ? Domain\Company
    {
        /** @var DaData\ApiClient\Suggest $daDataClient */
        $daDataClient = self::$sm->get(Factory::DADATA_API_CLIENT);

        $suggestion = $daDataClient->suggestOneCompany($inn);

        if (!$suggestion) {
            return null;
        }

        $company = new Domain\Company($suggestion->getOfficialName());
        $company
            ->setOpf($suggestion->getOpfShort())
            ->setKpp($suggestion->getKpp())
            ->setInn($inn)
            ->setOgrn((string) $suggestion->getOgrn())
            ->setAddress($suggestion->getAddress())
        ;

        self::$ds->companyMapper()->save($company);

        return $company;
    }

    public function create(string $officialName, string $description = null) : Domain\Company
    {
        $company = new Domain\Company($officialName);
        $company->setDescription($description);

        self::$ds->companyMapper()->save($company);

        return $company;
    }

    public function update(Domain\Company $company, array $formData)
    {
        $this->_setCompanyFields($company, $formData);

        self::$ds->companyMapper()->save($company);
    }

    public function queryFromElasticsearch(CompanyFilter $filter) : array
    {
        $esService = self::$ds->elasticSearchService();

        $must = [];

        if ($filter->getKeyword() !== null) {
            $must[] = [
                "query_string" => [
                    'fields' => ['official_name'],
                    'query' => $filter->getKeyword()
                ]
            ];
        } else {
            $must[]['match_all'] = new \stdClass();
        }

        if ($filter->getUserId() !== null) {
            $must[] = [
                'term' => [
                    Domain\UserCompany\UserCompanyMapper::COL_USER_ID => $filter->getUserId()
                ]
            ];
        }

        $query = [
            'bool' => [
                'must' => $must
            ]
        ];

        return $esService->query(
            ElasticSearch::INDEX_COMPANY,
            $query,
            $filter->getPageNumber(),
            $filter->getItemsPerPage(),
            $filter->getOrder() ?? []
        );
    }

    public function suggestionFromModel(Domain\Company $company) : array
    {
        return [
            'id'    => $company->getId(),
            'title' => ($company->getOpf()
                    ? "{$company->getOpf()} "
                    : '')
                . $company->getOfficialName(),
            'type'  => ElasticSearch::INDEX_COMPANY,
        ];
    }

    protected function _setCompanyFields(Domain\Company $company, array $formData)
    {
        $offName     = $formData[\Main\View\Form\Company::EL_OFFICIAL_NAME];
        $address     = $formData[\Main\View\Form\Company::EL_ADDRESS];
        $inn         = $formData[\Main\View\Form\Company::EL_INN];
        $kpp         = $formData[\Main\View\Form\Company::EL_KPP];
        $ogrn        = $formData[\Main\View\Form\Company::EL_OGRN];
        $opf         = $formData[\Main\View\Form\Company::EL_OPF];
        $description = $formData[\Main\View\Form\Company::EL_DESCRIPTION];

        $company
            ->setOpf($opf)
            ->setOfficialName($offName)
            ->setAddress($address)
            ->setInn($inn)
            ->setKpp($kpp)
            ->setOgrn($ogrn)
            ->setDescription($description)
        ;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        if (!$filter instanceof CompanyFilter) {
            throw new \Exception('Invalid filter type');
        }

        $companyTableName = self::$ds->companyMapper()->getTable()->getName();

        $where = '';

        $select = self::$ds->companyMapper()->select()->cols(["$companyTableName.*"]);

        if ($filter->getUserId() !== null) {
            $userCompanyTableName = self::$ds->userCompanyMapper()->getTable()->getName();
            $select->join('LEFT', "$userCompanyTableName uc", "uc.company_id = $companyTableName.id");

            $where = $this->_concatenateQueryParts($where, "uc.user_id = {$filter->getUserId()}");
        }

        if ($where) {
            $select->where($where);
        }

        if ($filter->getOrder() !== null) {
            $ordering = [];

            foreach ($filter->getOrder() as $fieldName => $order) {
                $ordering[] = "$fieldName $order";
            }

            $select->orderBy($ordering);
        }

        return $this->_setPaging($filter, $select);
    }
}