<?php

namespace Application\Model\Service;

use Application\Model\Domain;
use Application\Model\Domain\City\CityMapper;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\CityFilter;
use Atlas\Orm\Mapper\MapperSelect;

class City extends AbstractService
{
    public function getDataForSelection(int $regionId) : array
    {
        $cities = self::$ds->cityMapper()->fetchByRegionId($regionId);

        $result = [];

        foreach ($cities as $city) {
            $tableRow = $city->getTableRow();
            unset($tableRow['id']);
            unset($tableRow['country_id']);
            unset($tableRow['region_id']);

            $result[$city->getId()] = $tableRow;
        }

        return $result;
    }

    public function indexiseModel(Domain\City $city) : array
    {
        $description = '';

        if ($city->getArea()) {
            $description .= $city->getArea();
        }

        if ($city->getRegionId()) {
            $description .= ($description ? ', ' : '') . $city->getRegion()->getTitle();
        }

        $description .= ($description ? ', ' : '') . $city->getCountry()->getTitleRu();

        return [
            'id'          => $city->getId(),
            'region_id'   => $city->getRegionId() ?? 0,
            'country_id'  => $city->getCountryId(),
            'title_en'    => $city->getTitleEn(),
            'title_ru'    => $city->getTitleRu(),
            'description' => $description
        ];
    }

    public function makeSuggestion(Domain\City $city)
    {
        $cityData = self::$ds->cityService()->indexiseModel($city);
        $cityData['type'] = ElasticSearch::INDEX_CITY;
        $cityData['title'] = $cityData['title_ru'] ?? $cityData['title_en'];

        unset($cityData[CityMapper::COL_COUNTRY_ID]);
        unset($cityData[CityMapper::COL_REGION_ID]);
        unset($cityData['title_ru']);
        unset($cityData['title_en']);

        return $cityData;
    }

    public function getFromCoords(float $lat, float $lng) : ? Domain\City
    {
        $dbConnection = self::$ds->cityMapper()->getReadConnection();

        $stmt = $dbConnection->prepare("call FindNearest(:lat, :lng, :stepKm, :limitKm, 1, '1=1')");
        $stmt->execute([
            'lat' => $lat,
            'lng' => $lng,
            'stepKm' => 5,
            'limitKm' => 100
        ]);

        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        $stmt->closeCursor();

        $city = self::$ds->cityMapper()->fetch($result['id']);

        return $city;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        if (!$filter instanceof CityFilter) {
            throw new \Exception('Invalid sideFilter type');
        }

        $where = [];

        if ($filter->getCountryId() !== null) {
            $where[CityMapper::COL_COUNTRY_ID] = $filter->getCountryId();
        }

        $select = self::$ds->cityMapper()->select($where);

        if ($filter->getOrder() !== null) {
            $ordering = [];

            foreach ($filter->getOrder() as $fieldName => $order) {
                $ordering[] = "$fieldName $order";
            }

            $select->orderBy($ordering);
        }

        return $this->_setPaging($filter, $select);
    }
}