<?php

namespace Application\Model\Service;

use Application\Model\Interfaces\DomainModelInterface;
use Application\Model\Interfaces\SocialPublicationInterface;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\VacancyFilter;
use Application\Model\Domain\Vacancy\VacancyMapper;
use Application\Model\Service\ElasticSearch as ESService;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain;
use Main\View\Form;
use Application\Model\Traits\TraitFrontendUrl;

class Vacancy extends AbstractService implements SocialPublicationInterface
{
    use TraitFrontendUrl;

    public function create(array $formData) : ?Domain\Vacancy
    {
        $userId        = $this->_user()->getId();
        $positionName  = $formData[Form\Vacancy::EL_POSITION_NAME] ?? null;
        $requirements  = $formData[Form\Vacancy::EL_REQUIREMENTS] ?? null;
        $liabilities   = $formData[Form\Vacancy::EL_LIABILITIES] ?? null;

        if ($this->_user()->isAdmin()) {
            $companyId = $formData[Form\Vacancy::EL_COMPANY];
        } else {
            $companyId = $this->_user()->getCompanies()->asArray()[0]->getId();
        }

        $vacancy = new Domain\Vacancy($userId, $companyId, $positionName, $requirements, $liabilities);

        $this->_setVacancyFields($vacancy, $formData);

        self::$ds->vacancyMapper()->save($vacancy);

        if (!$this->update($vacancy, $formData)) {
            return null;
        }

        return $vacancy;
    }

    public function update(Domain\Vacancy $vacancy, array $formData) : bool
    {
        $this->_setVacancyFields($vacancy, $formData);

        $oldLocations = $vacancy->getLocations();

        $locationsData = $this->_setEntityId(
            $formData[Form\Vacancy::EL_LOCATIONS_COLLECTION],
            $vacancy->getId(),
            Domain\VacancyLocation\VacancyLocationMapper::COL_VACANCY_ID
        );

        $vacancyMapper = self::$ds->vacancyMapper();

        try {
            $transactionName = 'resumeSave_' . microtime();

            $vacancyMapper->beginTransaction($transactionName);

            $newVacancyLocations = self::$ds->vacancyLocationService()->createLocationsCollection($locationsData);

            $this->_updateCollection($oldLocations, $newVacancyLocations, self::$ds->vacancyLocationMapper());

            $vacancyMapper->save($vacancy);

            $vacancyMapper->commit($transactionName);

            return true;
        } catch (\Exception $ex) {
            $this->_logException($ex);

            $vacancyMapper->rollBack();
        }

        return false;
    }

    public function getPublicationDescription(DomainModelInterface $model) : string
    {
        if (! $model instanceof Domain\Vacancy) {
            throw new \Exception('Invalid model instance given. Instance of Model\Domain\Vacancy expected.');
        }

        $format = "Занятость: %s\nОбязанности: %s\nТребования: %s\nОплата: %s";

        $localityTitles = '';

        /** @var Domain\VacancyLocation $location */
        foreach ($model->getLocations()->asArray() as $location) {
            $localityTitle = $location->getCityId()
                ? ($location->getCity()->getTitleRu() ?? $location->getCity()->getTitleEn())
                : ($location->getCountry()->getTitleRu() ?? $location->getCountry()->getTitleEn())
            ;

            $localityTitles .= $localityTitles ? ", $localityTitle" : $localityTitle;
        }

        if ($localityTitles) {
            $format = "$localityTitles\n$format";
        }

        $employment   = Form\Vacancy::EMPLOYMENTS[$model->getEmployment()];
        $liabilities  = $this->_shortenLongText($this->purgeHtml($model->getLiabilities()));
        $requirements = $this->_shortenLongText($this->purgeHtml($model->getRequirements()));
        $payment      = $this->_makePaycheckString(
            $model->getPaycheckConst(),
            $model->getPaycheckMin(),
            $model->getPaycheckMax(),
            $model->getCurrency() ? $model->getCurrency()->getIsoCode() : ''
        );

        $payment = $payment ?: 'По договоренности';

        $text = sprintf($format, $employment, $liabilities, $requirements, $payment);

        return $text;
    }

    public function getLocationsAsTags(Domain\Vacancy $vacancy) : \stdClass
    {
        $result = new \stdClass();

        /** @var Domain\VacancyLocation $location */
        foreach ($vacancy->getLocations()->asArray() as $key => $location) {
            $title = null;
            $type  = null;

            if ($location->getCityId()) {
                $city  = $location->getCity();
                $title = $city->getTitleRu() ?? $city->getTitleEn();
                $type  = Form\Fieldset\Location::EL_TYPE_CITY;
            } else {
                $country = $location->getCountry();
                $title   = $country->getTitleRu() ?? $country->getTitleEn();
                $type    = Form\Fieldset\Location::EL_TYPE_COUNTRY;
            }

            $locationTag = [
                'location_id' => $location->getId(),
                'type'  => $type,
                'title' => $title,
            ];

            $result->$key = $locationTag;
        }

        return $result;
    }

    /**
     * @param Domain\Vacancy[] $vacancies
     */
    public function bulkIndex(array $vacancies, bool $recreateIndex = false) : array
    {
        $createBulkParams = function () use ($vacancies) {
            $params = [];

            foreach ($vacancies as $vacancy) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_VACANCY,
                        '_type' => ESService::INDEX_VACANCY
                    ]
                ];

                $params['body'][] = $this->indexiseModel($vacancy);
            }

            return $params;
        };

        return self::$ds->elasticSearchService()->bulkIndexModels(
            ESService::INDEX_VACANCY,
            $createBulkParams,
            $recreateIndex
        );
    }

    public function indexiseModel(Domain\Vacancy $vacancy) : array
    {
        $currency = $vacancy->getCurrency();

        $result = [
            VacancyMapper::PRIMARY                 => $vacancy->getId(),
            'user_id'                              => $vacancy->getUserId(),
            'city_ids'                             => [],
            'region_ids'                           => [],
            'country_ids'                          => [],
            VacancyMapper::COL_COMPANY_ID          => $vacancy->getCompanyId(),
            VacancyMapper::COL_PAYCHECK_CONST      => $vacancy->getPaycheckConst(),
            VacancyMapper::COL_PAYCHECK_MAX        => $vacancy->getPaycheckMax(),
            VacancyMapper::COL_PAYCHECK_MIN        => $vacancy->getPaycheckMin(),
            'currency'                             => $currency ? $currency->getIsoCode() : null,
            VacancyMapper::COL_PROFAREA_ID         => $vacancy->getProfareaId(),
            VacancyMapper::COL_POSITION_NAME       => $vacancy->getPositionName(),
            VacancyMapper::COL_REQUIRED_EXPERIENCE => $vacancy->getRequiredExperience(),
            VacancyMapper::COL_IS_REMOTE           => $vacancy->isRemote(),
            VacancyMapper::COL_EMPLOYMENT          => $vacancy->getEmployment(),
            'full_description'                     => self::$ds->vacancyService()->getFullDescWithoutHtml($vacancy),
            VacancyMapper::COL_VISIBILITY          => $vacancy->getVisibility(),
            VacancyMapper::COL_UPDATE_DATE         => $vacancy->getUpdateDate()->mysqlFormat()
        ];

        /** @var Domain\VacancyLocation $vacancyLocation */
        foreach ($vacancy->getLocations()->asArray() as $vacancyLocation) {
            if ($vacancyLocation->getCityId()) {
                $cityId = $vacancyLocation->getCityId();
                $result['city_ids'][$cityId] = $cityId;

                if ($vacancyLocation->getCity()->getRegionId()) {
                    $regionId = $vacancyLocation->getCity()->getRegionId();
                    $result['region_ids'][$regionId] = $regionId;
                }
            }

            $countryId = $vacancyLocation->getCountryId() ?? $vacancyLocation->getCity()->getCountryId();
            $result['country_ids'][$countryId] = $countryId;
        }

        $result['city_ids']    = array_values($result['city_ids']);
        $result['region_ids']  = array_values($result['region_ids']);
        $result['country_ids'] = array_values($result['country_ids']);

        return $result;
    }

    public function queryFromElasticsearch(VacancyFilter $filter) : array
    {
        $esService = self::$ds->elasticSearchService();

        $must = [];

        if ($filter->getKeywords() !== null) {
            $keywords = $this->_buildKeywordQueryString($filter->getKeywords());

            $keywordFields = [
                'position_name^2',
            ];

            if ($filter->getTitleOnly() !== true) {
                $keywordFields[] = 'full_description';
            }

            $must[] = [
                "query_string" => [
                    'fields' => $keywordFields,
                    'query' => $keywords,
                    'default_operator' => 'AND'
                ]
            ];
        } else {
            $must[]['match_all'] = new \stdClass();
        }

        if ($filter->getCityIds() !== null || $filter->getCountryIds() !== null || $filter->getRegionIds() !== null) {
            $terms = [];

            if ($filter->getCityIds() !== null) {
                $terms[]['terms'] = ['city_ids' => $filter->getCityIds()];
            }

            if ($filter->getRegionIds() !== null) {
                $terms[]['terms'] = ['region_ids' => $filter->getRegionIds()];
            }

            if ($filter->getCountryIds() !== null) {
                $terms[]['terms'] = ['country_ids' => $filter->getCountryIds()];
            }

            $must = array_merge($must, $terms);
        }

        if ($filter->getUserId() !== null) {
            $must[] = [
                'term' => [
                    VacancyMapper::COL_USER_ID => $filter->getUserId()
                ]
            ];
        }

        if ($filter->getProfareaIds() !== null) {
           $must[] = [
                'terms' => [
                    'profarea_id' => $filter->getProfareaIds()
                ]
            ];
        }

        if ($filter->getPaycheck() !== null) {
            $must[] = [
                'bool' => [
                    'should' => [
                        [
                            'range' => [
                                'paycheck_const' => [
                                    'gte' => $filter->getPaycheck()
                                ]
                            ]
                        ],
                        [
                            'range' => [
                                'paycheck_max' => [
                                    'gte' => $filter->getPaycheck()
                                ]
                            ]
                        ],
                        [
                            'range' => [
                                'paycheck_min' => [
                                    'gte' => $filter->getPaycheck()
                                ]
                            ]
                        ],
                        [
                            "bool" => [
                                "must_not" => [
                                    [
                                        'exists' => [
                                            'field' => 'paycheck_const'
                                        ]
                                    ],
                                    [
                                        'exists' => [
                                            'field' => 'paycheck_max'
                                        ]
                                    ],
                                    [
                                        'exists' => [
                                            'field' => 'paycheck_min'
                                        ]
                                    ]
                                ],
                            ]
                        ]
                    ],
                    "minimum_should_match" => 1
                ],

            ];
        }

        if ($filter->getIsRemote() !== null) {
            $must[] = [
                'term' => [
                    'is_remote' => $filter->getIsRemote()
                ]
            ];
        }

        if ($filter->getEmployments() !== null) {
            $must[] = [
                'terms' => [
                    'employment' => $filter->getEmployments()
                ]
            ];
        }

        if ($filter->getExperience() !== null) {
            $rangeConditions = [];

            foreach ($filter->getExperience() as $expValue) {
                $rangeConditions[] = [
                    'term' => [
                        'required_experience' => $expValue
                    ]
                ];
            }

            $must[] = [
                'bool' => [
                    'should' => $rangeConditions,
                    'minimum_should_match' => 1
                ]
            ];
        }

        if ($filter->getPublishPeriod() !== VacancyFilter::PUBLISH_ALL_TIME) {
            $rangeProps = [];

            switch($filter->getPublishPeriod()) {
                case VacancyFilter::PUBLISH_PER_DAY:
                    $rangeProps = [
                        'gte' => 'now-1d'
                    ];
                    break;
                case VacancyFilter::PUBLISH_PER_WEEK:
                    $rangeProps = [
                        'gte' => 'now-1w'
                    ];
                    break;
                case VacancyFilter::PUBLISH_PER_2_WEEKS:
                    $rangeProps = [
                        'gte' => 'now-2w'
                    ];
                    break;
                case VacancyFilter::PUBLISH_PER_MONTH:
                    $rangeProps = [
                        'gte' => 'now-1M'
                    ];
                    break;
            }

            $must[] = [
                'range' => [
                    'update_date' => $rangeProps
                ]
            ];
        }

        if ($filter->getVisibility() !== null) {
            $must[] = [
                'term' => [
                    'visibility' => $filter->getVisibility()
                ]
            ];
        }

        $query = [
            'bool' => [
                'must' => $must
            ]
        ];

        return $esService->query(
            ElasticSearch::INDEX_VACANCY,
            $query,
            $filter->getPageNumber(),
            $filter->getItemsPerPage(),
            $filter->getOrder() ?? []
        );
    }

    /**
     * Вытаскивает id из индексных доков и фетчит модели из кеша/базы по ним.
     * Из моделей достает их record-представление и возвращает массивом.
     *
     * @return array
     */
    public function elasticDataToJsonModels(array $elasticSearchDocuments, bool $withCount = false) : array
    {
        $vacancyArrayRecords = [];
        $vacancyArrayRecordsNoPaycheck = [];

        $vacancyMapper = self::$ds->vacancyMapper();

        foreach ($elasticSearchDocuments['hits'] as $document) {
            $vacancy = $vacancyMapper->fetch($document['_source']['id']);

            if (!$vacancy) {
                continue;
            }

            $vacancyRecord = $this->getVacancyRowsAsListItem($vacancy);

            $views = self::$ds->viewService()->getEntityViewsCount($vacancy->getId(), Domain\View::ENTITY_TYPE_VACANCY);

            $vacancyRecord['views'] = $views;

            if ($vacancyRecord['paycheck']) {
                $vacancyArrayRecords[] = $vacancyRecord;
            } else {
                $vacancyArrayRecordsNoPaycheck[] = $vacancyRecord;
            }
        }

        $vacancyArrayRecords = array_merge($vacancyArrayRecords, $vacancyArrayRecordsNoPaycheck);

        if ($withCount) {
            $vacancyArrayRecords[self::KEY_TOTAL_ITEMS] = $elasticSearchDocuments['total'];
        }

        return $vacancyArrayRecords;
    }

    public function makeSimpleListJson(array $elasticSearchResult) : array
    {
        $result = [];

        foreach ($elasticSearchResult['hits'] as $document) {
            $vacancyData = [];

            $elasticRow = $document['_source'];

            $company = self::$ds->companyMapper()->fetch($elasticRow[VacancyMapper::COL_COMPANY_ID]);

            $vacancyData[VacancyMapper::PRIMARY]           = $elasticRow[VacancyMapper::PRIMARY];
            $vacancyData[VacancyMapper::COL_POSITION_NAME] = $elasticRow[VacancyMapper::COL_POSITION_NAME];
            $vacancyData['company']                        = ($company->getOpf() ? "{$company->getOpf()} " : '')
                . $company->getOfficialName()
            ;

            $vacancyData['paycheck'] = $this->_makePaycheckString(
                $elasticRow[VacancyMapper::COL_PAYCHECK_CONST],
                $elasticRow[VacancyMapper::COL_PAYCHECK_MIN],
                $elasticRow[VacancyMapper::COL_PAYCHECK_MAX],
                $elasticRow['currency']
            );

            $result[] = $vacancyData;
        }

        return $result;
    }

    public function getFullDescWithoutHtml(Domain\Vacancy $vacancy) : string
    {
        return $this->purgeHtml("{$vacancy->getRequirements()} {$vacancy->getLiabilities()} {$vacancy->getDescription()}");
    }

    public function getVacancyRowsAsListItem(Domain\Vacancy $vacancy) : array
    {
        $vacancyRecord                  = $vacancy->getTableRow();
        $vacancyRecord['company']       = "{$vacancy->getCompany()->getOpf()} {$vacancy->getCompany()->getOfficialName()}";
        $vacancyRecord['currency_code'] = $vacancy->getCurrency() ? $vacancy->getCurrency()->getIsoCode() : null;
        $vacancyRecord['currency_name'] = $vacancy->getCurrency() ? $vacancy->getCurrency()->getName() : null;

        $vacancyRecord['paycheck'] = $this->_makePaycheckString(
            $vacancyRecord[VacancyMapper::COL_PAYCHECK_CONST],
            $vacancyRecord[VacancyMapper::COL_PAYCHECK_MIN],
            $vacancyRecord[VacancyMapper::COL_PAYCHECK_MAX]
        );

        unset($vacancyRecord[VacancyMapper::COL_PAYCHECK_CONST]);
        unset($vacancyRecord[VacancyMapper::COL_PAYCHECK_MIN]);
        unset($vacancyRecord[VacancyMapper::COL_PAYCHECK_MAX]);
        unset($vacancyRecord[VacancyMapper::COL_REQUIREMENTS]);
        unset($vacancyRecord[VacancyMapper::COL_DESCRIPTION]);
        unset($vacancyRecord[VacancyMapper::COL_LIABILITIES]);
        unset($vacancyRecord[VacancyMapper::COL_CONDITIONS]);

        $vacancyRecord['full_description'] = $this->_shortenLongText($this->getFullDescWithoutHtml($vacancy));

        if ($vacancyRecord['paycheck']) {
            $vacancyArrayRecords[] = $vacancyRecord;
        } else {
            $vacancyArrayRecordsNoPaycheck[] = $vacancyRecord;
        }

        return $vacancyRecord;
    }

    protected function _setVacancyFields(Domain\Vacancy $vacancy, array $formData)
    {
        $companyId     = $formData[Form\Vacancy::EL_COMPANY] ?? $vacancy->getCompanyId();
        $positionName  = $formData[Form\Vacancy::EL_POSITION_NAME] ?? null;
        $scheduleType  = $formData[Form\Vacancy::EL_EMPLOYMENT] ?? null;
        $isRemote      = $formData[Form\Vacancy::EL_IS_REMOTE] ?? false;
        $requirements  = $formData[Form\Vacancy::EL_REQUIREMENTS] ?? null;
        $liabilities   = $formData[Form\Vacancy::EL_LIABILITIES] ?? null;
        $conditions    = $formData[Form\Vacancy::EL_CONDITIONS] ?? null;
        $description   = $formData[Form\Vacancy::EL_DESCRIPTION] ?? null;
        $paycheckMin   = $formData[Form\Vacancy::EL_PAYCHECK_MIN] ?: null;
        $paycheckMax   = $formData[Form\Vacancy::EL_PAYCHECK_MAX] ?: null;
        $currencyId    = $formData[Form\Vacancy::EL_CURRENCY] ?: null;
        $profareaId    = $formData[Form\Vacancy::EL_PROFAREA] ?: null;
        $reqExperience = $formData[Form\Vacancy::EL_REQUIRED_EXPERIENCE] ?? Domain\Vacancy::EXP_NOT_REQUIRED;
        $visibility    = $formData[Form\Vacancy::EL_VISIBILITY];

        $vacancy
            ->setCompanyId($companyId)
            ->setPositionName($positionName)
            ->setIsRemote($isRemote === Form\Vacancy::CHECKED)
            ->setEmployment($scheduleType)
            ->setRequirements($requirements)
            ->setLiabilities($liabilities)
            ->setConditions($conditions)
            ->setDescription($description)
            ->setProfareaId($profareaId)
            ->setRequiredExperience($reqExperience)
            ->setCurrencyId($currencyId)
            ->setVisibility($visibility)
        ;

        if ($paycheckMin && $paycheckMax && $paycheckMin === $paycheckMax) {
            $vacancy
                ->setPaycheckConst($paycheckMin)
                ->setPaycheckMin(null)
                ->setPaycheckMax(null)
            ;
        } else {
            $vacancy
                ->setPaycheckConst(null)
                ->setPaycheckMin($paycheckMin)
                ->setPaycheckMax($paycheckMax)
            ;
        }
    }

    protected function _makePaycheckString(?int $paycheckConst, ?int $paycheckMin, ?int $paycheckMax, string $currency = null) : string
    {
        $paycheck = '';

        $currency = $currency ? " $currency" : '';

        if ($paycheckConst) {
            $paycheck = number_format($paycheckConst, 0, '.', ' ') . ($currency);
        } else {
            if ($paycheckMin) {
                $paycheckMin = number_format($paycheckMin, 0, '.', ' ');
                $paycheck = "от {$paycheckMin}";

                if (!$paycheckMax) {
                    $paycheck .= $currency;
                }
            }

            if ($paycheckMax) {
                $paycheckMax = number_format($paycheckMax, 0, '.', ' ');
                $paycheck .= ($paycheck ? ' ' : '') . "до {$paycheckMax}{$currency}";
            }
        }

        return $paycheck;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        if (!$filter instanceof VacancyFilter) {
            throw new \Exception('Invalid sideFilter type');
        }

        $vacancyTableName = self::$ds->vacancyMapper()->getTable()->getName();

        $where = '';

        if ($filter->getUserId() !== null) {
            $where = $this->_concatenateQueryParts($where, "user_id = {$filter->getUserId()}");
        }

        $select = self::$ds->vacancyMapper()->select()->cols(["$vacancyTableName.*"]);

        if ($filter->getCountryIds() || $filter->getRegionIds() || $filter->getCityIds()) {
            $regionWherePart = '';

            if ($filter->getCityIds()) {
                $cityIds = join(',', $filter->getCityIds());

                $regionWherePart = "city_id IN ($cityIds)";
            }

            if ($filter->getCountryIds() || $filter->getRegionIds()) {
                $cityTableName = self::$ds->cityMapper()->getTable()->getName();
                $select->join('INNER', "$cityTableName c", "c.id = $vacancyTableName.city_id");

                if ($filter->getCountryIds()) {
                    $countryIds = join(',', $filter->getCountryIds());

                    $regionWherePart .= $regionWherePart ? " OR " : '';
                    $regionWherePart .= "c.country_id IN ($countryIds)";
                }

                if ($filter->getRegionIds()) {
                    $regionIds = join(',', $filter->getRegionIds());

                    $regionWherePart .= $regionWherePart ? " OR " : '';
                    $regionWherePart .= "c.region_id IN ($regionIds)";
                }
            }

            $where = $this->_concatenateQueryParts($where, "($regionWherePart)", 'AND');
        }

        if ($filter->getProfareaIds() !== null) {
            $profareaIds = join(',', $filter->getProfareaIds());
            $where = $this->_concatenateQueryParts($where, "$vacancyTableName.profarea_id IN ($profareaIds)", 'AND');
        }

        if ($where) {
            $select->where($where);
        }

        if ($filter->getOrder() !== null) {
            $ordering = [];

            foreach ($filter->getOrder() as $field => $order) {
                if ($field === VacancyFilter::PAYCHECK) {
                    $exactValue = sprintf(
                        'COALESCE(%s, COALESCE(%s, %s))',
                        VacancyMapper::COL_PAYCHECK_CONST,
                        VacancyMapper::COL_PAYCHECK_MIN,
                        VacancyMapper::COL_PAYCHECK_MAX
                    );

                    $ordering[] = "($exactValue IS NOT NULL) DESC";
                    $ordering[] = "$exactValue {$filter->getOrder()}";

                    continue;
                }

                $ordering[] = "($field IS NOT NULL) DESC";
                $ordering[] = "$field $order";
            }


            $select->orderBy($ordering);
        }

        return $this->_setPaging($filter, $select);
    }
}