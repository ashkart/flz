<?php

namespace Application\Model\Service\Filter;

use Application\Model\Domain\Vacancy;

class VacancyFilter extends AbstractFilter
{
    const PAYCHECK = 'paycheck';

    /**
     * @var int | null
     */
    protected $_userId = null;

    /**
     * @var string | null
     */
    protected $_positionName = null;

    /**
     * @var string | null
     */
    protected $_requirements = null;

    /**
     * @var string | null
     */
    protected $_liabilities = null;

    /**
     * @var string | null
     */
    protected $_conditions = null;

    /**
     * @var string | null
     */
    protected $_description = null;

    /**
     * @var int[] | null
     */
    protected $_cityIds = null;

    /**
     * @var int[] | null
     */
    protected $_countryIds = null;

    /**
     * @var int[] | null
     */
    protected $_regionIds = null;

    /**
     * @var null | int[]
     */
    protected $_profareaIds = null;

    /**
     * @var null | string
     */
    protected $_keywords = null;

    /**
     * @var bool | null
     */
    protected $_titleOnly = null;

    /**
     * @var null | int
     */
    protected $_paycheck = null;

    /**
     * @var null | bool
     */
    protected $_isRemote = null;

    /**
     * @var null | string[]
     */
    protected $_employments = null;

    /**
     * @var null | int[]
     */
    protected $_experience = null;

    /**
     * @var int
     */
    protected $_publishPeriod = self::PUBLISH_PER_DAY;

    /**
     * @var string | null
     */
    protected $_visibility;

    public function getVisibility(): ? string
    {
        return $this->_visibility;
    }

    public function setVisibility(string $visibility): self
    {
        $this->_visibility = $visibility;
        return $this;
    }

    public function getCityIds(): ?array
    {
        return $this->_cityIds;
    }

    public function setCityIds(?array $cityIds): self
    {
        $this->_cityIds = $cityIds;
        return $this;
    }

    public function getCountryIds(): ?array
    {
        return $this->_countryIds;
    }

    public function setCountryIds(?array $countryIds): self
    {
        $this->_countryIds = $countryIds;
        return $this;
    }

    public function getRegionIds(): ?array
    {
        return $this->_regionIds;
    }

    public function setRegionIds(?array $regionIds): self
    {
        $this->_regionIds = $regionIds;
        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->_userId;
    }

    public function setUserId(?int $userId): void
    {
        $this->_userId = $userId;
    }

    public function getPositionName(): ?string
    {
        return $this->_positionName;
    }

    public function setPositionName(?string $positionName): void
    {
        $this->_positionName = $positionName;
    }

    public function getRequirements(): ?string
    {
        return $this->_requirements;
    }

    public function setRequirements(?string $requirements): void
    {
        $this->_requirements = $requirements;
    }

    public function getLiabilities(): ?string
    {
        return $this->_liabilities;
    }

    public function setLiabilities(?string $liabilities): void
    {
        $this->_liabilities = $liabilities;
    }

    public function getConditions(): ?string
    {
        return $this->_conditions;
    }

    public function setConditions(?string $conditions): void
    {
        $this->_conditions = $conditions;
    }

    public function getDescription(): ?string
    {
        return $this->_description;
    }

    public function setDescription(?string $description): void
    {
        $this->_description = $description;
    }

    /**
     * @return int[]|null
     */
    public function getProfareaIds(): ?array
    {
        return $this->_profareaIds;
    }

    /**
     * @param int[]|null $profareaIds
     */
    public function setProfareaIds(?array $profareaIds): self
    {
        $this->_profareaIds = $profareaIds;
        return $this;
    }

    public function setKeywords(?string $keywords) : self
    {
        $this->_keywords = $keywords;
        return $this;
    }

    public function getKeywords() : ? string
    {
        return $this->_keywords;
    }

    public function getTitleOnly(): ?bool
    {
        return $this->_titleOnly;
    }

    public function setTitleOnly(?bool $titleOnly): self
    {
        $this->_titleOnly = $titleOnly;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaycheck(): ?int
    {
        return $this->_paycheck;
    }

    /**
     * @param int|null $paycheck
     */
    public function setPaycheck(?int $paycheck): self
    {
        $this->_paycheck = $paycheck;
        return $this;
    }

    public function getIsRemote(): ?bool
    {
        return $this->_isRemote;
    }

    public function setIsRemote(?bool $isRemote): self
    {
        $this->_isRemote = $isRemote;
        return $this;
    }

    public function getEmployments(): ?array
    {
        return $this->_employments;
    }

    public function setEmployments(?array $employments): self
    {
        if ($employments) {
            foreach ($employments as $employment) {
                if (!in_array($employment, Vacancy::EMPLOYMENTS)) {
                    throw new \Exception('Wrong employment type given');
                }
            }
        }

        $this->_employments = $employments;
        return $this;
    }

    public function getExperience(): ?array
    {
        return $this->_experience;
    }

    public function setExperience(?array $experience): self
    {
        if ($experience) {
            foreach ($experience as $experienceValue) {
                if (!in_array($experienceValue, Vacancy::EXP_VALUES)) {
                    throw new \Exception('Wrong experience filter value given');
                }
            }
        }

        $this->_experience = $experience;
        return $this;
    }

    public function getPublishPeriod(): ?int
    {
        return $this->_publishPeriod;
    }

    public function setPublishPeriod(int $updateDatePeriod): self
    {
        if (!in_array($updateDatePeriod, self::PUBLISH_PERIODS)) {
            throw new \Exception('Wrong update period filter value');
        }

        $this->_publishPeriod = $updateDatePeriod;
        return $this;
    }
}