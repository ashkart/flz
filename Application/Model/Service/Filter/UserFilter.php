<?php

namespace Application\Model\Service\Filter;

class UserFilter extends AbstractFilter
{
    /**
     * @var int | null
     */
    protected $_id = null;

    /**
     * @var int | null
     */
    protected $_cityId = null;

    public function getCityId(): ?int
    {
        return $this->_cityId;
    }

    public function setCityId(?int $cityId): self
    {
        $this->_cityId = $cityId;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->_id;
    }

    public function setId(?int $id): self
    {
        $this->_id = $id;
        return $this;
    }
}