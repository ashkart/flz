<?php

namespace Application\Model\Service\Filter;

class CompanyFilter extends AbstractFilter
{
    /**
     * @var int | null
     */
    protected $_userId = null;

    /**
     * @var null | string
     */
    protected $_keyword = null;

    public function getUserId() : ?int
    {
        return $this->_userId;
    }

    public function setUserId($userId): self
    {
        $this->_userId = $userId;
        return $this;
    }

    public function getKeyword() : ? string
    {
        return $this->_keyword;
    }

    public function setKeyword(?string $officialName): self
    {
        $this->_keyword = $officialName;
        return $this;
    }
}