<?php

namespace Application\Model\Service\Filter;

class AbstractFilter
{
    const ORDER_ASC  = 'ASC';
    const ORDER_DESC = 'DESC';

    const ORDER_VALUES = [
        self::ORDER_ASC,
        self::ORDER_DESC
    ];

    const PUBLISH_PER_DAY     = 1;
    const PUBLISH_PER_WEEK    = 2;
    const PUBLISH_PER_2_WEEKS = 3;
    const PUBLISH_PER_MONTH   = 4;
    const PUBLISH_ALL_TIME    = 5;

    const PUBLISH_PERIODS = [
        self::PUBLISH_PER_DAY,
        self::PUBLISH_PER_WEEK,
        self::PUBLISH_PER_2_WEEKS,
        self::PUBLISH_PER_MONTH,
        self::PUBLISH_ALL_TIME,
    ];

    /**
     * Дефолтное количество строк для списков
     */
    const PER_PAGE_DEFAULT = 10;

    /**
     * Дефолтный номер страницы для списков
     */
    const PAGE_DEFAULT     = 0;

    /**
     * @var int
     */
    protected $_itemsPerPage = self::PER_PAGE_DEFAULT;
    
    /**
     * @var int
     */
    protected $_pageNumber = self::PAGE_DEFAULT;
    
    /**
     * @var string[] | null
     */
    protected $_order = null;

    public function getItemsPerPage(): ?int
    {
        return $this->_itemsPerPage;
    }

    public function getPageNumber(): ?int
    {
        return $this->_pageNumber;
    }

    public function setPaging(int $itemsPerPage = self::PER_PAGE_DEFAULT, int $pageNumber = self::PAGE_DEFAULT): self
    {
        if ($itemsPerPage === null XOR $pageNumber === null) {
            throw new \Exception('Both parameters required to be null or set');
        }

        $this->_itemsPerPage = $itemsPerPage;
        $this->_pageNumber   = $pageNumber;
        return $this;
    }

    public function getOrder(): ?array
    {
        return $this->_order;
    }

    public function setOrder(array $orderMapping): self
    {
        $this->_order = $orderMapping;
        return $this;
    }

    protected function _checkValues(?array $values, array $haystack)
    {
        if ($values === null) {
            return;
        }

        if (array_diff($values, $haystack)) {
            throw new \Exception('Invalid filter value');
        }
    }
}