<?php

namespace Application\Model\Service\Filter;

use Application\Model\Domain\View;

class ViewFilter extends AbstractFilter
{
    /**
     * @var null | int
     */
    protected $_userId = null;

    /**
     * @var null | int
     */
    protected $_companyId = null;

    /**
     * @var null | int
     */
    protected $_entityId = null;

    /**
     * @var null | string
     */
    protected $_entityType = null;

    public function getUserId(): ?int
    {
        return $this->_userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->_userId = $userId;
        return $this;
    }

    public function getCompanyId(): ?int
    {
        return $this->_companyId;
    }

    public function setCompanyId(?int $companyId): self
    {
        $this->_companyId = $companyId;
        return $this;
    }

    public function getEntityId(): ?int
    {
        return $this->_entityId;
    }

    public function setEntityId(?int $entityId): self
    {
        $this->_entityId = $entityId;
        return $this;
    }

    public function getEntityType(): ?string
    {
        return $this->_entityType;
    }

    public function setEntityType(?string $entityType): self
    {
        if (!in_array($entityType, View::ENTITY_TYPES)) {
            throw new \Exception('Invalid entity type given.');
        }

        $this->_entityType = $entityType;
        return $this;
    }
}