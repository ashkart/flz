<?php

namespace Application\Model\Service\Filter;

class CityFilter extends AbstractFilter
{
    /**
     * @var int | null
     */
    protected $_countryId = null;

    /**
     * @var int | null
     */
    protected $_regionId = null;

    /**
     * @var string | null
     */
    protected $_title = null;

    /**
     * @var string | null
     */
    protected $_area = null;

    /**
     * @var string | null
     */
    protected $_region = null;

    public function getTitle(): ?string
    {
        return $this->_title;
    }

    public function setTitle(?string $title): self
    {
        $this->_title = $title;
        return $this;
    }

    public function getArea(): ?string
    {
        return $this->_area;
    }

    public function setArea(?string $area): self
    {
        $this->_area = $area;
        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->_region;
    }

    public function setRegion(?string $region): self
    {
        $this->_region = $region;
        return $this;
    }

    public function getCountryId(): ?int
    {
        return $this->_countryId;
    }

    public function setCountryId(?int $countryId): self
    {
        $this->_countryId = $countryId;
        return $this;
    }

    public function getRegionId(): ?int
    {
        return $this->_regionId;
    }

    public function setRegionId(?int $regionId): self
    {
        $this->_regionId = $regionId;
        return $this;
    }
}