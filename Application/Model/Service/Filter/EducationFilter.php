<?php

namespace Application\Model\Service\Filter;

class EducationFilter extends AbstractFilter
{
    /**
     * @var int | null
     */
    protected $_resumeId = null;

    /**
     * @var int | null
     */
    protected $_cityId = null;

    public function getResumeId(): ?int
    {
        return $this->_resumeId;
    }

    public function setResumeId(?int $resumeId): self
    {
        $this->_resumeId = $resumeId;
        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->_cityId;
    }

    public function setCityId(?int $cityId): self
    {
        $this->_cityId = $cityId;
        return $this;
    }
}