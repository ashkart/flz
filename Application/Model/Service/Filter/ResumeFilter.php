<?php

namespace Application\Model\Service\Filter;

use Application\Model\Domain\Resume;
use Application\Model\Domain\Vacancy;

class ResumeFilter extends AbstractFilter
{
    const PAYCHECK = 'paycheck';

    const PUBLISH_PER_DAY     = 1;
    const PUBLISH_PER_WEEK    = 2;
    const PUBLISH_PER_2_WEEKS = 3;
    const PUBLISH_PER_MONTH   = 4;
    const PUBLISH_ALL_TIME    = 5;

    const PUBLISH_PERIODS = [
        self::PUBLISH_PER_DAY,
        self::PUBLISH_PER_WEEK,
        self::PUBLISH_PER_2_WEEKS,
        self::PUBLISH_PER_MONTH,
        self::PUBLISH_ALL_TIME,
    ];

    const GENDER_ALL = 'all';

    const AGE_14_18   = 0;
    const AGE_18_30   = 1;
    const AGE_30_40   = 2;
    const AGE_40_50   = 3;
    const AGE_50_60   = 4;
    const AGE_FROM_60 = 5;

    const AGES = [
        self::AGE_14_18,
        self::AGE_18_30,
        self::AGE_30_40,
        self::AGE_40_50,
        self::AGE_50_60,
        self::AGE_FROM_60,
    ];

    const AGE_14 = 14;
    const AGE_18 = 18;
    const AGE_30 = 30;
    const AGE_40 = 40;
    const AGE_50 = 50;
    const AGE_60 = 60;

    const EXP_NONE  = 0;
    const EXP_1_YR  = 1;
    const EXP_2_YRS = 2;
    const EXP_3_YRS = 3;
    const EXP_5_YRS = 5;

    const EDUCATION_LEVEL_NO_MATTER = 'no_matter';

    /**
     * @var int | null
     */
    protected $_cityId = null;

    /**
     * @var int | null
     */
    protected $_userId = null;

    /**
     * @var string | null
     */
    protected $_positionName = null;

    /**
     * @var string | null
     */
    protected $_description = null;

    /**
     * @var int[] | null
     */
    protected $_cityIds = null;

    /**
     * @var int[] | null
     */
    protected $_countryIds = null;

    /**
     * @var int[] | null
     */
    protected $_regionIds = null;

    /**
     * @var null | bool
     */
    protected $_isRelocateReady = null;

    /**
     * @var null | string
     */
    protected $_keywords = null;

    /**
     * @var bool | null
     */
    protected $_titleOnly = null;

    /**
     * @var null | int
     */
    protected $_paycheckMin = null;

    /**
     * @var null | int
     */
    protected $_paycheckMax = null;

    /**
     * @var null | bool
     */
    protected $_isRemote = null;

    /**
     * @var null | string[]
     */
    protected $_employments = null;

    /**
     * @var null | int[]
     */
    protected $_experience = null;

    /**
     * @var null | string[]
     */
    protected $_educationLevels = null;

    /**
     * @var null | int[]
     */
    protected $_age = null;

    /**
     * @var string | null
     */
    protected $_gender = ResumeFilter::GENDER_ALL;

    /**
     * @var int
     */
    protected $_publishPeriod = self::PUBLISH_PER_DAY;

    /**
     * @var null | string[]
     */
    protected $_skills = null;

    /**
     * @var string | null
     */
    protected $_visibility;

    public function getVisibility(): ? string
    {
        return $this->_visibility;
    }

    public function setVisibility(string $visibility): self
    {
        $this->_visibility = $visibility;
        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->_cityId;
    }

    public function setCityId(?int $cityId): self
    {
        $this->_cityId = $cityId;
        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->_userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->_userId = $userId;
        return $this;
    }

    public function getPositionName(): ?string
    {
        return $this->_positionName;
    }

    public function setPositionName(?string $positionName): self
    {
        $this->_positionName = $positionName;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->_description;
    }

    public function setDescription(?string $description): self
    {
        $this->_description = $description;
        return $this;
    }

    public function getCityIds(): ?array
    {
        return $this->_cityIds;
    }

    public function setCityIds(?array $cityIds): self
    {
        $this->_cityIds = $cityIds;
        return $this;
    }

    public function getCountryIds(): ?array
    {
        return $this->_countryIds;
    }

    public function setCountryIds(?array $countryIds): self
    {
        $this->_countryIds = $countryIds;
        return $this;
    }

    public function getRegionIds(): ?array
    {
        return $this->_regionIds;
    }

    public function setRegionIds(?array $regionIds): self
    {
        $this->_regionIds = $regionIds;
        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->_keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->_keywords = $keywords;
        return $this;
    }

    public function getTitleOnly(): ?bool
    {
        return $this->_titleOnly;
    }

    public function setTitleOnly(?bool $titleOnly): self
    {
        $this->_titleOnly = $titleOnly;
        return $this;
    }

    public function getPaycheckMin(): ?int
    {
        return $this->_paycheckMin;
    }

    public function setPaycheckMin(?int $value): self
    {
        $this->_paycheckMin = $value;
        return $this;
    }

    public function getPaycheckMax(): ?int
    {
        return $this->_paycheckMax;
    }

    public function setPaycheckMax(?int $value): self
    {
        $this->_paycheckMax = $value;
        return $this;
    }

    public function isRemote(): ?bool
    {
        return $this->_isRemote;
    }

    public function setIsRemote(?bool $isRemote): self
    {
        $this->_isRemote = $isRemote;
        return $this;
    }

    public function getEmployments(): ?array
    {
        return $this->_employments;
    }

    public function setEmployments(?array $employments): self
    {
        $this->_checkValues($employments, Vacancy::EMPLOYMENTS);

        $this->_employments = $employments;
        return $this;
    }

    /**
     * @return int[]|null
     */
    public function getExperience(): ? array
    {
        return $this->_experience;
    }

    /**
     * @param int[]|null $experience
     */
    public function setExperience(?array $experience): self
    {
        $this->_checkValues($experience, Vacancy::EXP_VALUES);

        $this->_experience = $experience;
        return $this;
    }

    public function getAge(): ?array
    {
        return $this->_age;
    }

    public function setAge(?array $age): self
    {
        $this->_checkValues($age, self::AGES);

        $this->_age = $age;
        return $this;
    }

    public function getPublishPeriod(): int
    {
        return $this->_publishPeriod;
    }

    public function setPublishPeriod(int $publishPeriod): self
    {
        $this->_checkValues([$publishPeriod], self::PUBLISH_PERIODS);

        $this->_publishPeriod = $publishPeriod;
        return $this;
    }

    public function getGender(): ?string
    {
        return $this->_gender;
    }

    public function setGender(?string $gender): self
    {
        $this->_checkValues([$gender], Resume::GENDERS);

        $this->_gender = $gender;
        return $this;
    }

    public function getRelocateReady(): ?bool
    {
        return $this->_isRelocateReady;
    }

    public function setRelocateReady(?bool $isRelocateReady): self
    {
        $this->_isRelocateReady = $isRelocateReady;
        return $this;
    }

    public function getEducationLevels(): ?array
    {
        return $this->_educationLevels;
    }

    public function setEducationLevels(?array $educationLevels): self
    {
        $this->_checkValues($educationLevels, array_merge([self::EDUCATION_LEVEL_NO_MATTER], Resume::EDUCATION_LEVELS));

        $this->_educationLevels = $educationLevels;
        return $this;
    }

    public function getSkills(): ?array
    {
        return $this->_skills;
    }

    public function setSkills(?array $skills): self
    {
        $this->_skills = $skills;
        return $this;
    }
}