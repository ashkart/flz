<?php

namespace Application\Model\Service\Filter;

use Application\Model\Domain\Callback;

class CallbackFilter extends AbstractFilter
{
    /**
     * @var null | int
     */
    protected $_userId = null;

    /**
     * @var null | string
     */
    protected $_entityType = null;

    /**
     * @var null | int
     */
    protected $_entityId = null;

    /**
     * @var null | array
     */
    protected $_states = null;

    public function getUserId(): ?int
    {
        return $this->_userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->_userId = $userId;
        return $this;
    }

    public function getEntityType(): ?string
    {
        return $this->_entityType;
    }

    public function setEntityType(?string $entityType): self
    {
        $this->_entityType = $entityType;
        return $this;
    }

    public function getEntityId(): ?int
    {
        return $this->_entityId;
    }

    public function setEntityId(?int $entityId): self
    {
        $this->_entityId = $entityId;
        return $this;
    }

    public function getStates(): ? array
    {
        return $this->_states;
    }

    public function setStates(?array $states): self
    {
        if (array_diff($states, Callback::STATES)) {
            throw new \Exception('Invalid state given');
        }

        $this->_states = $states;

        return $this;
    }
}