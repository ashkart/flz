<?php

namespace Application\Model\Service;

use Application\Model\Domain\ClientData;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;

class Country extends AbstractService
{
    public function getLocalizedDataForSelection() : array
    {
        $locale = self::_user()->getLocale();

        $countries = self::$ds->countryMapper()->fetchAll([]);

        switch($locale) {
            case ClientData::LOCALE_RU:
                $getTitle = 'getTitleRu';
                break;
            default:
                $getTitle = 'getTitleEn';
        }

        $result = [];

        foreach ($countries as $country) {
            $result[$country->getId()] = $country->$getTitle();
        }

        return $result;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented yet');
        // TODO: Implement _prepareSelect() method.
    }
}