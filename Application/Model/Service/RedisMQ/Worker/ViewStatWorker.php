<?php

namespace Application\Model\Service\RedisMQ\Worker;

use Application\Model\Domain\View;
use Application\Model\Service\Filter\ViewFilter;

use Library\Master\Date\DateTime;
use Library\Master\QueueManager\AbstractWorker;
use Psr;
use Resque;
use Resque_Event;
use Resque_Job_DirtyExitException;

class ViewStatWorker extends AbstractWorker
{
    const DEFAULT_MAX_BUNCH_SIZE = 1000;

    /**
     * @var \Application\Model\Service\View
     */
    protected $_viewService;

    /**
     * @var int
     */
    protected $_maxBunchSize;

    public function __construct($queues, \Application\Model\Service\View $viewService, int $maxBunchSize = 1000)
    {
        $this->_viewService  = $viewService;
        $this->_maxBunchSize = $maxBunchSize;

        parent::__construct($queues);
    }

    public function work($interval = Resque::DEFAULT_INTERVAL, $blocking = false)
    {
        $this->updateProcLine('Starting');
        $this->startup();

        $views = [];

        $sleptTime = 0;

        while(true) {
            if($this->shutdown) {
                break;
            }

            // Attempt to find and reserve a job
            $job = false;
            if(!$this->paused) {
                if($blocking === true) {
                    $this->logger->log(Psr\Log\LogLevel::INFO, 'Starting blocking with timeout of {interval}', array('interval' => $interval));
                    $this->updateProcLine('Waiting for ' . implode(',', $this->queues) . ' with blocking timeout ' . $interval);
                } else {
                    $this->updateProcLine('Waiting for ' . implode(',', $this->queues) . ' with interval ' . $interval);
                }

                /** @var \Resque_Job $job */
                $job = $this->reserve($blocking, $interval);
            }

            if ($views && ($sleptTime >= $interval || count($views) >= $this->_maxBunchSize)) {
                $this->processViews($views);
                $views = [];
                $sleptTime = 0;
            }

            if(!$job) {
                if($blocking === false)
                {
                    // If no job was found, we sleep for $interval before continuing and checking again
                    $this->logger->log(Psr\Log\LogLevel::INFO, 'Sleeping for {interval}', array('interval' => $interval));
                    if($this->paused) {
                        $this->updateProcLine('Paused');
                    }
                    else {
                        $this->updateProcLine('Waiting for ' . implode(',', $this->queues));
                    }

                    usleep($interval * 1000000);
                    $sleptTime += $interval;
                }

                continue;
            }

            $this->logger->log(Psr\Log\LogLevel::NOTICE, 'Starting work on {job}', array('job' => $job));
            Resque_Event::trigger('beforeFork', $job);
            $this->workingOn($job);

            $status = 'Processing ' . $job->queue . ' since ' . strftime('%F %T');
            $this->updateProcLine($status);
            $this->logger->log(Psr\Log\LogLevel::INFO, $status);
            $this->perform($job);

            $views[] = $job->payload['args'][0];

            $this->doneWorking();
        }

        $this->unregisterWorker();
    }

    protected function processViews(array $views)
    {
        $filters = [];

        $views = array_map(
            function ($row) {
                return new View(
                    $row[View\ViewMapper::COL_ENTITY_ID],
                    $row[View\ViewMapper::COL_ENTITY_TYPE],
                    $row[View\ViewMapper::COL_USER_ID],
                    $row[View\ViewMapper::COL_COMPANY_ID],
                    DateTime::createFromMysqlFormat($row[View\ViewMapper::COL_DATE_TIME])
                );
            },
            $views
        );

        foreach ($views as $view) {
            /** @var View $view */
            $filter = new ViewFilter();
            $filter
                ->setUserId($view->getUserId())
                ->setCompanyId($view->getCompanyId())
                ->setEntityId($view->getEntityId())
                ->setEntityType($view->getEntityType())
            ;

            $filters[] = $filter;
        }

        $indexedViews = $this->_viewService->queryFromElasticSearch($filters);

        $viewsToPersist = [];

        array_map(
            function ($view) use ($indexedViews, &$viewsToPersist) {
                foreach ($indexedViews as $indexedView) {
                    $row = $indexedView['_source'];

                    /** @var View $view */
                    if (
                        $view->getUserId() === $row[View\ViewMapper::COL_USER_ID] &&
                        $view->getCompanyId() === $row[View\ViewMapper::COL_COMPANY_ID] &&
                        $view->getEntityId() === $row[View\ViewMapper::COL_ENTITY_ID] &&
                        $view->getEntityType() === $row[View\ViewMapper::COL_ENTITY_TYPE]
                    ) {
                        return;
                    }
                }

                $deduplicationKey = "{$view->getEntityId()}_{$view->getEntityType()}_{$view->getUserId()}_{$view->getCompanyId()}";

                $viewsToPersist[$deduplicationKey] = $view;
            },
            $views
        );

        $this->_viewService->saveViews($viewsToPersist);
    }
}