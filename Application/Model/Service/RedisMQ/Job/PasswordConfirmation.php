<?php

namespace Application\Model\Service\RedisMQ\Job;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Service\Mail;
use Zend\Mime\Message;
use Zend\Mime\Part;
use Zend\View\Model\ViewModel;

class PasswordConfirmation extends AbstractJob
{
    const ARG_USER_ID = 'user_id';
    const ARG_TOKEN   = 'token';

    public function perform()
    {
        $userId = $this->args[self::ARG_USER_ID];
        $token  = $this->args[self::ARG_TOKEN];

        self::$sm->get(Factory::DB_ADAPTER); // db connection refresh

        $user = self::$ds->userMapper()->fetch($userId);

        $confirmationMailView = new ViewModel([
            'user'  => $user,
            'token' => $token
        ]);


        $confirmationMailView->setTerminal(true)->setTemplate('/mail/password-change-confirm.phtml');

        $message = self::$ds->mailService()->createMessage(Mail::SUBJECT_PASSWORD_CHANGE_CONFIRM, $user->getEmail());

        $html = new Part($this->_renderViewModel($confirmationMailView));
        $html->setType("text/html");

        $body = new Message();
        $body->addPart($html);

        $message->setBody($body);

        self::$ds->mailService()->sendEmail($message);
    }
}