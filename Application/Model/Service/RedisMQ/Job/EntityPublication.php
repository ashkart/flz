<?php


namespace Application\Model\Service\RedisMQ\Job;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Interfaces\EntityRelationInterface;
use Application\Model\Interfaces\SocialPublicationInterface;
use Zend\Log\PsrLoggerAdapter;

class EntityPublication extends AbstractJob
{
    const ARG_ENTITY_ID   = 'id';
    const ARG_ENTITY_TYPE = 'type';

    public function perform()
    {
        try {
            $id   = $this->args[self::ARG_ENTITY_ID];
            $type = $this->args[self::ARG_ENTITY_TYPE];

            $mapper  = null;
            $service = null;

            switch ($type) {
                case EntityRelationInterface::ENTITY_TYPE_VACANCY:
                    $mapper  = self::$ds->vacancyMapper();
                    $service = self::$ds->vacancyService();
                    break;

                case EntityRelationInterface::ENTITY_TYPE_RESUME:
                    $mapper  = self::$ds->resumeMapper();
                    $service = self::$ds->resumeService();
            }

            if (!$mapper || !$service instanceof SocialPublicationInterface) {
                /** @var PsrLoggerAdapter $logger */
                $logger = self::$sm->get(Factory::LOGGER);

                $logger->error('Неизвестный тип публикации.');
            }

            $entity = $mapper->fetch($id);

            if (!$entity) {
                /** @var PsrLoggerAdapter $logger */
                $logger = self::$sm->get(Factory::LOGGER);

                $logger->info("Документ не найден: $type id=$id.");

                return;
            }

            $url  = $service->getUrl($entity);
            $text = $service->getPublicationDescription($entity);

            self::$ds->socnetService()->postWallMessages("$text\n", $url);
        } catch (\Throwable $ex) {
            /** @var PsrLoggerAdapter $logger */
            $logger = self::$sm->get(Factory::LOGGER);

            $logger->error($ex->getMessage());
            $logger->error($ex->getTraceAsString());
        }
    }
}