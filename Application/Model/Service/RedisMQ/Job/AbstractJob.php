<?php

namespace Application\Model\Service\RedisMQ\Job;

use Application\Helper\TraitViewRenderer;
use Library\Master\QueueManager\AbstractJob as MasterAbstractJob;

abstract class AbstractJob extends MasterAbstractJob
{
    use TraitViewRenderer;
}