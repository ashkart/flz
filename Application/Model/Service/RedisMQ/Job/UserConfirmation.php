<?php

namespace Application\Model\Service\RedisMQ\Job;

use Main\View\Helper\Contact;
use Zend\View\Model\ViewModel;
use Zend\Mime;

class UserConfirmation extends AbstractJob
{
    public function perform()
    {
        $user = self::$ds->userMapper()->fetch($this->args['userId']);

        $token = self::$ds->authService()->getConfirmToken($user);

        $confirmationMailView = new ViewModel([
            'user'        => $user,
            'regPassword' => $this->args['password'],
            'token'       => $token
        ]);

        $confirmationMailView->setTerminal(true)->setTemplate('/mail/email-confirm.phtml');

        $msg = self::$ds->mailService()->createMessage('Регистрация на ' . Contact::BRAND_NAME, $user->getEmail());

        $html = new Mime\Part($this->_renderViewModel($confirmationMailView));
        $html->setType("text/html");

        $body = new Mime\Message();
        $body->addPart($html);

        $msg->setBody($body);

        self::$ds->mailService()->sendEmail($msg);
    }
}