<?php

namespace Application\Model\Service;

use Application\Cache\UserSession;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\ClientData;
use Application\Model\Domain\User;
use Application\Model\Domain\ContactPerson;
use Application\Model\Service\Auth\Adapter;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Frontend\Form\RegistrationWithCompany;
use Library\Master\Date\DateTime;
use Zend\Authentication\AuthenticationService;

class Auth extends AbstractService
{
    /**
     * Blowfish (BCRYPT) обрезает хешируемую строку до 72 символов.
     * @see http://php.net/manual/ru/function.crypt.php
     */
    const BLOWFISH_PASS_MAX_LENGTH = 72;

    const CONFIRM_TOKEN_SALT = '%GH@#%gh2';

    public function auth(string $email, string $password, bool $verifyPassword = true) : bool
    {
        /** @var AuthenticationService $authService */
        $authService = self::$sm->get(Factory::AUTH_SERVICE);
        $authAdapter = new Auth\Adapter($email, $this->_passwordLengthFix($password), $verifyPassword);

        $authResult = $authService->authenticate($authAdapter)->isValid();

        if ($authResult && !\Main\Module::isCli()) {
            $clientData = new ClientData();

            $user = $this->_user();

            $userSessions = new UserSession($user->getEmail());
            $sessions = $userSessions->load() ?: [];

            $sessions[] = $clientData->getSessionId();
            $userSessions->save($sessions);

            $user->setLastVisitDate(new DateTime());
            self::$ds->userMapper()->save($user);
        }

        return $authResult;
    }

    public function logout()
    {
        /** @var AuthenticationService $authService */
        $authService = self::$sm->get(Factory::AUTH_SERVICE);

        $userEmail   = $this->_user()->getEmail();
        $sessionHash = (new ClientData())->getSessionId();

        if ($authService->hasIdentity()) {
            $authService->clearIdentity();
        }

        $this->dropSessionFromCache($userEmail, $sessionHash);
    }

    public function dropSessionFromCache(string $userEmail, string $sessionHash)
    {
        $userSession = new UserSession($userEmail);
        $userSession->destroy($sessionHash);
    }

    public function dropAllUserSessions(string $userEmail)
    {
        if ($this->_user()->getEmail() === $userEmail) {
            $this->logout();
        }

        $userSession = new UserSession($userEmail);
        $userSession->destroyAll();
    }

    public function registerApplicant(array $regFormData) : User
    {
        return self::$ds->userService()->create(User::ROLE_EMPLOYEE, $regFormData);
    }

    public function registerUserWithCompany(array $regFormData) : User
    {
        $newUser = self::$ds->userService()->create(User::ROLE_RECRUITER, $regFormData);

        $company = self::$ds->companyService()->create(
            $regFormData[RegistrationWithCompany::EL_COMPANY_NAME],
            $regFormData[RegistrationWithCompany::EL_COMPANY_DESCRIPTION]
        );

        self::$ds->userCompanyService()->createNewForUser($newUser, $company);

        $companyContact = new ContactPerson(
            $company->getId(),
            $regFormData[RegistrationWithCompany::EL_CONTACT_POSITION],
            $regFormData[RegistrationWithCompany::EL_CONTACT_FIRST_NAME],
            $regFormData[RegistrationWithCompany::EL_CONTACT_LAST_NAME],
            $regFormData[RegistrationWithCompany::EL_CONTACT_MIDDLE_NAME]
        );

        self::$ds->contactPersonMapper()->save($companyContact);

        return $newUser;
    }

    public function hashPasswordForSaving(string $password) : string
    {
        return password_hash($this->_passwordLengthFix($password), PASSWORD_BCRYPT);
    }

    public function getConfirmToken(User $user) : string
    {
        return md5($user->getId() . self::CONFIRM_TOKEN_SALT);
    }

    public function verifyPassword(string $password, string $passwordHashFromDb) : bool
    {
        return Adapter::verifyPassword($password, $passwordHashFromDb, true);
    }

    protected function _passwordLengthFix(string $password) : string
    {
        if (strlen($password) > self::BLOWFISH_PASS_MAX_LENGTH) {
            $password = md5($password);
        }

        return $password;
    }

    protected function _prepareSelect(AbstractFilter $filter) : MapperSelect
    {
        throw new \Exception('Not implemented');
    }

    protected function _query(MapperSelect $select) : array
    {
        throw new \Exception('Not implemented');
    }
}