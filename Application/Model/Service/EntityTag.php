<?php

namespace Application\Model\Service;

use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain;
use Application\Model\Domain\EntityTag\EntityTagMapper;

class EntityTag extends AbstractService
{
    public function createTagCollection(array $dataRows) : Domain\Collection\DomainModelCollection
    {
        if (!$dataRows) {
            return new Domain\Collection\DomainModelCollection([]);
        }

        $entityTagMapper = self::$ds->entityTagMapper();

        $etModels = [];

        $tagsForIndex = [];

        foreach ($dataRows as $dataRow) {
            if (!isset($dataRow[EntityTagMapper::COL_TAG_ID])) {
                $tagName = $dataRow['title'];

                $tag = new Domain\Tag($tagName);
                self::$ds->tagMapper()->save($tag);

                $tagsForIndex[] = $tag;

                $dataRow[EntityTagMapper::COL_TAG_ID] = $tag->getId();
            }

            unset($dataRow['title']);

            $etModels[] = $entityTagMapper->createModel($entityTagMapper->newRecord($dataRow));
        }

        self::$ds->tagService()->bulkIndex($tagsForIndex);

        return new Domain\Collection\DomainModelCollection($etModels);
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}