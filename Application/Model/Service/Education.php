<?php

namespace Application\Model\Service;

use Application\Model\Domain\Education\EducationMapper;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\EducationFilter;
use Application\Model\Domain;
use Atlas\Orm\Mapper\MapperSelect;

use Library\Master\Date\Date;
use Library\Master\Date\DateRange;
use Main\View\Form\Fieldset;

class Education extends AbstractService
{
    public function create(int $resumeId, array $formData) : Domain\Education
    {
        $speciality     = $formData[Fieldset\Education::EL_SPECIALTY];
        $educationalOrg = $formData[Fieldset\Education::EL_EDUCATIONAL_ORGANIZATION];

        $dateStart = Date::createFromMysqlFormat($formData[Fieldset\Education::EL_DATE_START]);
        $dateEnd   = Date::createFromMysqlFormat($formData[Fieldset\Education::EL_DATE_END]);

        $period = new DateRange($dateStart, $dateEnd);

        $education = new Domain\Education($resumeId, $speciality, $educationalOrg, $period);

        self::$ds->educationMapper()->save($education);

        return $education;
    }

    public function update(Domain\Education $education, array $formData)
    {
        $speciality     = $formData[Fieldset\Education::EL_SPECIALTY];
        $educationalOrg = $formData[Fieldset\Education::EL_EDUCATIONAL_ORGANIZATION];

        $dateStart = Date::createFromMysqlFormat($formData[Fieldset\Education::EL_DATE_START]);
        $dateEnd   = Date::createFromMysqlFormat($formData[Fieldset\Education::EL_DATE_END]);

        $period = new DateRange($dateStart, $dateEnd);

        $education
            ->setSpecialty($speciality)
            ->setEducationalOrganization($educationalOrg)
            ->setPeriod($period)
        ;

        self::$ds->educationMapper()->save($education);
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        if (!$filter instanceof EducationFilter) {
            throw new \Exception('Invalid sideFilter type');
        }

        $where = [];

        if ($filter->getResumeId() !== null) {
            $where[EducationMapper::COL_RESUME_ID] =$filter->getResumeId();
        }

        $select = self::$ds->educationMapper()->select($where);

        if ($filter->getOrder() !== null) {
            $ordering = [];

            foreach ($filter->getOrder() as $fieldName => $order) {
                $ordering[] = "$fieldName $order";
            }

            $select->orderBy($ordering);
        }

        return $this->_setPaging($filter, $select);
    }
}