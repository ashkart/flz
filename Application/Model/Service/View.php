<?php

namespace Application\Model\Service;

use Application\Cache\Slot\Stat\EntityViews;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\ViewFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Service\ElasticSearch as ESService;
use Application\Model\Domain;
use Application\Cache\Slot\Stat\EntityViewCount;

class View extends AbstractService
{
    public function add(int $entityId, string $entityType)
    {
        if (!in_array($entityType, Domain\View::ENTITY_TYPES)) {
            throw new \Exception('Invalid entity type given');
        }

        $companyId = null;

        if ($this->_user()->isRecruiter()) {
            $companyId = $this->_user()->getCompanies()->asArray()[0]->getId();
        }

        $view = new Domain\View($entityId, $entityType, $this->_user()->getId(), $companyId);

        self::$ds->redisMQService()->enqueueView($view);
    }

    public function selectByEntity(int $entityId, string $entityType, int $perPage = null, int $pageIndex = null)
    {
        $slot = new EntityViews($entityId, $entityType);
        $views = $slot->load();

        if (!$views) {
            $filter = new ViewFilter();
            $filter
                ->setEntityId($entityId)
                ->setEntityType($entityType)
            ;

            $esResult = $this->queryFromElasticSearch([$filter]);

            $viewMapper = self::$ds->viewMapper();

            $views = [];

            foreach ($esResult as $resultRow) {
                $views[] = $viewMapper->createModel($viewMapper->newRecord($resultRow['_source']));
            }

            $slot->save($views);
        }

        if (!$perPage || $pageIndex === null) {
            return $views;
        }

        return $this->getPagedFromArray($views, $pageIndex, $perPage);
    }

    public function getPagedFromArray(array $views, int $pageIndex, int $perPage)
    {
        $totalItems = count($views);

        if ($perPage && $pageIndex !== null) {
            $offset = $perPage * $pageIndex;
            $limit  = $offset + $perPage;
        } else {
            $offset = 0;
            $limit  = $totalItems;
        }

        $result = [];

        for ($i = $offset; $i < min($totalItems, $limit); $i++) {
            $result[] = $views[$i];
        }

        $result[self::KEY_TOTAL_ITEMS] = $totalItems;

        return $result;
    }

    public function bulkIndex(array $views, bool $preDeleteIndex = false)
    {
        $createBulkParams = function () use ($views) {
            $params = [];

            foreach ($views as $view) {
                $params['body'][] = [
                    'index' => [
                        '_index' => ESService::INDEX_VIEW,
                        '_type' => ESService::INDEX_VIEW
                    ]
                ];

                $params['body'][] = $this->indexiseModel($view);
            }

            return $params;
        };

        return self::$ds->elasticSearchService()->bulkIndexModels(
            ESService::INDEX_VIEW,
            $createBulkParams,
            $preDeleteIndex
        );
    }

    public function indexiseModel(Domain\View $view) : array
    {
        return [
            Domain\View\ViewMapper::COL_USER_ID     => $view->getUserId(),
            Domain\View\ViewMapper::COL_COMPANY_ID  => $view->getCompanyId(),
            Domain\View\ViewMapper::COL_ENTITY_ID   => $view->getEntityId(),
            Domain\View\ViewMapper::COL_ENTITY_TYPE => $view->getEntityType(),
            Domain\View\ViewMapper::COL_DATE_TIME   => $view->getDateTime()->mysqlFormat(),
        ];
    }

    /**
     * @param ViewFilter[] $filter
     * @return array
     */
    public function queryFromElasticSearch(array $filters) : array
    {
        $esService = self::$ds->elasticSearchService();

        $must = [
            [
                'match_all' => new \stdClass()
            ]
        ];

        $should = [];

        foreach ($filters as $filter) {
            $shouldMust = [];

            if ($filter->getUserId() !== null) {
                $shouldMust[] = [
                    'term' => [
                        Domain\View\ViewMapper::COL_USER_ID => $filter->getUserId()
                    ]
                ];
            }

            if ($filter->getCompanyId() !== null) {
                $shouldMust[] = [
                    'term' => [
                        Domain\View\ViewMapper::COL_COMPANY_ID => $filter->getCompanyId()
                    ]
                ];
            }

            if ($filter->getEntityId() !== null) {
                $shouldMust[] = [
                    'term' => [
                        Domain\View\ViewMapper::COL_ENTITY_ID => $filter->getEntityId()
                    ]
                ];
            }

            if ($filter->getEntityType() !== null) {
                $shouldMust[] = [
                    'term' => [
                        Domain\View\ViewMapper::COL_ENTITY_TYPE => $filter->getEntityType()
                    ]
                ];
            }

            $should[] = [
                'bool' => [
                    'must' => $shouldMust
                ]
            ];
        }

        $must[] = [
            'bool' => [
                'should' => $should,
                "minimum_should_match" => 1
            ]
        ];

        $query = [
            'bool' => [
                'must' => $must
            ]
        ];

        return $esService->query(
            ElasticSearch::INDEX_VIEW,
            $query,
            null,
            null,
            [Domain\View\ViewMapper::COL_DATE_TIME => ViewFilter::ORDER_DESC]
        )['hits'] ?? [];
    }

    public function getEntityViewsCount(int $entityId, string $entityType) : int
    {
        $slot = new EntityViewCount($entityId, $entityType);
        $result = $slot->load();

        if ($result !== false) {
            return $result;
        }

        $result = count($this->selectByEntity($entityId, $entityType));

        $slot->save($result);

        return $result;
    }

    /**
     * @param Domain\View[] $views
     */
    public function saveViews(array $views)
    {
        if (!$views) {
            return;
        }

        $viewMapper = self::$ds->viewMapper();

        $transactionId = 'viewInsert_' . microtime();

        try {
            $viewMapper->beginTransaction($transactionId);

            $pdoConn = self::$ds->viewMapper()->getWriteConnection();

            $viewTableName = $viewMapper->getTable()->getName();

            $rowPlaceholders = [];
            $cols = [];

            foreach ($views as $view) {
                $cols = array_keys($this->indexiseModel($view));
                $rowPlaceholders = array_pad($rowPlaceholders, count($cols), '?');
                break;
            }

            $colNames = join(",", $cols);

            $saveData        = [];
            $allPlaceholders = [];

            foreach ($views as $view) {
                $row = $this->indexiseModel($view);

                $saveData = array_merge($saveData, array_values($row));

                $allPlaceholders[] = "(" . join(',', $rowPlaceholders) . ")";
            }

            $sql = "INSERT INTO $viewTableName ($colNames) VALUES " . join(',', $allPlaceholders) . ';';

            $preparedStatement = $pdoConn->prepare($sql);

            $preparedStatement->execute($saveData);

            $viewMapper->commit($transactionId);
        } catch (\Exception $e) {
            $this->_logException($e);

            $viewMapper->rollBack();
            return;
        }

        $entitiesData    = [];

        foreach ($views as $view) {
            $slot = new EntityViewCount($view->getEntityId(), $view->getEntityType());
            $slot->delete();

            self::$ds->elasticSearchService()->indexDocument(
                ElasticSearch::INDEX_VIEW,
                $this->indexiseModel($view)
            );

            $entitiesData["{$view->getEntityType()}_{$view->getEntityId()}]"] = $view;
        }

        $this->countViewsByEntities($entitiesData);
    }

    /**
     * @param Domain\View[] $views
     */
    public function countViewsByEntities(array $views)
    {
        if (!$views) {
            return;
        }

        $vacancyIds  = [];
        $resumeIds   = [];
        $callbackIds = [];

        foreach ($views as $view) {
            switch ($view->getEntityType()) {
                case Domain\View::ENTITY_TYPE_RESUME:
                    $resumeIds[] = $view->getEntityId();
                    break;

                case Domain\View::ENTITY_TYPE_VACANCY:
                    $vacancyIds[] = $view->getEntityId();
                    break;

                case Domain\View::ENTITY_TYPE_CALLBACK:
                    $callbackIds[] = $view->getEntityId();
                    break;
            }
        }

        $vIds = join(',', $vacancyIds ?: ["''"]);
        $rIds = join(',', $resumeIds ?: ["''"]);
        $cIds = join(',', $callbackIds ?: ["''"]);

        $sql = <<<EOL
            select count(v.id) `count`, v.entity_id, v.entity_type
            from flm.`view` v
            where v.entity_id in ($vIds) and v.entity_type = 'vacancy' or 
            v.entity_id in ($rIds) and v.entity_type = 'resume' or 
            v.entity_id in ($cIds) and v.entity_type = 'callback'
            group by v.entity_type, v.entity_id;
EOL;

        $statement = self::$ds->vacancyMapper()->getReadConnection()->query($sql);
        $resultRows = $statement->fetchAll();
        $statement->closeCursor();

        foreach ($resultRows as $resultRow) {
            $slot = new EntityViewCount(
                $resultRow[Domain\View\ViewMapper::COL_ENTITY_ID],
                $resultRow[Domain\View\ViewMapper::COL_ENTITY_TYPE]
            );

            $slot->save($resultRow['count']);
        }
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Nit implemented');
    }
}