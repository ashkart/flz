<?php

namespace Application\Model\Service;

use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;

class Region extends AbstractService
{
    public function getDataForSelection(int $countryId) : array
    {
        $regions = self::$ds->regionMapper()->fetchByCountryId($countryId);

        $result = [];

        foreach ($regions as $region) {
            $result[$region->getId()] = $region->getTitle();
        }

        return $result;
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented yet');
        // TODO: Implement _prepareSelect() method.
    }
}