<?php

namespace Application\Model\Service;

use Application\Cache\Slot\EntityCallbacks;
use Application\Cache\Slot\UserCallbacks;
use Application\Model\Domain\AbstractMapper;
use Application\Model\Domain\Conversation;
use Application\Model\Domain\ConversationMessage;
use Application\Model\Domain\ConversationUser;
use Application\Model\Domain\View\ViewMapper;
use Application\Model\Service\Filter\AbstractFilter;
use Application\Model\Service\Filter\CallbackFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Application\Model\Domain\Callback as CallbackModel;
use Library\Master\Date\DateTime;

class Callback extends AbstractService
{
    public function add(
        int $entityId,
        string $entityType,
        int $fromEntityId,
        string $fromEntityType,
        ?string $coverLetter
    )
    {
        $transactionId = "callback_" . $this->_user()->getId() . '_' . microtime();

        try {
            self::$ds->callbackMapper()->beginTransaction($transactionId);

            $conversation = new Conversation($this->_user()->getId());
            self::$ds->conversationMapper()->save($conversation, Conversation\ConversationMapper::COLS, false);

            $conversationUser = new ConversationUser($this->_user()->getId(), $conversation->getId());
            self::$ds->conversationUserMapper()->save($conversationUser);

            $callback = new CallbackModel(
                $this->_user()->getId(),
                $entityType,
                $entityId,
                $fromEntityType,
                $fromEntityId,
                $conversation->getId()
            );
            self::$ds->callbackMapper()->save($callback);

            if ($coverLetter) {
                $message = new ConversationMessage($conversation->getId(), $this->_user()->getId(), $coverLetter);
                self::$ds->conversationMessageMapper()->save($message);
            }

            self::$ds->callbackMapper()->commit($transactionId);
        } catch (\Exception $ex) {
            self::$ds->callbackMapper()->rollBack();

            $this->_logException($ex);
        }
    }

    public function applyRowsUserCallbackData(array $domainRows, string $callbackEntityType)
    {
        $userCallbacks = [];

        if (!$this->_user()->isGuest()) {
            $userCallbacks = $this->getUserCreated(
                $this->_user()->getId(),
                $callbackEntityType
            );
        }

        foreach ($domainRows as &$row) {
            if (is_array($row)) {
                $this->applyRowUserCallbackData($row, $userCallbacks);
            }
        }

        return $domainRows;
    }

    /**
     * @param CallbackModel[] $userCallbacks
     */
    public function applyRowUserCallbackData(array &$row, array $userCallbacks)
    {
        foreach ($userCallbacks as $userCallback) {
            if (
                isset($row[AbstractMapper::PRIMARY]) &&
                $userCallback->getEntityId() === (int) $row[AbstractMapper::PRIMARY]
            ) {
                $row['has_user_callback'] = true;
                $row['callback_state'] = $userCallback->getState();
                break;
            }
        }
    }

    /**
     * @return CallbackModel[]
     * @throws \Exception
     */
    public function getUserCreated(int $userId, string $entityType = null) : array
    {
        $userCallbacks = self::$ds->callbackMapper()->fetchByIds($this->selectIdsByUser($userId));

        if ($entityType && in_array($entityType, CallbackModel::ENTITY_TYPES)) {
            /** @var CallbackModel $userCallback */
            foreach ($userCallbacks as $i => $userCallback) {
                if ($userCallback->getEntityType() !== $entityType) {
                    unset($userCallbacks[$i]);
                }
            }
        }

        return $userCallbacks;
    }

    public function selectByEntityId(int $entityId, string $entityType, int $perPage = null, int $pageIndex = null) : array
    {
        $slot = new EntityCallbacks($entityId, $entityType);
        $callbacks = $slot->load();

        if (!$callbacks) {
            $filter = new CallbackFilter();
            $filter
                ->setEntityId($entityId)
                ->setEntityType($entityType)
                ->setStates(CallbackModel::STATES_NOT_DELETED)
                ->setOrder([CallbackModel\CallbackMapper::PRIMARY => $filter::ORDER_DESC])
            ;

            $select = $this->_prepareSelect($filter, [self::$ds->callbackMapper()->getTable()->getName() . "." . CallbackModel\CallbackMapper::PRIMARY]);

            switch($entityType) {
                case CallbackModel::ENTITY_TYPE_RESUME:
                    $select
                        ->join('LEFT', 'vacancy v', "v.id = callback.from_entity_id AND callback.from_entity_type = 'vacancy'")
                        ->join('LEFT', 'company c', "c.id = v.company_id")
                    ;

                    $select->cols([
                        'c.opf',
                        'c.official_name company_name',
                        'v.position_name'
                    ]);

                    break;

                case CallbackModel::ENTITY_TYPE_VACANCY:
                    $select->join('LEFT', 'resume r', "r.id = callback.from_entity_id AND callback.from_entity_type = 'resume'");

                    $select->cols([
                        'r.position_name',
                    ]);

                    break;
            }

            $callbacks = array_values($select->fetchAssoc());

            $slot->save($callbacks);
        }

        if (!$perPage || $pageIndex === null) {
            return $callbacks;
        }

        $getResultRow = function($dataRow) {
            return array_merge($dataRow, self::$ds->callbackMapper()->fetch($dataRow['id'])->getTableRow());
        };

        return $this->fetchPagedFromIds($callbacks, $getResultRow, $pageIndex, $perPage);
    }

    public function selectIdsByUser(int $userId) : array
    {
        $slot = new UserCallbacks($userId);
        $callbackIds = $slot->load();

        if (!$callbackIds) {
            $filter = new CallbackFilter();
            $filter
                ->setUserId($userId)
                ->setStates(CallbackModel::STATES_NOT_DELETED)
                ->setOrder([CallbackModel\CallbackMapper::PRIMARY => $filter::ORDER_DESC])
            ;

            $select = $this->_prepareSelect($filter, [CallbackModel\CallbackMapper::PRIMARY]);

            $fetchResult = array_values($select->fetchAssoc());

            $callbackIds = array_map(function($item) {return $item['id']; }, $fetchResult);

            $slot->save($callbackIds);
        }

        return $callbackIds;
    }

    public function fetchCallbackEntityRow(string $type, int $id) : array
    {
        $mapper = null;

        switch ($type) {
            case CallbackModel::ENTITY_TYPE_RESUME:
                $mapper = self::$ds->resumeMapper();
                break;

            case CallbackModel::ENTITY_TYPE_VACANCY:
                $mapper = self::$ds->vacancyMapper();
        }

        return $mapper
            ->fetch($id)
            ->getTableRow(false, [AbstractMapper::PRIMARY, 'position_name'])
        ;
    }

    public function normalizeDataRowsForView(array $callbacksDataRows) : array
    {
        $result = [];

        foreach ($callbacksDataRows as $key => $row) {
            $result[$key] = $row;

            if ($key === self::KEY_TOTAL_ITEMS) {
                continue;
            }

            $result[$key][CallbackModel\CallbackMapper::COL_CREATE_DATE] = DateTime::reformat(
                $row[CallbackModel\CallbackMapper::COL_CREATE_DATE],
                DateTime::DATE_FORMAT_RU
            );

            if ($row[CallbackModel\CallbackMapper::COL_UPDATE_DATE]) {
                $result[$key][CallbackModel\CallbackMapper::COL_UPDATE_DATE] = DateTime::reformat(
                    $row[CallbackModel\CallbackMapper::COL_UPDATE_DATE],
                    DateTime::DATE_FORMAT_RU
                );
            }

            if ($row['views']) {
                foreach ($row['views'] as $i => &$view) {
                    /** @var \Application\Model\Domain\View $view */
                    $result[$key]['views'][$i] = $view->getTableRow();

                    $result[$key]['views'][$i][ViewMapper::COL_DATE_TIME] = DateTime::reformat(
                        $result[$key]['views'][$i][ViewMapper::COL_DATE_TIME],
                        DateTime::DATE_FORMAT_RU
                    );
                }
            }
        }

        return $result;
    }

    public function fetchPagedFromIds(array $callbackIds, \Closure $getResultRow, int $pageIndex = null, int $perPage = null) : array
    {
        $totalItems = count($callbackIds);

        if ($perPage && $pageIndex !== null) {
            $offset = $perPage * $pageIndex;
            $limit  = $offset + $perPage;
        } else {
            $offset = 0;
            $limit  = $totalItems;
        }

        $result = [];

        for ($i = $offset; $i < min($totalItems, $limit); $i++) {
            $result[] = $getResultRow($callbackIds[$i]);
        }

        $result[self::KEY_TOTAL_ITEMS] = $totalItems;

        return $result;
    }

    /**
     * @param AbstractFilter | CallbackFilter $filter
     * @return MapperSelect
     * @throws \Exception
     */
    protected function _prepareSelect(AbstractFilter $filter, array $cols = null): MapperSelect
    {
        if (!$filter instanceof CallbackFilter) {
            throw new \Exception('Invalid filter instance given.');
        }

        $callbackTableName = self::$ds->callbackMapper()->getTable()->getName();

        $where = '';

        if ($filter->getUserId() !== null) {
            $where = $this->_concatenateQueryParts($where, "user_id = {$filter->getUserId()}");
        }

        if ($filter->getEntityType() !== null) {
            $where = $this->_concatenateQueryParts($where, "entity_type = '{$filter->getEntityType()}'", 'AND');
        }

        if ($filter->getEntityId() !== null) {
            $where = $this->_concatenateQueryParts($where, "entity_id = {$filter->getEntityId()}", 'AND');
        }

        if ($filter->getStates() !== null) {
            $states = join("','", $filter->getStates());

            $where = $this->_concatenateQueryParts(
                $where, "state IN('$states')", 'AND'
            );
        }

        $select = self::$ds->callbackMapper()->select();

        if ($cols === null) {
            $select->cols(["$callbackTableName.*"]);
        } else {
            $select->cols($cols);
        }

        if ($where) {
            $select->where($where);
        }

        if ($filter->getOrder() !== null) {
            if ($filter->getOrder() !== null) {
                $ordering = [];

                foreach ($filter->getOrder() as $fieldName => $order) {
                    $ordering[] = "$fieldName $order";
                }

                $select->orderBy($ordering);
            }
        }

        return $this->_setPaging($filter, $select);
    }
}