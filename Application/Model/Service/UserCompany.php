<?php

namespace Application\Model\Service;

use Application\Model\Domain;
use Application\Model\Domain\Company;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;

class UserCompany extends AbstractService
{
    public function createNewForCurrentUser(Company $company)
    {
        $this->createNewForUser($this->_user(), $company);
    }

    public function createNewForUser(Domain\User $user, Company $company)
    {
        $userCompanies = self::$ds->userCompanyMapper()->fetchByUserId($user->getId());

        foreach ($userCompanies as $userCompany) {
            if ($userCompany->getCompanyId() === $company->getId()) {
                // уже есть такая связка
                return;
            }
        }

        $userCompany = new Domain\UserCompany($user->getId(), $company->getId());

        self::$ds->userCompanyMapper()->save($userCompany);

        self::$ds->elasticSearchService()->indexDocument(
            ElasticSearch::INDEX_COMPANY,
            $this->indexiseModel($userCompany)
        );
    }

    public function indexiseModel(Domain\UserCompany $userCompany) : array
    {
        $company = $userCompany->getCompany();

        return [
            'id'            => $company->getId(),
            'user_id'       => $userCompany->getUserId(),
            'opf'           => $company->getOpf(),
            'official_name' => $company->getOfficialName()
        ];
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}