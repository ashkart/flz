<?php

namespace Application\Model\Service\Auth;

use Application\Helper\DataSource\TraitDataSource;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class Adapter implements AdapterInterface
{
    use TraitDataSource;

    /**
     * @var string
     */
    protected $_login;

    /**
     * @var string
     */
    protected $_password;

    /**
     * @var bool
     */
    protected $_needVerifyPassword = true;

    public static function verifyPassword(string $password, string $passwordHashFromDb, bool $needVerifyPassword = true) : bool
    {
        if ($needVerifyPassword) {
            return password_verify($password, $passwordHashFromDb);
        }

        return $password === $passwordHashFromDb;
    }

    public function __construct(string $login, string $password, bool $needVerifyPassword = true)
    {
        $this->_login              = $login;
        $this->_password           = $password;
        $this->_needVerifyPassword = $needVerifyPassword;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     */
    public function authenticate()
    {
        $user = self::$ds->userMapper()->fetchByEmail($this->_login);

        $messages = [];

        if ($user !== null) {
            $verificationResult = self::verifyPassword($this->_password, $user->getPassword(), $this->_needVerifyPassword);

            $resultCode = $verificationResult ? Result::SUCCESS : Result::FAILURE;
        } else {
            $resultCode = Result::FAILURE;
        }

        if ($resultCode === Result::FAILURE) {
            $messages = [
                'Неверный логин или пароль.'
            ];
        }

        return new Result($resultCode, $user, $messages);
    }
}