<?php


namespace Application\Model\Service;

use Application\Model\Domain\VacancyLocation as VacancyLocationModel;
use Application\Model\Domain\Collection\DomainModelCollection;
use Application\Model\Domain\VacancyLocation\VacancyLocationMapper;
use Application\Model\Service\Filter\AbstractFilter;
use Atlas\Orm\Mapper\MapperSelect;
use Main\View\Form\Fieldset\Location;

class VacancyLocation extends AbstractService
{
    public function createLocationsCollection(array $dataRows) : DomainModelCollection
    {
        if (!$dataRows) {
            return new DomainModelCollection([]);
        }

        $mapper = self::$ds->vacancyLocationMapper();

        $vlModels = [];

        foreach ($dataRows as $dataRow) {
            if (!isset($dataRow['location_id'])) {
                $cityId    = null;
                $countryId = null;

                if ($dataRow[Location::EL_TYPE] === Location::EL_TYPE_CITY) {
                    $cityId = $dataRow['id'];
                }

                if ($dataRow[Location::EL_TYPE] === Location::EL_TYPE_COUNTRY) {
                    $countryId = $dataRow['id'];
                }

                $vacancyLocation = new VacancyLocationModel($dataRow[VacancyLocationMapper::COL_VACANCY_ID], $countryId, $cityId);

                $mapper->save($vacancyLocation);

                $vlModels[] = $vacancyLocation;
            } else {
                $vlModels[] = $mapper->fetch($dataRow['location_id']);
            }
        }

        return new DomainModelCollection($vlModels);
    }

    protected function _prepareSelect(AbstractFilter $filter): MapperSelect
    {
        throw new \Exception('Not implemented');
    }
}