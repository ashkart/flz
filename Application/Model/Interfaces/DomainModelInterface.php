<?php

namespace Application\Model\Interfaces;

interface DomainModelInterface
{
    public function getId();
    public function setId(? int $id);
    public function getTableRow() : array;
    public function setTableRow(array $record);
}