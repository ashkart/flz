<?php

namespace Application\Model\Interfaces;

interface EntityRelationInterface
{
    const ENTITY_TYPE_RESUME  = 'resume';
    const ENTITY_TYPE_VACANCY = 'vacancy';
    const ENTITY_TYPE_CALLBACK = 'callback';

    const ENTITY_TYPES = [
        self::ENTITY_TYPE_RESUME,
        self::ENTITY_TYPE_VACANCY,
        self::ENTITY_TYPE_CALLBACK
    ];
}