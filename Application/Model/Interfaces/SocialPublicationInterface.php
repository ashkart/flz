<?php


namespace Application\Model\Interfaces;


interface SocialPublicationInterface
{
    public function getUrl(DomainModelInterface $model) : string;

    public function getPublicationDescription(DomainModelInterface $model) : string;
}