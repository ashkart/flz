<?php


namespace Application\Model\Traits;

use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain\Resume;
use Application\Model\Domain\Vacancy;
use Application\Model\Interfaces\DomainModelInterface;
use Application\View\PhpRenderer;

trait TraitFrontendUrl
{
    use TraitDataSource;

    public function getUrl(DomainModelInterface $model): string
    {
        /** @var PhpRenderer $phpRenderer */
        $phpRenderer = self::$sm->get(Factory::VIEW_RENDERER);

        $controller = null;

        if ($model instanceof Vacancy) {
            $controller = 'vacancy';
        }

        if ($model instanceof Resume) {
            $controller = 'resume';
        }

        if (!$controller) {
            throw new \Exception('Invalid model instance given.');
        }

        $baseUrl = $phpRenderer->url()->frontend($controller, 'view');

        $baseUrl .= "/{$model->getId()}";

        return $baseUrl;
    }
}