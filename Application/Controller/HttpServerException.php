<?php

namespace Application\Controller;

class HttpServerException extends HttpException
{
    const INTERNAL_SERVER_ERROR_500 = 500;
    const SERVICE_UNAVAILABLE_503   = 503;

    /**
     * Retry-After header value
     *
     * @var int | null
     */
    protected $_retryAfter = null;

    public function getRetryAfter() : ? int
    {
        return $this->_retryAfter;
    }

    public function setRetryAfter(? int $retryAfter) : self
    {
        $this->_retryAfter = $retryAfter;
        return $this;
    }

    protected function _message(int $code)
    {
        switch ($code) {
            case self::INTERNAL_SERVER_ERROR_500:
                return 'internal server error';
            case self::SERVICE_UNAVAILABLE_503:
                return 'service unavailable';
            default:
                return '';
        }
    }
}