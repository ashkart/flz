<?php

namespace Application\Controller;

class HttpRedirectException extends HttpException
{
    const MESSAGE = 'redirect';

    const MOVED_PERMANENTLY_301  = 301;
    const FOUND_302              = 302;
    const SEE_OTHER_303          = 303;
    const TEMPORARY_REDIRECT_307 = 307;

    /**
     * @var string
     */
    protected $_url;

    public function __construct(int $code = 0, $url)
    {
        $this->_url = $url;

        parent::__construct($code, '', null);
    }

    public function getUrl() : string
    {
        return $this->_url;
    }

    /**
     * @return mixed
     */
    protected function _message(int $code)
    {
        switch ($code) {
            case self::MOVED_PERMANENTLY_301:
                return 'moved permanently';
            case self::FOUND_302:
                return 'found';
            case self::SEE_OTHER_303:
                return 'see other';
            case self::TEMPORARY_REDIRECT_307:
                return 'temporary redirect';
            default:
                return '';
        }
    }
}