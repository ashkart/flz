import React from 'react';
import Layout from "./Layout";

const PageNotFound = (props) => {
    return (
        <Layout>
            <div className="block">
                <div className="row error-code">
                    <p>
                        404
                    </p>
                </div>

                <div className="row error-text">Запрашиваемая страница не найдена.</div>
            </div>
        </Layout>
    );
};

export default PageNotFound;

