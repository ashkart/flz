import React from "react";
import {connect} from 'react-redux';
import {fetchResumeListContextAction} from "../../actions/actionCreators/dataFetchActions";
import Layout from '../Layout';
import List from "../../components/List";
import SuggestInputFilter from "../../components/Filters/SuggestInputFilter";
import Keyword from "../../components/Filters/Keyword";
import CheckboxGroup from "../../components/Filters/CheckboxGroup";
import Select from "../../components/Filters/Select";
import {
    AGES, EDUCATION,
    EMPLOYMENT_LIST,
    EXPERIENCE_OPTIONS,
    FILTER_NAME_KEYWORD,
    PUBLISH_PERIODS
} from "../../helper/constants";
import ResumeListItem from "../../components/ViewModel/ResumeListItem";
import Checkbox from "../../components/Filters/Checkbox";
import PaycheckRange from "../../components/Filters/PaycheckRange";
import {DataContext} from "../../components/context";
import {DEFAULT_RANGE} from "../../components/Range";
import {renderGeoSuggestion} from "../../components/FormElements/renderElement";

export const RESUME_LIST_ITEM_MAPPING = {
    id: 'id',
    position: 'position_name',
    age: 'age',
    paycheck: 'desired_paycheck',
    updateDate: 'update_date',
    currencyCode: 'currency_code',
    currencyName: 'currency_name',
    city: 'city',
    lastWorkPosition: 'last_work_position',
    lastWorkDateFrom: 'last_work_date_from',
    lastWorkDateTo: 'last_work_date_to',
    workExp: 'work_exp_string',
    hasUserCallback: 'has_user_callback',
    callbackState: 'callback_state',
    callbacks: 'callbacks',
    views: 'views'
};

class ResumeList extends React.Component {
    constructor(props) {
        super(props);

        const noMatterValue = {value: '', label: 'Не имеет значения'};

        const filters = [
            <Keyword
                label="Ключевые слова в названии"
                inputProps={{title: "Укажите ключевые слова через пробел, если требуется их обязательное присутсвие в вакансии, или через запятую, чтобы подобрать вакансии, содержащие хотя бы одно из перечисленных слов."}}
                key="filter_keywords"
                isStrict={false}
                withOption={false}
                name={FILTER_NAME_KEYWORD}
                resource="suggest/resumeKeyword"
                renderSuggestionItem={(suggestion) => {
                    return <div className="suggestion" style={{display: 'block'}}>{suggestion.title}</div>;
                }}
                onFocusOut={() => {  }}
            />,
            <SuggestInputFilter
                className="filter"
                label="Регион"
                key="filter_region"
                multiSelect={true}
                name="region"
                resource="suggest/locality"
                renderSuggestionItem={renderGeoSuggestion}
            />,
            <Checkbox
                label="Только готовые к переезду"
                name="isRelocateReady"
                key="filter_is_relocate_ready"
            />,
            <Checkbox
                label="Только удаленная работа"
                name="isRemote"
                key="filter_is_remote"
            />,
            <Select
                label="Период публикации"
                name="publishPeriod"
                key="filter_publish_period"
                options={PUBLISH_PERIODS}
            />,
            <CheckboxGroup
                label="Занятость"
                name="employment"
                key="filter_employment"
                options={EMPLOYMENT_LIST}
            />,
            <PaycheckRange
                label="Зарплата"
                name="paycheck"
                key="filter_paycheck"
                step={10000}
            />,
            <CheckboxGroup
                label="Опыт работы"
                name="experience"
                key="filter_experience"
                options={EXPERIENCE_OPTIONS}
            />,
            <CheckboxGroup
                label="Образование"
                name="educationLvl"
                key="filter_education_level"
                options={EDUCATION}
            />,
            <CheckboxGroup
                label="Возраст"
                name="age"
                key="filter_age"
                options={AGES}
            />
        ];

        this.state = {
            filters: filters
        };

        this.fetchContext = this.fetchContext.bind(this);

        this.fetchContext();
    }

    fetchContext() {
        this.props.dispatch(fetchResumeListContextAction());
    }

    render() {
        return (
            <Layout>
                <DataContext.Provider value={{paycheckRange: this.props.context.paycheckRange}}>
                    <List
                        className="list-resume"
                        resource="resume/list"
                        pageName="Поиск по резюме"
                        filters={this.state.filters}
                        filterInitial={{publishPeriod: 4}}
                        {...this.props}
                    >
                        <ResumeListItem
                            mapping={RESUME_LIST_ITEM_MAPPING}
                        />
                    </List>
                </DataContext.Provider>
            </Layout>
        );
    }
}

export default connect((state, props) => {
    const resumeContext = state.context.resume || {paycheckRange: DEFAULT_RANGE};

    return {
        context: resumeContext, ...props
    };
})(ResumeList);
