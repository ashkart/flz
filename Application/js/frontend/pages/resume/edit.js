import React from 'react';
import Layout from "../Layout";
import ResumeEdit, {FORM_ID} from "../../components/Forms/ResumeEdit";

export class ResumeEditPage extends React.Component {
    render() {
        const matchParams = this.props.match.params || {};

        return (
            <Layout>
                <ResumeEdit id={FORM_ID} resumeId={matchParams.id} />
            </Layout>
        );
    }
}
