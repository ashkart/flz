import React from 'react';
import Layout from "../Layout";
import ResumeView, {FORM_ID} from "../../components/ViewModel/ResumeView";

export class ResumeViewPage extends React.Component {
    render() {
        const matchParams = this.props.match.params || {};

        return (
            <Layout>
                <ResumeView id={FORM_ID} resumeId={matchParams.id} callbackId={matchParams.callbackId}/>
            </Layout>
        );
    }
}
