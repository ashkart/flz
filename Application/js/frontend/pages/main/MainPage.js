import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Layout from '../Layout';
import CreateEntity from "../../components/CreateEntity";
import List from "../../components/List";
import SimpleListItem from "../../components/ViewModel/SimpleListItem";
import Geolocate from "../../components/Forms/Geolocate";
import { withCookies } from 'react-cookie';
import {COOKIE_CITY_ID} from "../../helper/constants";
import {geoReverseAction, URL_REVERSE_GEOLOCATE} from "../../actions/actionCreators/postActions";

const CITY_ID_DEFAULT = 1;

class MainPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            geolocate: !this.props.cookies.get(COOKIE_CITY_ID),
            cityFilter: {}
        };

        this.onGeolocationSuccess = this.onGeolocationSuccess.bind(this);
        this.onGeolocationError = this.onGeolocationError.bind(this);
        this.setCityCookie = this.setCityCookie.bind(this);
    }

    setCityCookie(cityId) {
        const {
            cookies
        } = this.props;

        const nextYear = new Date();
        nextYear.setFullYear(nextYear.getFullYear() + 1);

        cookies.set(COOKIE_CITY_ID, cityId, {
            path: '/',
            expires: nextYear,
            httpOnly: false,
            secure: true
        });

        this.setState(prevState => ({
            geolocate: false,
            cityFilter: {region: {0: {id: cityId, type: 'city'}}}
        }));
    }

    onGeolocationSuccess(pos) {
        this.props.dispatch(geoReverseAction(pos.coords.latitude, pos.coords.longitude));
    }

    onGeolocationError(err) {
        const {
            cookies
        } = this.props;

        if (!cookies.get(COOKIE_CITY_ID)) {
            this.setCityCookie(CITY_ID_DEFAULT);
        }
    }

    render() {
        return (
            <Layout>
                {
                    this.state.geolocate &&
                    <Geolocate
                        onSuccess={this.onGeolocationSuccess}
                        onError={this.onGeolocationError}
                        userCity={this.props.userCity}
                        onCityUpdate={this.setCityCookie}
                    />
                }

                <div className="landing">
                    <div className="col">
                        <CreateEntity
                            className="resume"
                            url="/resume/edit"
                            btnCaption="Разместить резюме"
                            text="Разместите резюме, чтобы работодатели быстрее узнали о Вас."
                        />

                        <List resource="vacancy/recommended-list" filterOverride={this.state.cityFilter}>
                            <SimpleListItem
                                linkUrl={'/vacancy/view'}
                                mapping={{
                                    id: 'id',
                                    position: 'position_name',
                                    company: 'company',
                                    paycheck: 'paycheck'
                                }}
                                defaultPaycheck="По договоренности"
                            />
                        </List>
                    </div>

                    <div className="col">
                        <CreateEntity
                            className="vacancy"
                            url="/vacancy/edit"
                            btnCaption="Разместить вакансию"
                            text="Разместите вакансию, чтобы найти лучших сотрудников."
                        />

                        <List resource="resume/recommended-list" filterOverride={this.state.cityFilter}>
                            <SimpleListItem
                                linkUrl={'/resume/view'}
                                mapping={{id: 'id', position: 'position_name', paycheck: 'paycheck'}}
                                defaultPaycheck="По договоренности"
                            />
                        </List>
                    </div>
                </div>
            </Layout>
        );
    }
}

MainPage.propTypes = {
    userCity: PropTypes.object,
};

export default withCookies(connect(
    (state, props) => {
        return {userCity: state.data[URL_REVERSE_GEOLOCATE], ...props};
    }
)(MainPage));