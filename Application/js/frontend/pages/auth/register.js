import React from 'react';
import Layout from '../Layout';
import Logo from '../../components/Logo';
import TabLayout from '../../components/TabLayout';
import ApplicantReg from "../../components/Forms/ApplicantReg";
import CompanyReg from "../../components/Forms/CompanyReg";
import Tab from "../../components/TabLayout/Tab";
import SocialLoginButtons from "../../components/SocialLoginButtons";

export default class RegisterPage extends React.Component {
    render() {
        return (
            <Layout className='center' showNavbar={false} withFooter={false}>
                <div className="flex-container column center">
                    <Logo/>
                    <div className="flex-item">
                        <TabLayout defaultActiveId="applicant-reg">
                            <Tab id="applicant-reg" tabLabel="Ищу работу">
                                <ApplicantReg />
                            </Tab>
                            <Tab id="company-reg" tabLabel="Ищу сотрудников">
                                <CompanyReg />
                            </Tab>
                        </TabLayout>
                    </div>
                    <SocialLoginButtons/>
                </div>
            </Layout>
        );
    }
}