import React from 'react';
import Logo from "../../components/Logo";
import Layout from "../Layout";
import PasswordReset from "../../components/Forms/PasswordReset";

const PasswordChangePage = (props) => {
    return (
        <Layout className='center' showNavbar={false} withFooter={false}>
            <div className="flex-container column center">
                <Logo/>
                <div className="flex-item">
                    <PasswordReset />
                </div>
            </div>
        </Layout>
    );
};

export default PasswordChangePage;