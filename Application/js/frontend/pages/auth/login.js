import React from 'react';
import Layout from '../Layout';
import FormLogin from "../../components/Forms/FormLogin";
import Logo from '../../components/Logo';
import SocialLoginButtons from "../../components/SocialLoginButtons";

export default class LoginPage extends React.Component {
    componentWillMount() {
        let uri = document.location.href;

        let socnetParams = uri.split('#')[1];

        if (socnetParams) {
            window.location.href = `/auth/vk-auth?${socnetParams}`;
        }
    }

    render() {
        return (
            <Layout className="center" showNavbar={false} withFooter={false}>
                <div className="flex-container column center">
                    <Logo/>
                    <div className="flex-item">
                        <FormLogin />
                    </div>
                    <SocialLoginButtons/>
                </div>
            </Layout>
        );
    }
}