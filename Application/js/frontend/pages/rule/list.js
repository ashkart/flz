import React from 'react';
import Layout from "../Layout";

export default class RuleList extends React.Component {
    render() {
        return (
            <Layout>
                <div className="simple-block">
                    <span className="row">
                        <h2>Правовая информация</h2>
                    </span>
                    <div className="row">
                        <ul className="ulist">
                            <li>
                                <nobr><a href="/doc/confidential-policy.pdf">Политика конфиденциальности</a></nobr>
                            </li>
                            <li>
                                <nobr><a href="/doc/conditions.pdf">Условия использования ресурса</a></nobr>
                            </li>
                        </ul>
                    </div>
                </div>
            </Layout>
        );
    }
}
