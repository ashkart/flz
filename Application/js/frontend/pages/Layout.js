import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import Navbar from '../components/Navbar';
import JDataNotifications from "../components/JDataNotifications";
import {Link} from "react-router-dom";

const Layout = (props) => {
    const {
        withFooter,
        className,
        children,
        showNavbar
    } = props;

    return (
        <div id="mainWrap">
            <div className={`app-layout ${className}`}>
                {
                    showNavbar &&
                    <Navbar/>
                }
                <div className="app-content">
                    <JDataNotifications />
                    {
                        children
                    }
                </div>
            </div>
            {
                withFooter &&
                <footer>
                    <div className="row space-between">
                        <div className="column">
                            <span className="row"><nobr><Link to="/rule/list">Правовая информация</Link></nobr></span>
                        </div>
                        <div className="column align-end">
                            <span className="row"><nobr>{JDATA.brandName} ©</nobr></span>
                            <span className="row"><nobr>Техническая поддержка {<span className="action-link">{JDATA.supportEmail}</span>}</nobr></span>
                        </div>
                    </div>
                </footer>
            }
        </div>
    );
};

Layout.propTypes = {
    className: PropTypes.string,
    withFooter: PropTypes.bool,
    showNavbar: PropTypes.bool
};

Layout.defaultProps = {
    className: '',
    withFooter: true,
    showNavbar: true
};

export default connect(
    (state, props) => {
        return {state, ...props};
    }
)(Layout);