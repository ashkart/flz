import React from 'react';
import Layout from "../Layout";
import VacancyEdit, {FORM_ID} from "../../components/Forms/VacancyEdit";

export class VacancyEditPage extends React.Component {
    render() {
        const matchParams = this.props.match.params || {};

        return (
            <Layout>
                <VacancyEdit id={FORM_ID} vacancyId={matchParams.id} />
            </Layout>
        );
    }
}
