import React from "react";
import Layout from '../Layout';
import List from "../../components/List";
import VacancyListItem from "../../components/ViewModel/VacancyListItem";
import SuggestInputFilter from "../../components/Filters/SuggestInputFilter";
import TreeSelectFilter from "../../components/Filters/TreeSelectFilter";
import Paycheck from '../../components/Filters/Paycheck';
import Keyword from "../../components/Filters/Keyword";
import Checkbox from "../../components/Filters/Checkbox";
import CheckboxGroup from "../../components/Filters/CheckboxGroup";
import Select from "../../components/Filters/Select";
import {EMPLOYMENT_LIST, EXPERIENCE_OPTIONS, FILTER_NAME_KEYWORD, PUBLISH_PERIODS} from "../../helper/constants";
import {renderGeoSuggestion} from "../../components/FormElements/renderElement";

export const VACANCY_LIST_ITEM_MAPPING = {
    id: 'id',
    position: 'position_name',
    paycheck: 'paycheck',
    date: 'update_date',
    currencyCode: 'currency_code',
    currencyName: 'currency_name',
    company: 'company',
    city: 'city',
    body: 'full_description',
    callbackState: 'callback_state',
    hasUserCallback: 'has_user_callback',
    views: 'views'
};

export default class VacancyList extends React.Component {
    constructor(props) {
        super(props);

        const filters = [
            <Keyword
                label="Ключевые слова"
                checkBoxLabel="Только в названии вакансии"
                inputProps={{title: "Укажите ключевые слова через пробел, если требуется их обязательное присутсвие в вакансии, или через запятую, чтобы подобрать вакансии, содержащие хотя бы одно из перечисленных слов."}}
                key="filter_keywords"
                isStrict={false}
                name={FILTER_NAME_KEYWORD}
                resource="suggest/vacancyKeyword"
                renderSuggestionItem={(suggestion) => {
                    return <div className="suggestion" style={{display: 'block'}}>{suggestion.title}</div>;
                }}
                onFocusOut={() => {  }}
            />,
            <SuggestInputFilter
                className="filter"
                label="Регион"
                key="filter_region"
                multiSelect={true}
                name="region"
                resource="suggest/locality"
                renderSuggestionItem={renderGeoSuggestion}
            />,
            <Select
                label="Период публикации"
                name="publishPeriod"
                key="filter_publish_period"
                options={PUBLISH_PERIODS}
            />,

            <TreeSelectFilter
                label="Профобласть"
                key="filter_profarea"
                resource="suggest/profarea"
                name="profarea"
                className="tree"
                maxHeight="400px"
            />,
            <Paycheck
                label="Желаемая зарплата"
                name="paycheck"
                key="filter_paycheck"
            />,
            <Checkbox
                label="Удаленная работа"
                name="isRemote"
                key="filter_is_remote"
            />,
            <CheckboxGroup
                label="Занятость"
                name="employment"
                key="filter_employment"
                options={EMPLOYMENT_LIST}
            />,
            <CheckboxGroup
                label="Опыт работы"
                name="experience"
                key="filter_experience"
                options={EXPERIENCE_OPTIONS}
            />
        ];

        this.state = {
            filters: filters
        };
    }

    render() {
        return (
            <Layout>
                <List
                    className="list-vacancy"
                    resource="vacancy/list"
                    filters={this.state.filters}
                    pageName="Поиск по вакансиям"
                    filterInitial={{publishPeriod: 4}}
                    {...this.props}
                >
                    <VacancyListItem
                        mapping={VACANCY_LIST_ITEM_MAPPING}
                    />
                </List>
            </Layout>
        );
    }
}
