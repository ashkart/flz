import React from 'react';
import Layout from "../Layout";
import VacancyView, {FORM_ID} from "../../components/ViewModel/VacancyView";

export class VacancyViewPage extends React.Component {
    render() {
        const matchParams = this.props.match.params || {};

        return (
            <Layout>
                <VacancyView id={FORM_ID} vacancyId={matchParams.id} />
            </Layout>
        );
    }
}
