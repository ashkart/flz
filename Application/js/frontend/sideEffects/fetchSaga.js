import {takeLatest, takeEvery, put, call} from 'redux-saga/effects';
import {
    SUGGEST_ACTION,
    FETCH_TREE,
    FETCH_CURRENCY,
    FETCH_LANGUAGE,
    FETCH_RESUME_CONTEXT,
    FETCH_LIST_FOR_SELECT
} from "../actions/actionCreators/dataFetchActions";
import {CRUD_DELETE, CRUD_GET_LIST, CRUD_GET_ONE, CRUD_UPDATE} from "../actions/crud";


function* handleFetch(dataProvider, action) {
    let response = yield call(
        dataProvider,
        action.fetch,
        action.resource,
        action.payload
    );

    yield put({
        type: `${action.type}_END`,
        payload: {
            ...action.payload,
            resource: action.resource,
            response: response,
        }
    });
}

export function suggestSaga(dataProvider) {
    return function* () {
        yield takeLatest(SUGGEST_ACTION, handleFetch, dataProvider);
        yield takeLatest(FETCH_TREE, handleFetch, dataProvider);
        yield takeLatest(FETCH_CURRENCY, handleFetch, dataProvider);
        yield takeLatest(FETCH_LANGUAGE, handleFetch, dataProvider);
        yield takeLatest(FETCH_RESUME_CONTEXT, handleFetch, dataProvider);
        yield takeLatest(FETCH_LIST_FOR_SELECT, handleFetch, dataProvider);
    }
}

export function fetchSaga(dataProvider) {
    return function* () {
        yield takeEvery(CRUD_GET_ONE, handleFetch, dataProvider);
        yield takeLatest(CRUD_DELETE, handleFetch, dataProvider);
        yield takeLatest(CRUD_UPDATE, handleFetch, dataProvider);
        yield takeEvery(CRUD_GET_LIST, handleFetch, dataProvider);
    }
}
