import {all} from 'redux-saga/effects';

import {fetchSaga, suggestSaga} from "./fetchSaga";
import {dataStoreSaga} from './dataStoreSaga';

export default function rootSaga(dataProvider, updateLocalStorage) {
    return function* saga() {
        yield all([
            fetchSaga(dataProvider)(),
            suggestSaga(dataProvider)(),
            dataStoreSaga(updateLocalStorage)()
        ]);
    }
}