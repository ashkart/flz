import {put, call, takeEvery} from 'redux-saga/effects';
import {UPDATE_UI_STATE} from "../actions/actionCreators/storeUpdateActions";

function* handleStoreUpdate(updateLocalStorage, action) {
    let result = yield call(
        updateLocalStorage,
        action.payload
    );

    yield put({
        type: `${action.type}_END`,
        payload: action.payload,
        result
    });
}

export function dataStoreSaga(updateLocalStorage) {
    return function* () {
        yield takeEvery(UPDATE_UI_STATE, handleStoreUpdate, updateLocalStorage);
    }
}
