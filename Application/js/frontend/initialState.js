import {LOCAL_STORAGE_KEY_UI_STATE} from "./helper/constants";

export const getInitialState = () => {
    let localStorageData = localStorage.getItem(LOCAL_STORAGE_KEY_UI_STATE);

    if (localStorageData) {
        localStorageData = JSON.parse(localStorageData);
    } else {
        localStorageData = null;
    }

    return {
        uiState: localStorageData
    };
};