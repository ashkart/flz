import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import {CookiesProvider} from 'react-cookie';

import configureStore from "./configureStore";
import rootSaga from "./sideEffects/rootSaga";
import dataProvider from "./dataProvider";
import updateLocalStorage from "./localStorage";

import VacancyList from "./pages/vacancy/list";
import LoginPage from "./pages/auth/login";
import RegisterPage from "./pages/auth/register";
import MainPage from "./pages/main/MainPage";
import {VacancyEditPage} from "./pages/vacancy/edit";
import {VacancyViewPage} from "./pages/vacancy/view";
import {ResumeEditPage} from "./pages/resume/edit";
import {ProfilePage} from "./pages/profile/options";
import ResumeList from "./pages/resume/list";
import {ResumeViewPage} from "./pages/resume/view";
import PageNotFound from "./pages/PageNotFound";
import PasswordChangePage from "./pages/auth/password-change";
import RuleList from "./pages/rule/list";

const store = configureStore();
store.runSaga(rootSaga(dataProvider, updateLocalStorage));

class Main extends React.Component {
    render() {
        return (
            <Switch>
                <Route path="/auth/register" component={RegisterPage} />
                <Route path="/auth/password-change" component={PasswordChangePage} />
                <Route path="/auth" component={LoginPage} />
                <Route path="/index/confirm/result-:val" component={MainPage} />
                <Route path="/(index/index)?" exact component={MainPage} />
                <Route path="/profile/edit" component={ProfilePage} />
                <Route path="/profile/confirm-new-password(/:params)?" component={MainPage} />
                <Route path="/resume/view/:id/:callbackId?" component={ResumeViewPage} />
                <Route path="/resume/edit/:id?" component={ResumeEditPage} />
                <Route path="/resume/search" component={ResumeList} />
                <Route path="/rule/list" component={RuleList} />
                <Route path="/vacancy/edit/:id?" component={VacancyEditPage} />
                <Route path="/vacancy/view/:id/:callbackId?" component={VacancyViewPage} />
                <Route path="/vacancy/search" component={VacancyList} />
                <Route component={PageNotFound} status={404} />
            </Switch>
        );
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const mainContent = document.getElementById("mainContent");

    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter>
                <CookiesProvider>
                    <Main/>
                </CookiesProvider>
            </BrowserRouter>
        </Provider>,
        mainContent
    );
});