import {LOCAL_STORAGE_KEY_UI_STATE} from "./helper/constants";

export default function updateLocalStorage({uiKey, value}) {
    return new Promise(
        () => {
            let userStorageData = localStorage.getItem(LOCAL_STORAGE_KEY_UI_STATE);

            if (userStorageData) {
                userStorageData = JSON.parse(userStorageData);
            } else {
                userStorageData = {};
            }

            userStorageData[uiKey] = value;
            localStorage.setItem(LOCAL_STORAGE_KEY_UI_STATE, JSON.stringify(userStorageData));
            return localStorage[LOCAL_STORAGE_KEY_UI_STATE];
        }
    );
};