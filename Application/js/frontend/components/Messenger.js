import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withCookies } from 'react-cookie';
import MessageForm from "./Forms/MessageForm";
import {COOKIE_USER_ID} from "../helper/constants";
import Message from "./controlled/Message";
import humanReadableDate from "../helper/humanReadableDate";
import {normalizeDate} from "../helper/utils";

class Messenger extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: []
        };

        this._selfUserId = null;

        this.addMessage = this.addMessage.bind(this);
    }

    componentWillMount() {
        this._selfUserId = this.props.cookies.get(COOKIE_USER_ID);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.messages !== this.state.messages) {
            this.setState(prevState => ({messages: nextProps.messages}))
        }
    }

    addMessage(text) {
        let now = new Date();

        let newMessage = {
            text,
            id: (now).valueOf(),
            from_user_id: this._selfUserId,
            create_date: normalizeDate(now, true)
        };

        let messages = this.state.messages;

        messages.push(newMessage);

        this.setState(prevState => ({messages}));
    }

    render() {
        const {
            className,
            action,
            label,
            messages,
            conversationId,
            conversations
        } = this.props;

        return (
            <div className="messenger">
                <h3>{label}</h3>
                <div className="message-container">
                    {
                        this.state.messages.slice(0).reverse().map((message, index) => {
                            return (
                                <Message
                                    className={`${this._selfUserId === message.from_user_id ? 'out' : 'in'}`}
                                    text={message.text}
                                    date={message.create_date}
                                    key={message.id}
                                />
                            );
                        })
                    }
                    {
                        !this.state.messages.length &&
                            <p className="message-empty">Сообщений нет.</p>
                    }
                </div>
                <MessageForm action={action} onSuccess={this.addMessage} />
            </div>
        );
    }
}

Messenger.propTypes = {
    label: PropTypes.string,
    action: PropTypes.string.isRequired,
    className: PropTypes.string,
    messageResource: PropTypes.string.isRequired,
    conversationId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    conversations: PropTypes.object,
    messages: PropTypes.array
};

Messenger.defaultProps = {
    className: '',
    messages: [],
    conversations: {}
};

export default withCookies(connect((state, props) => {
    const conversation = props.conversations && props.conversations[props.conversationId]
        ? props.conversations[props.conversationId]
        : false
    ;

    return {state, ...props, messages: conversation ? conversation.messages : []};
})(Messenger));