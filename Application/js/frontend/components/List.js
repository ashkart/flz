import React, { Children } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import queryString from "query-string";

import ReactPaginate from 'react-paginate';

import {fetchListAction} from "../actions/actionCreators/dataFetchActions";

import Filters from "./Filters";
import {
    LIST_RESUME,
    LIST_VACANCY,
    UI_STATE_KEY_FILTER_RESUME,
    UI_STATE_KEY_FILTER_VACANCY,
    UI_STATE_KEY_SEARCH_FOR
} from "../helper/constants";
import {updateUiStateAction} from "../actions/actionCreators/storeUpdateActions";

const defaultPaging = {
    page: 1,
    perPage: 10
};

const PAGE_DEFAULT = 1;
const PER_PAGE_DEFAULT = 10;

class ListController extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data:  {},
            filters: {},
            paging: {
                page: PAGE_DEFAULT,
                perPage: PER_PAGE_DEFAULT
            }
        };

        this.onFilterChange = this.onFilterChange.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.pagingHandler = this.pagingHandler.bind(this);

        this._updateUrl = this._updateUrl.bind(this);
    }

    fetchData() {
        this.props.fetchList(this.props.resource, this.state.filters, this.state.paging);
    }

    componentWillMount() {
        let filters = this.props.filterInitial;

        let paging = this.state.paging;

        if (this.props.savedFilterState) {
            filters = Object.assign({}, filters, this.props.savedFilterState);
        }

        if (this.props.location && typeof this.props.location.search === 'string') {
            const query = queryString.parse(this.props.location.search);

            if (query.filters) {
                filters = Object.assign(filters, JSON.parse(query.filters));
            }

            if (query.page) {
                paging.page = JSON.parse(query.page);
            }

            if (query.perPage) {
                paging.perPage = JSON.parse(query.perPage);
            }
        }

        this.setState(
            prevState => ({filters, paging}),
            this.fetchData
        );
    }

    componentWillReceiveProps(nextProps) {
        const prevFilters = this.state.filters;

        let filters = Object.assign({}, nextProps.state.filters || {}, prevFilters);

        if (this.props.filterOverride) {
            for (let key in this.props.filterOverride) {
                filters[key] = this.props.filterOverride[key];
            }
        }

        this.setState(prevState => ({
                ...this.state,
                filters,
                data: nextProps.state.data[this.props.resource] || {}
            }),
            () => {
                if (JSON.stringify(this.state.filters) !== JSON.stringify(prevFilters)) {
                    this.onFilterChange(this.state.filters, false);
                }
            }
        );
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.state.data &&
            (
                JSON.stringify(this.state.data) !== JSON.stringify(nextProps.state.data) ||
                JSON.stringify(this.props.filters) !== JSON.stringify(nextProps.filters)
            );
    }

    onFilterChange(filters, updateUrl = true) {
        this.setState(
            prevState => {
                return {
                    filters: filters,
                    paging: {
                        page: PAGE_DEFAULT,
                        perPage: PER_PAGE_DEFAULT
                    }
                }
            },
            () => {
                if (updateUrl) {
                    this._updateUrl();
                }

                this.fetchData();
                this.props.persistFilterState(filters, this.props.resource);
            }
        );
    }

    pagingHandler({selected}) {
        let page = ++selected;

        this.setState(prevState => ({
                paging: {
                    ...this.state.paging,
                    page
                }
            }),
            () => {
                this.fetchData();
                this._updateUrl();
            }
        );
    }

    _updateUrl() {
        let params = queryString.parse(this.props.location.search);

        let filters = {};

        for (let filterName in this.state.filters) {
            filters[filterName] = this.state.filters[filterName];
        }

        params = {
            ...params,
            ...this.state.paging
        };

        if (Object.keys(filters).length) {
            params.filters = JSON.stringify(filters);
        } else {
            delete params.filters;
        }

        this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(params)}`);
    }

    render() {
        const className = this.props.className;

        let data = this.state.data;

        const filters = this.props.filters;

        return (
            <div className={`list ${className}`}>
                {
                    !!filters.length &&
                    <Filters onChange={this.onFilterChange} filtersInitState={this.state.filters}>
                        {
                            filters.map((filter) => {
                                return React.cloneElement(filter, {
                                    ...filter.props,
                                    filterValues: this.state.filters[filter.props.name]
                                });
                            })
                        }
                    </Filters>
                }
                <ListView data={data} paging={this.state.paging} pageName={this.props.pageName} onPageChange={this.pagingHandler}>
                    {this.props.children}
                </ListView>
            </div>
        );
    }
}

class ListView extends React.Component {
    render() {
        const {
            children,
            paging,
            onPageChange,
            pageName
        } = this.props;

        let data = this.props.data || {};
        const totalItems = data.totalItems || 0;
        const page = paging.page - 1;
        const perPage = paging.perPage;
        const pagesTotal = Math.ceil(totalItems / perPage);

        delete data.unexplicitFilters;

        let dataRowsOnly = {...data};
        delete dataRowsOnly.totalItems;

        return (
            <div className="list-view">
                {!!pageName && <h2>{pageName}</h2>}
                {
                    !Object.keys(dataRowsOnly).length &&
                    <p>К сожалению, по Вашему запросу ничего не найдено.</p>
                }
                {
                    Children.map(children, (child) => {
                        return Object.keys(dataRowsOnly).map((key) => {

                            return React.cloneElement(child, {
                                ...child.props,
                                data: dataRowsOnly[key]
                            });
                        });
                    })
                }
                {
                    pagesTotal > 1 &&
                    <ReactPaginate
                        previousLabel={"previous"}
                        nextLabel={"next"}
                        breakLabel={<a href="">...</a>}
                        breakClassName={"break-me"}
                        pageCount={pagesTotal}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={onPageChange}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}
                        forcePage={page}
                    />
                }
            </div>
        );
    }
}

ListController.propTypes = {
    className: PropTypes.string,
    resource: PropTypes.string.isRequired,
    filters: PropTypes.arrayOf(PropTypes.object),
    filterOverride: PropTypes.object,
    filterInitial: PropTypes.object,
    pageName: PropTypes.string
};

ListController.defaultProps = {
    className: '',
    pageName: '',
    filterOverride: {},
    filterInitial: {},
    filters: []
};

const getUiFiltersKey = (resource) => {
    let uiFiltersKey = null;

    switch(resource) {
        case LIST_VACANCY:
            uiFiltersKey = UI_STATE_KEY_FILTER_VACANCY;
            break;
        case LIST_RESUME:
            uiFiltersKey = UI_STATE_KEY_FILTER_RESUME;
    }

    return uiFiltersKey;
};

export default connect(
    (state, props) => {
        let uiFiltersKey = getUiFiltersKey(props.resource);

        const uiState = state.uiState;

        const savedFilterState = uiState ? uiState[uiFiltersKey] : null;

        return { state, ...props, savedFilterState };
    },
    (dispatch) => ({
        persistFilterState: (filters, resource) => {
            dispatch(updateUiStateAction(getUiFiltersKey(resource), filters));
        },
        fetchList: (resource, filters, paging) => {
            dispatch(fetchListAction(resource, {filters, paging}));
        }
    })
)(ListController);