import React from "react";
import TabLayout from "./TabLayout";
import Tab from "./TabLayout/Tab";
import UserSettings from "./Forms/UserSettings";
import List from "./List";
import ResumeListItem, {RESUME_CALLBACKS_RESOURCE} from "./ViewModel/ResumeListItem";
import {RESUME_LIST_ITEM_MAPPING} from "../pages/resume/list";
import VacancyListItem, {VACANCY_CALLBACKS_RESOURCE} from "./ViewModel/VacancyListItem";
import {VACANCY_LIST_ITEM_MAPPING} from "../pages/vacancy/list";
import CallbackListItem, {CALLBACK_LIST_ITEM_MAPPING} from "./ViewModel/CallbackListItem";
import {
    COOKIE_ROLE,
    LIST_PROFILE_CALLBACKS,
    LIST_PROFILE_RESUMES,
    LIST_PROFILE_VACANCIES, ROLE_ADMIN,
    ROLE_EMPLOYEE, ROLE_RECRUITER
} from "../helper/constants";
import {withCookies} from "react-cookie";

class Options extends React.Component {
    render() {
        const {
            cookies
        } = this.props;

        return (
            <div className="options-container">
                <h2>Мой профиль</h2>
                <TabLayout defaultActiveId="tabPersonalInfo">
                    <Tab id="tabPersonalInfo" tabLabel="Персональная информация">
                        <UserSettings />
                    </Tab>
                    <Tab id="callbacks" tabLabel="Мои отклики">
                        <List className="list-callback"  resource={LIST_PROFILE_CALLBACKS}>
                            <CallbackListItem
                                mapping={CALLBACK_LIST_ITEM_MAPPING}
                            />
                        </List>
                    </Tab>
                    {
                        (cookies.get(COOKIE_ROLE) === ROLE_EMPLOYEE ||
                        cookies.get(COOKIE_ROLE) === ROLE_ADMIN) &&
                        <Tab id="resumes" tabLabel="Мои резюме">
                            <List className="list-resume" resource={LIST_PROFILE_RESUMES}>
                                <ResumeListItem
                                    mapping={RESUME_LIST_ITEM_MAPPING}
                                    withCallbacks={true}
                                    callbacksResource={RESUME_CALLBACKS_RESOURCE}
                                />
                            </List>
                        </Tab>
                    }
                    {
                        (cookies.get(COOKIE_ROLE) === ROLE_RECRUITER ||
                        cookies.get(COOKIE_ROLE) === ROLE_ADMIN) &&
                        <Tab id="vacancies" tabLabel="Мои вакансии">
                            <List className="list-vacancy" resource={LIST_PROFILE_VACANCIES}>
                                <VacancyListItem
                                    mapping={VACANCY_LIST_ITEM_MAPPING}
                                    withCallbacks={true}
                                    callbacksResource={VACANCY_CALLBACKS_RESOURCE}
                                />
                            </List>
                        </Tab>
                    }
                </TabLayout>
            </div>
        );
    }
}

export default withCookies(Options);

