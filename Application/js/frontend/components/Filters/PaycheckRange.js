import React from "react";
import PropTypes from 'prop-types';
import composeFilter from "../hoc/composeFilter";
import {DataContext} from "../context";
import {DEFAULT_RANGE} from "../Range";
import Range from "../Range";
import {clamp, formatNumber} from "../../helper/utils";

class PaycheckRange extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.filterValues
        };

        this.onChange = this.onChange.bind(this);
        this.onChangeComplete = this.onChangeComplete.bind(this);
    }

    onChange(value) {
        this.setState(prevState => ({value}));
    }

    onChangeComplete(value) {
        this.props.updateFilter(value)
    }

    render() {
        const {
            label,
            name,
            step,
            filterValues
        } = this.props;

        return (
            <DataContext.Consumer>
                {
                    ({paycheckRange}) => {
                        return <div className={`filter ${name}`}>
                            {!!label && <span className="label">{label}</span>}
                            <Range
                                step={step}
                                minValue={paycheckRange.min}
                                maxValue={paycheckRange.max}
                                value={this.state.value}
                                onChangeComplete={this.onChangeComplete}
                                onChange={this.onChange}
                                allowSameValues={true}
                                formatLabel={() => ('')}
                            />
                            <span className="text">от {formatNumber(clamp(this.state.value.min, paycheckRange.min, paycheckRange.max))}</span>
                            <span className="text"> до {formatNumber(clamp(this.state.value.max, paycheckRange.min, paycheckRange.max))}</span>
                            <span className="currency-icon"> руб.</span>
                        </div>
                    }
                }
            </DataContext.Consumer>
        );
    }
}

PaycheckRange.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    filterValues: PropTypes.object,
    step: PropTypes.number
};

PaycheckRange.defaultProps = {
    filterValues: DEFAULT_RANGE
};

export default composeFilter(PaycheckRange);