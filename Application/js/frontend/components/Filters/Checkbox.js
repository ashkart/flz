import React from "react";
import PropTypes from 'prop-types';
import composeFilter from "../hoc/composeFilter";

import CheckboxInput, {CHECKED_STATE_FULL, CHECKED_STATE_NONE} from "../controlled/Checkbox";

class Checkbox extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filterValue: props.filterValues
        };

        this.onClick = this.onClick.bind(this);
        this.onUpdateFilter = this.onUpdateFilter.bind(this);
        this._getCheckboxState = this._getCheckboxState.bind(this);
    }

    onClick() {
        this.setState(
            prevState => ({filterValue: !this.state.filterValue}),
            this.onUpdateFilter
        );
    }

    onUpdateFilter() {
        this.props.updateFilter(this.state.filterValue);
    }

    _getCheckboxState() {
        if (this.state.filterValue) {
            return CHECKED_STATE_FULL;
        }

        return CHECKED_STATE_NONE;
    }

    render() {
        const {
            name,
            label
        } = this.props;

        return (
            <div className={`filter ${name}`}>
                <CheckboxInput label={label} onClick={this.onClick} checkedState={this._getCheckboxState()}/>
            </div>
        );
    }
}

Checkbox.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    filterValues: PropTypes.bool
};

export default composeFilter(Checkbox);