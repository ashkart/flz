import React from "react";
import PropTypes from "prop-types";

import composeFilter from "../hoc/composeFilter";
import {fetchProfareaTreeAction} from "../../actions/actionCreators/dataFetchActions";
import TreeSelect from "../TreeSelect";

class TreeSelectFilter extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            inputProps,
            name,
            maxHeight,
            label,
            selectedValues,
            updateFilter,
            resource,
            data
        } = this.props;

        return (
            <TreeSelect
                fetchTree={this.props.filterDataAction}
                actionCreator={fetchProfareaTreeAction}
                name={name}
                onUpdateSelection={updateFilter}
                selectedValues={selectedValues}
                maxHeight={maxHeight}
                label={label}
                inputProps={inputProps}
                resource={resource}
                data={data}
            />
        );
    }
}

TreeSelectFilter.propTypes = {
    selectedValues: PropTypes.array,
    updateFilter: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    maxHeight: PropTypes.string,
    data: PropTypes.object
};

TreeSelectFilter.defaultProps = {
    selectedValues: [],
    data: {}
};

export default composeFilter(TreeSelectFilter);