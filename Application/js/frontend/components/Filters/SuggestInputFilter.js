import React from "react";
import PropTypes from 'prop-types';

import composeFilter from "../hoc/composeFilter";
import SuggestInput from "../SuggestInput";

export class SuggestInputFilter extends React.Component {
    constructor(props) {
        super(props);

        this.onSelectionUpdated = this.onSelectionUpdated.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps) {
            console.log();
        }
    }

    onSelectionUpdated(selectedItems) {
        this.props.updateFilter(selectedItems);
    }

    render() {
        const {
            resource,
            inputProps,
            name,
            renderSuggestionItem,
            label,
            multiSelect,
            isStrict,
            suggestions,
            filterValues,
            className,
            filterDataAction
        } = this.props;

        return (
            <SuggestInput
                resource={resource}
                fetchSuggestions={filterDataAction}
                className={className}
                name={name}
                renderSuggestionItem={renderSuggestionItem}
                multiSelect={multiSelect}
                label={label}
                isStrict={isStrict}
                selectedValues={filterValues}
                suggestions={suggestions}
                inputProps={inputProps}
                onUpdateSelection={this.onSelectionUpdated}
            />
        );
    }
}

SuggestInputFilter.propTypes = {
    /*
    * Только предложенные значения?
    */
    isStrict: PropTypes.bool,

    multiSelect: PropTypes.bool,
    updateFilter: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    renderSuggestionItem: PropTypes.func.isRequired,
    onFocusOut: PropTypes.func,
    filterValues: PropTypes.object,
    label: PropTypes.string,
    suggestions: PropTypes.object,
    inputProps: PropTypes.object,
    resource: PropTypes.string.isRequired,
};

SuggestInputFilter.defaultProps = {
    isStrict: true,
    multiSelect: false,
    filterValues: {},
    suggestions: {}
};

export default composeFilter(SuggestInputFilter);