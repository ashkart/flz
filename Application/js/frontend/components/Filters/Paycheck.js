import React from "react";
import PropTypes from 'prop-types';
import composeFilter from "../hoc/composeFilter";
import Input from "../controlled/Input";
import attachOnBlurListener from "../../helper/attachOnBlurListener";

class Paycheck extends React.Component {
    constructor(props) {
        super(props);

        const initValue = this.props.filterValues;

        this.state = {
            filterValue: initValue,
            hasChanges: false
        };

        this.onChange = this.onChange.bind(this);
        this.onFilterUpdate = this.onFilterUpdate.bind(this);
        this.onClearInputClick = this.onClearInputClick.bind(this);

        const self = this;

        attachOnBlurListener(
            [`input-container-${this.props.inputName}`],
            () => self.state.hasChanges,
            self.onFilterUpdate
        );
    }

    onFilterUpdate() {
        this.props.updateFilter(this.state.filterValue);

        this.setState(prevState => ({hasChanges: false}));
    }

    onClearInputClick() {
        this.setState(
            prevState => ({
                filterValue: ''
            }),
            this.onFilterUpdate
        );
    }

    onChange(newValue) {
        this.setState(prevState => ({
            filterValue: newValue,
            hasChanges: newValue !== this.state.filterValue
        }));
    }

    render() {
        const {
            label,
            name
        } = this.props;

        return (
            <div className={`filter ${name}`}>
                {!!label && <span className="label">{label}</span>}
                <Input
                    inputName={name}
                    onBlur={this.onFilterUpdate}
                    onClearInputClick={this.onClearInputClick}
                    onChange={this.onChange}
                    value={this.state.filterValue}
                />
            </div>
        );
    }
}

Paycheck.propTypes = {
    updateFilter: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    filterValues: PropTypes.string,
};

Paycheck.defaultProps = {
    updateFilter: () => {}
};

export default composeFilter(Paycheck);