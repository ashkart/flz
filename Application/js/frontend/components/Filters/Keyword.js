import React from 'react';
import PropTypes from 'prop-types';
import {SuggestInputFilter} from "./SuggestInputFilter";
import Checkbox from "../controlled/Checkbox";
import {CHECKED_STATE_NONE, CHECKED_STATE_FULL} from "../controlled/Checkbox";
import composeFilter from "../hoc/composeFilter";

class Keyword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checkedOption: this.props.filterValues.checkedOption
        };

        let values = {};

        if (this.props.filterValues.hasOwnProperty('values')) {
            values = this.props.filterValues.values;
        }

        this.state.values = values;

        this.onCheckboxClick = this.onCheckboxClick.bind(this);
        this.onUpdateFilter = this.onUpdateFilter.bind(this);
        this._getCheckboxState = this._getCheckboxState.bind(this);
    }

    onCheckboxClick() {
        this.setState(
            prevState => ({checkedOption: !this.state.checkedOption}),
            () => {this.props.updateFilter(this.state)}
        );
    }

    onUpdateFilter(filterValues) {
        this.setState(
            prevState => ({
                values: filterValues,
                checkedOption: this.state.checkedOption
            }),
            () => {this.props.updateFilter(this.state)}
        );
    }

    _getCheckboxState() {
        if (this.state.checkedOption) {
            return CHECKED_STATE_FULL;
        }

        return CHECKED_STATE_NONE;
    }

    render() {
        const {
            checkBoxLabel,
            withOption,
            name,
            updateFilter,
            filterValues,
            ...rest
        } = this.props;

        const values = this.state.values;

        return (
            <div className={`filter keyword-${name}`}>
                <SuggestInputFilter
                    filterValues={values}
                    name={name}
                    updateFilter={this.onUpdateFilter}
                    {...rest}
                />
                {
                    withOption &&
                    <Checkbox label={checkBoxLabel} onClick={this.onCheckboxClick} checkedState={this._getCheckboxState()}/>
                }
            </div>
        );
    }
}

Keyword.propTypes = {
    withOption: PropTypes.bool,
    checkedOption: PropTypes.bool,
    ...SuggestInputFilter.propTypes
};

Keyword.defaultProps = {
    withOption: true,
    checkedOption: false,
    ...SuggestInputFilter.defaultProps
};

export default composeFilter(Keyword);