import React from "react";
import PropTypes from 'prop-types';
import composeFilter from "../hoc/composeFilter";
import Checkbox, {CHECKED_STATE_NONE, CHECKED_STATE_FULL} from "../controlled/Checkbox";

class CheckboxGroup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: this.props.filterValues
        };

        this.onClick = this.onClick.bind(this);
        this.onOptionClick = this.onOptionClick.bind(this);
    }

    onOptionClick(value) {
        let selected = Object.assign({}, this.state.selected);

        if (selected.hasOwnProperty(value)) {
            delete selected[value];
        } else {
            selected[value] = value;
        }

        this.setState(
            prevState => ({selected}),
            () => {this.props.updateFilter(this.state.selected)}
        );
    }

    _getOptionCheckedState(option) {
        if (this.state.selected.hasOwnProperty(option.value)) {
            return CHECKED_STATE_FULL;
        }

        return CHECKED_STATE_NONE;
    }

    onClick(e) {
        const value = e.currentTarget.getAttribute('data-value');
        this.onOptionClick(value);
    }

    render() {
        const {
            label,
            name,
            options
        } = this.props;

        return (
            <div className={`filter ${name}`}>
                {!!label && <span className="label">{label}</span>}
                {
                    options.map(option => {
                        return (
                            <div
                                key={option.value}
                                className="filter-option"
                                onClick={this.onClick}
                                data-value={option.value}
                            >
                                <Checkbox
                                    label={option.label}
                                    checkedState={this._getOptionCheckedState(option)}
                                />
                            </div>
                        );
                    })
                }
            </div>
        );
    }
}

CheckboxGroup.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })),
    filterValues: PropTypes.object
};

CheckboxGroup.defaultProps = {
    options: [],
    filterValues: {}
};

export default composeFilter(CheckboxGroup);