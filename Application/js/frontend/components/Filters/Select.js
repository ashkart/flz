import React from 'react';
import PropTypes from 'prop-types';
import composeFilter from "../hoc/composeFilter";

class Select extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: this.props.filterValues !== null ? this.props.filterValues : this.props.options[0].value
        };

        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        const value = e.target.value;

        this.setState(
            prevState => ({selected: value}),
            () => {this.props.updateFilter(this.state.selected)}
        );
    }

    render() {
        const {
            name,
            options,
            label
        } = this.props;

        return (
            <div className={`filter ${name}`}>
                {!!label && <span className="label">{label}</span>}
                <label className="select-wrap">
                    <select className="select" value={this.state.selected} onChange={this.onChange}>
                        {
                            options.map(option => {
                                return <option key={option.value} value={option.value}>{option.label}</option>;
                            })
                        }
                    </select>
                </label>
            </div>
        );
    }
}

Select.proptTypes = {
    name: PropTypes.string.isRequired,
    filterValues: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })),
};

Select.defaultProps = {
    options: [{label: '', value: ''}],
    filterValues: null
};

export default composeFilter(Select);