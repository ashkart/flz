import React from 'react';
import PropTypes from 'prop-types';

export default class Tab extends React.Component {
    render() {
        const {
            id,
            tabLabel,
            className,
            ...rest
        } = this.props;

        return (
            <li id={id} className={className} {...rest}>
                {tabLabel}
            </li>
        );
    }
}

Tab.propTypes = {
    id: PropTypes.string.isRequired,
    tabLabel: PropTypes.string.isRequired,
    className: PropTypes.string,
    children: PropTypes.element.isRequired
};

Tab.defaltProps = {
    className: ''
};