import React from 'react';
import PropTypes from 'prop-types';

class Notification extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: true
        };

        this.hideMessage = this.hideMessage.bind(this);
    }

    hideMessage() {
        this.setState(
            prevState => ({visible: false}),
            this.props.onClose
        );
    }

    render() {
        const {
            message
        } = this.props;

        return (
            <div className="row notification alert alert-warning" style={{display: this.state.visible ? 'flex' : 'none'}}>
                <span className="text">{message}</span>
                <span
                    className="btn-close fas fa-times"
                    onClick={this.hideMessage}
                ></span>
            </div>
        );
    }
}

Notification.propTypes = {
    message: PropTypes.string.isRequired,
    onClose: PropTypes.func
};

Notification.defaultProps = {
    onClose: () => {}
};

export default Notification;
