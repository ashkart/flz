import React from "react";
import {DEFAULT_RANGE} from "./Range";

export const TreeSelectContext = React.createContext({
    isChecked: (id) => {}
});

export const DataContext = React.createContext({
    paycheckRange: DEFAULT_RANGE
});