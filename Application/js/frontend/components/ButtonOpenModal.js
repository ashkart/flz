import React from 'react';
import PropTypes from 'prop-types';
import VanillaModal from 'vanilla-modal';

export default class ButtonOpenModal extends React.Component {
    constructor(props) {
        super(props);

        this._modal = null;

        this.getNewModal = this.getNewModal.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onConfirmSuccess = this.onConfirmSuccess.bind(this);
    }

    componentDidMount() {
        /*
         * приходится инициализировать разметку с модальным окном скрытым,
         * иначе "едут" размеры контейнеров модалок.
         */
        this._modal = this.getNewModal();
    }

    getNewModal() {
        const modalId = this.props.modalId;

        return new VanillaModal({
            modal: `#modal-${modalId}`,
            modalInner: `#modal-inner-${modalId}`,
            modalContent: `#modal-content-${modalId}`,
            open: '[data-modal-open]',
            close: '[data-modal-close]',
            page: this.props.modalMainContainerSelector,
            loadClass: 'modal-container-main',
            class: 'modal-visible',
            clickOutside: true,
            closeKeys: [27],
            transitions: true,
            onBeforeOpen: null,
            onBeforeClose: null,
            onOpen: null,
            onClose: null
        });
    }

    openModal() {
        if (this._modal) {
            this._modal.destroy();
        }

        this._modal = this.getNewModal();

        this.props.onOpen();
        this._modal.open(`#${this.props.modalId}`);
    }

    closeModal() {
        this.props.onClose();

        if (this._modal) {
            this._modal.close(`#${this.props.modalId}`);

            this._modal.destroy();
        }
    }

    onConfirmSuccess() {
        this.closeModal();
        this.props.onConfirmSuccess();
    }

    render() {
        const {
            className,
            children,
            modalContent,
            modalId,
            closeModalComponentProp,
            onConfirmSuccess,
            ...modalProps
        } = this.props;

        const ModalContent = modalContent;

        if (closeModalComponentProp) {
            modalProps[closeModalComponentProp] = this.onConfirmSuccess;
        }

        return (
            <div className={className}>
                <button className="btn-open-modal" onClick={this.openModal} type="button">
                    {children}
                </button>
                <div id={`modal-${modalId}`} className={`modal`}>
                    <div id={`modal-inner-${modalId}`} className={`modal-inner`}>
                        {
                            <div id={`modal-content-${modalId}`} className={`modal-content`}></div>
                        }
                    </div>
                </div>
                {
                    <div id={modalId} style={{display: 'none'}}>
                            <ModalContent {...modalProps}/>
                    </div>
                }
            </div>
        );
    }
}

ButtonOpenModal.propTypes = {
    className: PropTypes.string,
    modalContent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    onConfirmSuccess: PropTypes.func,
    modalId: PropTypes.string.isRequired,
    modalMainContainerSelector: PropTypes.string,
    closeModalComponentProp: PropTypes.string
};

ButtonOpenModal.defaultProps = {
    modalMainContainerSelector: 'body',
    className: '',
    onOpen: () => {},
    onClose: () => {},
    onConfirmSuccess: () => {}
};