import React from 'react';
import PropTypes from 'prop-types';
import {withJData} from "./hoc/withJData";
import Notification from "./Notification";

class JDataNotification extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: props.jData.messages || []
        };

        this.removeMessage = this.removeMessage.bind(this);
    }

    removeMessage(index) {
        let messages = Array.from(this.state.messages);

        messages.splice(index, 1);

        this.setState(
            prevState => ({messages}),
            () => {
                let jData = this.props.jData;
                jData.messages = this.state.messages;
                this.props.updateJdata(jData);
            }
        );
    }

    render() {
        const {
            jData
        } = this.props;

        const hasMessages = !!this.state.messages.length;

        return (
            <div className="notifications" style={{display: hasMessages ? 'block' : 'none'}}>
                {
                    this.state.messages.map((messageText, index) => {
                        return (
                            <Notification key={index} message={messageText} onClose={() => this.removeMessage(index)}/>
                        );
                    })
                }
            </div>
        );
    }
}

JDataNotification.propTypes = {
    jData: PropTypes.object,
    updateJdata: PropTypes.func
};

JDataNotification.defaultProps = {
    jData: {},
    updateJdata: () => {}
};

export default withJData(JDataNotification);