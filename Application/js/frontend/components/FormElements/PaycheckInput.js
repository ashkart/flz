import React from 'react';
import PropTypes from 'prop-types';
import {renderInput, renderSelect} from "./renderElement";
import {Field} from 'redux-form';

export default class PaycheckInput extends React.Component {
    render() {
        const {
            className,
            label,
            options,
            onSelect,
            mapping: {currency: currencyFieldName, value: valueFieldName},
            [currencyFieldName]: {input: currencyInput},
            [valueFieldName]: {input: valueInput}
        } = this.props;

        return (
            <div className={`element ${className}`}>
                {!!label && <span className="label">{label}</span>}
                <div className="inline" style={{marginBottom: 0}}>
                    <Field
                        className="select"
                        name={currencyFieldName}
                        options={options}
                        component={renderSelect}
                        onChange={onSelect}
                        renderErrors={false}
                    />
                    <Field
                        className="input"
                        type="number"
                        placeholder={valueInput.placeholder}
                        name={valueFieldName}
                        component={renderInput}
                    />
                </div>
            </div>
        );
    }
}

PaycheckInput.propTypes = {
    label: PropTypes.string,
    mapping: PropTypes.shape({currency: PropTypes.string, value: PropTypes.string}).isRequired,
    options: PropTypes.array.isRequired,
    onSelect: PropTypes.func.isRequired
};

PaycheckInput.defaultProps = {
    label: ''
};