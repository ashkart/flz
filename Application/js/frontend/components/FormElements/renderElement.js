import React from "react";
import SuggestInput from "../SuggestInput";
import {asArray} from "../../helper/utils";
import ViewCount from "../controlled/ViewCount";
import { Editor } from "@tinymce/tinymce-react";
import HtmlToReactParser from 'html-to-react/lib/parser';
import CreatableSelect from '../controlled/CreatableSelect';

export function renderInput({ autocomplete, className, wrapClassName = '', input, label = '', style, placeholder, type, meta: { touched, error }, renderErrors = true}) {
    let inputClassName = buildClassName(className, touched, error);

    let errors = null;

    if (typeof error === 'object') {
        errors = asArray(error);
    } else if (error) {
        errors = error;
    }

    return (
        <div className={`element ${wrapClassName}`} style={style}>
            {!!label && <span className="label">{label}</span>}
            <input
                {...input}
                autoComplete={autocomplete}
                className={inputClassName}
                placeholder={placeholder}
                type={type}
            />
            {renderErrors && touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
}

export function renderCheckbox({ className, wrapClassName = '', input, label, meta: { touched, error } }) {
    let inputClassName = buildClassName(className, touched, error);
    let errors = null;

    if (error) {
        errors = asArray(error);
    }

    return (
        <div className={`element-secondary ${wrapClassName}`}>
            <label className="checkbox-label">
                <input
                    {...input}
                    checked={input.value}
                    className={inputClassName}
                    type="checkbox"
                />
                <span className="label-text">{label}</span>
            </label>
            {touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
}

export function renderSelect({ className, wrapClassName = '', input, label = '', options, meta: { touched, error }, renderErrors = true }) {
    let selectClassName = buildClassName(className, touched, error);
    let errors = null;

    if (error) {
        errors = asArray(error);
    }

    return (
        <div className={`element ${wrapClassName}`}>
            {!!label && <span className="label">{label}</span>}
            <label className="select-wrap">
                <select
                    {...input}
                    onChange={input.onChange}
                    className={selectClassName}
                >
                    {
                        options.map(option => {
                            return <option key={option.value} value={option.value}>{option.label}</option>;
                        })
                    }
                </select>
            </label>
            {renderErrors && touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
}

export function renderCreatableSelect({ className, wrapClassName = '', selected, input, label = '', options, meta: { touched, error }, renderErrors = true }) {
    let selectClassName = buildClassName(className, touched, error);
    let errors = null;

    if (error) {
        errors = asArray(error);
    }

    return (
        <div className={`element ${wrapClassName}`}>
            {!!label && <span className="label">{label}</span>}
            <label className="creatable-select-wrap">
                <CreatableSelect
                    {...input}
                    selected={selected}
                    onChange={input.onChange}
                    className={selectClassName}
                    options={options}
                />
            </label>
            {renderErrors && touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
}

export function renderTextarea(props) {
    const { className, input, label = '', placeholder, meta: { touched, error } } = props;
    let inputClassName = buildClassName(className, touched, error);
    let errors = null;

    if (error) {
        errors = asArray(error);
    }

    return (
        <div className="element flex-container column" style={{width: '100%'}}>
            {!!label && <span className="label">{label}</span>}
            <textarea
                {...input}
                className={inputClassName}
                placeholder={placeholder}
            >{input.value}</textarea>
            {touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
}

export function renderTextEditor(props) {
    const { className, input, label = '', description, meta: { touched, error }, editor } = props;
    let inputClassName = buildClassName(className, touched, error);
    let errors = null;

    if (error) {
        errors = asArray(error);
    }

    return (
        <div className="element flex-container column" style={{width: '100%'}}>
            {!!label && <span className="label">{label}</span>}
            <Editor
                textareaName={input.name}
                initialValue={input.value}
                init={{menubar: false}}
                plugins="lists"
                toolbar='undo redo | formatselect | bold italic | numlist bullist | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
                onBlur={(event) => { input.onChange(event.target.getContent()); }}
            />
            <span className="description text-secondary">{description}</span>
            {touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
}

export function renderTextView(props) {
    const { className, input, label = '', editor } = props;

    const htmlToReactParser = new HtmlToReactParser();
    const reactElement = htmlToReactParser.parse(decodeURIComponent(input.value));

    return (
        <div className={`element ${className}`} style={{width: '100%'}}>
            {!!label && <span className="label">{label}</span>}
            {reactElement}
        </div>
    );
}

export function renderSuggestInput({
    input: {name},
    resource,
    wrapClassName = '',
    multiSelect,
    placeholder = '',
    fetchSuggestions,
    renderSuggestionItem,
    onUpdateSelection,
    selectedValues,
    suggestions,
    label = '',
    className,
    isStrict,
    meta: { touched, error }
}) {
    let errors = null;

    if (error) {
        errors = asArray(error);
    }

    className = buildClassName(className, touched, error);

    return (
        <div className={`element ${wrapClassName}`}>
            {!!label && <span className="label">{label}</span>}
            <SuggestInput
                className={className}
                name={name}
                multiSelect={multiSelect}
                isStrict={isStrict}
                onUpdateSelection={onUpdateSelection}
                fetchSuggestions={fetchSuggestions}
                renderSuggestionItem={renderSuggestionItem}
                resource={resource}
                inputProps={{placeholder}}
                suggestions={suggestions}
                selectedValues={selectedValues}
            />
            {touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
}

export function renderGeoSuggestion({title, description}) {
    return (
        <div className="suggestion">
            <div className="title">{title}</div>
            <div className="description">{description}</div>
        </div>
    );
}

export function renderViewCount({className, title, input, ...rest}) {
    return <ViewCount className={className} title={title} count={input.value}/>;
}

function buildClassName(className, touched, error) {
    let newClassName = className;

    if (touched && error) {
        newClassName = `${newClassName} has-error`;
    }

    return newClassName;
}