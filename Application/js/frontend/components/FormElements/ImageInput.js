import React from 'react';
import PropTypes from 'prop-types';

export default class ImageInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            image: null
        };

        this._imageInput = null;

        this.onChange = this.onChange.bind(this);
        this.onImageClick = this.onImageClick.bind(this);
        this.onRemoveClick = this.onRemoveClick.bind(this);
        this._ref = this._ref.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const {
            mapping: {imgData: dataFieldName, imgPath: pathFieldName},
            [dataFieldName]: {input: dataInput},
            [pathFieldName]: {input: pathInput},
        } = nextProps;

        if (pathInput.value && this.state.image === null) {
            this.setState({image: pathInput.value});
        }
    }

    onChange(e) {
        const self = this;
        const fileReader = new FileReader();

        if (e.target.files.length) {
            fileReader.addEventListener("load", function (e) {
                self.setState(
                    prevState => ({image: e.target.result}),
                    () => {self.props.onImgChange(self.state.image);}
                );
            });

            fileReader.readAsDataURL(e.target.files[0]);
        }
    }

    onImageClick(e) {
        if (this._imageInput) {
            this._imageInput.click(e);
        }
    }

    onRemoveClick() {
        this.setState(
            prevState => ({image: null}),
            () => {this.props.onImgChange(null);}
        );
    }

    _ref(ref) {
        this._imageInput = ref;
    }

    render() {
        const {
            className,
            mapping: {imgData: dataFieldName, imgPath: pathFieldName},
            [dataFieldName]: {input: dataInput},
            [pathFieldName]: {input: pathInput},
            previewWidth
        } = this.props;

        const previewStyle = {
            width: previewWidth,
        };

        let imageAlt = '';

        if (this.state.image) {
            previewStyle.backgroundColor = 'transparent';
            imageAlt = <img name={pathFieldName} style={{width: previewWidth, height: 'auto'}} src={this.state.image} />;
        } else {
            previewStyle.height = `calc(${previewWidth} * 1.25)`;
            previewStyle.display = 'flex';
            imageAlt = <div title="Добавить фото" className="fas fa-plus" style={{margin: 'auto'}}> </div>;
        }

        return (
            <div className="element">
                <div
                    className='preview-container'
                    style={previewStyle}
                    onClick={this.onImageClick}
                >
                    {imageAlt}
                </div>
                <input
                    name={dataFieldName}
                    type="file"
                    className={`hidden ${className}`}
                    multiple={false}
                    onChange={this.onChange}
                    ref={this._ref}
                />
                <button
                    style={{width: previewWidth}}
                    type="button"
                    className="remove-image danger"
                    onClick={this.onRemoveClick}
                    disabled={!this.state.image}
                >
                    <div className="fas fa-times">&nbsp;</div>
                    Удалить фото
                </button>
            </div>
        );
    }
}

ImageInput.propTypes = {
    photo: PropTypes.object.isRequired,
    photo_path: PropTypes.object.isRequired,
    previewWidth: PropTypes.string,
    onImgChange: PropTypes.func.isRequired,
    mapping: PropTypes.shape({imgData: PropTypes.string, imgPath: PropTypes.string}).isRequired
};

ImageInput.defaultProps = {
    previewWidth: '150px',
    photo: {},
    photo_path: {}
};