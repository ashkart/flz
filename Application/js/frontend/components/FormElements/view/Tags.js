import React from 'react';
import PropTypes from 'prop-types';
import ItemHolder from "../../controlled/ItemHolder";

const Tags = (props) => {
    const {
        className,
        label,
        tags,
        emptyText
    } = props;

    const hasTags = Object.values(tags).length > 0;

    return (
        <div className={`element ${className}`}>
            {!!label && <span className="label">{label}</span>}
            {!hasTags && emptyText}
            {
                hasTags &&
                <ItemHolder
                    items={tags}
                    withRemoveBtn={false}
                />
            }
        </div>
    );
};

Tags.propTypes = {
    tags: PropTypes.shape({title: PropTypes.string}),
    label: PropTypes.string,
    input: PropTypes.object.isRequired,
    className: PropTypes.string,
    emptyText: PropTypes.string
};

Tags.defaultProps = {
    tags: {},
    label: '',
    emptyText: ''
};

export default Tags;