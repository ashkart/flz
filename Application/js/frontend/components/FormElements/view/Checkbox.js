import React from 'react';
import PropTypes from 'prop-types';

export const CHECKED = 1;
export const UNCHECKED = 0;

const Checkbox = (props) => {
    const {
        className,
        label,
        input: {value},
        valueText,
        icon
    } = props;

    let checkedState;

    if (value === 'true' || value === true || value === 1 || value === '1') {
        checkedState = CHECKED;
    } else {
        checkedState = UNCHECKED;
    }

    return (
        <div className={`element ${className}`}>
            {!!label && <span className="label">{label}</span>}
            <div className="text">
                {!!icon[checkedState] && <span className={icon[checkedState]}>&nbsp;</span>}
                {valueText[checkedState]}
            </div>
        </div>
    );
};

Checkbox.propTypes = {
    label: PropTypes.string,
    input: PropTypes.object.isRequired,
    className: PropTypes.string,
    valueText: PropTypes.shape({[UNCHECKED]: PropTypes.string, [CHECKED]: PropTypes.string}).isRequired,
    icon: PropTypes.shape({[UNCHECKED]: PropTypes.string, [CHECKED]: PropTypes.string})
};

Checkbox.defaultProps = {
    label: '',
    icon: {},
    className: ''
};

export default Checkbox;