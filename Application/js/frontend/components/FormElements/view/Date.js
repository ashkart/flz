import React from 'react';
import PropTypes from 'prop-types';
import {parseDateFromRuFormat} from "../../../helper/utils";
import humanReadableDate from "../../../helper/humanReadableDate";

const Date = (props) => {
    const {
        className,
        label,
        input: {value}
    } = props;

    const date = parseDateFromRuFormat(value);

    return (
        <div className={`element ${className}`}>
            {!!label && <span className="label">{label}</span>}
            <div className="date">
                {!!date && humanReadableDate(date)}
            </div>
        </div>
    );
};

Date.propTypes = {
    label: PropTypes.string,
    input: PropTypes.object.isRequired,
    className: PropTypes.string
};

Date.defaultProps = {
    label: '',
    className: ''
};

export default Date;