import React from 'react';
import PropTypes from 'prop-types';

const Text = (props) => {
    const {
        className,
        label,
        input: {value},
        icon
    } = props;

    return (
        <div className={`element ${className}`}>
            {!!label && <span className="label">{label}</span>}
            <div className="text">
                {!!icon && <span className={icon}>&nbsp;</span>}
                {value}
            </div>
        </div>
    );
};

Text.propTypes = {
    label: PropTypes.string,
    input: PropTypes.object.isRequired,
    className: PropTypes.string,
    icon: PropTypes.string
};

Text.defaultProps = {
    label: '',
    className: '',
    icon: ''
};

export default Text;