import React from 'react';
import PropTypes from 'prop-types';

const Image = (props) => {
    const {
        className,
        label,
        input: {value},
        imgWidth
    } = props;

    const style = {
        width: imgWidth,
        height: `calc(${imgWidth} * 1.25)`,
        display: 'flex',
        fontSize: '1.5rem',
        alignItems: 'center',
        justifyContent: 'center'
    };

    return (
        <div className={`element ${className}`}>
            {!!label && <span className="label">{label}</span>}
            {
                !value &&
                <div
                    className='preview-container'
                    style={style}
                >
                    Нет фото
                </div>
            }
            {
                !!value &&
                <img src={value} style={{width: imgWidth}}/>
            }
        </div>
    );
};

Image.propTypes = {
    label: PropTypes.string,
    input: PropTypes.object.isRequired,
    className: PropTypes.string,
    imgWidth: PropTypes.string
};

Image.defaultProps = {
    label: '',
    imgWidth: '150px'
};

export default Image;