import React from 'react';
import PropTypes from 'prop-types';
import {
    dateDiffMonths,
    humanDateNoDays, jsUcFirst,
    parseDateFromRuFormat,
    pluralRu,
    resolveObjectPath
} from "../../../helper/utils";

const DateRangeText = (props) => {
    const {
        className,
        mapping: {dateFrom: dateFromName, dateTo: dateToName},
    } = props;

    const dateFromInput = (resolveObjectPath(dateFromName, props) || {}).input;
    const dateToInput = (resolveObjectPath(dateToName, props) || {}).input;

    let years;
    let months;

    let dateFromObject;
    let dateToObject;

    if (dateFromInput && dateToInput) {
        let dateFromValue = dateFromInput.value;
        let dateToValue = dateToInput.value;

        dateFromObject = parseDateFromRuFormat(dateFromValue);
        dateToObject = dateToValue ? parseDateFromRuFormat(dateToValue) : new Date();

        months = dateDiffMonths(dateFromObject, dateToObject);
        years = Math.floor(months / 12);
        months = months % 12;
    }

    const totalYears = years ? `${years} ${pluralRu(years, 'год', 'года', 'лет')}` : '';
    const totalMonths = months ? `${months} ${pluralRu(months, 'месяц', 'месяца', 'месяцев')}` : '';

    let totalExp = totalYears;
    totalExp = totalExp ? `${totalExp} ` : totalExp;
    totalExp = `${totalExp}${totalMonths}`;

    let from = jsUcFirst(humanDateNoDays(dateFromObject));

    return (
        <div className={`element ${className}`}>
            <span className="date from">{from}</span>
            <span className="dash">&nbsp;&mdash;&nbsp;</span>
            <span className="date to">{dateToInput.value ? humanDateNoDays(dateToObject) : 'по настоящее время'}</span>
            <span className="text-secondary">{totalExp}</span>
        </div>
    );
};

DateRangeText.propTypes = {
    dateFrom: PropTypes.string,
    dateTo: PropTypes.string,
    className: PropTypes.string,
    mapping: PropTypes.shape({dateFrom: PropTypes.string, dateTo: PropTypes.string}).isRequired,
};

DateRangeText.defaultProps = {
    className: ''
};

export default DateRangeText;