import React from 'react';
import PropTypes from "prop-types";
import TreeSelect from '../TreeSelect';
import {asArray} from "../../helper/utils";

const TreeSelectElement = (props) => {
    const {
        input: {name},
        className,
        label,
        multiSelect,
        onUpdateSelection,
        resource,
        fetchTree,
        actionCreator,
        data,
        selectedValues,
        meta: {touched, error}
    } = props;

    let errors = null;

    if (error) {
        errors = asArray(error);
    }

    return (
        <div className="element">
            {!!label && <span className="label">{label}</span>}
            <TreeSelect
                className={className}
                name={name}
                multiSelect={multiSelect}
                onUpdateSelection={onUpdateSelection}
                resource={resource}
                fetchTree={fetchTree}
                actionCreator={actionCreator}
                data={data}
                selectedValues={selectedValues}
                allowType={false}
            />
            {touched && errors && <span className="field-error">{errors}</span>}
        </div>
    );
};

TreeSelectElement.propTypes = {
    multiSelect: PropTypes.bool,
    selectedValues: PropTypes.array,
    onUpdateSelection: PropTypes.func.isRequired,
    maxHeight: PropTypes.string,
    fetchTree: PropTypes.func.isRequired,
    actionCreator: PropTypes.func.isRequired,
    label: PropTypes.string,
    resource: PropTypes.string.isRequired,
    data: PropTypes.object
};

TreeSelectElement.defaultProps = {
    multiSelect: true,
    selectedValues: [],
    data: {},
    label: ''
};

export default TreeSelectElement;