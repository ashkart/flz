import React from 'react';
import DatePicker from 'react-date-picker';
import {asArray, parseDateFromRuFormat} from "../../helper/utils";

export default class DateInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            date: null
        };

        this.onChange = this.onChange.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.input && nextProps.input.value) {
            let date = parseDateFromRuFormat(nextProps.input.value);

            this.setState({date: date});
        }
    }

    onChange(date) {
        this.setState({date});
        this.props.input.onChange(date ? date : '');
    }

    render() {
        const {
            className,
            name,
            label,
            input: {value},
            meta: {touched, error}
        } = this.props;

        let errors = null;

        if (error) {
            errors = asArray(error);
        }

        let dateValue = this.state.date;

        if (value) {
            dateValue = parseDateFromRuFormat(value);
        }

        return (
            <div className={`element`}>
                <div className="date-input-container">
                    {!!label && <span className="label">{label}</span>}
                    <DatePicker
                        className={`date-picker ${touched && errors ? 'with-error' : ''}`}
                        showLeadingZeros={true}
                        locale='ru'
                        name={name}
                        onChange={this.onChange}
                        value={dateValue}
                    />
                </div>
                {touched && errors && <span className="field-error">{errors}</span>}
            </div>
        );
    }
}