import React from 'react';
import PropTypes from 'prop-types';

export default class Spoiler extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.toggleOpen = this.toggleOpen.bind(this);
    }

    toggleOpen() {
        this.setState(
            prevState => ({isOpen: !this.state.isOpen}),
            () => {
                if (this.state.isOpen) {
                    this.props.onOpen();
                }
            }
        );
    }

    render() {
        const {
            label,
            bodyId,
            onOpen,
            children
        } = this.props;

        const stateIconClass = `fas fa-${this.state.isOpen ? 'caret-down' : 'caret-right'}`;

        return (
            <div className="spoiler">
                <span className="spoiler-label" onClick={this.toggleOpen}>
                    <span className={stateIconClass}></span>&nbsp;{label}
                </span>
                <div className="spoiler-body" style={{display: this.state.isOpen ? 'flex' : 'none' }}>
                    {children}
                </div>
            </div>
        );
    }
}

Spoiler.propTypes = {
    label: PropTypes.string.isRequired,
    onOpen: PropTypes.func
};

Spoiler.defaultProps = {
    onOpen: () => {}
};