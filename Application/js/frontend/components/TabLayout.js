import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Tab from "./TabLayout/Tab";

class TabLayout extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeChildId: this.props.defaultActiveId
        };

        this.switchTab = this.switchTab.bind(this);
    }

    switchTab(childId) {
        this.setState(prevState => ({
            activeChildId: childId
        }));
    }

    render() {
        return (
            <div className="tab-control">
                <ul className="tabs">
                    {
                        React.Children.map(this.props.children, child => {
                            if (!child) {
                                return;
                            }

                            let classNameActive = this.state.activeChildId === child.props.id ? 'active' : '';

                            return React.cloneElement(child, {
                                className: `tab ${classNameActive}`,
                                key: child.props.id,
                                onClick: () => {this.switchTab(child.props.id)}
                            });
                        })
                    }
                </ul>
                <div className="tabs-content-container">
                    {
                        React.Children.map(this.props.children, child => {
                            if (!child) {
                                return;
                            }

                            if (this.state.activeChildId !== child.props.id) {
                                return '';
                            }

                            let className = `${child.props.className || ''} tab-content`;

                            const tabContent = child.props.children;

                            return React.cloneElement(tabContent, {
                                ...tabContent.props,
                                className
                            });
                        })
                    }
                </div>
            </div>
        );
    }
}

TabLayout.propTypes = {
    defaultActiveId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    children: function(values, key, componentName) {
        let children = values.children;

        if (!Array.isArray(children)) {
            children = [children];
        }

        for (let i in children) {
            if (!children[i] instanceof Tab) {
                return new Error(
                    `The children[${i}] type is invalid. Instance of Tab required.`
                );
            }
        }
    }
};

export default connect((state, props) => {
    return { state: state, ...props };
})(TabLayout);