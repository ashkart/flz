import React from "react";
import PropTypes from 'prop-types';

import {suggestAction} from "../actions/actionCreators/dataFetchActions";
import DropdownSelect from "./controlled/DropdownSelect";

export default class SuggestInput extends React.Component {
    constructor(props) {
        super(props);

        this.onSelectionUpdated = this.onSelectionUpdated.bind(this);
        this.onType = this.onType.bind(this);
    }

    onSelectionUpdated(selectedItems) {
        this.props.onUpdateSelection(selectedItems);
    }

    onType(newValue) {
        const {
            name,
            resource,
            fetchSuggestions
        } = this.props;

        if (typeof newValue === 'string' && newValue.length > 1) {
            fetchSuggestions(suggestAction(resource, {
                name,
                queryString: newValue
            }));
        }
    }

    renderSuggestionItem(suggestion) {
        return <div className="suggestion">{suggestion.title}</div>
    }

    render() {
        const {
            inputProps,
            name,
            renderSuggestionItem = this.renderSuggestionItem,
            label,
            multiSelect,
            isStrict,
            suggestions,
            selectedValues,
            className
        } = this.props;

        return (
            <div className={`${className} ${name}`}>
                {!!label && <span className="label">{label}</span>}
                <DropdownSelect
                    selectedValues={selectedValues}
                    multiSelect={multiSelect}
                    isStrict={isStrict}
                    selectId={name}
                    options={suggestions}
                    onType={this.onType}
                    inputProps={{
                        type: "text",
                        className: `suggest-input ${name}-input`,
                        ...inputProps
                    }}
                    renderOption={renderSuggestionItem}
                    onSelectionUpdated={this.onSelectionUpdated}
                    optionsContainerClassName={`${name}-suggest-data-container`}
                    clearBtnIcon='fas fa-backspace'
                    clearBtnTitle="Очистить"
                    className={`${name}-select`}
                />
            </div>
        );
    }
}

SuggestInput.propTypes = {
    /*
    * Только предложенные значения?
    */
    isStrict: PropTypes.bool,

    multiSelect: PropTypes.bool,
    name: PropTypes.string.isRequired,
    onFocusOut: PropTypes.func,
    selectedValues: PropTypes.object,
    onUpdateSelection: PropTypes.func,
    label: PropTypes.string,
    suggestions: PropTypes.object,
    inputProps: PropTypes.object,
    fetchSuggestions: PropTypes.func,
    resource: PropTypes.string.isRequired,
};

SuggestInput.defaultProps = {
    onUpdateSelection: (selectedItems) => {},
    fetchSuggestions: (action) => {},
    isStrict: true,
    multiSelect: false,
    selectedValues: {},
    suggestions: {},
    inputProps: {placeholder: ''}
};