import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {Link} from "react-router-dom";
import Form from "./Form";
import {renderInput} from "../FormElements/renderElement";

class FormLogin extends React.Component {
    render() {
        const {
            handleSubmit,
            error,
            reset,
            submitting
        } = this.props;

        return (
            <Form
                action="/auth/login"
                method="POST"
                className="flex-container auth-form"
                handleSubmit={handleSubmit}
                errorText={"Неверный логин или пароль"}
            >
                <Field
                    className="flex-item input"
                    placeholder="Email"
                    name="login"
                    component={renderInput}
                    type="email"
                />
                <Field
                    className="flex-item input"
                    placeholder="Password"
                    name="password"
                    component={renderInput}
                    type="password"
                />

                {error && <strong className="form-error">{error}</strong>}

                <div className="links">
                    <nobr>Нет аккаунта? <Link to="/auth/register">Регистрация</Link></nobr>
                    <nobr>Забыли пароль? <Link to="/auth/password-change">Сброс пароля</Link></nobr>
                </div>
                <button type="submit" className="submit flex-item form-control btn btn-primary">
                    Вход
                </button>
            </Form>
        );
    }
}

export default reduxForm({
    form: 'formLogin'
})(FormLogin);