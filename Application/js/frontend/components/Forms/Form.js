import React from 'react';
import composeForm from "../hoc/composeForm";

function Form(props) {
    const {
        children,
        ...rest
    } = props;

    return (
        <form {...rest}>
            {
                props.children
            }
        </form>
    );
}

export default composeForm(Form);