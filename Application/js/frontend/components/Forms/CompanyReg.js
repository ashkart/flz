import React from 'react';
import { Field, reduxForm } from 'redux-form';
import Form from "./Form";
import {renderCheckbox, renderInput, renderSelect, renderTextEditor} from "../FormElements/renderElement";
import {OPF_LIST} from "../../helper/constants";
import { change } from 'redux-form';
import {validate} from "../../helper/regFormValidate";

const FORM_ID = 'companyReg';

class CompanyReg extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(change(FORM_ID, 'type', 'company'));
        this.props.dispatch(change(FORM_ID, 'opf', "ООО"));

        this.onOpfChange = this.onOpfChange.bind(this);
    }

    onOpfChange(e) {
        this.props.dispatch(change(FORM_ID, e.target.name, e.target.value));
    }

    render() {
        const {
            className,
            handleSubmit,
            error,
            reset,
            submitting
        } = this.props;

        return (
            <Form
                action="/auth/register"
                method="POST"
                className={`flex-container auth-form ${className}`}
                handleSubmit={handleSubmit}
                errorText={"Ошибка регистрации"}
            >
                <Field
                    name="type"
                    component="input"
                    type="hidden"
                />
                <Field
                    className="flex-item input"
                    placeholder="Имя пользователя"
                    name="user_name"
                    component={renderInput}
                    type="text"
                />
                <Field
                    className="flex-item input"
                    placeholder="Email"
                    name="login"
                    component={renderInput}
                    type="email"
                />
                <Field
                    className="flex-item input"
                    placeholder="Пароль"
                    name="password"
                    component={renderInput}
                    type="password"
                />
                <Field
                    className="flex-item input"
                    placeholder="Повтор пароля"
                    name="password_repeat"
                    component={renderInput}
                    type="password"
                />

                <span className="sub-header">Компания</span>
                <div className="inline flex-item" style={{marginBottom: 0}}>
                    <Field
                        className="flex-item select"
                        name="opf"
                        options={OPF_LIST}
                        component={renderSelect}
                        onChange={this.onOpfChange}
                        renderErrors={false}
                    />
                    <Field
                        className="flex-item input"
                        name="company"
                        placeholder="Название"
                        component={renderInput}
                        renderErrors={false}
                    />
                </div>
                <Field
                    className="flex-item input"
                    name="company"
                    component={renderInput}
                    type="hidden"
                />
                <Field
                    className="flex-item textarea"
                    name="company_description"
                    label="О компании"
                    component={renderTextEditor}
                    type="text"
                />

                <span className="sub-header">Контактное лицо</span>

                <Field
                    className="flex-item input"
                    placeholder="Должность"
                    name="position"
                    component={renderInput}
                    type="text"
                />
                <Field
                    className="flex-item input"
                    placeholder="Фамилия"
                    name="last_name"
                    component={renderInput}
                    type="text"
                />
                <Field
                    className="flex-item input"
                    placeholder="Имя"
                    name="first_name"
                    component={renderInput}
                    type="text"
                />
                <Field
                    className="flex-item input"
                    placeholder="Отчество"
                    name="middle_name"
                    component={renderInput}
                    type="text"
                />
                <Field
                    className="flex-item checkbox"
                    label={
                        <span>
                            Согласен с условиями <a href="/doc/conditions.pdf" target="_blank">
                                Пользовательского соглашения
                            </a> и <a href="/doc/confidential-policy.pdf" target="_blank">
                                Политикой конфиденциальности
                            </a>
                        </span>
                    }
                    name="agree"
                    component={renderCheckbox}
                />

                {/**@TODO phone fieldset collection*/}

                {error && <strong className="form-error">{error}</strong>}

                <button type="submit" className="submit flex-item form-control btn btn-primary">
                    Зарегистрироваться
                </button>
            </Form>
        );
    }
}

export default reduxForm({
    form: FORM_ID,
    validate
})(CompanyReg);