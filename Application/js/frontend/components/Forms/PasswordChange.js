import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import Form from "./Form";
import {renderInput} from "../FormElements/renderElement";

const FORM_ID = 'passwordChange';

const validate = values => {
    const errors = {};

    if (!values.password_old) {
        errors.password_old = 'Введите текущий пароль.';
    }

    if (!values.password_new) {
        errors.password_new = 'Введите новый пароль.';
    }

    if (!values.password_repeat) {
        errors.password_repeat = 'Повторите новый пароль.';
    }

    if (values.password_new !== values.password_repeat) {
        errors.password_repeat = 'Повторный ввод нового пароля не совпадает.';
    }

    return errors;
};

class PasswordChange extends React.Component {
    render() {
        const {
            formCaption,
            handleSubmit,
            error,
            reset,
            onSuccess,
            submitting
        } = this.props;

        return (
            <Form
                action="/profile/update-password"
                method="POST"
                className="flex-container form password-change"
                handleSubmit={handleSubmit}
                errorText={"Ошибка сохранения"}
                onSuccess={onSuccess}
            >
                {!!formCaption && <h2>{formCaption}</h2>}
                <div className="row">
                    <Field
                        className="flex-item input"
                        label="Текущий пароль"
                        name="password_old"
                        component={renderInput}
                        type="password"
                    />
                </div>
                <div className="row">
                    <Field
                        className="flex-item input"
                        label="Новый пароль"
                        name="password_new"
                        component={renderInput}
                        type="password"
                    />
                </div>
                <div className="row">
                    <Field
                        className="flex-item input"
                        label="Повторно новый пароль"
                        name="password_repeat"
                        component={renderInput}
                        type="password"
                    />
                </div>

                {error && <strong className="form-error">{error}</strong>}

                <div className="row form-buttons">
                    <button type="button" name="cancel" data-modal-close>Отмена</button>
                    <button type="submit" name="submit">Сохранить</button>
                </div>
            </Form>
        );
    }
}

PasswordChange.propTypes = {
    onSuccess: PropTypes.func,
    formCaption: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    error: PropTypes.string,
    reset: PropTypes.func,
    submitting: PropTypes.bool
};

PasswordChange.defaultProps = {
    onSuccess: () => {},
};

export default reduxForm({
    form: FORM_ID,
    validate
})(PasswordChange);