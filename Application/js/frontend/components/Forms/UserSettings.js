import React from 'react';
import PropTypes from 'prop-types';
import { Field, Fields, reduxForm, formValueSelector } from 'redux-form';
import {connect} from 'react-redux';
import {change} from 'redux-form';
import {
    renderInput,
    renderSuggestInput
} from "../FormElements/renderElement";
import formWithDictionary from "../hoc/formWithDictionary";
import {fetchOneAction} from "../../actions/actionCreators/dataFetchActions";
import SingleField from "./SingleField";
import ButtonOpenModal from "../ButtonOpenModal";
import PasswordChange from "./PasswordChange";
import CityChange, {FORM_ID as FORM_CITY_CHANGE} from "./CityChange";
import Text from "../FormElements/view/Text";
import {PASSWORD_CHANGE_INITIATED} from "../../helper/constants";
import Notification from "../Notification";
import {arrayToObject} from "../../helper/utils";

const EL_EMAIL     = 'email';
const EL_USER_NAME = 'user_name';
const EL_CITY_ID   = 'city_id';
const EL_LOCATION  = 'geo_location';
const EL_CITY      = 'city';
const EL_REGION    = 'region';
const EL_COUNTRY   = 'country';

export const FORM_ID = 'userSettings';

const emptyFormState = {
    [EL_EMAIL]: '',
    [EL_USER_NAME]: '',
    [EL_LOCATION]: {
        [EL_CITY]: '',
        [EL_REGION]: '',
        [EL_COUNTRY]: ''
    },
};

const PROFILE_VIEW = 'profile/view';

class UserSettings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            passwordChangeMessage: null
        };

        this.loadFormData = this.loadFormData.bind(this);
        this.onPasswordChangeInitiated = this.onPasswordChangeInitiated.bind(this);
        this._renderChangeCity = this._renderChangeCity.bind(this);
    }

    componentWillMount() {
        this.loadFormData();
    }

    onSelectChange(e) {
        this.props.dispatch(change(FORM_ID, e.target.name, e.target.value));
    }

    loadFormData() {
        this.props.dispatch(fetchOneAction(PROFILE_VIEW));
    }

    onProfileUpdated() {
        document.location.reload(true);
    }

    onPasswordChangeInitiated() {
        this.setState(prevState => ({passwordChangeMessage: PASSWORD_CHANGE_INITIATED}));
    }

    _renderChangeCity(props) {
        const {
            city,
            onSuggestSelect,
            onCitySelect,
            dictionary,
            fetchComponentData,
            formCustomValues
        } = this.props;

        return (
            <div className="row">
                {
                    !!city.id &&
                    <CityChange
                        id={FORM_CITY_CHANGE}
                        fieldLabel="Город"
                        formCaption="Изменить город по умолчанию"
                        onSuccess={this.onProfileUpdated}
                        userCityInitial={city}
                        {...props}
                    />
                }
            </div>
        );
    }

    _renderChangeUserName(props) {
        return (
            <div className="row">
                <SingleField
                    formCaption="Изменить имя пользователя"
                    action="/profile/update-username"
                    className="input"
                    label="Новое имя пользователя"
                    fieldComponent={renderInput}
                    name={EL_USER_NAME}
                    {...props}
                />
            </div>
        );
    }

    _renderChangeEmail(props) {
        return (
            <div className="row">
                <SingleField
                    formCaption="Изменить email"
                    action="/profile/update-email"
                    className="input"
                    label="Новый email"
                    fieldComponent={renderInput}
                    name={EL_EMAIL}
                    {...props}
                />
            </div>
        );
    }

    _renderField(value, label, modalContent, className, modalId, modalMainContainerId) {
        return (
            <div id={modalMainContainerId} className={className}>
                <span className="label">{label}</span>
                <div className="row">
                    <div className={`element`}>
                        <div className="text">
                            {value}
                        </div>
                    </div>
                    <ButtonOpenModal
                        modalContent={modalContent}
                        modalId={modalId}
                        modalMainContainerSelector={`#${modalMainContainerId}`}
                        closeModalComponentProp="onSuccess"
                        onConfirmSuccess={this.onProfileUpdated}
                    >
                        <span className="fas fa-pen">&nbsp;</span>
                        Изменить
                    </ButtonOpenModal>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="form-settings">
                <Field
                    component={Text}
                    label="Email"
                    name="email"
                />

                {
                    this._renderField(
                        this.props.userName,
                        "Имя пользователя",
                        this._renderChangeUserName,
                        'column',
                        'userNameChangeModal',
                        'userNameChange'
                    )
                }

                {
                    this._renderField(
                        this.props.geoLocation,
                        "Город",
                        this._renderChangeCity,
                        'column',
                        'geoLocationChangeModal',
                        'geoLocationChange'
                    )
                }

                <div id="changePassword" className="row">
                    <ButtonOpenModal
                        formCaption="Изменить пароль"
                        modalContent={PasswordChange}
                        modalId="changePasswordModal"
                        modalMainContainerSelector={`#changePassword`}
                        closeModalComponentProp="onSuccess"
                        onConfirmSuccess={this.onPasswordChangeInitiated}
                    >
                        <span className="fas fa-pen">&nbsp;</span>
                        Сменить пароль
                    </ButtonOpenModal>
                </div>
                {
                    !!this.state.passwordChangeMessage &&
                    <Notification message={this.state.passwordChangeMessage}/>
                }
            </div>
        );
    }
}

UserSettings.propTypes = {
    city: PropTypes.object
};

UserSettings.defaultProps = {
    city: {}
};

const UserSettingsReduxForm = reduxForm({
    form: FORM_ID,
    enableReinitialize: true
})(formWithDictionary(UserSettings));

const selector = formValueSelector(FORM_ID);

export default connect((state, props) => {
    const userName = selector(state, EL_USER_NAME);
    const email = selector(state, EL_EMAIL);

    const city = selector(state, 'geo_location.city');
    const region = selector(state, 'geo_location.region');
    const country = selector(state, 'geo_location.country');

    let geoLocation = '';

    if (city) {
        geoLocation = city.title;

        if (region) {
            geoLocation = `${geoLocation}, ${region}`;

            if (country) {
                geoLocation = `${geoLocation}, ${country}`;
            }
        }
    }

    return {
        state,
        ...props,
        initialValues: state.data[PROFILE_VIEW] || emptyFormState,
        userName,
        email,
        geoLocation,
        city: city || {}
    };
})(UserSettingsReduxForm);