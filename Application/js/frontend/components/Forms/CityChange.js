import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from "./Form";
import {renderGeoSuggestion, renderSuggestInput} from "../FormElements/renderElement";
import {arrayToObject, asArray} from "../../helper/utils";
import formWithDictionary from "../hoc/formWithDictionary";

export const FORM_ID = 'cityChange';

class CityChange extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            city: arrayToObject([this.props.userCityInitial])
        };

        this.onChange = this.onChange.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
    }

    componentWillMount() {
        this.onChange(this.state.city);
    }

    onChange(selected) {
        this.setState(
            prevState => ({city: selected}),
            () => {this.props.onCitySelect(selected)}
        );
    }

    onSuccess() {
        this.props.onSuccess();
        this.props.onCityUpdate(asArray(this.state.city)[0].id);
    }

    render() {
        const {
            formCaption,
            fieldLabel,
            handleSubmit,
            error,
            reset,
            submitting,
            onSuccess,
            fetchComponentData,
            suggestions,
            isCompactView
        } = this.props;

        return (
            <Form
                action="/profile/update-city"
                method="POST"
                className="flex-container form city-change"
                handleSubmit={handleSubmit}
                errorText={"Ошибка сохранения"}
                onSuccess={this.onSuccess}
            >
                {!!formCaption && <h2>{formCaption}</h2>}
                <div className="row">
                    <Field
                        className="suggest-input"
                        label={fieldLabel}
                        name="city_id"
                        multiSelect={false}
                        fetchSuggestions={fetchComponentData}
                        resource={'suggest/city'}
                        onUpdateSelection={this.onChange}
                        suggestions={suggestions}
                        selectedValues={this.state.city}
                        component={renderSuggestInput}
                        renderSuggestionItem={renderGeoSuggestion}
                    />
                </div>

                {error && <strong className="form-error">{error}</strong>}

                {
                    !isCompactView &&
                    <div className="row form-buttons">
                        <button type="button" name="cancel" data-modal-close>Отмена</button>
                        <button type="submit" name="submit">Сохранить</button>
                    </div>
                }
                {
                    isCompactView &&
                    <button type="submit" name="submit" className="fas fa-check"></button>
                }
            </Form>
        );
    }
}

CityChange.propTypes = {
    formCaption: PropTypes.string,
    fieldLabel: PropTypes.string,
    userCityInitial: PropTypes.object,
    fetchComponentData: PropTypes.func.isRequired,
    suggestions: PropTypes.object,
    onCitySelect: PropTypes.func.isRequired,
    selectedValues: PropTypes.object,
    onSuccess: PropTypes.func,
    isCompactView: PropTypes.bool,
    onCityUpdate: PropTypes.func
};

CityChange.defaultProps = {
    isCompactView: false,
    suggestions: {},
    selectedValues: {},
    onSuccess: () => {},
    onCityUpdate: () => {},
};

const reduxFormCityChange = reduxForm({
    form: FORM_ID
})(formWithDictionary(CityChange));

export default connect(
    (state, props) => {
        const dictionary = state.dictionary;
        const suggestions = dictionary.city_id ? dictionary.city_id.suggestions : {};
        return {state, ...props, suggestions};
    }
)(reduxFormCityChange);