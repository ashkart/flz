import React from 'react';
import PropTypes from 'prop-types';
import { Field, Fields, FieldArray, reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import {change} from 'redux-form';
import {fetchOneAction, fetchCurrenciesAction, fetchLanguagesAction} from "../../actions/actionCreators/dataFetchActions";
import Form from "./Form";
import {
    renderCheckbox,
    renderGeoSuggestion,
    renderInput,
    renderSelect,
    renderSuggestInput,
    renderTextEditor
} from "../FormElements/renderElement";
import {asArray, normalizeDate} from "../../helper/utils";
import DateInput from "../FormElements/DateInput";
import {
    COOKIE_ROLE,
    EDUCATION,
    EDUCATION_SECONDARY_VOCATIONAL, EL_VISIBILITY,
    EMPLOYMENT_LIST,
    GENDER,
    GENDER_MALE,
    LANG_LEVEL_BASE,
    LANG_LEVELS,
    ROLE_GUEST,
    VISIBILITY_ALL,
    VISIBILITY_NONE
} from "../../helper/constants";
import Collection from "../Collection";
import formWithDictionary from "../hoc/formWithDictionary";
import ImageInput from "../FormElements/ImageInput";
import PaycheckInput from "../FormElements/PaycheckInput";
import {withCookies} from "react-cookie";
import {Redirect} from "react-router-dom";

export const EL_PHOTO = 'photo';
export const EL_PHOTO_PATH = 'photo_path';
export const EL_PHOTO_DATA = 'photo_data';
export const EL_POSITION_NAME = 'position_name';
export const EL_CITY_ID = 'city_id';
export const EL_FIRST_NAME = 'first_name';
export const EL_MIDDLE_NAME = 'middle_name';
export const EL_LAST_NAME = 'last_name';
export const EL_BIRTH_DATE = 'birth_date';
export const EL_GENDER = 'gender';
export const EL_IS_RELOCATE_READY = 'is_relocate_ready';
export const EL_IS_REMOTE = 'is_remote';
export const EL_EMPLOYMENT = 'employment';
export const EL_IS_MARRIED = 'is_married';
export const EL_HAS_CHILDREN = 'has_children';
export const EL_TAGS = 'tags';
export const EL_EDUCATION_LEVEL = 'education_level';
export const EL_AUTODESCRIPTION = 'autodescription';
export const EL_DESIRED_PAYCHECK = 'desired_paycheck';
export const EL_CURRENCY = 'currency_id';

export const EL_ID = 'id';

export const EL_CONTACT_COLLECTION = 'contacts';
export const EL_TYPE = 'type';
export const EL_VALUE = 'value';

export const EL_EDUCATION_COLLECTION = 'education';
export const EL_SPECIALTY = 'speciality';
export const EL_EDUCATIONAL_ORGANIZATION = 'educational_organization';
export const EL_DATE_START = 'date_start';
export const EL_DATE_END = 'date_end';

export const EL_WORK_EXPERIENCE_COLLECTION = 'work_experience';
export const EL_DESCRIPTION = 'description';

export const EL_LANGUAGES_COLLECTION = 'languages';
export const EL_CODE = 'language_code';
export const EL_LEVEL = 'level';

const emptyFormState = {
    [EL_PHOTO_PATH]: null,
    [EL_PHOTO]: null,
    [EL_CITY_ID]: null,
    [EL_POSITION_NAME]: '',
    [EL_DESIRED_PAYCHECK]: '',
    [EL_CURRENCY]: 1,
    [EL_FIRST_NAME]: '',
    [EL_MIDDLE_NAME]: '',
    [EL_LAST_NAME]: '',
    [EL_BIRTH_DATE]: null,
    [EL_GENDER]: GENDER_MALE,
    [EL_IS_RELOCATE_READY]: false,
    [EL_IS_MARRIED]: null,
    [EL_HAS_CHILDREN]: null,
    [EL_EDUCATION_LEVEL]: EDUCATION_SECONDARY_VOCATIONAL,
    [EL_AUTODESCRIPTION]: '',
    [EL_CONTACT_COLLECTION]: [],
    [EL_EDUCATION_COLLECTION]: [],
    [EL_WORK_EXPERIENCE_COLLECTION]: [],
    [EL_LANGUAGES_COLLECTION]: [],
    [EL_TAGS]: {},
    [EL_VISIBILITY]: VISIBILITY_ALL
};

export const FORM_ID = 'resumeEdit';

const RESUME_EDIT = 'resume/edit';

class ResumeEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dictionary: {
                languages: {}
            },

            values: {
                [EL_CITY_ID]: {},
                [EL_LANGUAGES_COLLECTION]: [],
                [EL_TAGS]: {},
                [EL_CONTACT_COLLECTION]: []
            }
        };

        if (props.resumeId) {
            this.loadFormData();
        }

        this.onSelectChange = this.onSelectChange.bind(this);
        this.onPhotoSelect = this.onPhotoSelect.bind(this);
        this.loadFormData = this.loadFormData.bind(this);
        this._renderLanguageCollection = this._renderLanguageCollection.bind(this);
        this._renderContactCollection = this._renderContactCollection.bind(this);

        this.props.dispatch(fetchCurrenciesAction());
        this.props.dispatch(fetchLanguagesAction());
    }

    componentWillReceiveProps(nextProps) {
        let values = nextProps.formCustomValues;

        this.setState(prevState => ({values}));
    }

    loadFormData() {
        this.props.dispatch(fetchOneAction(RESUME_EDIT, {id: this.props.resumeId}));
    }

    onSelectChange(e) {
        this.props.dispatch(change(FORM_ID, e.target.name, e.target.value));
    }

    onPhotoSelect(imageB64) {
        this.props.dispatch(change(FORM_ID, EL_PHOTO, imageB64));
        this.props.dispatch(change(FORM_ID, EL_PHOTO_PATH, null));
    }

    _renderLanguageCollection({fields, label, meta}) {
        return (
            <Collection
                className="languages"
                label={label}
                name={EL_LANGUAGES_COLLECTION}
                fields={fields}
                isEditMode={true}
                onSpawnFieldset={() => {
                    this.props.dispatch(change(FORM_ID, `${EL_LANGUAGES_COLLECTION}[${fields.length}].${EL_CODE}`, 'en'));
                    this.props.dispatch(change(FORM_ID, `${EL_LANGUAGES_COLLECTION}[${fields.length}].${EL_LEVEL}`, LANG_LEVEL_BASE));
                }}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="select"
                    label="Язык"
                    name={EL_CODE}
                    component={renderSelect}
                    onChange={this.onSelectChange}
                    options={this.props.createSelectOptions(this.props.languages)}
                />

                <Field
                    className="select"
                    label="Уровень владения"
                    name={EL_LEVEL}
                    component={renderSelect}
                    onChange={this.onSelectChange}
                    options={LANG_LEVELS}
                />
            </Collection>
        );
    }

    _renderContactCollection({fields, label, meta}) {
        return (
            <Collection
                className="contacts"
                label={label}
                name={EL_CONTACT_COLLECTION}
                fields={fields}
                isEditMode={true}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="input"
                    label="Тип связи"
                    placeholder="Например email или skype"
                    name={EL_TYPE}
                    component={renderInput}
                />

                <Field
                    className="input"
                    label="Контакт"
                    placeholder="Контакт"
                    name={EL_VALUE}
                    component={renderInput}
                />
            </Collection>
        );
    }

    _renderEducationCollection({fields, meta}) {
        return (
            <Collection
                name={EL_EDUCATION_COLLECTION}
                fields={fields}
                isEditMode={true}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="input"
                    wrapClassName="row"
                    label="Специальность или название курсов"
                    placeholder="Специальность или название курсов"
                    name={EL_SPECIALTY}
                    component={renderInput}
                />

                <Field
                    className="input"
                    wrapClassName="row"
                    label="Учебное заведение"
                    placeholder="Учебное заведение"
                    name={EL_EDUCATIONAL_ORGANIZATION}
                    component={renderInput}
                />

                <Field
                    className="date"
                    label="Дата начала"
                    name={EL_DATE_START}
                    component={DateInput}
                    normalize={normalizeDate}
                />
                <Field
                    className="date"
                    label="Дата окончания"
                    name={EL_DATE_END}
                    component={DateInput}
                    normalize={normalizeDate}
                />
            </Collection>
        );
    }

    _renderWorkExpCollection({fields, label, meta}) {
        return (
            <Collection
                label={label}
                name={EL_WORK_EXPERIENCE_COLLECTION}
                fields={fields}
                isEditMode={true}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="input"
                    wrapClassName="row"
                    label="Название должности"
                    placeholder="Название должности"
                    name={EL_POSITION_NAME}
                    component={renderInput}
                />

                <Field
                    className="textarea vertical"
                    name={EL_DESCRIPTION}
                    label="Описание"
                    placeholder="Описание"
                    component={renderTextEditor}
                    type="text"
                />

                <Field
                    className="date"
                    label="Дата начала"
                    name={EL_DATE_START}
                    component={DateInput}
                    normalize={normalizeDate}
                />
                <Field
                    className="date"
                    label="Дата окончания"
                    name={EL_DATE_END}
                    component={DateInput}
                    normalize={normalizeDate}
                />
            </Collection>
        );
    }

    render() {
        const {
            cookies,
            className,
            handleSubmit,
            error,
            reset,
            submitting,
            languages,
            currencies,
            onCitySelect,
            onTagSelect,
            dictionary,
            fetchComponentData,
            dispatch,
            formCustomValues,
            resumeId,
            createSelectOptions
        } = this.props;

        let action = "/resume/save";
        let request = 'resume/edit';

        if (resumeId) {
            action = `${action}/id-${resumeId}`;
            request = `${request}/id-${request}`;
        }

        if (cookies.get(COOKIE_ROLE) === ROLE_GUEST) {
            return <Redirect to={`/auth?request=${encodeURIComponent(request)}`} />;
        }

        return (
            <Form
                className="form form-resume edit"
                action={action}
                method="POST"
                errorText="Ошибка сохранения резюме"
                handleSubmit={handleSubmit}
            >
                <h2>Создать резюме</h2>

                <div className="form-block">
                    <span className="label">Основная информация</span>

                    <div className="row">
                        <div className="col photo">
                            <Fields
                                className="input image"
                                mapping={{imgData: EL_PHOTO_DATA, imgPath: EL_PHOTO_PATH}}
                                names={[EL_PHOTO_DATA, EL_PHOTO_PATH]}
                                component={ImageInput}
                                onImgChange={this.onPhotoSelect}
                            />
                        </div>

                        <div className="col main-info">
                            <div className="row">
                                <Field
                                    className="input"
                                    type="text"
                                    placeholder="Имя"
                                    label="Имя"
                                    name={EL_FIRST_NAME}
                                    component={renderInput}
                                />
                                <Field
                                    className="input"
                                    type="text"
                                    placeholder="Фамилия"
                                    label="Фамилия"
                                    name={EL_LAST_NAME}
                                    component={renderInput}
                                />
                            </div>
                            <div className="row">
                                <Field
                                    className="select"
                                    label="Пол"
                                    name={EL_GENDER}
                                    component={renderSelect}
                                    onChange={this.onSelectChange}
                                    options={GENDER}
                                />
                                <Field
                                    className="date"
                                    label="Дата рождения"
                                    name={EL_BIRTH_DATE}
                                    component={DateInput}
                                    normalize={normalizeDate}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <Field
                            className="suggest-input"
                            wrapClassName="element-city"
                            label="Город"
                            placeholder="Город"
                            name={EL_CITY_ID}
                            multiSelect={false}
                            fetchSuggestions={fetchComponentData}
                            resource={'suggest/city'}
                            onUpdateSelection={onCitySelect}
                            suggestions={dictionary.citySuggestions}
                            component={renderSuggestInput}
                            renderSuggestionItem={renderGeoSuggestion}
                            selectedValues={this.state.values[EL_CITY_ID]}
                        />
                    </div>
                    <div className="row">
                        <Field
                            className="checkbox"
                            label="Готов к переезду"
                            name={EL_IS_RELOCATE_READY}
                            component={renderCheckbox}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <div className="row">
                        <Field
                            className="select"
                            label="Видимость"
                            name={EL_VISIBILITY}
                            component={renderSelect}
                            onChange={this.onSelectChange}
                            options={[
                                {label: 'Все', value: VISIBILITY_ALL},
                                {label: 'Никто', value: VISIBILITY_NONE}
                            ]}
                        />
                    </div>
                </div>

                <div className="form-block collection-container">
                    <div className="row">
                        <FieldArray
                            label="Контакты"
                            name={EL_CONTACT_COLLECTION}
                            component={this._renderContactCollection}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <span className="label">Должность и зарплата</span>
                    <div className="row">
                        <Field
                            className="input"
                            type="text"
                            placeholder="Желаемая должность"
                            label="Название должности"
                            name={EL_POSITION_NAME}
                            component={renderInput}
                        />
                    </div>

                    <div className="row">
                        <Fields
                            label="Желаемая зарплата"
                            className="paycheck"
                            mapping={{currency: EL_CURRENCY, value: EL_DESIRED_PAYCHECK}}
                            names={[EL_CURRENCY, EL_DESIRED_PAYCHECK]}
                            component={PaycheckInput}
                            options={createSelectOptions(currencies)}
                            onSelect={this.onSelectChange}
                        />

                        <Field
                            className="select"
                            label="Предпочитаемая занятость"
                            name={EL_EMPLOYMENT}
                            component={renderSelect}
                            onChange={this.onSelectChange}
                            options={EMPLOYMENT_LIST}
                        />
                    </div>

                    <div className="row">
                        <Field
                            className="checkbox"
                            label="Ищу удаленную работу"
                            name={EL_IS_REMOTE}
                            component={renderCheckbox}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <span className="label">Ключевые навыки</span>
                    <div className="row">
                        <Field
                            className="suggest-input"
                            placeholder="Навыки, технологии, программные средства, которыми владеете"
                            name={EL_TAGS}
                            multiSelect={true}
                            isStrict={false}
                            fetchSuggestions={fetchComponentData}
                            resource={'suggest/tag'}
                            onUpdateSelection={onTagSelect}
                            suggestions={dictionary.tagSuggestions}
                            component={renderSuggestInput}
                            selectedValues={this.state.values[EL_TAGS]}
                        />
                    </div>
                </div>

                <div className="form-block collection-container">
                    <div className="row">
                        <FieldArray
                            label="Опыт работы"
                            name={EL_WORK_EXPERIENCE_COLLECTION}
                            component={this._renderWorkExpCollection}
                        />
                    </div>
                </div>

                <div className="form-block collection-container">
                    <div className="row">
                        <FieldArray
                            label="Знание языков"
                            name={EL_LANGUAGES_COLLECTION}
                            component={this._renderLanguageCollection}
                        />
                    </div>
                </div>

                <div className="form-block collection-container">
                    <span className="label form-padding-horizontal">Образование</span>

                    <div className="row form-padding-horizontal">
                        <Field
                            className="select"
                            label="Уровень образования"
                            name={EL_EDUCATION_LEVEL}
                            component={renderSelect}
                            onChange={this.onSelectChange}
                            options={EDUCATION}
                        />
                    </div>

                    <div className="row">
                        <FieldArray
                            name={EL_EDUCATION_COLLECTION}
                            component={this._renderEducationCollection}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <div className="row">
                        <Field
                            className="textarea vertical"
                            type="text"
                            label="Дополнительная информация"
                            description="Здесь можно указать дополнительную информацию о себе и оставить ссылки на прошлые проекты и примеры работ."
                            name={EL_AUTODESCRIPTION}
                            component={renderTextEditor}
                        />
                    </div>
                </div>

                <div className="row margin-bot20">
                    {error && <strong className="form-error">{error}</strong>}
                </div>

                <div className="row form-buttons">
                    <button type="submit" name="submit">Сохранить</button>
                </div>
            </Form>
        );
    }
}

ResumeEdit.propTypes = {
    currencies: PropTypes.object,
    languages: PropTypes.object,
    tags: PropTypes.object
};

ResumeEdit.defaultProps = {
    currencies: {},
    languages: {},
    tags: {}
};

const ResumeEditReduxForm = reduxForm({
    form: FORM_ID,
    enableReinitialize: true
})(formWithDictionary(ResumeEdit));

export default withCookies(connect((state, props) => {
    let langs = {};
    let currencies = {};
    let tags = {};

    if (state.dictionary) {
        if (state.dictionary.languages) {
            langs = state.dictionary.languages;
        }

        if (state.dictionary.currencies) {
            currencies = state.dictionary.currencies;
        }

        if (state.dictionary.tags) {
            tags = state.dictionary.tags;
        }
    }

    const resumeData = !!state.data[RESUME_EDIT] && !!state.data[RESUME_EDIT][props.resumeId]
        ? state.data[RESUME_EDIT][props.resumeId]
        : false
    ;

    return {
        state,
        ...props,
        languages: langs,
        currencies: currencies,
        tags: tags,
        initialValues: resumeData || emptyFormState
    };
})(ResumeEditReduxForm));