import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import Form from "./Form";
import connect from "react-redux/es/connect/connect";

const FORM_ID = 'singleFieldForm';

class SingleField extends React.Component {
    constructor(props) {
        super(props);

        this.onSuccess = this.onSuccess.bind(this);
    }

    onSuccess() {
        this.props.onSuccess();
        this.props.reset();
    }

    render() {
        const {
            action,
            label,
            formCaption,
            submitLabel,
            fieldComponent,
            handleSubmit,
            ...rest
        } = this.props;

        return (
            <Form
                className={`form single-field`}
                action={action}
                method="POST"
                errorText="Ошибка сохранения резюме"
                handleSubmit={handleSubmit}
                onSuccess={this.onSuccess}
            >
                {!!formCaption && <h2>{formCaption}</h2>}
                {!!label && <span className="label">{label}</span>}
                <div className="flex-container">
                    <Field
                        component={fieldComponent}
                        {...rest}
                    />
                </div>

                <div className="row form-buttons">
                    <button type="button" name="cancel" data-modal-close>Отмена</button>
                    <button type="submit" name="submit">Сохранить</button>
                </div>
            </Form>
        );
    }
}

SingleField.propTypes = {
    onSuccess: PropTypes.func,
    formCaption: PropTypes.string,
    className: PropTypes.string,
    action: PropTypes.string,
    submitLabel: PropTypes.string,
    fieldComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func, PropTypes.instanceOf(React.Component)])
};

SingleField.defaultProps = {
    onSuccess: () => {},
    formCaption: '',
    className: '',
    action: '',
    submitLabel: 'Сохранить',
};

const SingleFieldReduxForm = reduxForm({
    form: FORM_ID,
    enableReinitialize: true
})(SingleField);

export default connect((state, props) => {
    return {...props, state};
})(SingleFieldReduxForm);