import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import Form from "./Form";
import {renderInput} from "../FormElements/renderElement";
import {Link} from "react-router-dom";
import {PASSWORD_RESET_INITIATED} from "../../helper/constants";
import Notification from "../Notification";
import LoadingSpin from "../controlled/LoadingSpin";

const FORM_ID = 'passwordReset';

const validate = values => {
    const errors = {};

    if (!values.login) {
        errors.login = 'Введите зарегистрированный адрес электронной почты.';
    }

    if (!values.password_new) {
        errors.password_new = 'Введите новый пароль.';
    }

    if (!values.password_repeat) {
        errors.password_repeat = 'Повторите новый пароль.';
    }

    if (values.password_new !== values.password_repeat) {
        errors.password_repeat = 'Повторный ввод нового пароля не совпадает.';
    }

    return errors;
};

class PasswordReset extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            confirmationSent: null
        };

        this.onSuccess = this.onSuccess.bind(this);
    }

    onSuccess(arg) {
        this.props.onSuccess();

        this.setState(prevState => ({confirmationSent: PASSWORD_RESET_INITIATED}))
    }

    render() {
        const {
            className,
            formCaption,
            handleSubmit,
            error,
            reset,
            onSuccess,
            submitting
        } = this.props;

        return (
            <Form
                action="/profile/reset-password"
                method="POST"
                className="flex-container password-change auth-form"
                handleSubmit={handleSubmit}
                errorText={"Ошибка сохранения"}
                onSuccess={this.onSuccess}
            >
                {!!formCaption && <h2>{formCaption}</h2>}

                {
                    submitting &&
                    <div className="row element center">
                        <LoadingSpin/>
                    </div>
                }
                {
                    !!this.state.confirmationSent &&
                    <Notification message={this.state.confirmationSent}/>
                }

                <Field
                    className="flex-item input"
                    placeholder="Email"
                    name="email"
                    component={renderInput}
                    type="email"
                />
                <Field
                    className="flex-item input"
                    placeholder="Новый пароль"
                    name="password_new"
                    component={renderInput}
                    type="password"
                />
                <Field
                    className="flex-item input"
                    placeholder="Повторно новый пароль"
                    name="password_repeat"
                    component={renderInput}
                    type="password"
                />

                {error && <strong className="form-error">{error}</strong>}

                <div className="links">
                    <nobr><Link to="/auth/password-change">Вход</Link> | <Link to="/auth/register">Регистрация</Link></nobr>
                </div>
                <button type="submit" className="submit flex-item form-control btn btn-primary">
                    Отправить
                </button>
            </Form>
        );
    }
}

PasswordReset.propTypes = {
    className: PropTypes.string,
    onSuccess: PropTypes.func,
    formCaption: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    error: PropTypes.string,
    reset: PropTypes.func,
    submitting: PropTypes.bool
};

PasswordReset.defaultProps = {
    className: '',
    onSuccess: () => {},
};

export default reduxForm({
    form: FORM_ID,
    validate
})(PasswordReset);