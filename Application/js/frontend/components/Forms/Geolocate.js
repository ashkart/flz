import React from 'react';
import PropTypes from 'prop-types';
import {geolocated, geoPropTypes} from 'react-geolocated';
import CityChange, {FORM_ID} from "./CityChange";
import {asArray} from "../../helper/utils";

class Geolocate extends React.Component {
    render() {
        const {
            coords,
            userCity,
            onSuccess,
            onError,
            onCityUpdate
        } = this.props;

        return (
            <div className="form form-geolocate">
                {
                    !!asArray(userCity).length &&
                    <CityChange
                        id={FORM_ID}
                        fieldLabel={<nobr>Ваш город</nobr>}
                        userCityInitial={userCity}
                        onCityUpdate={onCityUpdate}
                        isCompactView={true}
                    />
                }
            </div>
        );
    }
}


Geolocate.propTypes = {
    ...geoPropTypes,
    onSuccess: PropTypes.func,
    onError: PropTypes.func,
    userCity: PropTypes.object,
    onCityUpdate: PropTypes.func.isRequired
};

Geolocate.defaultProps = {
    coords: {},
    onSuccess: () => {},
    onError: () => {},
    positionError: {},
    userCity: {}
};

export default geolocated({
    positionOptions: {
        enableHighAccuracy: false,
    },
    userDecisionTimeout: 30000,
    geolocationProvider: navigator.geolocation
})(Geolocate);
