import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import Form from "./Form";
import connect from "react-redux/es/connect/connect";
import {renderTextarea, renderTextEditor} from "../FormElements/renderElement";

export const FORM_ID = 'messaging';

class MessageForm extends React.Component {
    constructor(props) {
        super(props);

        this.onSuccess = this.onSuccess.bind(this);
    }

    onSuccess() {
        this.props.onSuccess(this.props.text);
        this.props.reset();
    }

    render() {
        const {
            action,
            className,
            handleSubmit,
            onSuccess
        } = this.props;

        return (
            <Form action={action}
                  method="POST"
                  className={`form ${className}`}
                  handleSubmit={handleSubmit}
                  errorText={"Ошибка при создании отклика"}
                  onSuccess={this.onSuccess}
            >
                <Field
                    component={renderTextarea}
                    className="textarea"
                    placeholder="Введите текст сообщения..."
                    name="text"
                />

                <div className="row form-buttons">
                    <button type="button" name="cancel" data-modal-close>Закрыть</button>
                    <button type="submit" name="submit">Отправить</button>
                </div>
            </Form>
        );
    }
}

MessageForm.propTypes = {
    action: PropTypes.string.isRequired,
    onSuccess: PropTypes.func
};

MessageForm.defaultProps = {
    className: '',
    onSuccess: () => {}
};

const selector = formValueSelector(FORM_ID);

export default connect((state, props) => {
    const text = selector(state, 'text');

    return {
        state,
        ...props,
        text
    };
})(
    reduxForm({
        form: FORM_ID,
        enableReinitialize: true
    })(MessageForm)
);