import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import {connect} from 'react-redux';
import {change} from 'redux-form';
import {
    fetchCurrenciesAction,
    fetchOneAction,
    fetchProfareaTreeAction
} from "../../actions/actionCreators/dataFetchActions";
import Form from "./Form";
import {
    renderCheckbox, renderGeoSuggestion,
    renderInput,
    renderSelect,
    renderSuggestInput,
    renderTextEditor
} from "../FormElements/renderElement";
import {RadioGroup, Radio} from 'react-radio-group'
import {
    COOKIE_ROLE,
    COOKIE_USER_ID, EL_VISIBILITY,
    EMPLOYMENT_LIST,
    EMPLOYMENT_REGULAR,
    EXP_NOT_REQUIRED,
    EXPERIENCE_OPTIONS, ROLE_ADMIN, ROLE_GUEST, VISIBILITY_ALL, VISIBILITY_NONE
} from "../../helper/constants";
import TreeSelectElement from "../FormElements/TreeSelectElement";
import formWithDictionary from "../hoc/formWithDictionary";
import {withCookies} from "react-cookie";
import {Redirect} from "react-router-dom";
import {asArray, isEmpty} from "../../helper/utils";

export const FORM_ID = 'vacancyEdit';

const emptyFormState = {
    company_id: null,
    company: '',
    city_id: null,
    currency_id: 1,
    position_name: '',
    profarea_id: null,
    is_remote: false,
    required_experience: EXP_NOT_REQUIRED,
    description: '',
    requirements: '',
    liabilities: '',
    conditions: '',
    employment: EMPLOYMENT_REGULAR,
    visibility: VISIBILITY_ALL
};

const VACANCY_EDIT = 'vacancy/edit';

const RESOURCE_DEFAULT_COMPANY = 'profile/default-company';

const PAYCHECK_DISPLAY_TYPE_PLUG = 'plug';
const PAYCHECK_DISPLAY_TYPE_INTERVIEW = 'interview';

class VacancyEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            initialized: false,
            paycheckDisplayType: PAYCHECK_DISPLAY_TYPE_PLUG,
            selectedCompany: {},
            values: {
                locations: {}
            }
        };

        this.onCompanySelect = this.onCompanySelect.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.loadFormData = this.loadFormData.bind(this);
        this.updatePaycheckDisplayType = this.updatePaycheckDisplayType.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(fetchCurrenciesAction());

        if (this.props.vacancyId) {
            this.loadFormData();
        }

        if (this.props.cookies.get(COOKIE_USER_ID)) {
            if (!JSON.stringify(this.props.state.data[RESOURCE_DEFAULT_COMPANY])) {
                this.props.dispatch(fetchOneAction(RESOURCE_DEFAULT_COMPANY));
            } else {
                this.onCompanySelect({0: this.props.state.data[RESOURCE_DEFAULT_COMPANY]});
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.state.data[RESOURCE_DEFAULT_COMPANY]) !== JSON.stringify(this.props.state.data[RESOURCE_DEFAULT_COMPANY])) {
            this.onCompanySelect({0: nextProps.state.data[RESOURCE_DEFAULT_COMPANY]});
        }

        let paycheckDisplayType = !this.state.initialized && nextProps.hasPaycheckValue ? PAYCHECK_DISPLAY_TYPE_PLUG : this.state.paycheckDisplayType;
        let values = nextProps.formCustomValues;

        this.setState(prevState => ({paycheckDisplayType, values, initialized: true}));
    }

    loadFormData() {
        this.props.dispatch(fetchOneAction(VACANCY_EDIT, {id: this.props.vacancyId}));
    }

    updatePaycheckDisplayType(paycheckDisplayType) {
        const {dispatch} = this.props;

        this.setState(
            prevState => ({paycheckDisplayType}),
            () => {
                if (this.state.paycheckDisplayType === PAYCHECK_DISPLAY_TYPE_INTERVIEW) {
                    dispatch(change(FORM_ID, 'paycheck_min', null));
                    dispatch(change(FORM_ID, 'paycheck_max', null));
                }
            }
        );
    }

    onSelectChange(e) {
        this.props.dispatch(change(FORM_ID, e.target.name, e.target.value));
    }

    onCompanySelect(selected) {
        const {
            onCompanySelect,
            cookies
        } = this.props;

        const selectedAsArray = asArray(selected);

        const companyId = !isEmpty(selectedAsArray[0]) ? selectedAsArray[0]['id'] : undefined;

        const role = cookies.get(COOKIE_ROLE);

        if (
            companyId ||
            isEmpty(selected['company_id']) ||
            role !== ROLE_ADMIN
        ) {
            onCompanySelect(selected);

            this.setState(prevState => ({selectedCompany: selected}));

            return;
        }

        if (role === ROLE_ADMIN) {
            this.setState(
                prevState => ({selectedCompany: selected}),
                () => this.props.dispatch(change(FORM_ID, 'company', selectedAsArray[0]['title']))
            );
        }
    }

    render() {
        const {
            cookies,
            className,
            handleSubmit,
            error,
            reset,
            submitting,
            vacancyId,
            dictionary,
            currencies,
            onSuggestSelect,
            onCitySelect,
            onCompanySelect,
            onProfareaSelect,
            onVacancyLocationSelect,
            fetchComponentData,
            formCustomValues,
            createSelectOptions,
            hasPaycheckValue
        } = this.props;

        let action = "/vacancy/save";
        let request = 'vacancy/edit';

        if (vacancyId) {
            action = `${action}/id-${vacancyId}`;
            request = `${request}/id-${vacancyId}`;
        }

        if (cookies.get(COOKIE_ROLE) === ROLE_GUEST) {
            return <Redirect to={`/auth?request=${encodeURIComponent(request)}`} />;
        }

        const profareaData = Object.assign({}, dictionary.profarea_id);

        return (
            <Form
                className="form form-vacancy edit"
                action={action}
                method="POST"
                errorText="Ошибка сохранения вакансии"
                handleSubmit={handleSubmit}
            >
                <h2>Создать вакансию</h2>

                <div className="form-block">
                    <span className="label">Основная информация</span>
                    {
                        <div className="row">
                            <Field
                                className="suggest-input"
                                label="Компания"
                                name="company_id"
                                multiSelect={false}
                                isStrict={false}
                                fetchSuggestions={fetchComponentData}
                                resource={'suggest/company'}
                                onUpdateSelection={this.onCompanySelect}
                                suggestions={dictionary.companySuggestions}
                                component={renderSuggestInput}
                                selectedValues={this.state.selectedCompany}
                            />
                        </div>
                    }

                    <div className="row">
                        <Field
                            className="input"
                            type="text"
                            label="Название должности"
                            placeholder="Название должности"
                            name="position_name"
                            component={renderInput}
                        />
                    </div>

                    <div className="row">
                        <span className="label">Регион</span>
                        <div className="row">
                            <Field
                                className="suggest-input locations tags"
                                placeholder="Города и страны доступности вакансии"
                                name={'locations'}
                                multiSelect={true}
                                isStrict={true}
                                fetchSuggestions={fetchComponentData}
                                resource={'suggest/locality'}
                                onUpdateSelection={onVacancyLocationSelect}
                                suggestions={dictionary.locationSuggestions}
                                component={renderSuggestInput}
                                renderSuggestionItem={renderGeoSuggestion}
                                selectedValues={this.state.values.locations}
                            />
                        </div>

                        <Field
                            className="checkbox"
                            label="Возможна постоянная удаленная работа"
                            name="is_remote"
                            component={renderCheckbox}
                        />
                    </div>

                    <div className="row">
                        <Field
                            className="select"
                            label="Занятость"
                            name="employment"
                            component={renderSelect}
                            options={EMPLOYMENT_LIST}
                            onChange={this.onSelectChange}
                        />
                    </div>

                    <div className="row">
                        <Field
                            component={TreeSelectElement}
                            className="select"
                            label="Профобласть"
                            name="profarea_id"
                            multiSelect={false}
                            onUpdateSelection={onProfareaSelect}
                            resource={'suggest/profarea'}
                            fetchTree={fetchComponentData}
                            actionCreator={fetchProfareaTreeAction}
                            data={profareaData}
                            selectedValues={formCustomValues.profarea_id}
                        />
                    </div>

                    <div className="row">
                        <Field
                            className="select"
                            label="Требуемый опыт работы"
                            name="required_experience"
                            component={renderSelect}
                            onChange={this.onSelectChange}
                            options={EXPERIENCE_OPTIONS}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <div className="row">
                        <Field
                            className="select"
                            label="Видимость"
                            name={EL_VISIBILITY}
                            component={renderSelect}
                            onChange={this.onSelectChange}
                            options={[
                                {label: 'Все', value: VISIBILITY_ALL},
                                {label: 'Никто', value: VISIBILITY_NONE}
                            ]}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <span className="label">Описание вакансии</span>
                    <div className="row">
                        <Field
                            className="textarea"
                            type="text"
                            placeholder="О вакансии"
                            label="О вакансии"
                            name="description"
                            component={renderTextEditor}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <div className="row">
                        <Field
                            className="textarea"
                            type="text"
                            placeholder="Требования"
                            label="Требования"
                            name="requirements"
                            component={renderTextEditor}
                        />
                    </div>
                    <div className="row">
                        <Field
                            className="textarea"
                            type="text"
                            placeholder="Обязанности"
                            label="Обязанности"
                            name="liabilities"
                            component={renderTextEditor}
                        />
                    </div>
                    <div className="row">
                        <Field
                            className="textarea"
                            type="text"
                            placeholder="Условия работы"
                            label="Условия работы"
                            name="conditions"
                            component={renderTextEditor}
                        />
                    </div>
                </div>

                <div className="form-block">
                    <span className="label">Зарплата</span>
                        <div className="row">
                            <RadioGroup
                                className="col radio-group"
                                name="paycheck_display_type"
                                selectedValue={this.state.paycheckDisplayType}
                                onChange={this.updatePaycheckDisplayType}
                            >
                                <label className="radio">
                                    <Radio value={PAYCHECK_DISPLAY_TYPE_INTERVIEW} />По результатам собеседования
                                </label>
                                <label className="radio">
                                    <Radio value={PAYCHECK_DISPLAY_TYPE_PLUG} />Указать
                                </label>
                            </RadioGroup>
                        </div>
                        {
                            this.state.paycheckDisplayType === PAYCHECK_DISPLAY_TYPE_PLUG &&

                            <div className="row">
                                <Field
                                    label="Валюта"
                                    className="select"
                                    options={createSelectOptions(currencies)}
                                    name="currency_id"
                                    onChange={this.onSelectChange}
                                    component={renderSelect}
                                />
                            </div>&&
                            <div className="row">
                                <Field
                                    className="input"
                                    type="number"
                                    placeholder="От"
                                    name="paycheck_min"
                                    component={renderInput}
                                />
                                <Field
                                    className="input"
                                    type="number"
                                    placeholder="До"
                                    name="paycheck_max"
                                    component={renderInput}
                                />
                            </div>
                        }
                </div>

                <div className="row margin-bot20">
                    {error && <strong className="form-error">{error}</strong>}
                </div>

                <div className="hidden">
                    <Field
                        className="input"
                        type="hidden"
                        name="company_id"
                        component={renderInput}
                    />
                </div>

                <div className="row form-buttons">
                    <button type="submit" name="submit">Сохранить</button>
                </div>
            </Form>
        );
    }
}

VacancyEdit.propTypes = {
    vacancyId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    currencies: PropTypes.object
};

VacancyEdit.defaultProps = {
    currencies: {}
};

const VacancyEditReduxForm = reduxForm({
    form: FORM_ID,
    enableReinitialize: true
})(formWithDictionary(VacancyEdit));

const selector = formValueSelector(FORM_ID);

export default withCookies(connect((state, props) => {
    let currencies = {};

    if (state.dictionary) {
        if (state.dictionary.currencies) {
            currencies = state.dictionary.currencies;
        }
    }

    const vacancyData = !!state.data[VACANCY_EDIT] && !!state.data[VACANCY_EDIT][props.vacancyId]
        ? state.data[VACANCY_EDIT][props.vacancyId]
        : false
    ;

    const hasPaycheckValue = !!selector(state, 'paycheck_min') || !!selector(state, 'paycheck_max');

    return {
        state,
        ...props,
        currencies,
        initialValues: vacancyData || emptyFormState,
        hasPaycheckValue
    };
})(VacancyEditReduxForm));