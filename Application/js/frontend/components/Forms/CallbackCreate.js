import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { Field, reduxForm, change, formValueSelector } from 'redux-form';
import Form from "./Form";
import {
    renderSelect, renderTextarea,
    renderTextEditor
} from "../FormElements/renderElement";

export const FORM_ID = 'callbackCreate';

class CallbackCreate extends React.Component {
    constructor(props) {
        super(props);

        this.onEntitySelect = this.onEntitySelect.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.entities[0] && nextProps.entities[0].value && !this.props.fromEntityId) {
            this.props.dispatch(change(FORM_ID, 'from_entity_id', nextProps.entities[0].value));
        }
    }

    componentWillMount() {
        this.props.dispatch(change(FORM_ID, 'entity_id', this.props.entityId));
    }

    onEntitySelect(e) {
        this.props.dispatch(change(FORM_ID, 'from_entity_id', e.target.value));
    }

    render() {
        const {
            action,
            className,
            handleSubmit,
            entities,
            entitiesLabel,
            label,
            onSuccess
        } = this.props;

        return (
            <Form action={action}
                  method="POST"
                  className={`form ${className}`}
                  handleSubmit={handleSubmit}
                  errorText={"Ошибка при создании отклика"}
                  onSuccess={onSuccess}
            >
                <h2>{label}</h2>

                <Field
                    component="input"
                    type="hidden"
                    name="entity_id"
                />

                <div className="row">
                    <Field
                        className="select"
                        label={entitiesLabel}
                        name="from_entity_id"
                        component={renderSelect}
                        onChange={this.onEntitySelect}
                        options={entities}
                    />
                </div>

                <div className="row">
                    <Field
                        className="textarea vertical"
                        type="text"
                        label="Сообщение"
                        placeholder="Текст"
                        name="cover_letter"
                        component={renderTextarea}
                    />
                </div>

                <div className="row form-buttons">
                    <button type="button" name="cancel" data-modal-close>Отмена</button>
                    <button type="submit" name="submit">Отправить</button>
                </div>
            </Form>
        );
    }
}

CallbackCreate.propTypes = {
    action: PropTypes.string.isRequired,
    entityId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    entities: PropTypes.array,
    label: PropTypes.string.isRequired,
    onSuccess: PropTypes.func
};

CallbackCreate.defaultProps = {
    onSuccess: () => {},
    entities: []
};

const selector = formValueSelector(FORM_ID);

export default connect((state, props) => {

    const fromEntityId = selector(state, 'from_entity_id');

    return {
        fromEntityId,
        state,
        ...props,
    };
})(
    reduxForm({
        form: FORM_ID,
        enableReinitialize: true
    })(CallbackCreate)
);
