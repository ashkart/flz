import React from 'react';
import { Field, reduxForm } from 'redux-form';
import Form from "./Form";
import {renderInput, renderCheckbox, renderHiddenInput} from "../FormElements/renderElement";
import { change } from 'redux-form';
import {validate} from "../../helper/regFormValidate";

const FORM_ID = 'applicantReg';

class ApplicantReg extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(change(FORM_ID, 'type', 'applicant'));
    }

    render() {
        const {
            className,
            handleSubmit,
            error,
            reset,
            submitting
        } = this.props;

        return (
            <Form
                action="/auth/register"
                method="POST"
                className={`flex-container auth-form ${className}`}
                handleSubmit={handleSubmit}
                errorText={"Ошибка регистрации"}
            >
                <Field
                    name="type"
                    component="input"
                    type="hidden"
                />
                <Field
                    className="flex-item input"
                    placeholder="Имя пользователя"
                    name="user_name"
                    component={renderInput}
                    type="text"
                />
                <Field
                    className="flex-item input"
                    placeholder="Email"
                    name="login"
                    component={renderInput}
                    type="email"
                />
                <Field
                    className="flex-item input"
                    placeholder="Пароль"
                    name="password"
                    component={renderInput}
                    type="password"
                />
                <Field
                    className="flex-item input"
                    placeholder="Повтор пароля"
                    name="password_repeat"
                    component={renderInput}
                    type="password"
                />
                <Field
                    className="flex-item checkbox"
                    label={
                        <span>
                            Согласен с условиями <a href="/doc/conditions.pdf" target="_blank">
                                Пользовательского соглашения
                            </a> и <a href="/doc/confidential-policy.pdf" target="_blank">
                                Политикой конфиденциальности
                            </a>
                        </span>
                    }
                    name="agree"
                    component={renderCheckbox}
                    type="checkbox"
                />

                {error && <strong className="form-error">{error}</strong>}

                <button type="submit" className="submit flex-item form-control btn btn-primary">
                    Зарегистрироваться
                </button>
            </Form>
        );
    }
}

export default reduxForm({
    form: FORM_ID,
    validate
})(ApplicantReg);