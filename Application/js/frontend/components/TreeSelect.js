import React from "react";
import PropTypes from "prop-types";

import {TreeSelectContext} from "./context";

import attachOnBlurListener from "../helper/attachOnBlurListener";
import {fetchProfareaTreeAction} from "../actions/actionCreators/dataFetchActions";
import DropdownSelect from "./controlled/DropdownSelect";
import TreeNode from "./TreeSelect/TreeNode";
import {isEmpty} from "../helper/utils";

export default class TreeSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            showTree: false,
            selected: {},
            showClearBtn: false
        };

        this._dataFlat = {};

        this.props.fetchTree(this.props.actionCreator(props.resource, {name: props.name}));

        this.renderBranch = this.renderBranch.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onToggleItem = this.onToggleItem.bind(this);
        this.onType = this.onType.bind(this);
        this.onSelectionUpdated = this.onSelectionUpdated.bind(this);

        this._isCheckedItem = this._isCheckedItem.bind(this);
        this._toggleSelectedChildren = this._toggleSelectedChildren.bind(this);
        this._hasCheckedChildren = this._hasCheckedChildren.bind(this);
        this._hasUncheckedChildren = this._hasUncheckedChildren.bind(this);
        this._nodeHasChildren = this._nodeHasChildren.bind(this);
        this._buildHolderItems = this._buildHolderItems.bind(this);

        const self = this;

        attachOnBlurListener(
            [props.name, `${props.name}-input`],
            () => self.state.showTree,
            () => {
                self.setState((prevState) => {
                    return {...prevState, showTree: false};
                });
            }
        );
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const nextData = nextProps.data || {};
        const showTree = this.state.showTree && !!Object.keys(nextData).length;

        this.setState(
            prevState => {
                return {
                    ...prevState,
                    data: nextData,
                    showTree
                };
            },
            () => {
                if (!Object.values(this._dataFlat).length) {
                    this._dataFlat = this._getDataFlat(this.state.data);

                    if (Object.keys(this._dataFlat).length && this.props.selectedValues) {
                        let selected = {};

                        for (let item of this.props.selectedValues) {
                            selected[item] = {title: this._dataFlat[item].label};
                        }

                        this.setState(prevState => ({
                            ...prevState,
                            selected
                        }));
                    }
                }
            }
        );
    }

    onSelectionUpdated(selectedItems) {
        this.setState(
            prevState => ({selected: selectedItems}),
            () => {this.props.onUpdateSelection(Object.keys(this.state.selected))}
        );
    }

    onToggleItem(id) {
        let selected = this.state.selected;

        const newSelectedState = !selected[id];
        const parentId = this._dataFlat[id].parent_id;

        if (!this.props.multiSelect && !parentId) {
            return;
        }

        if (newSelectedState) {
            selected[id] = {title: this._dataFlat[id].label};
        } else {
            delete selected[id];
        }

        if (parentId) {
            if (newSelectedState && !this._hasUncheckedChildren(parentId)) {
                selected[parentId] = {title: this._dataFlat[parentId].label};
            } else if (!newSelectedState) {
                delete selected[parentId];
            }
        }

        if (!this.props.multiSelect && !isEmpty(selected) && parentId) {

            selected = {[id]: {...selected[id]}};

        }

        selected = Object.assign({}, selected, this._toggleSelectedChildren(id, newSelectedState));

        this.setState(
            prevState => {
                return {
                    ...prevState,
                    selected: selected
                }
            },
            () => {
                this.props.onUpdateSelection(Object.keys(this.state.selected));
            }
        );
    }

    renderBranch(nodeData) {
        return (
            <TreeSelectContext.Provider value={{isChecked: this._isCheckedItem}}>
                <TreeNode
                    id={nodeData.id}
                    label={nodeData.label}
                    subNodes={nodeData.children}
                    isChecked={this._isCheckedItem(nodeData.id)}
                    onToggle={this.onToggleItem}
                    hasChecked={this._hasCheckedChildren(nodeData.id)}
                    hasUnchecked={this._hasUncheckedChildren(nodeData.id)}
                    multiSelect={this.props.multiSelect}
                />
            </TreeSelectContext.Provider>
        );
    }

    onFocus() {
        this.setState((prevState) => {
            return {
                ...prevState,
                showTree: true
            }
        })
    }

    onType(newValue) {
        if (typeof newValue === 'string') {
            if (newValue.length > 1) {
                this.props.fetchTree(
                    fetchProfareaTreeAction(this.props.resource, {
                        name: this.props.name,
                        queryString: newValue
                    })
                );
            } else if (newValue.length === 0) {
                this.setState(
                    prevState => ({
                        ...prevState,
                        data: {},
                        showTree: true
                    }),
                    () => {
                        this.props.fetchTree(
                            this.props.actionCreator(this.props.resource, {
                                name: this.props.name
                            })
                        );
                    }
                );
            }
        }
    }

    _isCheckedItem(id) {
        return this.state.selected.hasOwnProperty(id);
    }

    _toggleSelectedChildren(id, isChecked) {
        const children = this._dataFlat[id].children;

        if (children) {
            let selected = this.state.selected;

            for (let key in children)  {
                if (isChecked) {
                    selected[key] = {title: children[key].label};
                } else {
                    delete selected[key];
                }

                selected = Object.assign(selected, this._toggleSelectedChildren(children[key].id, isChecked));
            }

            return selected;
        }
    }

    _hasCheckedChildren(id) {
        if (!this._nodeHasChildren(id)) {
            return false;
        }

        const children = this._dataFlat[id] && this._dataFlat[id].children || {};

        for (let child of Object.values(children)) {
            if (this._isCheckedItem(child.id) || this._hasCheckedChildren(child.id)) {
                return true;
            }
        }

        return false;
    }

    _hasUncheckedChildren(id) {
        if (!this._nodeHasChildren(id)) {
            return false;
        }

        const children = this._dataFlat[id].children || {};

        for (let child of Object.values(children)) {
            if (!this._isCheckedItem(child.id) || this._hasUncheckedChildren(child.id)) {
                return true;
            }
        }
    }

    _nodeHasChildren(id) {
        return !!Object.keys(this._dataFlat[id] && this._dataFlat[id].children || {}).length;
    }

    _getDataFlat(data) {
        let result = {};
        for (let id in data) {
            result[id] = data[id];

            if (data[id].children) {
                Object.assign(result, this._getDataFlat(data[id].children));
            }
        }

        return result;
    }

    _buildHolderItems() {
        const selected = this.state.selected;

        let holderItems = {};

        for (let id in selected) {
            if (
                !this._dataFlat[id].hasOwnProperty('parent_id') ||
                !holderItems.hasOwnProperty(this._dataFlat[id].parent_id)
            ) {
                holderItems[id] = selected[id];
            }
        }

        return holderItems;
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextState.data !== this.state.data ||
            nextState.selected !== this.state.selected ||
            nextState.showTree !== this.state.showTree
            ;
    }

    render() {
        const {
            inputProps,
            name,
            maxHeight,
            label,
            selectedValues,
            multiSelect,
            allowType
        } = this.props;

        let style = {};

        if (maxHeight) {
            style = { maxHeight, overflowY: "scroll" };
        }

        return (
            <div className={`filter ${name}`}>
                {!!label && <span className="label">{label}</span>}
                <DropdownSelect
                    multiSelect={multiSelect}
                    selectedValues={this._buildHolderItems()}
                    onSelectionUpdated={this.onSelectionUpdated}
                    selectId={name}
                    options={this.state.data}
                    onFocus={this.onFocus}
                    onType={this.onType}
                    inputProps={{
                        id: `${name}_input`,
                        type: "text",
                        className: `tree-input ${name}-input`,
                        ...inputProps
                    }}
                    renderOption={this.renderBranch}
                    optionsContainerClassName="tree"
                    style={style}
                    clearBtnIcon='fas fa-backspace'
                    clearBtnTitle="Очистить"
                    className={`${name}-select`}
                    allowType={allowType}
                />
            </div>
        );
    }
}

TreeSelect.propTypes = {
    multiSelect: PropTypes.bool,
    selectedValues: PropTypes.array,
    onUpdateSelection: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    maxHeight: PropTypes.string,
    fetchTree: PropTypes.func.isRequired,
    actionCreator: PropTypes.func.isRequired,
    label: PropTypes.string,
    resource: PropTypes.string.isRequired,
    data: PropTypes.object,
    allowType: PropTypes.bool
};

TreeSelect.defaultProps = {
    multiSelect: true,
    selectedValues: [],
    data: {},
    allowType: true
};