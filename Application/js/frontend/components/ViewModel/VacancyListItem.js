import React from "react";
import PropTypes from 'prop-types';
import humanReadableDate from "../../helper/humanReadableDate";
import {Link} from "react-router-dom";
import Spoiler from "../Spoiler";
import InfiniteScroll from 'react-infinite-scroll-component';
import LoadingSpin from "../controlled/LoadingSpin";
import withCallbacks, {entityListKey} from "../hoc/withCallbacks";
import {parseDateFromRuFormat} from "../../helper/utils";
import ButtonOpenModal from "../ButtonOpenModal";
import Messenger from "../Messenger";
import {updateAction} from "../../actions/actionCreators/dataFetchActions";
import ActionLink from "../ActionLink";
import {STATE_ACCEPTED, STATE_NEW, STATE_REJECTED, STATE_WATCHED} from "./CallbackListItem";
import ViewCount from "../controlled/ViewCount";

export const VACANCY_CALLBACKS_RESOURCE = 'vacancy/callbacks';

class VacancyListItem extends React.Component {
    render() {
        const {
            data,
            withCallbacks,
            loadCallbacks,
            onOpenCallbackSpoiler,
            hasMoreCallbacks,
            entityCallbacks,
            loadConversation,
            conversations,
            updateCallback,
            purgeCallback
        } = this.props;

        const {
            id,
            position,
            paycheck,
            date,
            currencyCode,
            currencyName,
            company,
            city,
            body,
            hasUserCallback,
            callbackState,
            totalCallbacksCount,
            newCallbacksCount,
            views
        } = this.props.mapping;

        let currencyValue = '';

        if (data[paycheck]) {
            currencyValue = <span title={data[currencyName]}>{data[currencyCode]}</span>;
        }

        const updateDateStr = humanReadableDate(new Date(Date.parse(data[date])));

        const paycheckValue = data[paycheck] || 'По договоренности';

        const callbackScrollContainerId = `vacancy${data[id]}-callbacks`;

        return (
            <div className="vacancy">
                <div className="row">
                    <Link to={`/vacancy/view/${data[id]}`} className="position-name">{data[position]}</Link>
                    <div className="paycheck">{paycheckValue} {currencyValue}</div>
                </div>
                <div className="row">
                    <div className="company">{data[company]}</div>
                </div>
                <p className="body">{data[body]}</p>
                {
                    data[hasUserCallback] &&
                    <div className={`row has-callback ${data[callbackState]}`}>
                        {
                            [STATE_NEW, STATE_WATCHED].indexOf(data[callbackState]) !== -1 &&
                            <div>
                                <span className="fas fa-clock"></span>&nbsp;Вы откликнулись
                            </div>
                        }
                        {
                            data[callbackState] === STATE_REJECTED &&
                            <div>
                                <span className="fas fa-times"></span>&nbsp;Получен отказ
                            </div>
                        }
                        {
                            data[callbackState] === STATE_ACCEPTED &&
                            <div>
                                <span className="fas fa-check-circle"></span>&nbsp;Приглашение
                            </div>
                        }
                    </div>
                }
                {
                    withCallbacks &&
                    <Spoiler label='Отклики' onOpen={onOpenCallbackSpoiler}>
                        <div
                            id={callbackScrollContainerId}
                            className="scroll-container"
                            style={{width: '100%'}}
                        >
                            <InfiniteScroll
                                dataLength={entityCallbacks.length}
                                next={loadCallbacks}
                                hasMore={hasMoreCallbacks}
                                loader={<LoadingSpin/>}
                            >
                                {
                                    !!entityCallbacks.length &&
                                    <table cellSpacing="0">
                                        <thead>
                                            <tr>
                                                <th>Приложенное резюме</th>
                                                <th>Отправлено</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            entityCallbacks.map((callback, index) => {
                                                if (!callback.hasOwnProperty('id')) {
                                                    return;
                                                }

                                                return (
                                                    <tr key={callback.id} id={`callback${callback.id}`} className={callback.state}>
                                                        <td className="position-name">
                                                            <Link to={`/resume/view/${callback.from_entity_id}/${callback.id}`}>
                                                                {callback.position_name}
                                                            </Link>
                                                        </td>
                                                        <td className="col-date">
                                                            {humanReadableDate(parseDateFromRuFormat(callback.create_date))}
                                                        </td>
                                                        <td className="col-message">
                                                            <ButtonOpenModal
                                                                modalContent={Messenger}
                                                                action={`/callback/add-message/conversation-${callback.conversation_id}`}
                                                                messageResource={`callback/conversation`}
                                                                conversationId={callback.conversation_id}
                                                                modalId={`callback${callback.id}_messages`}
                                                                className="messages"
                                                                label={`Сообщения к отклику на вакансию ${data[position]}`}
                                                                modalMainContainerSelector={`#callback${callback.id}`}
                                                                onOpen={() => { loadConversation(callback.conversation_id) }}
                                                                conversations={conversations}
                                                            >
                                                                <span className="fas fa-envelope">&nbsp;</span>
                                                                Сообщения
                                                            </ButtonOpenModal>
                                                        </td>
                                                        <td>
                                                            {
                                                                callback.state === STATE_ACCEPTED &&
                                                                <span className="success">Приглашен</span>
                                                            }
                                                            {
                                                                callback.state === STATE_REJECTED &&
                                                                <span className="danger">{'Отклонено'}</span>
                                                            }
                                                        </td>
                                                        <td className="col-action">
                                                            {
                                                                callback.state === STATE_NEW &&
                                                                <ActionLink
                                                                    className="success"
                                                                    label="Пригласить"
                                                                    action={updateAction("callback/accept", callback.id, entityListKey(VACANCY_CALLBACKS_RESOURCE, data[id]))}
                                                                    withConfirm={true}
                                                                    confirmLabel="Отправить приглашение?"
                                                                    onSuccess={() => updateCallback(index)}
                                                                />
                                                            }
                                                        </td>
                                                        <td className="col-action">
                                                            {
                                                                callback.state === 'new' &&
                                                                <ActionLink
                                                                    className="warning"
                                                                    label="Отклонить"
                                                                    action={updateAction("callback/reject", callback.id, entityListKey(VACANCY_CALLBACKS_RESOURCE, data[id]))}
                                                                    withConfirm={true}
                                                                    confirmLabel="Отклонить отклик?"
                                                                    onSuccess={() => updateCallback(index)}
                                                                />
                                                            }
                                                        </td>
                                                    </tr>
                                                );
                                            })
                                        }
                                        </tbody>
                                    </table>
                                }
                            </InfiniteScroll>
                        </div>
                    </Spoiler>
                }
                <div className="row">
                    <div className="date">Обновлено {updateDateStr}</div>
                    <ViewCount className="text-secondary" title="Просмотры" count={data[views]} />
                </div>

            </div>
        );
    }
}

VacancyListItem.propTypes = {
    data: PropTypes.object,
    withCallbacks: PropTypes.bool,
    purgeCallback: PropTypes.func,
    loadCallbacks: PropTypes.func,
    loadConversation: PropTypes.func,
    conversations: PropTypes.object,
    onOpenCallbackSpoiler: PropTypes.func,
    hasMoreCallbacks: PropTypes.bool,
    entityCallbacks: PropTypes.array,
    mapping: PropTypes.shape({
        position: PropTypes.string.isRequired,
        paycheck: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        currencyCode: PropTypes.string.isRequired,
        currencyName: PropTypes.string.isRequired,
        company: PropTypes.string.isRequired,
        city: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
        callbacks: PropTypes.string,
        hasUserCallback: PropTypes.string.isRequired,
        totalCallbacksCount: PropTypes.string,
        newCallbacksCount: PropTypes.string,
        views: PropTypes.string
    }).isRequired
};

VacancyListItem.defaultProps = {
    data: {},
    entityCallbacks: [],
    conversations: {},
    withCallbacks: false,
    purgeCallback: () => {},
    loadCallbacks: () => {},
    loadConversation: () => {},
    onOpenCallbackSpoiler: () => {},
    hasMoreCallbacks: true
};

export default withCallbacks(VacancyListItem);