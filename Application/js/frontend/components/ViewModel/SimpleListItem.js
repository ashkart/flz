import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

const SimpleListItem = (props) => {
    const {
        mapping,
        data,
        defaultPaycheck,
        linkUrl
    } = props;

    const {
        id,
        position,
        company,
        paycheck
    } = mapping;

    return (
        <div className="simple-list-item">
            <Link to={`${linkUrl}/${data[id]}`} className={mapping.position}>{data[position]}</Link>
            {
                !!mapping.company &&
                <span className={mapping.company}>{data[company]}</span>
            }
            <span className={mapping.paycheck}>{data[paycheck] || defaultPaycheck}</span>
        </div>
    );
};

SimpleListItem.propTypes = {
    data: PropTypes.object,
    mapping: PropTypes.shape({
        id: PropTypes.string.isRequired,
        position: PropTypes.string.isRequired,
        company: PropTypes.string,
        paycheck: PropTypes.string.isRequired,
    }).isRequired,
    defaultPaycheck: PropTypes.string
};

SimpleListItem.defaultProps = {
    data: {},
    defaultPaycheck: ''
};

export default SimpleListItem;
