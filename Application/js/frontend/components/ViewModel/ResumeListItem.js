import React from "react";
import PropTypes from 'prop-types';
import withCallbacks, {entityListKey} from "../hoc/withCallbacks";
import humanReadableDate from "../../helper/humanReadableDate";
import {Link} from "react-router-dom";
import {
    dateAsMonthAndYear,
    dateDiffMonths,
    formatNumber, parseDateFromRuFormat,
    pluralMonths,
    pluralRu,
    pluralYears
} from "../../helper/utils";
import Spoiler from "../Spoiler";
import InfiniteScroll from "react-infinite-scroll-component";
import LoadingSpin from "../controlled/LoadingSpin";
import ButtonOpenModal from "../ButtonOpenModal";
import Messenger from "../Messenger";
import ActionLink from "../ActionLink";
import {updateAction} from "../../actions/actionCreators/dataFetchActions";
import {STATE_ACCEPTED, STATE_NEW, STATE_REJECTED, STATE_WATCHED} from "./CallbackListItem";
import ViewCount from "../controlled/ViewCount";

export const RESUME_CALLBACKS_RESOURCE = 'resume/callbacks';

class ResumeListItem extends React.Component {
    render() {
        const {
            data,
            withCallbacks,
            loadCallbacks,
            onOpenCallbackSpoiler,
            hasMoreCallbacks,
            entityCallbacks,
            loadConversation,
            conversations,
            updateCallback,
            purgeCallback
        } = this.props;

        const {
            id,
            position,
            paycheck,
            updateDate,
            currencyCode,
            currencyName,
            city,
            age,
            lastWorkPosition,
            lastWorkDateFrom,
            lastWorkDateTo,
            workExp,
            hasUserCallback,
            callbackState,
            totalCallbacksCount,
            newCallbacksCount,
            views
        } = this.props.mapping;

        let currencyValue = '';

        if (data[paycheck]) {
            currencyValue = <span title={data[currencyName]}>{data[currencyCode]}</span>;
        }

        let lastWorkFrom = new Date(Date.parse(data[lastWorkDateFrom]));
        let lastWorkTo = data[lastWorkDateTo] ? new Date(Date.parse(data[lastWorkDateTo])) : new Date();

        let months = dateDiffMonths(lastWorkFrom, lastWorkTo);
        let years = Math.floor(months / 12);
        months = months % 12;

        let pluralYearsLastExp = years ? `${pluralYears(years)} ` : '';
        let pluralMonthsLastExp = months ? pluralMonths(months) : '';
        let lastExpPeriodStr = `${pluralYearsLastExp}${pluralMonthsLastExp}`;

        const updateDateStr = humanReadableDate(new Date(Date.parse(data[updateDate])));

        const paycheckValue = data[paycheck] ? formatNumber(data[paycheck]) : 'По договоренности';

        const callbackScrollContainerId = `resume${data[id]}-callbacks`;

        return (
            <div className="resume">
                <div className="row">
                    <Link to={`/resume/view/${data[id]}`} className="position-name">{data[position]}</Link>
                    <div className="paycheck">{paycheckValue} {currencyValue}</div>
                </div>
                <div className="row">
                    <div className="city age">{`${data[city]}, ${data[age]} ${pluralRu(data[age], 'год', 'года', 'лет')}`}</div>
                </div>
                {
                    !!data[workExp] &&
                    <p className="total-work-exp">Общий опыт работы {data[workExp]}</p>
                }
                {
                    !!data[lastWorkPosition] &&
                    <div className="prev-exp">
                        <span className="dashed">Последнее место работы:</span>
                        <div className="position">{data[lastWorkPosition]}</div>

                        <div className="row flex-start">
                            <div className="date">{dateAsMonthAndYear(lastWorkFrom)}</div>
                            <div className="date"> &nbsp;&mdash;&nbsp;{data[lastWorkDateTo] ? dateAsMonthAndYear(lastWorkTo) : 'По настоящее время'}</div>&nbsp;
                            <div className="exp-size">{lastExpPeriodStr ? `(${lastExpPeriodStr})` : ''}</div>
                        </div>
                    </div>
                }
                {
                    data[hasUserCallback] &&
                    <div className={`has-callback ${data[callbackState]}`}>
                        {
                            [STATE_NEW, STATE_WATCHED].indexOf(data[callbackState]) !== -1 &&
                            <div>
                                <span className="fas fa-clock"></span>&nbsp;Отправлено приглашение
                            </div>
                        }
                        {
                            data[callbackState] === STATE_REJECTED &&
                            <div>
                                <span className="fas fa-times"></span>&nbsp;Получен отказ
                            </div>
                        }
                        {
                            data[callbackState] === STATE_ACCEPTED &&
                            <div>
                                <span className="fas fa-check-circle"></span>&nbsp;Приглашение принято
                            </div>
                        }
                    </div>
                }
                {
                    withCallbacks &&
                    <Spoiler label='Отклики' onOpen={onOpenCallbackSpoiler}>
                        <div
                            id={callbackScrollContainerId}
                            className="scroll-container"
                            style={{width: '100%'}}
                        >
                            <InfiniteScroll
                                dataLength={entityCallbacks.length}
                                next={loadCallbacks}
                                hasMore={hasMoreCallbacks}
                                loader={<LoadingSpin/>}
                                scrollableTarget={callbackScrollContainerId}
                            >
                                {
                                    !!entityCallbacks.length &&
                                    <table cellSpacing="0">
                                        <thead>
                                            <tr>
                                                <th>Компания</th>
                                                <th>Предложенная вакансия</th>
                                                <th>Отправлен</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            entityCallbacks.map((callback, index) => {
                                                if (!callback.hasOwnProperty('id')) {
                                                    return;
                                                }

                                                return (
                                                    <tr key={callback.id} id={`callback${callback.id}`} className={callback.state}>
                                                        <td>
                                                            {
                                                                !!callback.opf &&
                                                                <span>{callback.opf}&nbsp;</span>
                                                            }
                                                            <span>{callback.company_name}</span>
                                                        </td>
                                                        <td className="position-name">
                                                            <Link to={`/resume/view/${callback.from_entity_id}/${callback.id}`}>{callback.position_name}</Link>
                                                        </td>
                                                        <td>
                                                            {humanReadableDate(parseDateFromRuFormat(callback.create_date))}
                                                        </td>
                                                        <td>
                                                            <ButtonOpenModal
                                                                modalContent={Messenger}
                                                                action={'/callback/add-message'}
                                                                messageResource={`callback/conversation`}
                                                                conversationId={callback.conversation_id}
                                                                modalId={`callback${callback.id}_messages`}
                                                                className="messages"
                                                                label="Сообщения"
                                                                modalMainContainerSelector={`#callback${callback.id}`}
                                                                onOpen={() => { loadConversation(callback.conversation_id) }}
                                                                conversations={conversations}
                                                            >
                                                                <span className="fas fa-envelope">&nbsp;</span>
                                                                Сообщения
                                                            </ButtonOpenModal>
                                                        </td>
                                                        <td>
                                                            {
                                                                callback.state === 'accepted' &&
                                                                <span className="success">Принято</span>
                                                            }
                                                            {
                                                                callback.state === 'rejected' &&
                                                                <span className="danger">Отклонено</span>
                                                            }
                                                        </td>
                                                        <td className="col-action">
                                                            {
                                                                callback.state === 'new' &&
                                                                <ActionLink
                                                                    className="success"
                                                                    label="Принять"
                                                                    action={updateAction("callback/accept", callback.id, entityListKey(RESUME_CALLBACKS_RESOURCE, data[id]))}
                                                                    withConfirm={true}
                                                                    confirmLabel="Принять приглашение?"
                                                                    onSuccess={() => updateCallback(index)}
                                                                />
                                                            }
                                                        </td>
                                                        <td className="col-action">
                                                            {
                                                                callback.state === 'new' &&
                                                                <ActionLink
                                                                    className="warning"
                                                                    label="Отклонить"
                                                                    action={updateAction("callback/reject", callback.id, entityListKey(RESUME_CALLBACKS_RESOURCE, data[id]))}
                                                                    withConfirm={true}
                                                                    confirmLabel="Отклонить приглашение?"
                                                                    onSuccess={() => updateCallback(index)}
                                                                />
                                                            }
                                                        </td>
                                                    </tr>
                                                );
                                            })
                                        }
                                        </tbody>
                                    </table>
                                }
                            </InfiniteScroll>
                        </div>
                    </Spoiler>
                }
                <div className="row">
                    <div className="date">Обновлено {updateDateStr}</div>
                    <ViewCount className="text-secondary" title="Просмотры" count={data[views]} />
                </div>
            </div>
        );
    }
}

ResumeListItem.propTypes = {
    data: PropTypes.object,
    withCallbacks: PropTypes.bool,
    loadCallbacks: PropTypes.func,
    loadConversation: PropTypes.func,
    conversations: PropTypes.object,
    onOpenCallbackSpoiler: PropTypes.func,
    hasMoreCallbacks: PropTypes.bool,
    entityCallbacks: PropTypes.array,
    updateCallback: PropTypes.func,
    purgeCallback: PropTypes.func,
    mapping: PropTypes.shape({
        position: PropTypes.string.isRequired,
        paycheck: PropTypes.string.isRequired,
        updateDate: PropTypes.string.isRequired,
        currencyCode: PropTypes.string.isRequired,
        currencyName: PropTypes.string.isRequired,
        city: PropTypes.string.isRequired,
        callbacks: PropTypes.string,
        hasUserCallback: PropTypes.string.isRequired,
        callbackState: PropTypes.string,
        totalCallbacksCount: PropTypes.string,
        newCallbacksCount: PropTypes.string,
        views: PropTypes.string
    }).isRequired
};

ResumeListItem.defaultProps = {
    entityCallbacks: [],
    conversations: {},
    withCallbacks: false,
    purgeCallback: () => {},
    loadCallbacks: () => {},
    updateCallback: () => {},
    onOpenCallbackSpoiler: () => {},
    hasMoreCallbacks: true
};

export default withCallbacks(ResumeListItem);