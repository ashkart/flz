import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, change, formValueSelector} from 'redux-form';
import {connect} from 'react-redux';
import {fetchListForSelect, fetchOneAction} from "../../actions/actionCreators/dataFetchActions";
import Form from "../Forms/Form";
import {asArray, findKeyByValue, formatNumber} from "../../helper/utils";
import {
    COOKIE_ROLE, COOKIE_USER_ID,
    EMPLOYMENT_LIST,
    EMPLOYMENT_REGULAR,
    EXP_NOT_REQUIRED,
    EXPERIENCE_OPTIONS, ROLE_ADMIN, ROLE_GUEST, VISIBILITY_ALL, VISIBILITY_NONE
} from "../../helper/constants";
import formWithDictionary from "../hoc/formWithDictionary";
import Text from "../FormElements/view/Text";
import {CHECKED, UNCHECKED} from "../FormElements/view/Checkbox";
import Checkbox from "../FormElements/view/Checkbox";
import {Link} from "react-router-dom";
import CallbackForm from "../Forms/CallbackCreate";
import ButtonOpenModal from "../ButtonOpenModal";
import {STATE_ACCEPTED, STATE_NEW, STATE_REJECTED, STATE_WATCHED} from "./CallbackListItem";
import {renderTextView, renderViewCount} from "../FormElements/renderElement";
import {withCookies} from "react-cookie";
import {EL_TAGS} from "../Forms/ResumeEdit";
import Tags from "../FormElements/view/Tags";

export const FORM_ID = 'vacancyView';

const emptyFormState = {
    city_id: null,
    position_name: '',
    profarea_id: null,
    is_remote: false,
    required_experience: EXP_NOT_REQUIRED,
    description: '',
    requirements: '',
    liabilities: '',
    conditions: '',
    employment: EMPLOYMENT_REGULAR,
    has_user_callback: false,
    callback_state: null,
    views: 0
};

const VACANCY_VIEW = 'vacancy/view';
const CALLBACK_ENTITY_RESOURCE = 'profile/resumes';

class VacancyView extends React.Component {
    constructor(props) {
        super(props);

        this.loadFormData = this.loadFormData.bind(this);
        this.loadResumes = this.loadResumes.bind(this);
        this._setHasCallback = this._setHasCallback.bind(this);

        if (props.vacancyId) {
            this.loadFormData();
        }
    }

    loadFormData() {
        this.props.dispatch(fetchOneAction(VACANCY_VIEW, {id: this.props.vacancyId}));
    }

    loadResumes() {
        this.props.dispatch(fetchListForSelect(CALLBACK_ENTITY_RESOURCE));
    }

    _setHasCallback() {
        this.props.dispatch(change(FORM_ID, 'has_user_callback', true));
        this.props.dispatch(change(FORM_ID, 'callback_state', STATE_NEW));
    }

    render() {
        const {
            handleSubmit,
            cookies,
            userId,
            visibility,
            visibilityText,
            locations
        } = this.props;

        const callbackState = this.props.state.form[FORM_ID] && this.props.state.form[FORM_ID].values
            ? this.props.state.form[FORM_ID].values.callback_state
            : null
        ;

        const isUserOwner = cookies.get(COOKIE_USER_ID) == userId;
        const isAdmin = cookies.get(COOKIE_ROLE) === ROLE_ADMIN;

        return (
            <div>
                <Form
                    className="form form-vacancy view"
                    action="/"
                    errorText=""
                    handleSubmit={handleSubmit}
                >
                    <div className="row form-pre-head">
                        <h2>Просмотр вакансии</h2>
                        {
                            (
                                isAdmin ||
                                isUserOwner
                            ) &&
                            <Link to={`/vacancy/edit/${this.props.vacancyId}`} className="btn edit" type="button">
                                <span className="fas fa-edit">&nbsp;</span>
                                Редактировать
                            </Link>
                        }
                    </div>

                    <div className="form-block">
                        <div className="row item-head">
                            {
                                (
                                    isAdmin ||
                                    isUserOwner
                                ) &&
                                    <div className="row">
                                        <span className={`alert ${visibility === VISIBILITY_ALL ? 'alert-success' : 'alert-warning'} visibility`}>
                                                {visibilityText}
                                        </span>
                                    </div>
                            }
                            <div className="row">
                                <Field
                                    className="caption"
                                    name="position_name"
                                    component={Text}
                                />
                            </div>

                            <div className="row text-secondary">
                                <Field
                                    name="company"
                                    icon="fas fa-address-card brand-blue"
                                    component={Text}
                                />

                                <Field
                                    name="is_remote"
                                    valueText={{[CHECKED]: 'Возможна постоянная удаленная работа', [UNCHECKED]: 'Работа на территории работодателя'}}
                                    icon={{[UNCHECKED]: "fas fa-building brand-blue", [CHECKED]: "fas fa-home brand-blue"}}
                                    component={Checkbox}
                                />
                            </div>
                        </div>

                        <div className="cols">
                            <div className="row">
                                <div className="left">Зарплата</div>
                                <div className="right">
                                    <Field
                                        name="paycheck"
                                        component={Text}
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="left">Опыт работы</div>
                                <div className="right">
                                    <Field
                                        name="experience"
                                        component={Text}
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="left">Занятость</div>
                                <div className="right">
                                    <Field
                                        name="employment_name"
                                        component={Text}
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="left">Профобласть</div>
                                <div className="right">
                                    <Field
                                        component={Text}
                                        name="profarea"
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <Field
                                label="Регион"
                                className="locations tags"
                                emptyText=""
                                name="locations"
                                component={Tags}
                                tags={locations}
                            />
                        </div>

                        <div className="row">
                            {
                                typeof this.props.views === 'number' &&
                                <Field
                                    className="text-secondary"
                                    component={renderViewCount}
                                    name="views"
                                    title="Просмотры"
                                />
                            }
                        </div>
                    </div>

                    <div className="form-block">
                        <div className="row">
                            <Field
                                className="col"
                                label="О вакансии"
                                name="description"
                                component={renderTextView}
                            />
                        </div>
                    </div>

                    <div className="form-block details">
                        <div className="row">
                            <Field
                                className="col"
                                label="Требования"
                                name="requirements"
                                component={renderTextView}
                            />
                        </div>

                        <div className="row">
                            <Field
                                className="col"
                                label="Обязанности"
                                name="liabilities"
                                component={renderTextView}
                            />
                        </div>
                    </div>

                    <div className="form-block">
                        <div className="row">
                            <Field
                                className="col"
                                label="Условия работы"
                                name="conditions"
                                component={renderTextView}
                            />
                        </div>
                    </div>
                </Form>

                <div className="row form-buttons">
                    {
                        this.props.state.form[FORM_ID] &&
                        this.props.state.form[FORM_ID].values.has_user_callback &&
                        <div className={`row has-callback text-secondary ${callbackState}`}>
                            {
                                [STATE_NEW, STATE_WATCHED].indexOf(callbackState) !== -1 &&
                                <div>
                                    <span className="fas fa-clock"></span>&nbsp;Вы откликнулись
                                </div>
                            }
                            {
                                callbackState === STATE_REJECTED &&
                                <div>
                                    <span className="fas fa-times"></span>&nbsp;Получен отказ
                                </div>
                            }
                            {
                                callbackState === STATE_ACCEPTED &&
                                <div>
                                    <span className="fas fa-check-circle"></span>&nbsp;Приглашение
                                </div>
                            }
                        </div>
                    }
                    {
                        !this.props.state.form[FORM_ID] ||
                        !this.props.state.form[FORM_ID].values.has_user_callback &&
                        <ButtonOpenModal
                            modalContent={CallbackForm}
                            action={'/vacancy/add-callback'}
                            modalId={`vacancy${this.props.vacancyId}_callback`}
                            className="callback"
                            onOpen={this.loadResumes}
                            closeModalComponentProp="onSuccess"
                            entities={this.props.callbackEntities}
                            entitiesLabel="Резюме"
                            label="Отклик на вакансию"
                            entityId={this.props.vacancyId}
                            onConfirmSuccess={this._setHasCallback}
                        >
                            <span className="fas fa-envelope">&nbsp;</span>
                            Откликнуться
                        </ButtonOpenModal>
                    }
                </div>
            </div>
        );
    }
}

VacancyView.propTypes = {
    vacancyId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    callbackEntities: PropTypes.array
};

VacancyView.defaultProps = {
    callbackEntities: []
};

const VacancyViewReduxForm = reduxForm({
    form: FORM_ID,
    enableReinitialize: true
})(formWithDictionary(VacancyView));

const selector = formValueSelector(FORM_ID);

export default withCookies(connect((state, props) => {
    const vacancyData = !!state.data[VACANCY_VIEW] && !!state.data[VACANCY_VIEW][props.vacancyId]
        ? state.data[VACANCY_VIEW][props.vacancyId]
        : false
    ;

    let visibilityText;
    let visibility = selector(state, 'visibility');

    let locations = selector(state, 'locations');

    switch (visibility) {
        case VISIBILITY_ALL:
            visibilityText = 'Доступно всем';
            break;
        case VISIBILITY_NONE:
            visibilityText = 'Скрыто';
            break;
    }

    let result = {
        state,
        ...props,
        views: selector(state, 'views'),
        locations,
        initialValues: vacancyData || emptyFormState,
        userId: selector(state, 'user_id'),
        visibility,
        visibilityText
    };

    if (vacancyData) {
        const employment = vacancyData['employment'];
        const experience = vacancyData['required_experience'];

        if (!result.initialValues['employment_name']) {
            result.initialValues['employment_name'] = findKeyByValue(EMPLOYMENT_LIST, employment, 'label', 'value');
        }

        if (!result.initialValues['experience']) {
            result.initialValues['experience'] = findKeyByValue(EXPERIENCE_OPTIONS, experience, 'label', 'value');
        }

        if (result.initialValues['paycheck_const']) {
            result.initialValues['paycheck'] = formatNumber(result.initialValues['paycheck_const']);
        } else {
            result.initialValues['paycheck'] = '';

            if (result.initialValues['paycheck_min']) {
                result.initialValues['paycheck'] = `от ${formatNumber(result.initialValues['paycheck_min'])} `;
            }

            if (result.initialValues['paycheck_max']) {
                result.initialValues['paycheck'] = `${result.initialValues['paycheck']}до ${formatNumber(result.initialValues['paycheck_max'])}`;
            }
        }

        if (result.initialValues['paycheck']) {
            result.initialValues['paycheck'] = `${result.initialValues['paycheck']} ${result.initialValues['currency']}`;
        } else {
            result.initialValues['paycheck'] = 'По договоренности';
        }
    }

    if (state.data[CALLBACK_ENTITY_RESOURCE]) {
        result.callbackEntities = asArray(state.data[CALLBACK_ENTITY_RESOURCE]);
    }

    return result;
})(VacancyViewReduxForm));