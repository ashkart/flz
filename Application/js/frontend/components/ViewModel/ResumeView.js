import React from 'react';
import PropTypes from 'prop-types';
import { Field, Fields, FieldArray, reduxForm, formValueSelector} from 'redux-form';
import {Link} from "react-router-dom";
import {connect} from 'react-redux';
import {change} from 'redux-form';
import {fetchListAction, fetchListForSelect, fetchOneAction} from "../../actions/actionCreators/dataFetchActions";
import Form from "../Forms/Form";
import {
    COOKIE_ROLE,
    COOKIE_USER_ID,
    EDUCATION,
    EDUCATION_SECONDARY_VOCATIONAL,
    EMPLOYMENT_LIST,
    LANG_LEVEL_BASE, ROLE_ADMIN,
    VISIBILITY_ALL, VISIBILITY_NONE,
} from "../../helper/constants";
import Collection from "../Collection";
import formWithDictionary from "../hoc/formWithDictionary";
import Text from "../FormElements/view/Text";
import Image from "../FormElements/view/Image";
import Checkbox, {CHECKED, UNCHECKED} from "../FormElements/view/Checkbox";
import Tags from "../FormElements/view/Tags";
import {
    EL_PHOTO,
    EL_PHOTO_PATH,
    EL_POSITION_NAME,
    EL_CITY_ID,
    EL_FIRST_NAME,
    EL_MIDDLE_NAME,
    EL_LAST_NAME,
    EL_GENDER,
    EL_IS_RELOCATE_READY,
    EL_IS_REMOTE,
    EL_EMPLOYMENT,
    EL_IS_MARRIED,
    EL_HAS_CHILDREN,
    EL_TAGS,
    EL_EDUCATION_LEVEL,
    EL_AUTODESCRIPTION,
    EL_DESIRED_PAYCHECK,
    EL_CURRENCY,
    EL_ID,
    EL_CONTACT_COLLECTION,
    EL_TYPE,
    EL_VALUE,
    EL_EDUCATION_COLLECTION,
    EL_SPECIALTY,
    EL_EDUCATIONAL_ORGANIZATION,
    EL_DATE_START,
    EL_DATE_END,
    EL_WORK_EXPERIENCE_COLLECTION,
    EL_DESCRIPTION,
    EL_LANGUAGES_COLLECTION,
    EL_CODE,
    EL_LEVEL
} from "../Forms/ResumeEdit";
import {asArray, findKeyByValue, formatNumber} from "../../helper/utils";
import DateRangeText from "../FormElements/view/DateRangeText";
import ButtonOpenModal from "../ButtonOpenModal";
import CallbackForm from "../Forms/CallbackCreate";
import {STATE_ACCEPTED, STATE_NEW, STATE_REJECTED, STATE_WATCHED} from "./CallbackListItem";
import {renderTextView, renderViewCount} from "../FormElements/renderElement";
import {withCookies} from "react-cookie";

const EL_AGE = 'age';

const emptyFormState = {
    [EL_PHOTO_PATH]: null,
    [EL_PHOTO]: null,
    [EL_CITY_ID]: null,
    [EL_POSITION_NAME]: '',
    [EL_DESIRED_PAYCHECK]: '',
    [EL_CURRENCY]: 1,
    [EL_FIRST_NAME]: '',
    [EL_MIDDLE_NAME]: '',
    [EL_LAST_NAME]: '',
    [EL_AGE]: null,
    [EL_GENDER]: '',
    [EL_IS_RELOCATE_READY]: false,
    [EL_IS_MARRIED]: null,
    [EL_HAS_CHILDREN]: null,
    [EL_EDUCATION_LEVEL]: EDUCATION_SECONDARY_VOCATIONAL,
    [EL_AUTODESCRIPTION]: '',
    [EL_CONTACT_COLLECTION]: [],
    [EL_EDUCATION_COLLECTION]: [],
    [EL_WORK_EXPERIENCE_COLLECTION]: [],
    [EL_LANGUAGES_COLLECTION]: [],
    [EL_TAGS]: {},
    has_user_callback: false,
    callback_state: null,
    views: 0
};

export const FORM_ID = 'resumeView';

const RESUME_VIEW = 'resume/view';
const CALLBACK_ENTITY_RESOURCE = 'profile/vacancies';

class ResumeView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            values: {
                [EL_CITY_ID]: {},
                [EL_LANGUAGES_COLLECTION]: [],
                [EL_TAGS]: {}
            }
        };

        this.loadFormData = this.loadFormData.bind(this);
        this.loadVacancies = this.loadVacancies.bind(this);

        if (props.resumeId) {
            this.loadFormData();
        }

        this._setHasCallback = this._setHasCallback.bind(this);
        this._renderLanguageCollection = this._renderLanguageCollection.bind(this);
        this._renderContactCollection = this._renderContactCollection.bind(this);
        this._renderInviteForm = this._renderInviteForm.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState(prevState => ({values: nextProps.formCustomValues}));
    }

    loadFormData() {
        this.props.dispatch(fetchOneAction(RESUME_VIEW, {id: this.props.resumeId, callbackId: this.props.callbackId}));
    }

    loadVacancies() {
        this.props.dispatch(fetchListForSelect(CALLBACK_ENTITY_RESOURCE));
    }

    _setHasCallback() {
        this.props.dispatch(change(FORM_ID, 'has_user_callback', true));
        this.props.dispatch(change(FORM_ID, 'callback_state', STATE_NEW));
    }

    _renderLanguageCollection({fields, label, meta}) {
        return (
            <Collection
                className="languages"
                emptyText="Не указано"
                label={label}
                name={EL_LANGUAGES_COLLECTION}
                fields={fields}
                isEditMode={false}
                onSpawnFieldset={() => {
                    this.props.dispatch(change(FORM_ID, `${EL_LANGUAGES_COLLECTION}[${fields.length}].${EL_CODE}`, 'en'));
                    this.props.dispatch(change(FORM_ID, `${EL_LANGUAGES_COLLECTION}[${fields.length}].${EL_LEVEL}`, LANG_LEVEL_BASE));
                }}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="language"
                    name="language_name"
                    component={Text}
                />

                <Field
                    className="level"
                    name="language_level"
                    component={Text}
                />
            </Collection>
        );
    }

    _renderContactCollection({fields, label, meta}) {
        return (
            <Collection
                className="contacts"
                emptyText="Не указаны"
                label={label}
                name={EL_CONTACT_COLLECTION}
                fields={fields}
                isEditMode={false}
                onSpawnFieldset={() => {
                    this.props.dispatch(change(FORM_ID, `${EL_CONTACT_COLLECTION}[${fields.length}].${EL_TYPE}`, TYPE_EMAIL));
                }}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="type"
                    name={EL_TYPE}
                    component={Text}
                />

                <Field
                    className="value"
                    name={EL_VALUE}
                    component={Text}
                />
            </Collection>
        );
    }

    _renderEducationCollection({fields, label, meta}) {
        return (
            <Collection
                className="educations"
                label={label}
                name={EL_EDUCATION_COLLECTION}
                emptyText="Не указано"
                fields={fields}
                isEditMode={false}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="col speciality"
                    name={EL_SPECIALTY}
                    component={Text}
                />

                <Field
                    className="col"
                    name={EL_EDUCATIONAL_ORGANIZATION}
                    component={Text}
                />

                <Fields
                    className="date-range"
                    component={DateRangeText}
                    names={[EL_DATE_START, EL_DATE_END]}
                    mapping={{dateFrom: EL_DATE_START, dateTo: EL_DATE_END}}
                />
            </Collection>
        );
    }

    _renderWorkExpCollection({fields, label, meta}) {
        return (
            <Collection
                label={label}
                className="work-exp"
                emptyText="Не указан или отсутствует"
                name={EL_WORK_EXPERIENCE_COLLECTION}
                fields={fields}
                isEditMode={false}
            >
                <Field
                    name={EL_ID}
                    component="input"
                    type="hidden"
                />

                <Field
                    className="position"
                    name={EL_POSITION_NAME}
                    component={Text}
                />

                <Fields
                    className="date-range"
                    component={DateRangeText}
                    names={[EL_DATE_START, EL_DATE_END]}
                    mapping={{dateFrom: EL_DATE_START, dateTo: EL_DATE_END}}
                />

                <Field
                    className="col"
                    name={EL_DESCRIPTION}
                    label="Описание"
                    component={renderTextView}
                />
            </Collection>
        );
    }

    _renderInviteForm() {
        return (
            <div id={`resume${this.props.resumeId}_invite`} style={{display: 'none'}}>
                <CallbackForm />
            </div>
        );
    }

    render() {
        const {
            handleSubmit,
            cookies,
            userId,
            visibility,
            visibilityText
        } = this.props;

        const callbackState = this.props.state.form[FORM_ID] && this.props.state.form[FORM_ID].values
            ? this.props.state.form[FORM_ID].values.callback_state
            : null
        ;

        const isUserOwner = cookies.get(COOKIE_USER_ID) == userId;
        const isAdmin = cookies.get(COOKIE_ROLE) === ROLE_ADMIN;

        return (
            <div>
                <Form
                    action='/'
                    className="form form-resume view"
                    handleSubmit={handleSubmit}
                    errorText=""
                >
                    <div className="row form-pre-head">
                        <h2>Просмотр резюме</h2>
                        {
                            (
                                isAdmin ||
                                isUserOwner
                            ) &&
                            <Link to={`/resume/edit/${this.props.resumeId}`} className="btn edit" type="button">
                                <span className="fas fa-edit">&nbsp;</span>
                                Редактировать
                            </Link>
                        }
                    </div>

                    <div className="form-block">
                        {
                            (
                                isAdmin ||
                                isUserOwner
                            ) &&
                                <div className="row">
                                    <span className={`alert ${visibility === VISIBILITY_ALL ? 'alert-success' : 'alert-warning'} visibility`}>{visibilityText}</span>
                                </div>
                        }
                        <div className="row item-head">
                            <div className="col photo">
                                <Field
                                    name={EL_PHOTO_PATH}
                                    component={Image}
                                />
                            </div>

                            <div className="col">
                                <div className="row caption">
                                    <Field
                                        name="full_name"
                                        component={Text}
                                    />
                                </div>
                                <div className="row text-secondary">
                                    <Field
                                        name="gender_age"
                                        icon="fas fa-user-circle brand-blue"
                                        component={Text}
                                    />
                                    <Field
                                        name={'city'}
                                        icon="fas fa-map-marker-alt brand-blue"
                                        component={Text}
                                    />
                                    <Field
                                        name={EL_IS_RELOCATE_READY}
                                        valueText={{[UNCHECKED]: 'Не готов к переезду', [CHECKED]: 'Готов к переезду'}}
                                        icon={{[UNCHECKED]: "fas fa-city brand-blue", [CHECKED]: "fas fa-globe brand-blue"}}
                                        component={Checkbox}
                                    />
                                </div>

                                <div className="cols">
                                    <div className="row">
                                        <div className="left">Должность</div>
                                        <div className="right">
                                            <Field
                                                name={EL_POSITION_NAME}
                                                component={Text}
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="left">Зарплата</div>
                                        <div className="right">
                                            <Field
                                                name="paycheck"
                                                component={Text}
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="left">Предпочитаемая занятость</div>
                                        <div className="right">
                                            <Field
                                                name="employment_name"
                                                component={Text}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <Field
                                        name={EL_IS_REMOTE}
                                        valueText={{[CHECKED]: 'Ищу удаленную работу', [UNCHECKED]: ''}}
                                        icon={{[UNCHECKED]: "", [CHECKED]: "fas fa-satellite-dish brand-blue"}}
                                        component={Checkbox}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <Field
                                label="Ключевые навыки"
                                className="tags"
                                emptyText="Не указаны"
                                name={EL_TAGS}
                                component={Tags}
                                tags={this.state.values[EL_TAGS]}
                            />
                        </div>

                        <div className="row">
                            {
                                typeof this.props.views === 'number' &&
                                <Field
                                    className="text-secondary"
                                    component={renderViewCount}
                                    name="views"
                                    title="Просмотры"
                                />
                            }
                        </div>
                    </div>

                    <div className="form-block collection-container">
                        <span className="label">Контакты</span>
                        <div className="row">
                            <FieldArray
                                name={EL_CONTACT_COLLECTION}
                                component={this._renderContactCollection}
                            />
                        </div>
                    </div>

                    <Field
                        name={EL_IS_MARRIED}
                        component='input'
                        type='hidden'
                    />
                    <Field
                        name={EL_HAS_CHILDREN}
                        component='input'
                        type='hidden'
                    />

                    <div className="form-block collection-container">
                        <span className="label form-padding-horizontal">Опыт работы</span>
                        <div className="row">
                            <FieldArray
                                name={EL_WORK_EXPERIENCE_COLLECTION}
                                component={this._renderWorkExpCollection}
                            />
                        </div>
                    </div>

                    <div className="form-block collection-container">
                        <span className="label form-padding-horizontal">Знание языков</span>
                        <div className="row">
                            <FieldArray
                                name={EL_LANGUAGES_COLLECTION}
                                component={this._renderLanguageCollection}
                            />
                        </div>
                    </div>

                    <div className="form-block collection-container">
                        <span className="label form-padding-horizontal">Образование</span>

                        <div className="row form-padding-horizontal">
                            <Field
                                className="col"
                                label="Уровень образования"
                                name="education_lvl"
                                component={Text}
                            />
                        </div>

                        <div className="row">
                            <FieldArray
                                label="Учебные заведения, курсы"
                                name={EL_EDUCATION_COLLECTION}
                                component={this._renderEducationCollection}
                            />
                        </div>
                    </div>

                    <div className="form-block">
                        <div className="row">
                            <Field
                                className="col"
                                label="О себе"
                                name={EL_AUTODESCRIPTION}
                                component={renderTextView}
                            />
                        </div>
                    </div>
                </Form>

                <div className="row form-buttons">
                    {
                        this.props.state.form[FORM_ID] &&
                        this.props.state.form[FORM_ID].values.has_user_callback &&
                        <div className={`row has-callback text-secondary ${callbackState}`}>
                            {
                                [STATE_NEW, STATE_WATCHED].indexOf(callbackState) !== -1 &&
                                <div>
                                    <span className="fas fa-clock"></span>&nbsp;Отправлено приглашение
                                </div>
                            }
                            {
                                callbackState === STATE_REJECTED &&
                                <div>
                                    <span className="fas fa-times"></span>&nbsp;Получен отказ
                                </div>
                            }
                            {
                                callbackState === STATE_ACCEPTED &&
                                <div>
                                    <span className="fas fa-check-circle"></span>&nbsp;Приглашение принято
                                </div>
                            }
                        </div>
                    }
                    {
                        !this.props.state.form[FORM_ID] ||
                        !this.props.state.form[FORM_ID].values.has_user_callback &&
                        <ButtonOpenModal
                            modalContent={CallbackForm}
                            className="callback"
                            action={'/resume/add-callback'}
                            modalId={`resume${this.props.resumeId}_callback`}
                            onOpen={this.loadVacancies}
                            closeModalComponentProp="onSuccess"
                            entities={this.props.callbackEntities}
                            entitiesLabel="Вакансия"
                            label="Сообщение соискателю"
                            entityId={this.props.resumeId}
                            onConfirmSuccess={this._setHasCallback}
                        >
                            <span className="fas fa-envelope">&nbsp;</span>
                            Сообщение / Пригласить
                        </ButtonOpenModal>
                    }
                </div>
            </div>
        );
    }
}

ResumeView.propTypes = {
    tags: PropTypes.object,
    callbackEntities: PropTypes.array
};

ResumeView.defaultProps = {
    tags: {},
    callbackEntities: []
};

const ResumeViewReduxForm = reduxForm({
    form: FORM_ID,
    enableReinitialize: true
})(formWithDictionary(ResumeView));

const selector = formValueSelector(FORM_ID);

export default withCookies(connect((state, props) => {
    let tags = {};

    if (state.dictionary) {
        if (state.dictionary.tags) {
            tags = state.dictionary.tags;
        }
    }

    const resumeData = !!state.data[RESUME_VIEW] && !!state.data[RESUME_VIEW][props.resumeId]
        ? state.data[RESUME_VIEW][props.resumeId]
        : false
    ;

    let visibilityText;
    let visibility = selector(state, 'visibility');

    switch (visibility) {
        case VISIBILITY_ALL:
            visibilityText = 'Доступно всем';
            break;
        case VISIBILITY_NONE:
            visibilityText = 'Скрыто';
            break;
    }

    let result = {
        state,
        ...props,
        tags: tags,
        views: selector(state, 'views'),
        userId: selector(state, 'user_id'),
        visibility,
        visibilityText,
        initialValues: resumeData || emptyFormState
    };

    if (resumeData) {
        result.initialValues['full_name'] = `${result.initialValues[EL_LAST_NAME]} ${result.initialValues[EL_FIRST_NAME]}`;
        result.initialValues['gender_age'] = `${result.initialValues[EL_GENDER]}, ${result.initialValues[EL_AGE]}`;

        const employment = resumeData[EL_EMPLOYMENT];
        const educationLvl = resumeData[EL_EDUCATION_LEVEL];

        if (!result.initialValues['employment_name']) {
            result.initialValues['employment_name'] = findKeyByValue(EMPLOYMENT_LIST, employment, 'label', 'value') || 'Любая';
        }

        if (!result.initialValues['education_lvl']) {
            result.initialValues['education_lvl'] = findKeyByValue(EDUCATION, educationLvl, 'label', 'value') || 'Не указан';
        }

        if (!result.initialValues['paycheck']) {
            if (result.initialValues[EL_DESIRED_PAYCHECK]) {
                result.initialValues['paycheck'] = `${formatNumber(result.initialValues[EL_DESIRED_PAYCHECK])} ${result.initialValues['currency']}`;
            } else {
                result.initialValues['paycheck'] = 'По договоренности';
            }
        }
    }

    if (state.data[CALLBACK_ENTITY_RESOURCE]) {
        result.callbackEntities = asArray(state.data[CALLBACK_ENTITY_RESOURCE]);
    }

    return result;
})(ResumeViewReduxForm));