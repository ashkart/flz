import React from "react";
import PropTypes from 'prop-types';
import humanReadableDate from "../../helper/humanReadableDate";
import {Link} from "react-router-dom";
import ActionLink from "../ActionLink";
import {deleteAction} from "../../actions/actionCreators/dataFetchActions";
import {parseDateFromRuFormat} from "../../helper/utils";
import Messenger from "../Messenger";
import ButtonOpenModal from "../ButtonOpenModal";
import withCallbacks from "../hoc/withCallbacks";
import {LIST_PROFILE_CALLBACKS} from "../../helper/constants";
import ViewCount from "../controlled/ViewCount";
import Spoiler from "../Spoiler";
import ViewList from "../controlled/ViewList";
import MediaQuery from 'react-responsive';

export const STATE_NEW      = 'new';
export const STATE_WATCHED  = 'watched';
export const STATE_ACCEPTED = 'accepted';
export const STATE_REJECTED = 'rejected';
export const STATE_DELETED  = 'deleted';

const ENTITY_TYPE_VACANCY = 'vacancy';
const ENTITY_TYPE_RESUME = 'resume';

export const CALLBACK_LIST_ITEM_MAPPING = {
    id: 'id',
    entityType: 'entity_type',
    entityId: 'entity_id',
    fromEntityType: 'from_entity_type',
    fromEntityId: 'from_entity_id',
    conversationId: 'conversation_id',
    createDate: 'create_date',
    positionName: 'position_name',
    state: 'state',
    views: 'views'
};

class CallbackListItem extends React.Component {
    render() {
        const {
            data,
            mapping,
            loadConversation,
            conversations
        } = this.props;

        const {
            id,
            entityType,
            entityId,
            fromEntityType,
            fromEntityId,
            conversationId,
            createDate,
            positionName,
            state,
            views
        } = mapping;

        let subjectType;

        let entity = data[data[entityType]];
        let fromEntity = data[data[fromEntityType]];

        switch (data[entityType]) {
            case ENTITY_TYPE_VACANCY:
                subjectType = `вакансию`;
                break;
            case ENTITY_TYPE_RESUME:
                subjectType = `резюме`;
                break;
        }

        const textMain = `Отклик на ${subjectType}`;

        let attachedType;

        switch (data[fromEntityType]) {
            case ENTITY_TYPE_VACANCY:
                attachedType = `Приложена вакансия `;
                break;
            case ENTITY_TYPE_RESUME:
                attachedType = `Приложено резюме `;
                break;
        }

        const modalMainContainerId = `user-callback${data[id]}`;

        const positionNameLink = (
            <Link className="position-name" to={`/${data[entityType]}/view/${entity.id}`}>
                {entity[positionName]}
            </Link>
        );

        const recall = (
            <div>
            {
                data[state] === STATE_NEW &&
                <ActionLink
                    className="warning"
                    label="Отозвать"
                    action={deleteAction("callback/revoke", data[id], LIST_PROFILE_CALLBACKS)}
                    withConfirm={true}
                    confirmLabel="Отозвать этот отклик?"
                />
            }
            {
                data[state] === STATE_ACCEPTED &&
                <span className="success">{data[entityType] === ENTITY_TYPE_VACANCY ? 'Приглашение' : 'Принято'}</span>
            }
            {
                data[state] === STATE_REJECTED &&
                <span className="danger">{'Отклонено'}</span>
            }
            </div>
        );

        const messageBtn = (
            <ButtonOpenModal
                modalContent={Messenger}
                action={`/callback/add-message/conversation-${data[conversationId]}`}
                messageResource={`callback/conversation`}
                conversationId={data[conversationId]}
                modalId={`${modalMainContainerId}_messages`}
                className="messages"
                label={`Сообщения к отклику на ${subjectType} ${entity[positionName]}`}
                modalMainContainerSelector={`#${modalMainContainerId}`}
                onOpen={() => { loadConversation(data[conversationId]) }}
                conversations={conversations}
            >
                <span className="fas fa-envelope">&nbsp;</span>
                Сообщения
            </ButtonOpenModal>
        );

        return (
            <div id={modalMainContainerId} className={`callback ${data[state]}`}>
                <MediaQuery maxWidth={600}>
                    <div className="main-info">
                        <div className="row">
                            {textMain}&nbsp;
                            {positionNameLink}
                        </div>
                        <div className="row">
                            {messageBtn}
                        </div>
                        <div className="row">
                            {recall}
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={601}>
                <table cellSpacing={0}>
                    <tbody>
                        <tr>
                            <td>
                                {textMain}&nbsp;
                                {positionNameLink}
                            </td>
                            <td>
                                {messageBtn}
                            </td>
                            <td>
                                {recall}
                            </td>
                        </tr>
                    </tbody>
                </table>
                </MediaQuery>
                <div className="row secondary">
                    <div className='text-secondary'>{attachedType}</div>&nbsp;
                    <Link className="position-name" to={`/${data[fromEntityType]}/view/${fromEntity.id}`}>{fromEntity[positionName]}</Link>
                    <div className="date">
                        Отправлен {humanReadableDate(parseDateFromRuFormat(data[createDate]))}
                    </div>
                </div>
                <div className="row">
                    <Spoiler label="Просмотры">
                        <ViewList views={data[views]}/>
                    </Spoiler>
                </div>
            </div>
        );
    }
}

CallbackListItem.propTypes = {
    mapping: PropTypes.object.isRequired,
    data: PropTypes.object,
    removeItem: PropTypes.func
};

CallbackListItem.defaultProps = {
    data: {},
    removeItem: () => {}
};

export default withCallbacks(CallbackListItem);