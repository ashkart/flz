import React from "react";
import PropTypes from "prop-types";

import { TreeSelectContext } from "../context";

import Checkbox from "../controlled/Checkbox";
import {CHECKED_STATE_NONE, CHECKED_STATE_HAS, CHECKED_STATE_FULL} from "../controlled/Checkbox";
import {asArray} from "../../helper/utils";

export default class TreeNode extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.toggleExpandChildren = this.toggleExpandChildren.bind(this);
        this.getCheckedState = this.getCheckedState.bind(this);
        this.getAggregateIconClassName = this.getAggregateIconClassName.bind(this);
        this.toggleChecked = this.toggleChecked.bind(this);
    }

    toggleExpandChildren(e) {
        e.stopPropagation();

        this.setState((prevState) => {
            return {
                ...prevState,
                isOpen: !prevState.isOpen
            };
        })
    }

    toggleChecked() {
        this.props.onToggle(this.props.id);
    }

    getCheckedState() {
        if (this.props.isChecked) {
            return CHECKED_STATE_FULL;
        }

        if (this.props.hasChecked && this.props.hasUnchecked) {
            return CHECKED_STATE_HAS;
        }

        return CHECKED_STATE_NONE;
    }

    getAggregateIconClassName() {
        const {
            subNodes = {}
        } = this.props;

        if (this.state.isOpen) {
            return 'fas fa-minus';
        }

        if (!!Object.keys(subNodes).length) {
            return 'fas fa-plus';
        }

        return '';
    }

    render() {
        const {
            label,
            subNodes,
            onToggle,
            multiSelect
        } = this.props;

        const hasChildren = !!Object.keys(subNodes).length;

        return (
            <div className="tree-node">
                <div className="node-row" onClick={this.toggleChecked}>
                <span
                    className="table-cell"
                    onClick={this.toggleExpandChildren}
                ><span
                    className={`aggregate-icon ${this.getAggregateIconClassName()}`}
                > </span></span>
                    <div className="table-cell">
                        <Checkbox checkedState={this.getCheckedState()} label={label} disabled={!multiSelect && !!asArray(subNodes).length} hideDisabled={true} />
                    </div>
                </div>
                {
                    hasChildren &&
                    this.state.isOpen &&
                    <div className="children" style={{colspan: 3}}>
                        {
                            Object.keys(subNodes).map((key) => {
                                const subNode = subNodes[key];

                                return (
                                    <TreeSelectContext.Consumer key={subNode.id}>
                                        {
                                            ({isChecked}) => {
                                                const isCheckedItem = isChecked(subNode.id);

                                                return (
                                                    <TreeNode
                                                        id={subNode.id}
                                                        label={subNode.label}
                                                        subNodes={subNode.children}
                                                        isChecked={isCheckedItem}
                                                        onToggle={onToggle}
                                                        multiSelect={multiSelect}
                                                    />
                                                )
                                            }
                                        }
                                    </TreeSelectContext.Consumer>
                                );
                            })
                        }
                    </div>
                }
            </div>
        );
    }
}

TreeNode.propTypes = {
    onToggle: PropTypes.func.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    label: PropTypes.string.isRequired,
    subNodes: PropTypes.object,
    hasChecked: PropTypes.bool,
    isChecked: PropTypes.bool.isRequired,
    multiSelect: PropTypes.bool
};

TreeNode.defaultProps = {
    subNodes: {},
    multiSelect: true
};
