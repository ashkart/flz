import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

class ActionLink extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isConfirmationState: false
        };

        this.onMainClick = this.onMainClick.bind(this);
        this.executeAction = this.executeAction.bind(this);
        this.reset = this.reset.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isSuccess) {
            this.onSuccess();
        }
    }

    onMainClick() {
        if (this.props.withConfirm) {
            this.setState(prevState => ({isConfirmationState: true}));
        } else {
            this.executeAction();
        }
    }

    executeAction() {
        this.props.dispatch(this.props.action);
        this.reset();
    }

    reset() {
        this.setState(prevState => ({isConfirmationState: false}));
    }

    onSuccess() {
        this.props.onSuccess();
    }

    render() {
        const {
            className,
            label,
            action,
            withConfirm,
            confirmLabel,
            onSuccess
        } = this.props;

        return (
            <div className={`action-link-container`} >
                {
                    !this.state.isConfirmationState &&
                    <span className={`action-link ${className}`} onClick={this.onMainClick}>{label}</span>
                }
                {
                    withConfirm &&
                    this.state.isConfirmationState &&
                    <div className="confirmation">
                        <span className="confirm-label">{confirmLabel}</span>
                        <span className="action-link confirmation-yes" onClick={this.executeAction}>Да</span>
                        &nbsp;
                        <span className="action-link confirmation-no" onClick={this.reset}>Нет</span>
                    </div>
                }
            </div>
        );
    }
}

ActionLink.propTypes = {
    action: PropTypes.object.isRequired,
    label: PropTypes.string,
    className: PropTypes.string,
    withConfirm: PropTypes.bool,
    confirmLabel: PropTypes.string,
    isSuccess: PropTypes.bool,
    onSuccess: PropTypes.func
};

ActionLink.defaultProps = {
    className: '',
    withConfirm: false,
    confirmLabel: 'Вы уверены, что хотите выполнить это действие?',
    onSuccess: () => {}
};

export default connect(
    (state, props) => {
        let result = state.data[props.action.resource];

        if (props.action.payload.id) {
            result = result ? result[props.action.payload.id] : null;
        }

        result = result ? result.state : null;

        return {state, ...props, isSuccess: result};
    }
)(ActionLink);