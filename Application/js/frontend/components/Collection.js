import React from "react";
import PropTypes from "prop-types";

export default class Collection extends React.Component {
    constructor(props) {
        super(props);

        this.onAdd = this.onAdd.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    onAdd() {
        this.props.fields.push({});
        this.props.onSpawnFieldset();
    }

    onDelete(index) {
        this.props.fields.remove(index);
    }

    render() {
        const {
            className,
            children,
            name,
            label,
            fields,
            isEditMode,
            emptyText
        } = this.props;

        const self = this;

        return (
            <div className={`element collection ${className}`}>
                {!!label && <span className="label">{label}</span>}
                {
                    !fields.length &&
                    <span className="form-padding-horizontal">{emptyText}</span>
                }
                {
                    fields.map(function (fieldsetName, index) {
                        let fieldset = [];

                        React.Children.map(children, function (child) {
                            let newChild;

                            let names = [];
                            let mapping = {};

                            const childName = `${fieldsetName}.${child.props.name}`;

                            if (Array.isArray(child.props.names)) {
                                for (let name of child.props.names) {
                                    names.push(`${fieldsetName}.${name}`);
                                }

                                for (let name in child.props.mapping) {
                                    mapping[name] = `${fieldsetName}.${child.props.mapping[name]}`;
                                }

                                newChild = React.cloneElement(child, {
                                    ...child.props,
                                    names: names,
                                    mapping: mapping,
                                    key: childName
                                });
                            } else {
                                newChild = React.cloneElement(child, {
                                    ...child.props,
                                    name: childName,
                                    key: childName
                                });
                            }

                            fieldset.push(newChild);
                        });

                        let deleteBtn = '';

                        if (isEditMode) {
                            deleteBtn = (
                                <button
                                    className="delete-fieldset"
                                    type="button"
                                    key={`col_${name}_delbtn_${index}`}
                                    onClick={() => {
                                        self.onDelete(index)
                                    }}
                                ><span className="fas fa-times"></span>
                                </button>
                            );
                        }

                        return (
                            <div className="collection-item" key={`collection_${name}_${index}`}>
                                <hr/>
                                <div className="fieldset">
                                    {isEditMode && deleteBtn}
                                    <div className="fields">{fieldset}</div>
                                </div>
                            </div>
                        );
                    })
                }
                {
                    isEditMode &&
                    <button
                        className="add-fieldset"
                        key={`col_${name}_addbtn`}
                        type="button"
                        onClick={this.onAdd}
                    ><i className="fas fa-plus">&nbsp;</i>Добавить</button>
                }
            </div>
        );
    }
}

Collection.propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
    name: PropTypes.string.isRequired,
    fields: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    onSpawnFieldset: PropTypes.func,
    isEditMode: PropTypes.bool.isRequired,
    emptyText: PropTypes.string
};

Collection.defaultProps = {
    label: '',
    className: '',
    onSpawnFieldset: () => {},
    emptyText: ''
};

