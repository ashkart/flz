import React from "react";
import PropTypes from "prop-types";
import InputRange from "react-input-range";

import 'react-input-range/lib/css/index.css';
import {clamp} from "../helper/utils";

const DEFAULT_STEP = 1;
export const DEFAULT_RANGE = {min: 0, max: 1000000};

export default class Range extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value,
            range: DEFAULT_RANGE
        };

        this.onChange = this.onChange.bind(this);
        this._keepValueWithin = this._keepValueWithin.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState(prevState => ({
            value: this._keepValueWithin(nextProps, this.state.value),
            range: {min: nextProps.minValue, max: nextProps.maxValue}
        }));
    }

    onChange(value) {
        const newValue = this._keepValueWithin(this.props, value);

        this.setState(prevState => ({value: newValue}), () => this.props.onChange(newValue));
    }

    _keepValueWithin(props, value) {
        return {
            min: clamp(value.min, props.minValue, props.maxValue),
            max: clamp(value.max, props.minValue, props.maxValue),
        };
    }

    render() {
        return (
            <InputRange
                {...this.props}
                minValue={this.state.range.min}
                maxValue={this.state.range.max}
                onChange={this.onChange}
                value={this.state.value}
            />
        );
    }
}

Range.propTypes = {
    value: PropTypes.object.isRequired,
    minValue: PropTypes.number.isRequired,
    maxValue: PropTypes.number.isRequired,
    step: PropTypes.number,
    onChange: PropTypes.func
};

Range.defaultProps = {
    step: DEFAULT_STEP,
    onChange: (value) => {}
};