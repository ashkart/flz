import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

export default class CreateEntity extends React.Component {
    render() {
        const {
            className,
            text,
            btnCaption,
            url
        } = this.props;

        return (
            <div className="create-entity-container">
                <div className={`bg-image ${className}`}>
                    <div className="text">{text}</div>
                    <Link to={url} className="create-entity primary">{btnCaption}</Link>
                </div>
            </div>
        );
    }
}


CreateEntity.propTypes = {
    className: PropTypes.string,
    text: PropTypes.string,
    btnCaption: PropTypes.string,
    url: PropTypes.string.isRequired
};

CreateEntity.defaultProps = {
    className: ''
};
