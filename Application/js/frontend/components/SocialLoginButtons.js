import React from 'react';

const SocialLoginButtons = () => {
    return (
        <>
            <div className="flex-item center">
                Вход через соцсети
            </div>
            <div className="flex-item center social-buttons">
                <a className="vk" href={`/auth/init-vk-auth`}>
                    <img src="/img/vk.icon.png"/>
                </a>
                <a className="fb" href={`/auth/init-fb-auth`}>
                    <img src="/img/fb.icon.png"/>
                </a>
            </div>
        </>
    );
};

export default SocialLoginButtons;