import React, {Children} from "react";
import PropTypes from 'prop-types';
import {connect} from "react-redux";

class FilterController extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filters: this.props.filtersInitState || {}
        };

        this.filterDataAction = this.filterDataAction.bind(this);
        this.updateFilter = this.updateFilter.bind(this);
    }

    filterDataAction(action) {
        this.props.dispatch(action);
    }

    notifyChange() {
        this.props.onChange(this.state.filters);
    }

    updateFilter(filterName, filterValues) {
        let filters = {...this.state.filters};
        filters[filterName] = filterValues;

        this.setState(
            prevState => {return {filters}},
            this.notifyChange
        );
    }

    render() {
        return (
            <div id="filtersContainer">
                <FiltersView
                    dictionary={{...this.props.state.dictionary}}
                    {...this.props}
                    updateFilter={this.updateFilter}
                    filterDataAction={this.filterDataAction}
                />
            </div>
        );
    }
}

class FiltersView extends React.Component {
    render() {
        const {
            dictionary,
            updateFilter,
            filterDataAction
        } = this.props;

        return (
            <div className="filters">
                <h5>Параметры поиска</h5>
                {
                    Children.map(this.props.children, (child) => {
                        return React.cloneElement(child, {
                            ...child.props,
                            ...dictionary[child.props.name],
                            updateFilter: updateFilter,
                            filterDataAction: filterDataAction
                        });
                    })
                }
            </div>
        );
    }
}

FilterController.propTypes = {
    onChange: PropTypes.func.isRequired,
    filtersInitState: PropTypes.object
};

export default connect((state, props) => {
    return {state: state, ...props};
})(FilterController);
