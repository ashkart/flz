import React from "react";
import PropTypes from 'prop-types';
import { SubmissionError } from 'redux-form';
import {getRootUrl} from "../../helper/rootUrl";

const RESPONSE_SUCCESS = 1;
const RESPONSE_FAILURE = 0;

const rootUrl = getRootUrl();

export default function composeForm(FormComponent) {
    const formClass = class Form extends React.Component {
        constructor(props) {
            super(props);

            this.submit = this.submit.bind(this);
            this._processRedirect = this._processRedirect.bind(this);
            this._processSuccess = this._processSuccess.bind(this);
            this._processFailure = this._processFailure.bind(this);
        }

        submit(values) {
            let options = {
                method: 'post',
                headers : new Headers({
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }),
                credentials: 'same-origin',
                body: JSON.stringify(values)
            };

            const url = `${rootUrl}${this.props.action}${document.location.search}`;

            return fetch(url, options)
                .then(response => {
                    if (response.redirected) {
                        this._processRedirect(response.url);
                    }

                    return response.json();
                })
                .then(json => {
                    switch (json.state) {
                        case RESPONSE_SUCCESS:
                            this._processSuccess(json);
                            break;
                        case RESPONSE_FAILURE:
                            this._processFailure(json);
                    }
                })
            ;
        }

        _processRedirect(redirectUrl) {
            window.location.href = redirectUrl;
        }

        _processSuccess(json) {
            this.props.onSuccess();

            if (json.redirect) {
                this._processRedirect(json.redirect);
            }
        }

        _processFailure(json) {
            this.props.onFailure();

            const errors = json.errors;
            const errorTextObject = {_error: this.props.errorText};

            let errorsNormalized = {};

            for (let fieldName in errors) {
                errorsNormalized[fieldName] = errors[fieldName];
            }

            if (Object.values(errorsNormalized).length) {
                throw new SubmissionError(Object.assign({}, errorsNormalized, errorTextObject));
            } else {
                throw new SubmissionError(errorTextObject);
            }
        }

        render() {
            const {
                errorText,
                handleSubmit,
                onFailure,
                onSuccess,
                ...rest
            } = this.props;

            const props = {
                ...rest,
                onSubmit: handleSubmit(this.submit)
            };

            return <FormComponent {...props}/>;
        }
    };

    formClass.propTypes = {
        action: PropTypes.string.isRequired,
        errorText: PropTypes.string.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        onSuccess: PropTypes.func,
        onFailure: PropTypes.func
    };

    formClass.defaultProps = {
        onSuccess: () => {},
        onFailure: () => {}
    };

    return formClass;
}