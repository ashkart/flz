import React from "react";
import {asArray, isEmpty} from "../../helper/utils";
import {change} from 'redux-form';

export default function(FormComponent) {
    return class Form extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                initializedCityId: false,
                initializedCompanyId: false,
                initializedProfarea: false,
                initializedTags: false,

                dictionary: {
                    currencies: {},
                    profarea_id: {},
                    companySuggestions: {},
                    citySuggestions: {},
                    tagSuggestions: {},
                    locationSuggestions: {}
                },

                values: {
                    profarea_id: [],
                    company_id: {},
                    city_id: {},
                    tags: {},
                    contacts: {},
                    locations: {}
                }
            };

            this.fetchComponentData = this.fetchComponentData.bind(this);
            this.onProfareaSelect = this.onProfareaSelect.bind(this);
            this.onCitySelect = this.onCitySelect.bind(this);
            this.onCompanySelect = this.onCompanySelect.bind(this);
            this.onTagSelect = this.onTagSelect.bind(this);
            this.onVacancyLocationSelect = this.onVacancyLocationSelect.bind(this);
            this._changeCustomFieldValue = this._changeCustomFieldValue.bind(this);
        }

        componentWillReceiveProps(nextProps, nextContext) {
            let stateUpdate = Object.assign({}, this.state);
            let dictionary = Object.assign({}, this.state.dictionary);


            let doStateUpdate = false;

            if (
                nextProps.state.dictionary.profarea_id &&
                nextProps.state.dictionary.profarea_id.data !== this.state.dictionary.profarea_id
            ) {
                dictionary.profarea_id = nextProps.state.dictionary.profarea_id.data;

                doStateUpdate = true;
            }

            if (asArray(nextProps.state.dictionary.city_id).length) {
                dictionary.citySuggestions = nextProps.state.dictionary.city_id.suggestions;

                doStateUpdate = true;
            }

            if (asArray(nextProps.state.dictionary.company_id).length) {
                dictionary.companySuggestions = nextProps.state.dictionary.company_id.suggestions;

                doStateUpdate = true;
            }

            if (nextProps.state.dictionary.tags) {
                dictionary.tagSuggestions = nextProps.state.dictionary.tags.suggestions;

                doStateUpdate = true;
            }

            if (nextProps.state.dictionary.locations) {
                dictionary.locationSuggestions = nextProps.state.dictionary.locations.suggestions;

                doStateUpdate = true;
            }

            if (nextProps.state.dictionary.currencies) {
                dictionary.currencies = nextProps.state.dictionary.currencies;

                doStateUpdate = true;
            }

            stateUpdate.dictionary = dictionary;

            if (nextProps.initialValues) {
                if (
                    nextProps.initialValues.city_id &&
                    isEmpty(this.state.values.city_id) &&
                    !this.state.initializedCityId
                ) {
                    stateUpdate.values.city_id = {
                        city: {
                            id: nextProps.initialValues.city_id,
                            title: nextProps.initialValues.city,
                            type: 'city'
                        }
                    };

                    stateUpdate.initializedCityId = true;
                }

                if (
                    nextProps.initialValues.company_id &&
                    isEmpty(this.state.values.company_id) &&
                    !this.state.initializedCompanyId
                ) {
                    stateUpdate.values.company_id = {
                        company: {
                            id: nextProps.initialValues.company_id,
                            title: nextProps.initialValues.company,
                            type: 'company'
                        }
                    };

                    stateUpdate.initializedCompanyId = true;
                }

                if (nextProps.initialValues.profarea_id && isEmpty(this.state.values.profarea_id)) {
                    stateUpdate.values.profarea_id = [nextProps.initialValues.profarea_id];

                }

                if (!isEmpty(nextProps.initialValues.tags) && isEmpty(this.state.values.tags)) {
                    stateUpdate.values.tags = nextProps.initialValues.tags;
                }

                if (!isEmpty(nextProps.initialValues.locations) && isEmpty(this.state.values.locations)) {
                    stateUpdate.values.locations = nextProps.initialValues.locations;
                }

                if (!isEmpty(nextProps.initialValues.contacts) && isEmpty(this.state.values.contacts)) {
                    stateUpdate.values.contacts = nextProps.initialValues.contacts;
                }

                doStateUpdate = true;
            }

            if (doStateUpdate) {
                this.setState(prevState => stateUpdate);
            }
        }

        _createSelectOptions(dataObject) {
            let selectOptions = [];

            for (let key in dataObject) {
                selectOptions.push({value: key, label: dataObject[key]});
            }

            return selectOptions;
        }

        fetchComponentData(action) {
            this.props.dispatch(action);
        }

        onProfareaSelect(selected) {
            this._changeCustomFieldValue('profarea_id', selected, asArray(selected)[0]);
        }

        onCitySelect(selected) {
            this._changeCustomFieldValue('city_id', selected, this._getObjectIdOrNull(selected));
        }

        onCompanySelect(selected) {
            this._changeCustomFieldValue('company_id', selected, this._getObjectIdOrNull(selected));
        }

        onTagSelect(selected) {
            this._changeCustomFieldValue('tags', selected, selected);
        }

        onVacancyLocationSelect(selected) {
            this._changeCustomFieldValue('locations', selected, selected);
        }

        _getObjectIdOrNull(selected) {
            const selectedArray = asArray(selected);

            const selectedData = selectedArray.length ? selectedArray[0] : {};

            let idOrNull = null;

            if (selectedData.hasOwnProperty('id')) {
                idOrNull = selectedData.id;
            }

            return idOrNull;
        }

        _changeCustomFieldValue(fieldName, selected, newInputValue) {
            this.setState(
                prevState => {
                    let values = this.state.values;
                    values[fieldName] = selected;

                    return {values};
                },
                () => {
                    this.props.dispatch(change(this.props.id, fieldName, newInputValue));
                }
            );
        }

        render() {
            return (
                <FormComponent
                    {...this.props}
                    onProfareaSelect={this.onProfareaSelect}
                    onCitySelect={this.onCitySelect}
                    onCompanySelect={this.onCompanySelect}
                    onTagSelect={this.onTagSelect}
                    onVacancyLocationSelect={this.onVacancyLocationSelect}
                    dictionary={this.state.dictionary}
                    fetchComponentData={this.fetchComponentData}
                    dispatch={this.props.dispatch}
                    formCustomValues={this.state.values}
                    createSelectOptions={this._createSelectOptions}
                />
            );
        }
    };
}