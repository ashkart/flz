import React from 'react';

export function withJData(Component) {
    return (props) => {
        let jDataProp = JDATA;

        if (typeof jDataProp !== 'object') {
            jDataProp = {};
        }

        const updateJdata = (newJdata) => {
            JDATA = newJdata;
        };

        return <Component {...props} jData={jDataProp} updateJdata={updateJdata}/>;
    };
}