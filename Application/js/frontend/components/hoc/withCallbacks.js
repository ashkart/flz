import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {asArray} from "../../helper/utils";
import {fetchListScrolledAction, fetchOneAction} from "../../actions/actionCreators/dataFetchActions";

const CALLBACK_CONVERSATION_RESOURCE = 'callback/conversation';

export default function (EntityComponent) {
    const hoc = class WithCallbacks extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                hasMore: true,
                entityCallbacks: [],
                paging: {
                    perPage: 20,
                    page: 1
                }
            };

            this.loadCallbacks = this.loadCallbacks.bind(this);
            this.onOpenCallbackSpoiler = this.onOpenCallbackSpoiler.bind(this);
            this.loadConversation = this.loadConversation.bind(this);
            this.purgeCallback = this.purgeCallback.bind(this);
            this.updateCallback = this.updateCallback.bind(this);
        }

        componentWillReceiveProps(nextProps) {
            const callbacksKey = entityListKey(this.props.callbacksResource, this.props.data.id);

            if (nextProps.state.data[callbacksKey] !== this.props.state.data[callbacksKey]) {
                let currentCallbacks = this.state.entityCallbacks;
                let hasMore = this.state.hasMore;
                let newCallbackPortion = Object.assign({}, nextProps.entityCallbacks);
                delete newCallbackPortion.totalItems;

                currentCallbacks = currentCallbacks.concat(asArray(newCallbackPortion));

                let nextTotalItems = nextProps.entityCallbacks.totalItems || 0;

                if (currentCallbacks.length >= nextTotalItems) {
                    hasMore = false;
                }

                this.setState(prevState => ({
                    entityCallbacks: currentCallbacks,
                    hasMore,
                    paging: {
                        perPage: this.state.paging.perPage,
                        page: this.state.paging.page + 1
                    }
                }));
            }
        }

        updateCallback(index) {
            let entityCallbacks = this.state.entityCallbacks;

            entityCallbacks[index] = this.props.state.data[entityListKey(this.props.callbacksResource, this.props.data.id)][index];

            this.setState(prevState => ({entityCallbacks}));
        }

        loadCallbacks() {
            this.props.dispatch(fetchListScrolledAction(
                entityListKey(this.props.callbacksResource, this.props.data.id),
                {
                    entity: this.props.data[this.props.id],
                    paging: this.state.paging
                }
            ));
        }

        loadConversation(conversationId) {
            this.props.dispatch(fetchOneAction('callback/conversation', {id: conversationId}));
        }

        onOpenCallbackSpoiler() {
            if (!this.state.entityCallbacks.length) {
                this.loadCallbacks();
            }
        }

        purgeCallback(index) {
            let callbacks = Array.from(this.state.entityCallbacks);
            callbacks.splice(index, 1);

            this.setState(prevState => ({entityCallbacks: callbacks}));
        }

        render() {
            const {
                callbacksResource,
                conversations,
                ...rest
            } = this.props;

            return <EntityComponent
                {...rest}
                loadCallbacks={this.loadCallbacks}
                onOpenCallbackSpoiler={this.onOpenCallbackSpoiler}
                hasMoreCallbacks={this.state.hasMore}
                entityCallbacks={this.state.entityCallbacks}
                loadConversation={this.loadConversation}
                conversations={conversations}
                purgeCallback={this.purgeCallback}
                updateCallback={this.updateCallback}
            />;
        }
    };

    hoc.propTypes = {
        entityCallbacks: PropTypes.object,
        callbacksResource: PropTypes.string
    };

    hoc.defaultProps = {
        entityCallbacks: {
            totalItems: 0
        }
    };
    
    return connect((state, props) => {
        const entityCallbacks = state.data[entityListKey(props.callbacksResource, props.data.id)];

        const conversations = state.data[CALLBACK_CONVERSATION_RESOURCE] || {};

        return {state, ...props, entityCallbacks, conversations};
    })(hoc);
}

export const entityListKey = (resource, entityId) => {
    return `${resource}/entity-${entityId}`;
};