import React from "react";
import PropTypes from "prop-types";

export default function composeFilter(FilterComponent) {
    const $filterClass = class Filter extends React.Component {
        constructor(props) {
            super(props);

            this.updateFilter = this.updateFilter.bind(this);
        }

        updateFilter(value) {
            this.props.updateFilter(this.props.name, value);
        }

        render() {
            const props = {...this.props, updateFilter: this.updateFilter};

            return <FilterComponent {...props}/>;
        }
    };

    $filterClass.propTypes = {
        name: PropTypes.string.isRequired
    };

    return $filterClass;
}