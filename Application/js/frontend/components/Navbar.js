import React from "react";
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import { withCookies } from 'react-cookie';
import {
    FILTER_NAME_KEYWORD,
    LOCAL_STORAGE_KEY_UI_STATE,
    ROLE_RECRUITER,
    UI_STATE_KEY_SEARCH_FOR
} from "../helper/constants";
import queryString from "query-string";

import {
    SEARCH_OPTIONS,
    COOKIE_ROLE,
    COOKIE_USERNAME,
    COOKIE_CP_URL,
    ROLE_GUEST,
    ROLE_ADMIN,
    ROLE_EMPLOYEE
} from '../helper/constants';
import {updateUiStateAction} from "../actions/actionCreators/storeUpdateActions";

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        const cookies = this.props.cookies;
        const role = cookies.get(COOKIE_ROLE);

        this.state = {
            searchFor: role === ROLE_EMPLOYEE ? 'vacancy' : 'resume',
            role: role,
            userName: cookies.get(COOKIE_USERNAME),
            query: ''
        };

        this.onSearchForChange = this.onSearchForChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSearchInputChange = this.onSearchInputChange.bind(this);
    }

    componentWillMount() {
        if (typeof this.props.searchFor === 'string' && !!this.props.searchFor) {
            this.setState(prevState => ({
                searchFor: this.props.searchFor
            }));
        }
    }

    onSearchForChange(e) {
        const newValue = e.target.value;

        this.setState(
            prevState => ({searchFor: newValue}),
            () => {this.props.persistUiState(newValue)}
        )
    }

    onSearchInputChange(e) {
        const newValue = e.target.value;

        this.setState(prevState => ({query: newValue}))
    }

    onSearch(e) {
        e.preventDefault();

        window.location.href = this._getSearchFormAction();
    }

    _getSearchFormAction() {
        let params = {
            filters: JSON.stringify({
                [FILTER_NAME_KEYWORD]: {
                    values:{
                        keywords:{
                            title: this.state.query
                        }
                    }
                }
            })
        };

        return `/${this.state.searchFor}/search?${queryString.stringify(params)}`;
    }

    render() {
        return (
            <nav className="navbar navbar-main navbar-static-top flex-container" role="navigation">
                <div className="flex-container nav-content">
                    <Link to="/" className="flex-container column flex-item logo">
                        <span className="flex-item">staffz</span>
                    </Link>

                    <form className="form-inline search-form flex-item" onSubmit={this.onSearch}>
                        <div className="inline" style={{marginBottom: 0}}>
                            <div className='element nav-input'>
                                <input name="query" type="text" className='input' placeholder="Поиск" value={this.state.query} onChange={this.onSearchInputChange}/>
                            </div>
                            <div className='element nav-select'>
                                <label className="select-wrap">
                                    <select name='search_for' className='select' value={this.state.searchFor} onChange={this.onSearchForChange}>
                                        {
                                            SEARCH_OPTIONS.map((option) => {
                                                return <option key={option.value} value={option.value}>{option.label}</option>
                                            })
                                        }
                                    </select>
                                </label>
                            </div>
                            <div className="element nav-submit">
                                <button className="nav-btn-search">
                                    Найти
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul className="flex-item flex-container menu">
                        <li className="flex-item flex-container dropdown job">
                            <div className="dropdown-caption">Работа</div>
                            <ul className="dropdown-content">
                                {
                                    this.state.role !== ROLE_RECRUITER &&
                                    <li><Link to="/resume/edit"><nobr>Создать резюме</nobr></Link></li>
                                }
                                {
                                    this.state.role !== ROLE_EMPLOYEE &&
                                    <li><Link to="/vacancy/edit"><nobr>Разместить вакансию</nobr></Link></li>
                                }
                            </ul>
                        </li>

                        <li className="flex-item flex-container dropdown profile">
                            <div className="dropdown-caption">
                                {
                                    this.state.role === ROLE_GUEST ? 'Профиль' : this.state.userName
                                }
                            </div>
                            {
                                this.state.role === ROLE_GUEST &&
                                <ul className="dropdown-content">
                                    <li><Link to="/auth/index">Вход</Link></li>
                                    <li><Link to="/auth/register">Регистрация</Link></li>
                                </ul>
                            }
                            {
                                this.state.role !== ROLE_GUEST &&
                                <ul className="dropdown-content">
                                    <li><a href="/profile/edit">Профиль</a></li>
                                    <li><a href="/auth/logout">Выход</a></li>
                                </ul>
                            }
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default withCookies(connect(
    (state, props) => {
        const uiState = state.uiState;

        const searchFor = uiState ? uiState[UI_STATE_KEY_SEARCH_FOR] : null;

        return {
            ...props,
            searchFor: searchFor
        }
    },
    (dispatch) => ({
        persistUiState: (value) => { dispatch(updateUiStateAction(UI_STATE_KEY_SEARCH_FOR, value)) }
    })
)(Navbar));