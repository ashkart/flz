import React from "react";
import PropTypes from "prop-types";

export const CHECKED_STATE_NONE = 0;
export const CHECKED_STATE_HAS = 1;
export const CHECKED_STATE_FULL = 2;

export default class Checkbox extends React.Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        this.props.onClick(e);
    }

    render() {
        const {
            label,
            checkedState,
            onClick,
            disabled,
            hideDisabled
        } = this.props;

        let checkedClassName;

        switch(checkedState) {
            case CHECKED_STATE_HAS:
                checkedClassName = 'has-checked';
                break;
            case CHECKED_STATE_FULL:
                checkedClassName = 'checked fas fa-check';
                break;
            default:
                checkedClassName = '';
        }

        if (disabled) {
            checkedClassName = `${checkedClassName} disabled`;
        }

        let hideCheckBox = disabled && hideDisabled;

        return (
            <label className="checkbox-label" onClick={this.onClick}>
                {
                    !hideCheckBox &&
                    <span className={`checkbox ${checkedClassName}`}>
                        <span className="square"></span>
                    </span>
                }
                <span className="label">{label}</span>
            </label>
        );
    }
}

Checkbox.propTypes = {
    label: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    hideDisabled: PropTypes.bool,
    checkedState: PropTypes.oneOf([
        CHECKED_STATE_NONE,
        CHECKED_STATE_HAS,
        CHECKED_STATE_FULL
    ])
};

Checkbox.defaultProps = {
    checkedState: CHECKED_STATE_NONE,
    disabled: false,
    hideDisabled: false,
    onClick: () => {}
};