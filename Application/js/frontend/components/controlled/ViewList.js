import React from 'react';
import PropTypes from 'prop-types';
import {parseDateFromRuFormat} from "../../helper/utils";
import humanReadableDate from "../../helper/humanReadableDate";

const ViewList = (props) => {
    const {
        views
    } = props;

    return (
        <table className="views">
            <tbody>
            {
                views.map(view => {
                    return (
                        <tr key={view.date_time}>
                            <td>
                                <span className="fas fa-eye"></span> {humanReadableDate(parseDateFromRuFormat(view.date_time))}
                            </td>
                    </tr>);
                })
            }
            </tbody>
        </table>
    );
};

ViewList.propTypes = {
    views: PropTypes.array,
    className: PropTypes.string
};

ViewList.defaultProps = {
    views: [],
    className: ''
};

export default ViewList;