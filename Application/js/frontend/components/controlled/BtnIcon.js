import React from "react";
import PropTypes from 'prop-types';

const BtnIcon = function (props) {
    const {
        iconFaClass,
        title,
        className
    } = props;

    return (
        <span
            className={`icon-remove ${iconFaClass} ${className}`}
            onClick={props.onClick}
            title={title}
        ></span>
    );
};

BtnIcon.propTypes = {
    onClick: PropTypes.func.isRequired,
    iconFaClass: PropTypes.string,
    title: PropTypes.string
};

BtnIcon.defaultProps = {
    className: '',
    iconFaClass: 'fas fa-minus-square',
    title: ''
};

export default BtnIcon;