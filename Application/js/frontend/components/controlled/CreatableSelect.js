import React from 'react';
import PropTypes from 'prop-types';
import Creatable from 'react-select/lib/Creatable';

export default class CreatableSelect extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.selected !== this.props.selected;
    }

    render() {
        const {
            isClearable,
            isValidNewOption,
            formatCreateLabel,
            createOptionPosition,
            onChange,
            onInputChange,
            options,
            defaultValue,
            selected,
            name
        } = this.props;

        return <div className="createable-select" style={{width: '100%'}}>
            <Creatable
                isClearable={isClearable}
                classNamePrefix="creatable-select"
                isValidNewOption={isValidNewOption}
                formatCreateLabel={formatCreateLabel}
                createOptionPosition={createOptionPosition}
                onChange={onChange}
                onInputChange={onInputChange}
                options={options}
                placeholder="Выберите или впишите"
                defaultValue={defaultValue}
                value={selected}
            />

            <input type="hidden" name={name}/>
        </div>;
    }
}

CreatableSelect.propTypes = {
    isClearable: PropTypes.bool,
    isValidNewOption: PropTypes.func,
    formatCreateLabel: PropTypes.func,
    createOptionPosition: PropTypes.string,
    onChange: PropTypes.func,
    onInputChange: PropTypes.func,
    defaultValue: PropTypes.object,
    options: PropTypes.array,
    selected: PropTypes.object,
    name: PropTypes.string
};

CreatableSelect.defaultProps = {
    isClearable: false,
    isValidNewOption: (inputValue, selectValue, selectOptions) => true,
    formatCreateLabel: (inputValue) => inputValue ? `Указать "${inputValue}"` : undefined,
    createOptionPosition: 'first',
    onChange: () => {},
    onInputChange: () => {},
    options: [],
};
