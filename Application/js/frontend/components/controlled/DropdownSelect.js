import React from "react";
import PropTypes from 'prop-types';

import attachOnBlurListener from "../../helper/attachOnBlurListener";

import DropdownContainer from "./DropdownContainer";
import ItemHolder from "./ItemHolder";
import Input from "./Input";
import {asArray} from "../../helper/utils";

export default class DropdownSelect extends React.Component {
    constructor(props) {
        super(props);
        
        const selectedItems = Object.values(props.selectedValues);
        
        let inputValue = '';

        if (selectedItems.length && !props.multiSelect) {
            inputValue = selectedItems[0].title;
        }
        
        this.state = {
            showOptions: false,
            options: this.props.options,
            selectedValues: this.props.selectedValues,
            inputValue: inputValue,
            showClearBtn: !!inputValue
        };

        this._inputRef = null;

        /**
         * Индекс для уникального именования ключей генерируемых из пользовательского ввода опций.
         *
         * @type {number}
         * @private
         */
        this._unstrictOptionKeyIterator = 0;

        this.onFocus = this.onFocus.bind(this);
        this.onClearInputClick = this.onClearInputClick.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onRemoveItemClick = this.onRemoveItemClick.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.selectionUpdated = this.selectionUpdated.bind(this);

        this._deleteTheOnlyItem = this._deleteTheOnlyItem.bind(this);
        this._setSelectedValueFromInput = this._setSelectedValueFromInput.bind(this);
        this._setOptionFromInput = this._setOptionFromInput.bind(this);
        this._sanitizeProps = this._sanitizeProps.bind(this);

        const self = this;

        attachOnBlurListener(
            [`input-container-${this.props.selectId}`, this.props.optionsContainerClassName],
            () => self.state.showOptions,
            () => {self.onBlur()}
        );
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.hasOwnProperty('options')) {
            if (Object.keys(this.state.options).length || Object.keys(nextProps.options).length) {
                let nextOptions = nextProps.options;

                if (!this.props.isStrict && !!this.state.inputValue) {
                    nextOptions = Object.assign(
                        {[this.props.selectId]: {title: this.state.inputValue}},
                        nextOptions
                    );
                }

                this.setState(prevState => ({options: nextOptions}));
            }
        }

        if (nextProps.hasOwnProperty('selectedValues')) {
            this.setState(prevState => {
                let stateUpd = {selectedValues: nextProps.selectedValues};

                if (!this.props.multiSelect) {
                    const nextSelectedArray = asArray(nextProps.selectedValues);
                    const currentSelectedArray = asArray(this.state.selectedValues);

                    if (currentSelectedArray.length && !nextSelectedArray.length) {
                        stateUpd.inputValue = '';
                    } else if (
                        nextSelectedArray.length &&
                        currentSelectedArray[0] !== nextSelectedArray[0]
                    ) {
                        stateUpd.inputValue = nextSelectedArray[0].title;
                    }
                }

                return stateUpd;
            });
        }
    }

    onFocus() {
        this.setState(prevState => ({
            showOptions: true,
            showClearBtn: !!this.state.inputValue
        }));
    }

    onClearInputClick() {
        this.setState(
            prevState => {
                let selectedValues = prevState.selectedValues;

                if (!this.props.multiSelect) {
                    selectedValues = {};
                }

                return {
                    inputValue: '',
                    showClearBtn: false,
                    showOptions: false,
                    selectedValues: selectedValues
                };
            },
            () => {
                this.onInputChange('');
                this.selectionUpdated();
            }
        );
    }

    onInputChange(newValue) {
        if (!this.props.allowType) {
            return;
        }

        const onType = this.props.onType;

        const currentInputValue = this.state.inputValue;

        this.setState(
            prevState =>  {
                return {
                    inputValue: newValue,
                    showClearBtn: !!newValue.length
                };
            },
            () => {

                if (!this.props.isStrict) {
                    this._setOptionFromInput(newValue);
                }

                let stateChange;

                if (newValue.length > 1) {
                    stateChange = prevState => ({
                        showOptions: true
                    });
                } else if (!this.props.multiSelect) {
                    stateChange = this._deleteTheOnlyItem;
                }

                this.setState(
                    stateChange,
                    () => {
                        onType(newValue);

                        if (!newValue) {
                            this.selectionUpdated();
                        }
                    }
                );
            }
        );
    }

    onBlur() {
        this.setState(
            (prevState) => ({showOptions: false}),
        );

        if (!this.props.isStrict) {
            if (this.state.inputValue) {
                this._setSelectedValueFromInput();
            } else {
                this.selectionUpdated();
            }
        }
    }

    onSelect(key) {
        const options = this.state.options;

        const newItem = options[key];
        let selectedItems = this.state.selectedValues;
        let inputValue = '';

        if (this.props.multiSelect) {
            selectedItems[key] = newItem;
        } else {
            selectedItems = {[key]: newItem};
            inputValue = newItem.title;
        }

        this.setState(
            prevState => ({
                showOptions: false,
                selectedValues: selectedItems,
                inputValue: inputValue,
                showClearBtn: !this.props.multiSelect
            }),
            this.selectionUpdated
        );
    }

    selectionUpdated() {
        this.props.onSelectionUpdated(this.state.selectedValues);
    }

    onRemoveItemClick(key) {
        this.setState((prevState) => {
            let selectedItems = prevState.selectedValues;
            delete selectedItems[key];

            return {
                ...prevState,
                selectedValues: selectedItems
            };
        }, this.selectionUpdated);
    }

    _deleteTheOnlyItem(prevState) {
        let stateDiff = {
            ...prevState,
            options: {},
            showOptions: false
        };

        delete stateDiff.selectedValues[this.props.selectId];

        return stateDiff;
    }

    _setSelectedValueFromInput() {
        if (this.state.options.hasOwnProperty(this.props.selectId)) {
            this.onSelect(this.props.selectId);
        }
    }

    _setOptionFromInput(currentInput) {
        this.setState(
            prevState => {
                let options = prevState.options;


                let newOption;

                if (this.props.multiSelect) {
                    delete options[`${this.props.selectId}_${this._unstrictOptionKeyIterator}`];
                    newOption = {[`${this.props.selectId}_${++this._unstrictOptionKeyIterator}`]: {title: currentInput}};
                } else {
                    delete options[this.props.selectId];
                    newOption = {[this.props.selectId]: {title: currentInput}};
                }

                options = Object.assign(newOption, options);

                return {options};
            }
        );
    }

    _sanitizeProps() {
        const {
            options,
            onType,
            onSelectionUpdated,
            selectedValues,
            clearBtnIcon,
            clearBtnTitle,
            isStrict,
            ...sanitizedProps
        } = this.props;

        return sanitizedProps;
    }

    render() {
        const {
            multiSelect,
            selectId,
            className,
            optionsContainerClassName,
            renderOption,
            allowType,
            inputProps: {placeholder},
            ...rest
        } = this._sanitizeProps();

        const optionsToRender = this.state.options;

        const hasOptionsToShow = !!Object.keys(optionsToRender).length;

        return (
            <div className={className}>
                {
                    !!Object.keys(this.state.selectedValues).length &&
                    multiSelect &&
                    <ItemHolder
                        items={this.state.selectedValues}
                        onRemoveClick={this.onRemoveItemClick}
                    />
                }
                <Input
                    className={`${allowType ? '' : 'no-caret'}`}
                    inputName={selectId}
                    placeholder={placeholder}
                    onFocus={this.onFocus}
                    onClearInputClick={this.onClearInputClick}
                    onChange={this.onInputChange}
                    value={this.state.inputValue}
                />
                {
                    hasOptionsToShow &&
                    this.state.showOptions &&
                    <DropdownContainer className={`${optionsContainerClassName} suggest-data-container`} {...rest}>
                        {
                            Object.keys(optionsToRender).map((key) => {
                                const option = renderOption(optionsToRender[key]);
                                return React.cloneElement(option, {
                                    ...option.props,
                                    key: key,
                                    onClick: () => {this.onSelect(key)}
                                });
                            })
                        }
                    </DropdownContainer>
                }
            </div>
        );
    }
}

DropdownSelect.propTypes = {
    multiSelect: PropTypes.bool,
    isStrict: PropTypes.bool,
    selectId: PropTypes.string.isRequired,
    inputProps: PropTypes.object,
    className: PropTypes.string,
    optionsContainerClassName: PropTypes.string,
    optionClassName: PropTypes.string,
    renderOption: PropTypes.func.isRequired,
    onSelectionUpdated: PropTypes.func,
    selectedValues: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    allowType: PropTypes.bool
};

DropdownSelect.defaultProps = {
    multiSelect: false,
    isStrict: true,
    selectedValues: {},
    inputProps: () => {},
    onSelectionUpdated: (selectedItems) => {},
    className: '',
    optionsContainerClassName: '',
    options: {},
    allowType: true
};