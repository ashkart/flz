import React from "react";
import PropTypes from 'prop-types';

import BtnIcon from "./BtnIcon";

const SHORTEN_LENGTH_DEFAULT = 21;

export default class ItemHolder extends React.Component {
    checkShortenLabel(text, maxLength) {
        if (text.length <= maxLength) {
            return text;
        }

        return `${text.slice(0, maxLength)}...`;
    }

    render() {
        const {
            items,
            withRemoveBtn,
            onRemoveClick,
            labelMaxLength = SHORTEN_LENGTH_DEFAULT
        } = this.props;

        return <div className="item-holder">
            {
                Object.keys(items).map((key) => {
                    return (
                        <span key={`item-holder_${key}`} className="item">
                            <span className="item-content">
                                <span className="text" title={items[key]['title']}>
                                    <nobr>{this.checkShortenLabel(items[key]['title'], labelMaxLength)}</nobr>
                                </span>
                                {
                                    withRemoveBtn &&
                                    <BtnIcon
                                        onClick={() => {
                                            onRemoveClick(key);
                                        }}
                                        title="Удалить"
                                    />
                                }
                            </span>
                        </span>
                    );
                })
            }
        </div>;
    }
}

ItemHolder.propTypes = {
    items: PropTypes.shape({title: PropTypes.string}),
    withRemoveBtn: PropTypes.bool,
    onRemoveClick: PropTypes.func
};

ItemHolder.defaultProps = {
    withRemoveBtn: true,
    onRemoveClick: () => {}
};