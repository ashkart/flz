import React from "react";

const  DropdownContainer = function(props) {
    return <div {...props}>{props.children}</div>;
};

export default DropdownContainer;