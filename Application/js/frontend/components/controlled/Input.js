import React from 'react';
import PropTypes from 'prop-types';
import BtnIcon from "./BtnIcon";

export default class Input extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showClearBtn: !!props.value,
            hasFocus: false
        };

        this._inputRef = null;

        this.onChange = this.onChange.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState(
            prevState => ({
                showClearBtn: !!nextProps.value.length
            })
        );
    }

    onFocus() {
        this.setState(prevState => ({hasFocus: true}), this.props.onFocus);
    }

    onBlur() {
        this.setState(prevState => ({hasFocus: false}));
    }

    onChange(e) {
        const newValue = e.target.value;
        this.props.onChange(newValue);
    }

    render() {
        const {
            inputName,
            className,
            placeholder,
            onFocus,
            onClearInputClick,
            value,
            clearBtnIcon,
            clearBtnTitle,
            type,
        } = this.props;

        return (
            <div className={`input-with-button input-container-${inputName} ${this.state.hasFocus ? 'has-focus' : ''}`}>
                <input
                    placeholder={placeholder}
                    className={className}
                    onChange={this.onChange}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    type={type}
                    ref={(input) => {this._inputRef = input;}}
                    value={value}
                />
                {
                    <span
                        className="btn-remove-container"
                        onClick={() => {
                            this._inputRef.focus()
                        }}>
                            <BtnIcon
                                className={`clear-input ${!this.state.showClearBtn ? 'hidden' : ''}`}
                                onClick={onClearInputClick}
                                iconFaClass={clearBtnIcon}
                                title={clearBtnTitle}
                            />
                        </span>
                }
            </div>
        );
    }
}

Input.propTypes = {
    inputName: PropTypes.string,
    onFocus: PropTypes.func,
    onChange: PropTypes.func,
    onClearInputClick: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    clearBtnIcon: PropTypes.string,
    clearBtnTitle: PropTypes.string,
    type: PropTypes.string
};

Input.defaultProps = {
    inputName: 'default',
    onFocus: () => {},
    onChange: () => {},
    value: '',
    clearBtnIcon: 'fas fa-backspace',
    type: 'text',
    clearBtnTitle: 'Очистить',
};