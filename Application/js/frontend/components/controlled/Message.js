import React from 'react';
import humanReadableDate from "../../helper/humanReadableDate";
import {parseDateFromRuFormat} from "../../helper/utils";

const Message = (props) => {
    const {
        className,
        text,
        date
    } = props;

    return (
        <div className={`message ${className}`}>
            <div className="row text">{text}</div>
            <div className="row date">{humanReadableDate(parseDateFromRuFormat(date))}</div>
        </div>
    );
};

export default Message;