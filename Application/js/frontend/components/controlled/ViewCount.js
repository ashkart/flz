import React from 'react';
import PropTypes from 'prop-types';

const ViewCount = (props) => {
    const {
        className,
        count,
        title
    } = props;

    let titleProp = {};

    if (title) {
        titleProp.title = title;
    }

    return (
        <div className={`view-count ${className}`}>
            <span className="fas fa-eye" {...titleProp}></span>&nbsp;
            <span className="count">{count}</span>
        </div>
    );
};

ViewCount.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    count: PropTypes.number.isRequired
};

ViewCount.defaultProps = {
    className: '',
    title: ''
};

export default ViewCount;