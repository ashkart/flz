import React from 'react';
import PropTypes from 'prop-types';
import ReactLoading from 'react-loading';

const LoadingSpin = (props) => {
    return <ReactLoading
        type="spin"
        className="loading loading-spin"
        height={props.height} width={props.width}
    />;
};

LoadingSpin.propTypes = {
    height: PropTypes.number,
    width: PropTypes.number
};

LoadingSpin.defaultProps = {
    height: 30,
    width: 30
};

export default LoadingSpin;