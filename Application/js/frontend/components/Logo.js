import React from "react";
import {Link} from 'react-router-dom';

export default function() {
    return <Link to={'/'} className="flex-item center text-logo">staffz</Link>;
}