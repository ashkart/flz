export const UPDATE_UI_STATE = 'UPDATE_UI_STATE';
export const UPDATE_UI_STATE_END = 'UPDATE_UI_STATE_END';

export const updateUiStateAction = (uiKey, value) => {
    return {
        type: UPDATE_UI_STATE,
        payload: {uiKey, value}
    };
};