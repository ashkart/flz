import {CRUD_DELETE, CRUD_GET_LIST, CRUD_GET_ONE, CRUD_UPDATE} from "../crud";

export const GET_PARAMS = 'GET_PARAMS';

export const GET_ONE = 'GET_ONE';
export const GET_LIST = 'GET_LIST';
export const DELETE = 'DELETE';

const THROTTLE = 200;
const THROTTLE_SCROLLED = 1000;
const THROTTLE_SUGGEST = 50;

export const fetchListAction = (resource, payload) => {
    return {
        type: CRUD_GET_LIST,
        fetch: GET_LIST,
        resource: resource,
        payload: payload,
        meta: {
            throttle: THROTTLE
        }
    };
};

export const fetchListScrolledAction = (resource, payload) => {
    return {
        type: CRUD_GET_LIST,
        fetch: GET_LIST,
        resource: resource,
        payload: payload,
        meta: {
            throttle: THROTTLE_SCROLLED
        }
    };
};

export const fetchOneAction = (resource, payload) => {
    return {
        type: CRUD_GET_ONE,
        fetch: GET_ONE,
        resource: resource,
        payload: payload,
        meta: {
            throttle: THROTTLE
        }
    };
};

export const SUGGEST_ACTION = "SUGGEST_ACTION";
export const SUGGEST_ACTION_END = "SUGGEST_ACTION_END";

export const suggestAction = (resource, payload = {}) => {
    const {queryString, ...rest} = payload;
    return {
        type: SUGGEST_ACTION,
        fetch: GET_LIST,
        resource: resource,
        payload: {
            queryString: queryString,
            ...rest
        },
        meta: {
            throttle: THROTTLE_SUGGEST
        }
    };
};

export const FETCH_TREE = 'FETCH_TREE';
export const FETCH_TREE_END = 'FETCH_TREE_END';

export const fetchProfareaTreeAction = (resource, payload = {}) => {
    return {
        type: FETCH_TREE,
        fetch: GET_LIST,
        resource: resource,
        payload: payload,
        meta: {
            throttle: THROTTLE
        }
    };
};

export const FETCH_CURRENCY = 'FETCH_CURRENCY';
export const FETCH_CURRENCY_END = 'FETCH_CURRENCY_END';

export const fetchCurrenciesAction = () => {
    return {
        type: FETCH_CURRENCY,
        fetch: GET_LIST,
        resource: 'dictionary/currency',
        payload: {},
        meta: {
            throttle: THROTTLE
        }
    };
};

export const FETCH_LANGUAGE = 'FETCH_LANGUAGE';
export const FETCH_LANGUAGE_END = 'FETCH_LANGUAGE_END';

export const fetchLanguagesAction = () => {
    return {
        type: FETCH_LANGUAGE,
        fetch: GET_LIST,
        resource: 'dictionary/language',
        payload: {},
        meta: {
            throttle: THROTTLE
        }
    };
};

export const FETCH_RESUME_CONTEXT = 'FETCH_RESUME_CONTEXT';
export const FETCH_RESUME_CONTEXT_END = 'FETCH_RESUME_CONTEXT_END';

export const fetchResumeListContextAction = () => {
    return {
        type: FETCH_RESUME_CONTEXT,
        fetch: GET_PARAMS,
        resource: 'resume/context',
        payload: {}
    };
};

export const FETCH_LIST_FOR_SELECT = 'FETCH_LIST_FOR_SELECT';
export const FETCH_LIST_FOR_SELECT_END = 'FETCH_LIST_FOR_SELECT_END';

export const fetchListForSelect = (resource) => {
    return {
        type: FETCH_LIST_FOR_SELECT,
        fetch: GET_PARAMS,
        resource: resource,
        payload: {}
    };
};

export const updateAction = (url, id, clientListKey = null) => {
    return {
        type: CRUD_UPDATE,
        fetch: GET_ONE,
        resource: url,
        payload: {id, clientListKey}
    };
};

export const deleteAction = (url, id, clientListKey = null) => {
    return {
        type: CRUD_DELETE,
        fetch: GET_ONE,
        resource: url,
        payload: {id, clientListKey}
    };
};