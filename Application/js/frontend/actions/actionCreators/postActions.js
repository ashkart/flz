import {CRUD_UPDATE} from "../crud";

export const CREATE = 'CREATE';
export const UPDATE = 'UPDATE';

export const URL_REVERSE_GEOLOCATE = 'city/geo-reverse';

export const geoReverseAction = (lat, lng) => {
    return {
        type: CRUD_UPDATE,
        fetch: UPDATE,
        resource: URL_REVERSE_GEOLOCATE,
        payload: {lat, lng}
    };
};