export const CRUD_GET_ONE = 'CRUD_GET_ONE';
export const CRUD_GET_LIST = 'CRUD_GET_LIST';
export const CRUD_CREATE = 'CRUD_CREATE';
export const CRUD_UPDATE = 'CRUD_UPDATE';
export const CRUD_DELETE = 'CRUD_DELETE';

export const CRUD_GET_ONE_END = 'CRUD_GET_ONE_END';
export const CRUD_GET_LIST_END = 'CRUD_GET_LIST_END';
export const CRUD_CREATE_END = 'CRUD_CREATE_END';
export const CRUD_UPDATE_END = 'CRUD_UPDATE_END';
export const CRUD_DELETE_END = 'CRUD_DELETE_END';