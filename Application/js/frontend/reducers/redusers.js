import {combineReducers} from 'redux';
import { reducer as formReducer } from 'redux-form';

import dictionary from "./dictionary";
import data from "./data";
import filters from "./filters";
import context from "./context";
import dataStore from "./dataStore";

const reducers = combineReducers({
    context,
    dictionary,
    data,
    filters,
    uiState: dataStore,
    form: formReducer
});

export default reducers;