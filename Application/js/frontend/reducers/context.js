import {FETCH_RESUME_CONTEXT_END} from "../actions/actionCreators/dataFetchActions";

export default (context = {}, action) => {
    let result = Object.assign({}, context);

    switch (action.type) {
        case FETCH_RESUME_CONTEXT_END:
            result['resume'] = Object.assign({}, action.payload.response ? action.payload.response.data || {} : {});
            break;
    }

    return result;
};