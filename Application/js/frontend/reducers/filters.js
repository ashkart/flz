import {CRUD_GET_LIST_END, CRUD_GET_ONE_END} from "../actions/crud";
import {arrayToObject, asArray} from "../helper/utils";

export default (filters = {}, action) => {
    let result = Object.assign({}, filters);

    switch (action.type) {
        case CRUD_GET_LIST_END:
            const responseData = action.payload.response ? action.payload.response.data || {} : {};

            const unexplicitFilters = responseData.unexplicitFilters || {};

            let filterValueResult = {};

            if (asArray(unexplicitFilters).length) {
                for (let name in unexplicitFilters) {
                    if (name === 'region') {
                        filterValueResult[name] = arrayToObject(unexplicitFilters[name]);
                    }
                }

                result = Object.assign(filterValueResult, result);
            }

            break;
    }

    return result;
};