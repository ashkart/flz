import {UPDATE_UI_STATE_END} from "../actions/actionCreators/storeUpdateActions";
import {getInitialState} from '../initialState';

export default (uiState = getInitialState().uiState, action) => {
    let result = uiState;

    switch (action.type) {
        case UPDATE_UI_STATE_END:
            result = action.result || {};
            break;
    }

    return result;
};