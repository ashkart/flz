import {CRUD_DELETE_END, CRUD_GET_LIST_END, CRUD_GET_ONE_END, CRUD_UPDATE_END} from "../actions/crud";
import {FETCH_LIST_FOR_SELECT_END} from "../actions/actionCreators/dataFetchActions";
import {asArray} from "../helper/utils";
import {EL_ID, EL_POSITION_NAME} from "../components/Forms/ResumeEdit";

export default (data = {}, action) => {
    let result = Object.assign({}, data);

    let responseData = action.payload && action.payload.response ? action.payload.response.data : {};

    switch (action.type) {
        case CRUD_GET_ONE_END:
            const responseNewObject = Object.assign({}, responseData);
            let resourceResult = result[action.payload.resource];

            if (!!action.payload.id) {
                resourceResult = typeof resourceResult === 'object'
                    ? resourceResult
                    : {}
                ;

                resourceResult[action.payload.id] = responseNewObject;

                result[action.payload.resource] = resourceResult;
            } else {
                result[action.payload.resource] = responseNewObject;
            }

            break;

        case CRUD_GET_LIST_END:
            result[action.payload.resource] = Object.assign({}, responseData);
            delete result[action.payload.resource]['unexplicitFilters'];
            break;

        case FETCH_LIST_FOR_SELECT_END:
            let responseDataArray = asArray(responseData);
            let selectOptions = [];

            for (let entity of responseDataArray) {
                if (typeof entity === 'object') {
                    selectOptions.push({label: entity[EL_POSITION_NAME], value: entity[EL_ID]});
                }
            }

            result[action.payload.resource] = selectOptions;
            break;

        case CRUD_UPDATE_END:
        case CRUD_DELETE_END:
            if (!action.payload.clientListKey) {
                result[action.payload.resource] = responseData;
                return result;
            }

            for (let i in result[action.payload.clientListKey]) {
                if (result[action.payload.clientListKey][i].id == action.payload.id) {
                    if (action.type === CRUD_DELETE_END) {
                        delete result[action.payload.clientListKey][i];
                        break;
                    }

                    if (action.type === CRUD_UPDATE_END) {
                        result[action.payload.clientListKey][i] = responseData;

                        if (action.payload.id) {
                            result[action.payload.resource] = {[action.payload.id]: {state: true}};
                        } else {
                            result[action.payload.resource] = {state: true};
                        }

                        break;
                    }
                }
            }

            break;
    }

    return result;
};