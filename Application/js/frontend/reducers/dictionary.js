import {
    SUGGEST_ACTION_END,
    FETCH_TREE_END,
    FETCH_CURRENCY_END,
    FETCH_LANGUAGE_END, FETCH_LIST_FOR_SELECT_END
} from "../actions/actionCreators/dataFetchActions";

export default (dictionary = {}, action) => {
    let dictionaryResult = Object.assign({}, dictionary);

    let responseData = action.payload && action.payload.response ? action.payload.response.data : {};

    switch (action.type) {
        case FETCH_CURRENCY_END:
            dictionaryResult.currencies = responseData;
            break;

        case FETCH_LANGUAGE_END:
            dictionaryResult.languages = responseData;
            break;

        case SUGGEST_ACTION_END:
            let suggestions = {};

            for (let i in responseData) {
                suggestions[`${action.payload.resource}_${responseData[i].id || i}`] = responseData[i];
            }

            dictionaryResult[action.payload.name] = {suggestions};
            break;

        case FETCH_TREE_END:
            if (Array.isArray(responseData) && !responseData.length) {
                responseData = {};
            }

            dictionaryResult[action.payload.name] = {data: responseData};
            break;
    }

    return dictionaryResult;
}
