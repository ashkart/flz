import React from "react";
import {createStore, applyMiddleware} from "redux";
import createSagaMiddleware from "redux-saga";
import throttledMiddleware from './middleware/throttled';
import reducers from "./reducers/redusers";

const configureStore = () => {
    const sagaMiddleware = createSagaMiddleware();

    return {
        ...createStore(reducers, applyMiddleware(sagaMiddleware, throttledMiddleware)),
        runSaga: sagaMiddleware.run
    };
};

export default configureStore;
