/**
 * @param date {Date}
 * @return {string}
 */
export default function(date) {
    let dateStr;

    let dateOptions = {
        hour: 'numeric',
        minute: 'numeric'
    };

    const dateWithoutTime = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    let now = new Date();
    now = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    const timeDiff = Math.abs(now.getTime() - dateWithoutTime.getTime());
    const daysDiff = timeDiff / (1000 * 3600 * 24);

    switch (true) {
        case daysDiff === 1:
            return `Вчера в ${date.toLocaleString('ru', dateOptions)}`;

        case daysDiff < 1:
            return `Сегодня в ${date.toLocaleString('ru', dateOptions)}`;

        default:
            dateOptions = {
                month: 'long',
                day: 'numeric'
            };

            if (now.getFullYear() - date.getFullYear() >= 1) {
                dateOptions.year = 'numeric';
            }

            return date.toLocaleString('ru', dateOptions);
    }
};