export const validate = values => {
    const errors = {};

    if (!values.user_name) {
        errors.user_name = 'Укажите имя пользователя.';
    }

    if (!values.login) {
        errors.login = 'Укажите электронную почту.';
    }

    if (!values.password) {
        errors.password = 'Задайте пароль.';
    }

    if (!values.password_repeat) {
        errors.password_repeat = 'Повторите пароль.';
    }

    if (values.password !== values.password_repeat) {
        errors.password_repeat = 'Повторный ввод пароля не совпадает.';
    }

    return errors;
};