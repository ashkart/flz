export function getRootUrl() {
    return `${window.location.protocol}//${window.location.host}`;
}