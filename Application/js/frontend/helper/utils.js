/**
 * Пуст ли массив/объект
 */
export function isEmpty(arrayOrObject) {
    if (typeof arrayOrObject !== "object") {
        return true;
    }

    return Array.isArray(arrayOrObject) ? !arrayOrObject.length : !Object.values(arrayOrObject).length;
}

/**
 * Проверит, является ли аргумент массивом или объектом и вернет значения в виде массива в любом случае.
 */
export function asArray(arrayOrObject) {
    if (typeof arrayOrObject !== "object") {
        return [];
    }

    return Array.isArray(arrayOrObject) ? arrayOrObject : Object.values(arrayOrObject);
}

export function arrayToObject(array) {
    let object = {};

    for (let i = 0; i < array.length;i++) {
        object[i] = array[i];
    }

    return object;
}

/**
 * @param date {Date|null} locale 'en'
 * @param withTime {boolean}
 *
 * @returns {string|null}
 */
export function normalizeDate(date, withTime = false) {
    if (!date) {
        return null;
    }

    let options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric'
    };

    if (withTime === true) {
        options.hour = 'numeric';
        options.minute = 'numeric';
    }

    return date.toLocaleString('ru', options);
}

/**
 *
 * @param dateStr {string} locale 'ru'
 * @returns {Date}
 */
export function parseDateFromRuFormat(dateStr) {
    if (!dateStr) {
        return null;
    }

    dateStr = dateStr.replace(/(\d+)\.(\d+)\.(\d+)/, '$2/$1/$3');

    return new Date(Date.parse(dateStr));
}

/**
 * @param date {Date}
 * @returns {string}
 */
export function dateAsMonthAndYear(date) {
    let options = {
        year: 'numeric',
        month: 'long'
    };

    return date.toLocaleString('ru', options);
}

export function pluralRu(number, one, two, five) {
    let n = Math.abs(number);
    n %= 100;

    if (n >= 5 && n <= 20) {
        return five;
    }

    n %= 10;
    if (n === 1) {
        return one;
    }

    if (n >= 2 && n <= 4) {
        return two;
    }

    return five;
}

export function clamp(number, min, max) {
    return Math.max(min, Math.min(number, max));
}

export function findKeyByValue(objectsArray, value, keyName, valueName) {
    for (let {[keyName]: key, [valueName]: val} of objectsArray) {
        if (val == value) {
            return key;
        }
    }

    return null;
}

export function resolveObjectPath(path, object) {
    return path.replace('[', '.').replace(']', '').split('.').reduce(
        (result, currentKey) => {
            return result ? result[currentKey] : null;
        },
        object
    );
}

/**
 * Возвращает разницу между датами в количестве полных месяцев.
 *
 * @param dateOne {Date}
 * @param dateTwo {Date}
 * @return {number}
 */
export function dateDiffMonths(dateOne, dateTwo) {
    const INITIAL_YEAR = 1970; /* 01/01/1970 00:00:00.000 */

    let diff = Math.abs(dateTwo - dateOne);
    let diffDate = new Date(diff);
    let years = diffDate.getFullYear() - INITIAL_YEAR;

    return diffDate.getMonth() + (years * 12);
}

/**
 * @param date {Date}
 *
 * @return {string}
 */
export function humanDateNoDays(date) {
    let options = {
        year: 'numeric',
        month: 'long'
    };

    return date.toLocaleString('ru', options);
}

/**
 * @param string {string}
 * @returns {string}
 */
export function jsUcFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * @param number {number|string}
 */
export function formatNumber(number) {
    const numberOptions = {
        useGrouping: true,
    };

    return parseInt(number).toLocaleString('ru', numberOptions);
}

export function pluralString(number, one, two, five) {
    return `${number} ${pluralRu(number, one, two, five)}`;
}

export function pluralYears(number) {
    return pluralString(number, 'год', 'года', 'лет');
}

export function pluralMonths(number) {
    return pluralString(number, 'месяц', 'месяца', 'месяцев');
}