export const LOCAL_STORAGE_KEY_UI_STATE = 'uiState';

export const UI_STATE_KEY_FILTER_VACANCY = 'ui_state_key_filter_vacancy';
export const UI_STATE_KEY_FILTER_RESUME = 'ui_state_key_filter_resume';
export const UI_STATE_KEY_SEARCH_FOR = 'ui_state_key_search_for';

export const LIST_VACANCY = 'vacancy/list';
export const LIST_RESUME = 'resume/list';

export const LIST_PROFILE_CALLBACKS = "profile/callback-list";
export const LIST_PROFILE_VACANCIES = "profile/vacancy-list";
export const LIST_PROFILE_RESUMES = "profile/resume-list";

export const OPF_LIST = [
    {value: "ООО",label: "ООО"},
    {value: "ОАО",label: "ОАО"},
    {value: "ЗАО",label: "ЗАО"},
    {value: "ПАО",label: "ПАО"},
    {value: "АО",label: "АО"},
    {value: "УП",label: "УП"},
    {value: "ГУ",label: "ГУ"},
    {value: "ТОО",label: "ТОО"},
    {value: "ИП",label: "ИП"},
    {value: "НО",label: "НО"},
    {value: "",label: "Другое"}
];

export const EMPLOYMENT_REGULAR = 'full_day';
export const EMPLOYMENT_PART_TIME = 'part_time';
export const EMPLOYMENT_SHIFT = 'shift';
export const EMPLOYMENT_WATCH = 'watch';

export const EMPLOYMENT_LIST = [
    {label: 'Полная', value: EMPLOYMENT_REGULAR},
    {label: 'Частичная', value: EMPLOYMENT_PART_TIME},
    {label: 'Посменно', value: EMPLOYMENT_SHIFT},
    {label: 'Вахта', value: EMPLOYMENT_WATCH}
];

export const EXP_NOT_REQUIRED = 0;
export const EXP_LTE_1_YEAR = 1;
export const EXP_LTE_2_YEARS = 2;
export const EXP_LTE_3_YEARS = 3;
export const EXP_LTE_5_YEARS = 5;
export const EXP_GT_5_YEARS   = 6;

export const EXPERIENCE_OPTIONS = [
    {label: 'Не требуется', value: EXP_NOT_REQUIRED},
    {label: 'До года', value: EXP_LTE_1_YEAR},
    {label: 'От 1 до 2 лет', value: EXP_LTE_2_YEARS},
    {label: 'От 2 до 3 лет', value: EXP_LTE_3_YEARS},
    {label: 'От 3 до 5 лет', value: EXP_LTE_5_YEARS},
    {label: 'Более 5 лет', value: EXP_GT_5_YEARS}
];

export const PUBLISH_PERIODS = [
    {label: 'За сутки', value: 1},
    {label: 'За неделю', value: 2},
    {label: 'За 2 недели', value: 3},
    {label: 'За месяц', value: 4},
    {label: 'За все время', value: 5},
];

export const EL_VISIBILITY = 'visibility';

export const VISIBILITY_ALL  = 'all';
export const VISIBILITY_NONE = 'none';

export const SEARCH_FOR_VACANCY = 'vacancy';
export const SEARCH_FOR_RESUME = 'resume';
export const SEARCH_FOR_JOB = 'job';
export const SEARCH_FOR_FREELANCER = 'freelancer';

export const FILTER_NAME_KEYWORD = 'keywords';

export const SEARCH_OPTIONS = [
    {value: SEARCH_FOR_VACANCY, label: 'Вакансии'},
    {value: SEARCH_FOR_RESUME, label: 'Резюме'},
    /*{value: SEARCH_FOR_JOB, label: 'Заказы'},
    {value: SEARCH_FOR_FREELANCER, label: 'Фрилансеры'},*/
];

export const GENDER_MALE = 'male';
export const GENDER_FEMALE = 'female';

export const GENDER = [
    {value: GENDER_MALE, label: 'Мужской'},
    {value: GENDER_FEMALE, label: 'Женский'},
];

export const EDUCATION_BASIC_GENERAL = 'basic_general';
export const EDUCATION_SECONDARY_GENERAL = 'secondary_general';
export const EDUCATION_SECONDARY_VOCATIONAL = 'secondary_vocational';
export const EDUCATION_HIGHER_BACHELOR = 'higher_bachelor';
export const EDUCATION_HIGHER_MASTER = 'higher_master';

export const EDUCATION = [
    {value: EDUCATION_BASIC_GENERAL, label: 'Основное общее'},
    {value: EDUCATION_SECONDARY_GENERAL, label: 'Общее среднее'},
    {value: EDUCATION_SECONDARY_VOCATIONAL, label: 'Среднее профессиональное'},
    {value: EDUCATION_HIGHER_BACHELOR, label: 'Высшее, бакалавриат'},
    {value: EDUCATION_HIGHER_MASTER, label: 'Высшее, магистратура'}
];

export const LANG_LEVEL_BASE  = 'base';
export const LANG_LEVEL_TECH  = 'tech';
export const LANG_LEVEL_SPEAK = 'speak';
export const LANG_LEVEL_FREE  = 'free';

export const LANG_LEVELS = [
    {value: LANG_LEVEL_BASE, label: 'Базовый'},
    {value: LANG_LEVEL_TECH, label: 'Технический (чтение литературы)'},
    {value: LANG_LEVEL_SPEAK, label: 'Разговорный'},
    {value: LANG_LEVEL_FREE, label: 'Свободное владение'},
];

export const COOKIE_ROLE = 'role';
export const COOKIE_USERNAME = 'username';
export const COOKIE_USER_ID = 'userId';
export const COOKIE_CP_URL = 'cpURL';
export const COOKIE_CITY_ID = 'cityId';

export const ROLE_GUEST     = 'guest';
export const ROLE_EMPLOYEE  = 'employee';
export const ROLE_RECRUITER = 'recruiter';
export const ROLE_ADMIN     = 'admin';

export const ROLES = [
    {value: ROLE_EMPLOYEE, label: 'Соискатель'},
    {value: ROLE_RECRUITER, label: 'Компания'},
    {value: ROLE_ADMIN, label: 'Администратор'},
];

export const AGE_14_18   = 0;
export const AGE_18_30   = 1;
export const AGE_30_40   = 2;
export const AGE_40_50   = 3;
export const AGE_50_60   = 4;
export const AGE_FROM_60 = 5;

export const AGES = [
    {value: AGE_14_18, label: 'от 14 до 18'},
    {value: AGE_18_30, label: 'от 18 до 30'},
    {value: AGE_30_40, label: 'от 30 до 40'},
    {value: AGE_40_50, label: 'от 40 до 50'},
    {value: AGE_50_60, label: 'от 50 до 60'},
    {value: AGE_FROM_60, label: 'старше 60'},
];

export const PASSWORD_CHANGE_INITIATED = 'Письмо со ссылкой на подтверждение нового пароля отправлено на электронную почту, привязанную к аккаунту.';
export const PASSWORD_RESET_INITIATED = 'Письмо со ссылкой на подтверждение нового пароля отправлено на указанную электронную почту.';