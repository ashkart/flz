/**
 * @param blurElementClasses {string[]}
 * @param ifNeedCheckBlur {function}
 * @param onBlurCallback {function}
 */
export default (blurElementClasses, ifNeedCheckBlur, onBlurCallback) => {
    const hasDomTreeElementClass = (element, classNames) => {
        const elementClassName = element.getAttribute('class');

        for (let className of classNames) {
            if (elementClassName && elementClassName.indexOf(className) !== -1) {
                return true;
            }
        }

        if (element.parentElement && element.parentElement.tagName !== 'body') {
            return hasDomTreeElementClass(element.parentElement, classNames);
        }

        return false;
    };

    document.getElementsByTagName('html')[0].addEventListener('click', (e) => {
        if (ifNeedCheckBlur() && e.target) {
            if (!hasDomTreeElementClass(e.target, blurElementClasses)) {
                onBlurCallback();
            }
        }
    });
}