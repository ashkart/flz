import {GET_PARAMS, GET_LIST, GET_ONE} from "./actions/actionCreators/dataFetchActions";
import {getRootUrl} from "./helper/rootUrl";
import {UPDATE} from "./actions/actionCreators/postActions";

const rootUrl = getRootUrl();

export default function dataProvider(type, resource, params) {
    const convertDataRequestToHttp = (type, resource, params) => {
        let url = '';

        const options = {
            headers : new Headers({
                Accept: 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }),
            credentials: 'same-origin'
        };

        switch (type) {
            case GET_PARAMS:
                url = `${rootUrl}/${resource}`;
                break;
            case GET_ONE:
                url = `${rootUrl}/${resource}`;

                for (let key in params) {
                    if (params[key] !== undefined) {
                        url = `${url}/${key}-${params[key]}`;
                    }
                }

                break;
            case GET_LIST:
                let filters = '';
                let query = '';
                let paging = '';

                if (typeof params.filters === "object") {
                    filters = `filter=${JSON.stringify(params.filters)}&`;
                }

                if (params.paging) {
                    paging = `perPage=${params.paging.perPage}&page=${params.paging.page}&`;
                }

                if (params.queryString) {
                    query = `query=${encodeURIComponent(params.queryString)}`;
                }

                url = `${rootUrl}/${resource}?${filters}${paging}${query}`;

                break;

            case UPDATE:
                options.headers.append('Content-Type', 'application/json');
                options.method = 'post';
                options.body = JSON.stringify(params);

                url = `${rootUrl}/${resource}`;
                break;
        }

        return {url, options};
    };

    const extractDataFromHttpResponse = (response, type, resource, params) => {
        if (response.redirected) {
            window.location.href = response.url;
            return;
        }

        if (response.status !== 200) {
            return Promise.reject(response);
        }

        return response.json().then((json => {

            if (json.state === 0) {
                return Promise.reject(json.errors);
            }

            switch(type) {
                case GET_PARAMS:
                case GET_ONE:
                case GET_LIST:
                case UPDATE:
                    return { data: json };
            }
        }));
    };

    let { url, options } = convertDataRequestToHttp(type, resource, params);

    return fetch(url, options)
        .then(response => extractDataFromHttpResponse(response, type, resource, params))
        .catch(error => {
            console.log(error);
        })
    ;
}
