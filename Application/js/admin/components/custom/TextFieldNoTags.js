import React from 'react';
import { TextField } from 'react-admin';
import sanitizeRestProps from '../../sanitizeRestProps';
import get from 'lodash/get';

'use strict';

export default class TextFieldNoTags extends TextField {
    render() {
        let textContent = get(this.props.record, this.props.source);

        let regex = /(<([^>]+)>)/gi;
        textContent = typeof textContent === 'string' ? textContent.replace(regex, '') : textContent;

        return (
            <span className={this.props.className} {...sanitizeRestProps(this.props)}>
                {textContent}
            </span>
        );
    }
}