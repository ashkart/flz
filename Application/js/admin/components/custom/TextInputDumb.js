import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { addField, FieldTitle } from 'react-admin';

import sanitizeRestProps from '../../sanitizeRestProps';

export class TextInputDumb extends Component {
    constructor(props) {
        super(props);

        this.handleBlur = this.handleBlur.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleBlur(eventOrValue) {
        this.props.onBlur(eventOrValue);
    };

    handleFocus(event) {
        this.props.onFocus(event);
    };

    handleChange(eventOrValue) {
        this.props.onChange(eventOrValue);
    };

    render() {
        const {
            className,
            isRequired,
            label,
            meta,
            options,
            resource,
            source,
            type,
            ...rest
        } = this.props;

        const { touched, error } = meta || {};

        return (
            <TextField
                margin="normal"
                type={type}
                label={
                    <FieldTitle
                        label={label}
                        source={source}
                        resource={resource}
                        isRequired={isRequired}
                    />
                }
                error={!!(touched && error)}
                helperText={touched && error}
                className={className}
                {...options}
                {...sanitizeRestProps(rest)}
                onBlur={this.handleBlur}
                onFocus={this.handleFocus}
                onChange={this.handleChange}
            />
        );
    }
}

TextInputDumb.propTypes = {
    className: PropTypes.string,
    input: PropTypes.object,
    isRequired: PropTypes.bool,
    label: PropTypes.string,
    meta: PropTypes.object,
    name: PropTypes.string,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    options: PropTypes.object,
    resource: PropTypes.string,
    source: PropTypes.string,
    type: PropTypes.string,
};

TextInputDumb.defaultProps = {
    onBlur: () => {},
    onChange: () => {},
    onFocus: () => {},
    options: {},
    type: 'text',
};
