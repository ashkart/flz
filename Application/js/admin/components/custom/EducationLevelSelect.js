import React from 'react';
import {SelectInput} from 'react-admin';

export default class EducationLevelSelect extends React.Component {
    render() {
        return (
            <SelectInput
                allowEmpty
                source={this.props.source}
                choices={[
                    {id: 'basic_general', name: 'Basic general'},
                    {id: 'secondary_general', name: 'Secondary general'},
                    {id: 'secondary_vocational', name: 'Secondary vocational'},
                    {id: 'higher_bachelor', name: 'Higher bachelor'},
                    {id: 'higher_master', name: 'Higher master'},
                    {id: 'higher_hq', name: 'Highest qualification'}
                ]}
            />
        );
    }
}