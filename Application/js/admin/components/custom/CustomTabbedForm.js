import React, { Children, Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
    reduxForm,
    getFormAsyncErrors,
    getFormSyncErrors,
    getFormSubmitErrors,
    SubmissionError
} from 'redux-form';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import Divider from '@material-ui/core/Divider';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import { getDefaultValues, translate } from 'ra-core';
import { CREATE, UPDATE, showNotification as showNotificationAction} from 'react-admin';
import { push as pushAction } from 'react-router-redux';
import Toolbar  from 'ra-ui-materialui/lib/form/Toolbar';

import dataProvider from "../../providers/dataProvider";

const styles = theme => ({
    form: {
        [theme.breakpoints.up('sm')]: {
            padding: '0 1em 1em 1em',
        },
        [theme.breakpoints.down('xs')]: {
            padding: '0 1em 5em 1em',
        },
    },
    errorTabButton: { color: theme.palette.error.main },
});

const sanitizeRestProps = ({
   anyTouched,
   array,
   asyncValidate,
   asyncValidating,
   autofill,
   blur,
   change,
   clearAsyncError,
   clearFields,
   clearSubmit,
   clearSubmitErrors,
   destroy,
   dirty,
   dispatch,
   form,
   handleSubmit,
   initialize,
   initialized,
   initialValues,
   pristine,
   pure,
   redirect,
   reset,
   resetSection,
   save,
   submit,
   submitFailed,
   submitSucceeded,
   submitting,
   touch,
   translate,
   triggerSubmit,
   untouch,
   valid,
   validate,
   ...props
}) => props;

/*
 * Zend validation adapted catch(e) method.
 * Formatted as
 * e = {
 *    field_name: { errorType: 'messageText' }
 * }
 */
const submit = (data, resource) => {
    let actionType = data.id ? UPDATE : CREATE;

    return dataProvider(actionType, resource, {data: {...data}}).catch(e => {
        let errorObject = {};

        for (let fieldName in e) {
            let fieldErrors = e[fieldName];

            errorObject[fieldName] = Object.values(fieldErrors).map(value => `${value}\n`);
        }

        throw new SubmissionError(errorObject);
    });
};

export class CustomTabbedForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
        };
    }

    handleChange(event, value) {
        this.setState({ value });
    }

    handleSubmitWithRedirect(redirect = this.props.redirect) {
        return this.props.handleSubmit(data => {
            return submit(data, this.props.resource).then((result) => {
                let path;

                switch (redirect) {
                    case 'create':
                        path = `/${this.props.resource}/create`;
                        break;
                    case 'edit':
                        path = `/${this.props.resource}/${result.data.id}`;
                        break;
                    case 'show':
                        path = `/${this.props.resource}/${result.data.id}/show`;
                        break;
                    default:
                        path = `/${this.props.resource}`;
                }

                this.props.dispatch(this.props.showNotification(`${this.props.resource} saved`));

                return this.props.dispatch(this.props.push(path));
            });
        });
    }

    render() {
        const {
            basePath,
            children,
            className,
            classes = {},
            invalid,
            pristine,
            record,
            resource,
            submitOnEnter,
            tabsWithErrors,
            toolbar,
            translate,
            version,
            ...rest
        } = this.props;

        return (
            <form
                className={classnames('tabbed-form', className)}
                key={version}
                {...sanitizeRestProps(rest)}
            >
                <Tabs
                    scrollable
                    value={this.state.value}
                    onChange={this.handleChange}
                    indicatorColor="primary"
                >
                    {Children.map(
                        children,
                        (tab, index) =>
                            tab ? (
                                <Tab
                                    key={tab.props.label}
                                    label={translate(tab.props.label, {
                                        _: tab.props.label,
                                    })}
                                    value={index}
                                    icon={tab.props.icon}
                                    className={classnames(
                                        'form-tab',
                                        tabsWithErrors.includes(
                                            tab.props.label
                                        ) && this.state.value !== index
                                            ? classes.errorTabButton
                                            : null
                                    )}
                                />
                            ) : null
                    )}
                </Tabs>
                <Divider />
                <div className={classes.form}>
                    {/* All tabs are rendered (not only the one in focus), to allow validation
                    on tabs not in focus. The tabs receive a `hidden` property, which they'll
                    use to hide the tab using CSS if it's not the one in focus.
                    See https://github.com/marmelab/react-admin/issues/1866 */}
                    {Children.map(
                        children,
                        (tab, index) =>
                            tab &&
                            React.cloneElement(tab, {
                                resource,
                                record,
                                basePath,
                                hidden: this.state.value !== index,
                            })
                    )}
                    {toolbar &&
                    React.cloneElement(toolbar, {
                        className: 'toolbar',
                        handleSubmitWithRedirect: this
                            .handleSubmitWithRedirect,
                        invalid,
                        pristine,
                        submitOnEnter,
                    })}
                </div>
            </form>
        );
    }
}

TabbedForm.propTypes = {
    basePath: PropTypes.string,
    children: PropTypes.node,
    className: PropTypes.string,
    classes: PropTypes.object,
    defaultValue: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    handleSubmit: PropTypes.func, // passed by redux-form
    invalid: PropTypes.bool,
    pristine: PropTypes.bool,
    push: PropTypes.func,
    record: PropTypes.object,
    redirect: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    resource: PropTypes.string,
    save: PropTypes.func, // the handler defined in the parent, which triggers the REST submission
    showNotification: PropTypes.func,
    submitOnEnter: PropTypes.bool,
    tabsWithErrors: PropTypes.arrayOf(PropTypes.string),
    toolbar: PropTypes.element,
    translate: PropTypes.func,
    validate: PropTypes.func,
    version: PropTypes.number,
};

TabbedForm.defaultProps = {
    submitOnEnter: true,
    toolbar: <Toolbar />,
};

const collectErrors = state => {
    const syncErrors = getFormSyncErrors('record-form')(state);
    const asyncErrors = getFormAsyncErrors('record-form')(state);
    const submitErrors = getFormSubmitErrors('record-form')(state);

    return {
        ...syncErrors,
        ...asyncErrors,
        ...submitErrors,
    };
};

export const findTabsWithErrors = (
    state,
    props,
    collectErrorsImpl = collectErrors
) => {
    const errors = collectErrorsImpl(state);

    return Children.toArray(props.children).reduce((acc, child) => {
        const inputs = Children.toArray(child.props.children);

        if (inputs.some(input => errors[input.props.source])) {
            return [...acc, child.props.label];
        }

        return acc;
    }, []);
};

const enhance = compose(
    connect((state, props) => {
        const children = Children.toArray(props.children).reduce(
            (acc, child) => [...acc, ...Children.toArray(child.props.children)],
            []
        );

        return {
            tabsWithErrors: findTabsWithErrors(state, props),
            initialValues: getDefaultValues(state, { ...props, children }),
            push: pushAction,
            showNotification: showNotificationAction,
        };
    }),
    translate, // Must be before reduxForm so that it can be used in validation
    reduxForm({
        form: 'record-form',
        destroyOnUnmount: false,
        enableReinitialize: true,
    }),
    withStyles(styles)
);

export default enhance(CustomTabbedForm);