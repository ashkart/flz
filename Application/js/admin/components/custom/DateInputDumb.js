import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { addField, FieldTitle } from 'react-admin';

import sanitizeRestProps from 'ra-ui-materialui/lib/input/sanitizeRestProps';

/**
 * Convert Date object to String
 *
 * @param {Date} v value to convert
 * @returns {String} A standardized date (yyyy-MM-dd), to be passed to an <input type="date" />
 */
const dateFormatter = v => {
    if (!(v instanceof Date) || isNaN(v)) return;
    const pad = '00';
    const yyyy = v.getFullYear().toString();
    const MM = (v.getMonth() + 1).toString();
    const dd = v.getDate().toString();
    return `${yyyy}-${(pad + MM).slice(-2)}-${(pad + dd).slice(-2)}`;
};

const sanitizeValue = value => {
    // null, undefined and empty string values should not go through dateFormatter
    // otherwise, it returns undefined and will make the input an uncontrolled one.
    if (value == null || value === '') {
        return '';
    }

    const finalValue = typeof value instanceof Date ? value : new Date(value);
    return dateFormatter(finalValue);
};

export class DateInputDumb extends Component {
    render() {
        const {
            onChange,
            className,
            meta,
            isRequired,
            label,
            options,
            source,
            resource,
            ...rest
        } = this.props;

        const { touched, error } = meta || {};
        const value = sanitizeValue(this.props.value);

        return (
            <TextField
                className={className}
                type="date"
                margin="normal"
                error={!!(touched && error)}
                helperText={touched && error}
                label={
                    <FieldTitle
                        label={label}
                        source={source}
                        resource={resource}
                        isRequired={isRequired}
                    />
                }
                InputLabelProps={{
                    shrink: true,
                }}
                {...options}
                {...sanitizeRestProps(rest)}
                value={value}
                onChange={onChange}
                onBlur={this.onBlur}
            />
        );
    }
}

DateInputDumb.propTypes = {
    classes: PropTypes.object,
    className: PropTypes.string,
    input: PropTypes.object,
    isRequired: PropTypes.bool,
    label: PropTypes.string,
    meta: PropTypes.object,
    options: PropTypes.object,
    resource: PropTypes.string,
    source: PropTypes.string,
};

DateInputDumb.defaultProps = {
    options: {},
};