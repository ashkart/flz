import React from "react";
import { Button } from 'react-admin';
import PropTypes from 'prop-types';
import CollectionController from "./CollectionController";

const styles = {
    collectionContainer: {
        display: 'block',
        width: '100%'
    },

    fieldset: {
        display: 'flex',
        flexFlow: 'row wrap',
        alignContent: 'center',

        fields: {
            flexBasis: '200px',
            marginRight: '20px',
        }
    }
};

class CollectionView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
        this.state.items = props.items || {};

        this.childrenTemplate = {};

        React.Children.map(this.props.children, (fieldset, index) => {
            if (index === 0) { /* only for one fs */
                React.Children.map(fieldset.props.children, child => {
                    this.childrenTemplate[child.props.source] = null;
                });
            }
        });

        this.handleItemRemove = this.handleItemRemove.bind(this);
        this.handleItemAdd = this.handleItemAdd.bind(this);
    }

    handleItemRemove(actionTarget) {
        let items = this.state.items;

        delete items[actionTarget];

        this.setState({ items: items });
    }

    handleItemAdd() {
        let fieldsets = this.state.items;

        fieldsets[`newItem`] = {...this.childrenTemplate};

        let newItemsList = {};

        for (let i in fieldsets) {
            newItemsList[`${this.props.name}_item${Object.keys(newItemsList).length}`] = fieldsets[i];
        }

        this.setState({ items: newItemsList });
    }

    render() {
        let fieldSets = [];

        for (let itemName in this.state.items) {
            fieldSets.push(
                <div style={styles.collectionContainer} key={itemName}>
                    {React.Children.map(this.props.children, (child) => {
                        return React.cloneElement(
                            child,
                            {
                                checkEmpty: true,
                                data: this.state.items[itemName],
                                name: itemName,
                                style: styles.fieldset
                            }
                        );
                    })}
                    {Object.keys(this.state.items).length > 1 &&
                    <Button
                        name="btn_remove"
                        type="button"
                        onClick={event => {
                            this.handleItemRemove(itemName);
                        }}
                    >
                        Remove
                    </Button>
                    }
                </div>
            );
        }

        return (
            <div>
                {fieldSets}
                <br />
                <Button
                    style={{display: 'block'}}
                    name="btn_add"
                    type="button"
                    onClick={this.handleItemAdd}
                >
                    Add
                </Button>
            </div>
        );
    }
}

export default class Collection extends React.Component {
    render() {
        const {
            model,
            sort,
            filter,
            children,
            pagination = {perPage: 100, page: 1},
            name,
            ...rest
        } = this.props;

        return (
            <CollectionController model={model} filter={filter} sort={sort} pagination={pagination}>
                <CollectionView name={name} {...rest}>
                    {React.Children.map(children, child => child)}
                </CollectionView>
            </CollectionController>
        );
    }
}

CollectionView.propTypes = {
    items: PropTypes.object,
    name: PropTypes.string.isRequired
};