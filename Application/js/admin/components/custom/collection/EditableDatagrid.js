import React from 'react';
import {Button} from 'react-admin';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import TextFieldNoTags from '../TextFieldNoTags';
import CollectionController from './CollectionController';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    progress: { marginTop: '1em' },
};

class EditableDatagridView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        let data = {};
        let propsData = props.data ? Object.values(props.data) : [];

        propsData &&
        propsData.map(record => {
            data[record.id] = record;
            return record;
        });

        this.state.data = data;
        this.state.editMode = {};
    }

    handleEditClick(id) {
        let rowsEditMode = this.state.editMode;

        rowsEditMode[id] = true;
        this.setState({editMode: rowsEditMode});
    }

    handleSaveClick(key) {

        this.props.handleSave(this.state.data[key]);

        let rowsEditMode = this.state.editMode;

        rowsEditMode[key] = false;
        this.setState({editMode: rowsEditMode});
    }

    handleDeleteClick(key) {
        let rows = this.state.data;

        delete rows[key];
        this.setState({data: rows});
    }

    handleAddClick() {
        let rows = this.state.data;
        let newKey = (new Date()).getTime();
        rows[newKey] = {};

        React.Children.map(this.childrenTemplate, child => {
            rows[newKey][child.props.source] = '';

            return child;
        });

        let rowsEditMode = this.state.editMode;
        rowsEditMode[newKey] = true;

        this.setState({editMode: rowsEditMode, data: rows});
    }

    handleChange(key, fieldName, newValue) {
        let rows = this.state.data;
        rows[key][fieldName] = newValue;

        this.setState({data: rows});
    }

    isRowEmpty(key) {

    }

    render() {
        return (
            <table cellSpacing="0" style={{width: '100%'}}>
                <thead style={{textAlign: 'left'}}>
                    <tr key="header">
                        {
                            React.Children.map(this.props.children, child => {
                                return <th style={{padding: '12px'}}>{child.props.source}</th>;
                            })
                        }
                        <th colSpan="2"></th>
                    </tr>
                </thead>
                <tbody>
                {
                    Object.keys(this.state.data).map(id => {
                        let row = this.state.data[id];
                        let isEditMode = !!this.state.editMode[id];

                        let rowResult = React.Children.map(this.props.children, field => {
                            return (
                                <td key={`${field.props.source}_${id}`} style={{height: '50px', padding: '12px'}}>
                                    {
                                        isEditMode &&
                                        React.cloneElement(
                                            field,
                                            {
                                                style: {margin: '0'},
                                                label: field.props.label || field.props.source,
                                                value: row[field.props.source],
                                                onChange: eventOrValue => {
                                                    let value = null;
                                                    switch(true) {
                                                        case eventOrValue.hasOwnProperty('target'):
                                                            value = eventOrValue.target.value;
                                                            break;

                                                        case eventOrValue.hasOwnProperty('preventDefault'):
                                                            value = {...eventOrValue};
                                                            delete value['preventDefault'];

                                                            value = Object.values(value).join('');
                                                            break;
                                                    }
                                                    this.handleChange(id, field.props.source, value);
                                                }
                                            }
                                        )
                                    }
                                    {
                                        !isEditMode &&
                                        <TextFieldNoTags source={field.props.source} record={row}/>
                                    }
                                </td>
                            );
                        });

                        rowResult.push([
                            <td key={`btn_${id}`} style={{width: '60px', textAlign: 'right'}}>
                                {
                                    isEditMode &&
                                    <Button
                                        onClick={event => {
                                            this.handleSaveClick(id);
                                        }}
                                    >
                                        Save
                                    </Button>
                                }
                                {
                                    !isEditMode &&
                                    <Button
                                        onClick={event => {
                                            this.handleEditClick(id);
                                        }}
                                    >
                                        Edit
                                    </Button>
                                }
                            </td>,
                            <td key={`btnDelete_${id}`} style={{width: '60px', textAlign: 'right'}}>
                                <Button
                                    onClick={event => {
                                        this.handleDeleteClick(id);
                                    }}
                                >
                                    Delete
                                </Button>
                            </td>
                        ]);

                        return <tr key={id}>{rowResult}</tr>;
                    })
                }
                </tbody>
                <tfoot>
                <tr>
                    <td colSpan={this.state.data ? Object.keys(this.state.data).length + 1 : 2}
                        style={{padding: '12px 0', textAlign: 'left'}}>
                        <Button
                            style={{margin: '0', textAlign: 'left'}}
                            onClick={event => this.handleAddClick()}
                        >
                            Add
                        </Button>
                    </td>
                </tr>
                </tfoot>
            </table>
        );
    }
}

EditableDatagridView.propTypes = {
    handleSave: PropTypes.func.isRequired
};

class EditableDatagrid extends React.Component {
    render() {
        const {
            name,
            children
        } = this.props;

        return (
            <CollectionController {...this.props}>
                    {
                        controllerProps => {
                            const {
                                classes = {}
                            } = this.props;

                            if (controllerProps.isLoading) {
                                return <LinearProgress className={classes.progress} />;
                            }

                            return (
                                <EditableDatagridView name={name} {...this.props} {...controllerProps}>
                                    {React.Children.map(children, child => {
                                        return React.cloneElement(child);
                                    })}
                                </EditableDatagridView>
                            );
                        }
                    }
            </CollectionController>
        );
    }
}

const EnhancedEditableDatagrid = withStyles(styles)(EditableDatagrid);

export default EnhancedEditableDatagrid;