import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    crudGetManyReference as crudGetManyReferenceAction,
    crudCreate as crudCreateAction
} from '../../../../lib/ra-core/src/actions/dataActions';
import {
    SORT_ASC,
    SORT_DESC,
} from '../../../../lib/ra-core/src/reducer/admin/resource/list/queryReducer';
import {
    getIds,
    getReferences,
    nameRelatedTo,
} from '../../../../lib/ra-core/src/reducer/admin/references/oneToMany';

export class CollectionController extends Component {
    constructor(props) {
        super(props);
        this.state = { sort: props.sort };

        this.saveRow = this.saveRow.bind(this);
    }

    componentDidMount() {
        this.fetchReferences();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.record.id !== nextProps.record.id) {
            this.fetchReferences(nextProps);
        }
    }

    setSort(field) {
        const order =
            this.state.sort.field === field &&
            this.state.sort.order === SORT_ASC
                ? SORT_DESC
                : SORT_ASC;
        this.setState({ sort: { field, order } }, this.fetchReferences);
    };

    fetchReferences(
        { reference, record, resource, target, perPage, filter, source } = this
            .props
    ) {
        const { crudGetManyReference } = this.props;
        const pagination = { page: 1, perPage };
        const relatedTo = nameRelatedTo(
            reference,
            record[source],
            resource,
            target,
            filter
        );
        crudGetManyReference(
            reference,
            target,
            record[source],
            relatedTo,
            pagination,
            this.state.sort,
            filter
        );
    }

    saveRow(row) {
        const {
            reference,
            basePath
        } = this.props;

        let data = row;
        data[this.props.target] = this.props.record.id;
        
        this.props.crudCreate(reference, data, basePath);
    }

    render() {
        const {
            resource,
            reference,
            data,
            ids,
            children,
            basePath,
        } = this.props;

        const referenceBasePath = basePath.replace(resource, reference);

        return children({
            handleSave: this.saveRow,
            currentSort: this.state.sort,
            data,
            ids,
            isLoading: typeof ids === 'undefined',
            referenceBasePath,
            setSort: this.setSort,
        });
    }
}

function mapStateToProps(state, props) {
    const relatedTo = nameRelatedTo(
        props.reference,
        props.record[props.source],
        props.resource,
        props.target,
        props.filter
    );
    return {
        data: getReferences(state, props.reference, relatedTo),
        ids: getIds(state, relatedTo),
    };
}

CollectionController.propTypes = {
    basePath: PropTypes.string.isRequired,
    children: PropTypes.func.isRequired,
    crudGetManyReference: PropTypes.func,
    crudCreate: PropTypes.func,
    filter: PropTypes.object,
    ids: PropTypes.array,
    perPage: PropTypes.number,
    record: PropTypes.object,
    reference: PropTypes.string.isRequired,
    data: PropTypes.object,
    resource: PropTypes.string.isRequired,
    sort: PropTypes.shape({
        field: PropTypes.string,
        order: PropTypes.oneOf(['ASC', 'DESC']),
    }),
    source: PropTypes.string.isRequired,
    target: PropTypes.string.isRequired,
    isLoading: PropTypes.bool,
};

const ConnectedCollectionController = connect(mapStateToProps, {
    crudGetManyReference: crudGetManyReferenceAction,
    crudCreate: crudCreateAction,
})(CollectionController);

ConnectedCollectionController.defaultProps = {
    filter: {},
    perPage: 25,
    sort: { field: 'id', order: 'DESC' },
    source: 'id',
};

export default ConnectedCollectionController;