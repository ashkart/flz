import React from "react";
import { Button } from 'react-admin';
import TextFieldNoTags from '../TextFieldNoTags';

export default class Fieldset extends React.Component {
    constructor(props) {
        super(props);

        this.state = props.data;

        let initialMode;

        if (this.props.checkEmpty && !this.isEmpty()) {
            initialMode = 'view';
        } else {
            initialMode = 'edit';
        }

        this.state.mode = initialMode;

        this.containerStyle = {...props.style} || {};
        delete this.containerStyle.fields;

        this.childrenStyle = props.style.fields || {};
    }

    handleSave() {
        this.setState({mode: 'view'});
    }

    handleEdit() {
        this.setState({ mode: "edit" });
    }

    handleChange(fieldName, eventOrValue) {
        let fields = this.state;

        switch(true) {
            case eventOrValue.hasOwnProperty('target'):
                fields[fieldName] = eventOrValue.target.value;
                break;

            case eventOrValue.hasOwnProperty('preventDefault'):
                let valueCopy = {...eventOrValue};
                delete valueCopy['preventDefault'];

                fields[fieldName] = Object.values(valueCopy).join('');
                break;
        }

        this.setState({ ...fields });
    }

    isEmpty() {
        let hasValue = false;

        React.Children.map(this.props.children, child => {
            hasValue |= !!this.state[child.props.source];
            return child;
        });

        return !hasValue;
    }

    render() {
        let elements;

        const {children = []} = this.props;

        if (this.state.mode === "edit") {
            let canSave = this.props.checkEmpty ? !this.isEmpty() : true;

            elements = (
                <div style={this.containerStyle}>
                    {
                        React.Children.map(children, (child, index) => {
                            return <div
                                key={`${this.props.name}_${index}`}
                                style={this.childrenStyle}>
                                {
                                    React.cloneElement(child, {
                                        source: `${this.props.name}_${child.props.source}`,
                                        label: child.props.label || child.props.source,
                                        value: this.state[child.props.source],
                                        onChange: (event) => {
                                            this.handleChange(child.props.source, event);
                                        }})
                                }
                                </div>
                            ;
                        })
                    }
                    {
                        canSave &&
                        <Button onClick={this.handleSave.bind(this)} style={{marginLeft: 'auto'}}>
                            Confirm
                        </Button>
                    }
                </div>
            );
        } else if (this.state.mode === "view") {
            let recordProp = {...this.state};
            delete recordProp.mode;

            elements = (
                <div key={index} style={this.containerStyle}>
                    {
                        children.map((child, index) => {
                            return (<div style={this.childrenStyle}><TextFieldNoTags  record={recordProp} source={child.props.source} value={this.state[child.props.source]} /></div>);
                        })
                    }
                    <Button onClick={this.handleEdit.bind(this)} style={{marginLeft: 'auto'}}>
                        Edit
                    </Button>
                </div>
            );
        }

        return elements;
    }
}