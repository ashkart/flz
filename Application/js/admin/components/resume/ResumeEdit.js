import React from 'react';
import {
    TabbedForm,
    FormTab,
    Edit,
    List,
    Datagrid,
    NumberInput,
    TextInput,
    DateInput,
    TextField,
    DateField,
    ReferenceManyField,
    EditButton,
    SimpleFormIterator,
    ArrayInput
} from 'react-admin';
import RichTextInput from 'ra-input-rich-text';

import EducationLevelSelect from "../custom/EducationLevelSelect";

'use strict';

export default class ResumeCreate extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Edit {...this.props}>
                <TabbedForm>
                    <FormTab label="Common">
                        <TextInput source="position_name"/>
                        <NumberInput source="desired_paycheck"/>
                        <EducationLevelSelect
                            source="education_level"
                        />
                        <RichTextInput source="autodescription"/>
                    </FormTab>
                    <FormTab label="Education">
                        <ArrayInput source="education">
                            <SimpleFormIterator>
                                <TextField source="id"/>
                                <TextInput source="specialty" />
                                <TextInput source="educational_organization" />
                                <DateInput source="date_start"/>
                                <DateInput source="date_end"/>
                            </SimpleFormIterator>
                        </ArrayInput>
                    </FormTab>
                    <FormTab label="Work experience">
                        <ArrayInput source="work_experience">
                            <SimpleFormIterator>
                                <TextField source="id"/>
                                <TextInput source="position_name" />
                                <TextInput source="description" />
                                <DateInput source="date_start"/>
                                <DateInput source="date_end"/>
                            </SimpleFormIterator>
                        </ArrayInput>
                    </FormTab>
                </TabbedForm>
            </Edit>
        );
    }
}