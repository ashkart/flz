import React from 'react';
import { Show, TextField, RichTextField, SimpleShowLayout } from 'react-admin';

'use strict';

export default class ResumeShow extends React.Component {
    render() {
        return (
            <Show {...this.props}>
                <SimpleShowLayout>
                    <TextField source="position_name"/>
                    <TextField source="desired_paycheck"/>
                    <TextField source="education_level"/>
                    <RichTextField source="autodescription"/>
                </SimpleShowLayout>
            </Show>
        );
    }
}