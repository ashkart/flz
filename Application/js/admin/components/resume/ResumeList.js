import React from 'react';
import { List, Datagrid, TextField, ShowButton } from 'react-admin';
import TextFieldNoTags from "../custom/TextFieldNoTags";

'use strict';

export default class ResumeList extends React.Component {
    render() {
        return (
            <List {...this.props} perPage={10}>
                <Datagrid>
                    <TextField source="id" />
                    <TextField source="position_name" />
                    <TextField source="education_level" />
                    <TextFieldNoTags source="autodescription" />
                    <TextField source="desired_paycheck" />
                    <ShowButton basePath="/resume" />
                </Datagrid>
            </List>
        );
    }
}