import React from 'react';
import {
    TabbedForm,
    FormTab,
    Create,
    NumberInput,
    SelectInput,
    TextInput,
    TextField,
    DateField,
    Datagrid
} from 'react-admin';
import RichTextInput from 'ra-input-rich-text';

'use strict';

export default class ResumeCreate extends React.Component {
    render() {
        return (
            <Create {...this.props}>
                <TabbedForm>
                    <FormTab label="Common">
                        <TextInput source="position_name"/>
                        <NumberInput source="desired_paycheck"/>
                        <SelectInput
                            allowEmpty
                            source="education_level"
                            choices={[
                                {id: 'basic_general', name: 'Basic general'},
                                {id: 'secondary_general', name: 'Secondary general'},
                                {id: 'secondary_vocational', name: 'Secondary vocational'},
                                {id: 'higher_bachelor', name: 'Higher bachelor'},
                                {id: 'higher_master', name: 'Higher master'},
                                {id: 'higher_hq', name: 'Highest qualification'}
                            ]}
                        />
                        <RichTextInput source="autodescription"/>
                    </FormTab>
                </TabbedForm>
            </Create>
        );
    }
}