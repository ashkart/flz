import React from 'react';
import { List, Datagrid, TextField, ShowButton } from 'react-admin';

'use strict';

export default class UserList extends React.Component {
    render() {
        return (
            <List {...this.props} perPage={10}>
                <Datagrid>
                    <TextField source="id"/>
                    <TextField source="role"/>
                    <TextField source="email"/>
                    <TextField source="first_name"/>
                    <TextField source="last_name"/>
                    <TextField source="middle_name"/>
                    <TextField source="birth_date"/>
                    <TextField source="last_visit_date"/>
                    <ShowButton basePath="/user" />
                </Datagrid>
            </List>
        );
    }
}