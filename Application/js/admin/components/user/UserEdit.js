import React from 'react';
import { Edit, TextInput, DateInput, DisabledInput } from 'react-admin';
import CustomForm from "../custom/CustomForm";

'use strict';

export default class UserEdit extends React.Component {
    render() {
        return (
            <Edit {...this.props}>
                <CustomForm>
                    <TextInput source="email"/>
                    <TextInput source="first_name"/>
                    <TextInput source="last_name"/>
                    <TextInput source="middle_name"/>
                    <DateInput source="birth_date"/>
                    <TextInput source="role"/>
                    <DisabledInput source="last_visit_date"/>
                </CustomForm>
            </Edit>
        );
    }
}