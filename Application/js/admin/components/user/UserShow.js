import React from 'react';
import { Show, SimpleShowLayout, TextField } from 'react-admin';

'use strict';

export default class UserShow extends React.Component {
    render() {
        return (
            <Show {...this.props}>
                <SimpleShowLayout>
                    <TextField source="email"/>
                    <TextField source="first_name"/>
                    <TextField source="last_name"/>
                    <TextField source="middle_name"/>
                    <TextField source="birth_date"/>
                    <TextField source="role"/>
                    <TextField source="last_visit_date"/>
                </SimpleShowLayout>
            </Show>
        );
    }
}