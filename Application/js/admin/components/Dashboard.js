import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import { Title } from 'react-admin';

function getFrontendMainPageUrl() {
    let domains = window.location.host.split('.');
    domains.splice(0, 1);

    return `${window.location.protocol}//${domains.join('.')}`;
}

export default () => (
    <Card>
        <Title title="Действия" />
        <CardActions style={{ justifyContent: 'flex-start' }}>
            <Button href={getFrontendMainPageUrl()}>
                <HomeIcon style={{ paddingRight: '0.5em' }} />
                Покинуть панель управления
            </Button>
        </CardActions>
    </Card>
);