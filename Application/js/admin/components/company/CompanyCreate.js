import React from 'react';
import { Create, TextInput, DateInput, DisabledInput } from 'react-admin';
import CustomForm from "../custom/CustomForm";

export default class CompanyEdit extends React.Component {
    render() {
        return (
            <Create {...this.props}>
                <CustomForm>
                    <TextInput source="opf"/>
                    <TextInput source="official_name"/>
                    <TextInput source="full_with_opf"/>
                    <TextInput source="inn"/>
                    <TextInput source="kpp"/>
                    <TextInput source="ogrn"/>
                    <TextInput source="address"/>
                </CustomForm>
            </Create>
        );
    }
}