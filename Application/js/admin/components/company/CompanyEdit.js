import React from 'react';
import { Edit, TextInput, DateInput, DisabledInput } from 'react-admin';
import CustomForm from "../custom/CustomForm";

export default class CompanyEdit extends React.Component {
    render() {
        return (
            <Edit {...this.props}>
                <CustomForm>
                    <TextInput source="opf"/>
                    <TextInput source="official_name"/>
                    <TextInput source="full_with_opf"/>
                    <TextInput source="address"/>
                </CustomForm>
            </Edit>
        );
    }
}