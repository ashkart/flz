import React from 'react';
import { List, Datagrid, TextField, ShowButton } from 'react-admin';

export default class CompanyList extends React.Component {
    render() {
        return (
            <List {...this.props} perPage={10}>
                <Datagrid>
                    <TextField source="id"/>
                    <TextField source="opf"/>
                    <TextField source="official_name"/>
                    <ShowButton basePath="/company" />
                </Datagrid>
            </List>
        );
    }
}