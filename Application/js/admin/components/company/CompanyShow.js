import React from 'react';
import {Show, SimpleShowLayout, TextField, EditButton} from 'react-admin';

export default class CompanyShow extends React.Component {
    render() {
        return (
            <Show {...this.props}>
                <SimpleShowLayout>
                    <TextField source="opf"/>
                    <TextField source="official_name"/>
                    <TextField source="full_with_opf"/>
                    <TextField source="inn"/>
                    <TextField source="kpp"/>
                    <TextField source="ogrn"/>
                    <TextField source="address"/>
                </SimpleShowLayout>
            </Show>
        );
    }
}