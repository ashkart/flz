import React from 'react';
import { List, Datagrid, TextField, ShowButton } from 'react-admin';
import PaycheckField from './PaycheckField';
import TextFieldNoTags from "../custom/TextFieldNoTags";

'use strict';

export default class VacancyList extends React.Component {
    render() {
        return (
            <List {...this.props} perPage={10}>
                <Datagrid>
                    <TextField source="id" />
                    <TextField source="position_name" />
                    <TextFieldNoTags source="requirements" />
                    <TextFieldNoTags source="liabilities" />
                    <TextFieldNoTags source="conditions" />
                    <TextField source="schedule_type" />
                    <PaycheckField source="paycheck" />
                    <ShowButton basePath="/vacancy" />
                </Datagrid>
            </List>
        );
    }
}