import React from 'react';
import {
    FormDataConsumer,
    Create,
    DisabledInput,
    LongTextInput,
    ReferenceInput,
    SelectInput,
    SimpleForm,
    TextInput,
    BooleanInput,
    AutocompleteInput
} from 'react-admin';
import RichTextInput from 'ra-input-rich-text';
import CustomForm from '../custom/CustomForm';
import AutocompleteArrayInput from '../custom/AutocompleteArrayInput';

export default class VacancyCreate extends React.Component {
    render() {
        return (
            <Create {...this.props}>
                <CustomForm>
                    <TextInput name="position_name" source="position_name"/>
                    <ReferenceInput source="city_id" reference="city">
                        <AutocompleteInput optionText="title" translateChoice={false}/>
                    </ReferenceInput>
                    <ReferenceInput label="Профобласть" source="profarea_id" reference="profarea">
                        <AutocompleteArrayInput
                            optionText="title"
                            translateChoice={false}
                            listStyle={{overflowY: 'auto', maxHeight: '350px'}}
                        />
                    </ReferenceInput>
                    <RichTextInput name="requirements" source="requirements" />
                    <RichTextInput name="liabilities" source="liabilities"/>
                    <RichTextInput name="conditions" source="conditions"/>
                    <RichTextInput name="description" source="description"/>
                    <SelectInput
                        source="schedule_type"
                        choices={[
                            {id: 'full_day', name: 'Full day'},
                            {id: 'flexible', name: 'Flexible'},
                            {id: 'shift', name: 'Shift'},
                            {id: 'watch', name: 'Watch'},
                        ]}
                        defaultValue="full_day"
                    />
                    <SelectInput
                        source="paycheck"
                        choices={[
                            {id: 'fixed', name: 'Fixed'},
                            {id: 'range', name: 'Range'},
                        ]}
                        defaultValue="fixed"
                    />
                    <FormDataConsumer>
                        {({formData}) => {
                            if (formData.paycheck === 'fixed') {
                                return <TextInput source="paycheck_const" label="Paycheck"/>
                            } else if (formData.paycheck === 'range') {
                                return (
                                    <div>
                                        <TextInput source="paycheck_min" label="From"/>
                                        <TextInput source="paycheck_max" label="To"/>
                                    </div>
                                );
                            }
                        }}
                    </FormDataConsumer>
                    <BooleanInput source="is_remote" defaultValue={false}/>
                </CustomForm>
            </Create>
        );
    }
}