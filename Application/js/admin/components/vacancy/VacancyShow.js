import React from 'react';
import {
    Show,
    SimpleShowLayout,
    TextField,
    EditButton,
    RichTextField,
    BooleanField,
    FormDataConsumer,
    ReferenceField,
    DateField
} from 'react-admin';
import PaycheckField from './PaycheckField';

'use strict';

export default class VacancyShow extends React.Component {
    render() {
        return (
            <Show {...this.props}>
                <SimpleShowLayout>
                    <TextField source="position_name"/>
                    <TextField source="city_name"/>
                    <ReferenceField source="profarea_id" reference="profarea">
                        <TextField source="title" />
                    </ReferenceField>
                    <PaycheckField source="paycheck" />
                    <RichTextField source="requirements"/>
                    <RichTextField source="liabilities"/>
                    <RichTextField source="conditions"/>
                    <RichTextField source="description"/>
                    <TextField source="schedule_type" />
                    <BooleanField source="is_remote"/>
                    <DateField source="update_date"/>
                </SimpleShowLayout>
            </Show>
        );
    }
}