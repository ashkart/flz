import React from 'react';
import {
    Edit,
    FormDataConsumer,
    Datagrid,
    LongTextInput,
    SelectInput,
    TextInput,
    BooleanInput,
    ReferenceInput,
    AutocompleteInput,
    TextField
} from 'react-admin';
import RichTextInput from 'ra-input-rich-text';
import CustomForm from "../custom/CustomForm";

import { translate } from 'react-admin';
import AutocompleteArrayInput from "../custom/AutocompleteArrayInput";

class VacancyEdit extends React.Component {
    render() {
        return (
            <Edit {...this.props}>
                <CustomForm>
                    <TextInput source="position_name" />
                    <TextField source="city_name" label="Current city" />
                    <ReferenceInput source="city_id" reference="city">
                        <AutocompleteInput optionText="title" translateChoice={false}/>
                    </ReferenceInput>
                    <ReferenceInput label="Профобласть" source="profarea_id" reference="profarea">
                        <AutocompleteArrayInput
                            optionText="title"
                            translateChoice={false}
                            listStyle={{overflowY: 'auto', maxHeight: '350px'}}
                        />
                    </ReferenceInput>
                    <RichTextInput source="requirements" />
                    <RichTextInput source="liabilities"/>
                    <RichTextInput source="conditions"/>
                    <RichTextInput source="description"/>
                    <SelectInput
                        source="schedule_type"
                        choices={[
                            {id: 'full_day', name: 'Full day'},
                            {id: 'flexible', name: 'Flexible'},
                            {id: 'shift', name: 'Shift'},
                            {id: 'watch', name: 'Watch'},
                        ]}
                        defaultValue="full_day"
                    />
                    <SelectInput
                        source="paycheck"
                        choices={[
                            {id: 'fixed', name: 'paycheckChoices.fixed'},
                            {id: 'range', name: 'paycheckChoices.range'},
                        ]}
                        defaultValue="fixed"
                    />
                    <FormDataConsumer>
                        {({formData}) => {
                            if (formData.paycheck === 'fixed') {
                                return <TextInput source="paycheck_const" label={'resources.vacancy.fields.paycheck_const'} />
                            } else if (formData.paycheck === 'range') {
                                return (
                                    <div>
                                        <TextInput source="paycheck_min" label={'resources.vacancy.fields.paycheck_min'} />
                                        <TextInput source="paycheck_max" label={'resources.vacancy.fields.paycheck_max'} />
                                    </div>
                                );
                            }
                        }}
                    </FormDataConsumer>
                    <BooleanInput source="is_remote" />
                </CustomForm>
            </Edit>
        );
    }
}

export default translate(VacancyEdit);