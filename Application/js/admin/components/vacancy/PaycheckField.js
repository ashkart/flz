import React from 'react';
import {TextField} from 'react-admin';

import { translate } from 'react-admin';

'use strict';

class PaycheckField extends TextField {
    constructor(props) {
        super(props);

        let minPaycheck = parseInt(props.record['paycheck_min'], 10);
        let maxPaycheck = parseInt(props.record['paycheck_max'], 10);
        let constPaycheck = parseInt(props.record['paycheck_const'], 10);

        this._paycheck = '';

        if (constPaycheck) {
            this._paycheck = constPaycheck;
        } else {
            if (minPaycheck) {
                this._paycheck = `paycheckField.rangeFrom ${minPaycheck} `;
            }

            if (maxPaycheck) {
                this._paycheck = `paycheckField.rangeTo ${maxPaycheck}`;
            }
        }
    }

    render() {
        this.props.record['paycheck'] = this._paycheck;

        return super.render();
    }
}

export default PaycheckField;