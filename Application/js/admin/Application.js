'use strict';

import React from 'react';
import { Admin, Resource } from 'react-admin';
import dataProvider from './providers/dataProvider';

import Dashboard from './components/Dashboard';
import CompanyCreate from './components/company/CompanyCreate';
import CompanyEdit from './components/company/CompanyEdit';
import CompanyList from './components/company/CompanyList';
import CompanyShow from './components/company/CompanyShow';
import VacancyList from './components/vacancy/VacancyList';
import VacancyCreate from './components/vacancy/VacancyCreate';
import VacancyShow from './components/vacancy/VacancyShow';
import VacancyEdit from './components/vacancy/VacancyEdit';
import UserList from "./components/user/UserList";
import UserShow from "./components/user/UserShow";
import UserEdit from "./components/user/UserEdit";
import ResumeList from "./components/resume/ResumeList";
import ResumeCreate from "./components/resume/ResumeCreate";
import ResumeShow from "./components/resume/ResumeShow";
import ResumeEdit from "./components/resume/ResumeEdit";

import englishMessages from 'ra-language-english';
import russianMessages from 'ra-language-russian';

const messages = {
    en: englishMessages,
    ru: {
        ...russianMessages,
        resources: {
            vacancy: {
                fields: {
                    city_id: 'Город',
                    city_name: 'Город',
                    position_name: 'Должность',
                    profarea_id: 'Профобласть',
                    requirements: 'Требования',
                    liabilities: 'Обязанности',
                    conditions: 'Условия',
                    description: 'Описание',
                    schedule_type: 'Тип графика',
                    is_remote: 'Удаленная работа',
                    paycheck: 'Зарплата',
                    paycheck_const: 'Зарплата',
                    paycheck_min: 'Зарплата от',
                    paycheck_max: 'Зарплата до',
                    update_date: 'Обновлена'
                },

            }
        },
        paycheckField: {
            rangeFrom: 'От',
            rangeTo: 'До'
        },
        paycheckChoices: {
            fixed: 'Указать фиксированный размер зарплаты',
            range: 'Указать границы зарплаты'
        }
    }
};

const i18nProvider = locale => messages[locale];

const Application = () => (
    <Admin locale="ru" dataProvider={dataProvider} i18nProvider={i18nProvider} dashboard={Dashboard}>
        <Resource name='resume' show={ResumeShow} list={ResumeList} edit={ResumeEdit} create={ResumeCreate} />
        <Resource name='vacancy' show={VacancyShow} list={VacancyList} edit={VacancyEdit} create={VacancyCreate} />
        <Resource name='user' show={UserShow} list={UserList} edit={UserEdit} />
        <Resource name="company" show={CompanyShow} list={CompanyList}  edit={CompanyEdit} create={CompanyCreate}/>
        <Resource name="city" />
        <Resource name="profarea" />
    </Admin>
);

export default Application;