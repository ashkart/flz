'use strict';

import { stringify } from 'query-string';
import {
    GET_LIST,
    GET_ONE,
    CREATE,
    UPDATE,
    DELETE,
    GET_MANY,
    DELETE_MANY,
    GET_MANY_REFERENCE
} from 'react-admin';


function getUrl() {
    return `${window.location.protocol}//${window.location.host}`;
}

const rootUrl = getUrl();

/**
 *
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} params Request parameters. Depends on the action type
 * @returns {Promise} the Promise for a response
 */
const dataProvider = (type, resource, params) => {

    const convertDataRequestToHttp = (type, resource, params) => {
        let url = '';

        const options = {
            headers : new Headers({
                Accept: 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }),
        };

        switch (type) {
            case GET_MANY:
                url = `${rootUrl}/${resource}/list`;
                break;
            case GET_MANY_REFERENCE:
            case GET_LIST:

                let page    = params.pagination.page;
                let perPage = params.pagination.perPage;

                let sortBy = params.sort.field;
                let order  = params.sort.order;

                let filterQueryPart = `filter=${JSON.stringify(params.filter)}`;
                let pagingQueryPart = `perPage=${perPage}&page=${page}`;
                let orderQueryPart  = `sortBy=${sortBy}&order=${order}`;

                url = `${rootUrl}/${resource}/list`;

                if (type === GET_MANY_REFERENCE) {
                    url = `${url}/${resource}/${params.target}-${params.id}`
                }

                url = `${url}?${pagingQueryPart}&${orderQueryPart}&${filterQueryPart}`;
                break;

            case GET_ONE:
                url = `${rootUrl}/${resource}/id-${params.id}`;
                break;

            case UPDATE:
            case CREATE:
                options.method = 'POST';
                options.headers.append("Content-Type", "application/json");
                options.body = JSON.stringify(params.data);
                url = `${rootUrl}/${resource}/save`;

                if (type === UPDATE) {
                    url = `${url}/id-${params.data.id}`;
                }

                break;

            case DELETE:
                url = `${rootUrl}/${resource}/delete/id-${params.id}`;
                break;

            case DELETE_MANY:
                options.method = 'POST';
                options.headers.append("Content-Type", "application/json");
                options.body = JSON.stringify({ids: params.ids});
                url = `${rootUrl}/${resource}/delete_many`;
                break;
        }

        return {url, options};
    };

    const extractDataFromHttpResponse = (response, type, resource, params) => {
        if (response.redirected) {
            window.location.href = response.url;
            return;
        }

        if (response.status !== 200) {
            return Promise.reject(response);
        }

        return response.json().then((json => {

            if (json.state === 0) {
                return Promise.reject(json.errors);
            }

            switch(type) {
                case GET_MANY:
                    return {data: Object.values(json)};
                case GET_MANY_REFERENCE:
                case GET_LIST:
                    let total = json.totalItems;
                    delete json.totalItems;

                    return {
                        data: Object.values(json),
                        total: total,
                    };

                case CREATE:
                    return {
                        data: Object.assign({}, params.data, { id: json.id })
                    };

                default:
                    return { data: json };
            }
        }));
    };

    let { url, options } = convertDataRequestToHttp(type, resource, params);
    options.credentials = 'same-origin';

    return fetch(url, options)
        .then(response => extractDataFromHttpResponse(response, type, resource, params))
    ;
};

export default dataProvider;