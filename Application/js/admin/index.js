'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import Application from './Application';

window.onload =  function () {
    ReactDom.render(<Application/>, document.getElementById('contentMain'));
};