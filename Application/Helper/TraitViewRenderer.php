<?php

namespace Application\Helper;

use Application\Helper\DataSource\TraitIdentity;
use Application\Helper\ServiceManager\Factory;
use Application\View\PhpRenderer;
use Zend\View\Model\ViewModel;

trait TraitViewRenderer
{
    use TraitIdentity;

    protected function _getRenderer() : PhpRenderer
    {
        return self::$sm->get(Factory::VIEW_RENDERER);
    }

    /**
     * @return string разметка html
     */
    protected function _renderViewModel(ViewModel $vm) : string
    {
        return $this->_getRenderer()->render($vm);
    }
}