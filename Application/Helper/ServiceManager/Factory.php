<?php

namespace Application\Helper\ServiceManager;

use Application\Helper\DataSource\MapperRegistry;
use Application\Helper\ErrorHandling;
use Application\Helper\LibraryAcl;
use Application\Helper\Logger;
use Application\Helper\RouteHelper;
use Elasticsearch\ClientBuilder;
use Interop\Container\ContainerInterface;
use Library\Master\Acl\Acl;
use Library\Master\Cache\PRedisClient;
use Library\Master\Db\Adapter\Adapter;
use Main\Module;
use Library\Master\DaData;
use Zend\Authentication\AuthenticationService;
use Zend\Cache\Storage\Adapter\Redis;
use Zend\Cache\Storage\Adapter\RedisOptions;
use Zend\Log\PsrLoggerAdapter;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Router\Http\TreeRouteStack;
use Zend\Router\RouteStackInterface;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceManager;
use Library\Master;
use Zend\Session\Container;
use Zend\Session\SaveHandler;
use Predis;
use Zend\Stdlib\RequestInterface;
use Zend\Uri\Http;

class Factory implements AbstractFactoryInterface
{
    const PREFIX = 'staffz';

    const ROUTE_HELPER          = self::PREFIX . 'route_helper';
    const ROUTE_HELPER_HTTP     = self::PREFIX . 'route_helper_http';
    const ROUTE_MATCH           = self::PREFIX . 'route_match';
    const DB_ADAPTER            = self::PREFIX . 'dbadapter';
    const DATA_SOURCE           = self::PREFIX . 'datasource';
    const ERROR_HANDLER         = self::PREFIX . 'errorhandler';
    const LOGGER                = self::PREFIX . 'logger';
    const SESSION_MANAGER       = self::PREFIX . 'session';
    const REDIS_SESSION_CACHE   = self::PREFIX . 'redissessioncache';
    const REDIS_CACHE           = self::PREFIX . 'rediscache';
    const REDIS_MQ              = self::PREFIX . 'redismq';
    const REDIS_STAT            = self::PREFIX . 'redisstat';
    const ACL                   = self::PREFIX . 'acl';
    const AUTH_SERVICE          = self::PREFIX . 'auth_service';
    const ELASTICSEARCH_ADAPTER = self::PREFIX . 'elasticsearch';
    const DADATA_API_CLIENT     = self::PREFIX . 'dadata';
    const MAIL                  = self::PREFIX . 'mail';

    const CONFIG        = 'config';
    const ROUTER        = 'Router';
    const REQUEST       = 'Request';
    const RESPONSE      = 'Response';
    const EVENT         = 'EventManager';
    const VIEW_MANAGER  = 'ViewManager';
    const VIEW_RENDERER = 'ViewRenderer';


    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return array_key_exists($requestedName, $this->_getFactories());
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return $this->_getFactories()[$requestedName]($container);
    }

    private function _getFactories() : array
    {
        static $factories = null;

        if ($factories === null) {
            $factories = [
                /**
                 * Обработка ошибок в лог
                 */
                self::ERROR_HANDLER => function(ServiceManager $sm) {

                    /** @var PsrLoggerAdapter $logger */
                    $logger  = $sm->get(self::LOGGER);
                    $service = new ErrorHandling($logger);

                    return $service;
                },

                /**
                 * Логгер
                 */
                self::LOGGER => function (ServiceManager $sm) {

                    $logger = new Logger();


                    $logSettings = $sm->get(self::CONFIG)['logs'];

                    //лог ошибок
                    $logger->setErrorFilePath(Module::isCli() ? $logSettings['cli'] : $logSettings['error']);
                    //лог информации
                    $logger->setInfoFilePath($logSettings['info']);

                    $log = new PsrLoggerAdapter($logger);
                    return $log;
                },

                self::ROUTE_HELPER => function(ServiceManager $sm) {
                    $router = $sm->get('Router');
                    $routeMatch = $this->_createRouteMatch($sm);
                    return new RouteHelper($router, $routeMatch);
                },

                self::ROUTE_HELPER_HTTP => function(ServiceManager $sm) {
                    /** @var TreeRouteStack $router */
                    $router = $sm->get('HttpRouter');
                    $routeMatch = $this->_createRouteMatch($sm);

                    if (Module::isCli()) {
                        $uri = new Http('/');
                        $uri->setScheme($sm->get(self::CONFIG)['scheme']);
                        $router->setRequestUri($uri);
                    }

                    return new RouteHelper($router, $routeMatch);
                },

                self::ROUTE_MATCH => function(ServiceManager $sm) {
                    /**
                     * Используем внешний private-метод, чтобы была возможность получать null в методе выше (self::ROUTE_HELPER)
                     * Фабрика предполагает возврат instance, но не null
                     */
                    return $this->_createRouteMatch($sm);
                },

                /**
                 * Адаптер базы данных
                 */
                self::DB_ADAPTER => function (ServiceManager $sm) {

                    $dbConfig = $sm->get(self::CONFIG)['db'];

                    $dbAdapter = new Adapter(
                        "{$dbConfig['server']}:host={$dbConfig['host']};port={$dbConfig['port']};dbname={$dbConfig['default_database']};charset={$dbConfig['charset']}",
                        $dbConfig['username'],
                        $dbConfig['password']
                    );

                    $dbAdapter->setMappers(MapperRegistry::MAPPERS);

                    return $dbAdapter;
                },

                /**
                 * Фабрика для мапперов и сервисов моделей
                 */
                self::DATA_SOURCE => function (ServiceManager $sm) {
                    /** @var Adapter $dbAdapter */
                    $dbAdapter = $sm->get(self::DB_ADAPTER);

                    return new MapperRegistry($dbAdapter->getMapperLocator());
                },

                /**
                 * @see https://zf2.readthedocs.org/en/latest/modules/zend.session.manager.html
                 *
                 * Менеджер сессий
                 */
                self::SESSION_MANAGER => function (ServiceManager $sm) {

                    $config = $sm->get(self::CONFIG);
                    if (isset($config['session'])) {
                        $session = $config['session'];

                        $sessionConfig = null;
                        if (isset($session['config'])) {
                            $class = isset($session['config']['class'])  ? $session['config']['class'] : 'Zend\Session\Config\SessionConfig';
                            $options = isset($session['config']['options']) ? $session['config']['options'] : [];
                            /** @var \Zend\Session\Config\SessionConfig $sessionConfig */
                            $sessionConfig = new $class();
                            $sessionConfig->setOptions($options);
                        }

                        $sessionStorage = null;
                        if (isset($session['storage'])) {
                            $class = $session['storage'];
                            $sessionStorage = new $class();
                        }

                        $sessionSaveHandler = null;
                        if (isset($session['save_handler'])) {
                            // class should be fetched from service manager since it will require constructor arguments
                            /** @var SaveHandler\Cache $sessionSaveHandler */
                            $sessionSaveHandler = $sm->get($session['save_handler']);
                        }

                        $sessionManager = null;
                        if (isset($session['manager'])) {
                            $sessionManagerClass = $session['manager'];
                            $sessionManager = new $sessionManagerClass($sessionConfig, $sessionStorage, $sessionSaveHandler);
                        }

                    } else {
                        $sessionManager = new Master\Session\SessionManager();
                    }

                    Container::setDefaultManager($sessionManager);


                    return $sessionManager;
                },

                /**
                 * Полнотекстовый поиск
                 */
                self::ELASTICSEARCH_ADAPTER => function (ServiceManager $sm) {
                    $config = $sm->get(self::CONFIG);
                    $hosts = $config['elasticsearch']['hosts'];

                    $client = ClientBuilder::create()
                        ->setHosts($hosts)
                        ->setLogger($sm->get(self::LOGGER))
                        ->build()
                    ;

                    return $client;
                },

                self::REDIS_SESSION_CACHE => function (ServiceManager $sm) {
                    $config = $sm->get(self::CONFIG);
                    $options = $config['session']['save_handler_adapter']['options'];
                    $options['resource_manager'] = new $options['resource_manager']();
                    $redisOptions = new RedisOptions($options);

                    $redis = new Redis($redisOptions);

                    return new SaveHandler\Cache($redis);
                },

                self::REDIS_CACHE => function (ServiceManager $sm) {
                    $config          = $sm->get(self::CONFIG);
                    $redisConnParams = $config[self::REDIS_CACHE];

                    $redis = new PRedisClient($redisConnParams);

                    $redis->connect();

                    return $redis;
                },

                self::REDIS_MQ => function (ServiceManager $sm) {
                    $config          = $sm->get(self::CONFIG);
                    $redisConnParams = $config[self::REDIS_MQ];

                    $redisMq = new Master\QueueManager\Resque($redisConnParams['host'], $redisConnParams['port']);

                    return $redisMq;
                },

                self::REDIS_STAT => function (ServiceManager $sm) {
                    $config          = $sm->get(self::CONFIG);
                    $redisConnParams = $config[self::REDIS_STAT];

                    $redisStat = new Predis\Client($redisConnParams);

                    $redisStat->connect();

                    return $redisStat;
                },

                self::AUTH_SERVICE => function (ServiceManager $sm) {
                    // instantiate the authentication service
                    return new AuthenticationService();
                },

                self::ACL => function(ServiceManager $sm) {
                    return LibraryAcl::configure(new Acl());
                },

                self::DADATA_API_CLIENT => function(ServiceManager $sm) {
                    $config       = $sm->get(self::CONFIG);
                    $dadataConfig = $config[self::DADATA_API_CLIENT];

                    return new DaData\ApiClient\Suggest($dadataConfig['token'], $dadataConfig['url']);
                },

                self::MAIL => function(ServiceManager $sm) {
                    $config       = $sm->get(self::CONFIG);
                    $smtpOptions = $config['mail']['smtp'];

                    $options = new SmtpOptions($smtpOptions);
                    $transport = new Smtp($options);

                    return $transport;
                }
            ];
        }

        return $factories;
    }

    private function _createRouteMatch(ServiceManager $sm)
    {
        /** @var RouteStackInterface $router */
        $router  = $sm->get(self::ROUTER);
        $request = $sm->get(self::REQUEST);

        return $router->match($request);
    }
}