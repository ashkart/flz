<?php

namespace Application\Helper;

require_once APP_PATH . '/config/routes.literal_names.php';

use Application\Helper\DataSource\TraitDataSource;
use Traversable;
use Zend\Mvc\ModuleRouteListener;
use Zend\Router\Http\TreeRouteStack;
use Zend\Router\RouteMatch;
use Zend\Router\RouteStackInterface;
use Zend\Router\RouteInterface;
use Zend\Router\SimpleRouteStack;
use Zend\Http\Exception;

use Frontend;
use Admin;

use const Application\ROUTE_WC;

class RouteHelper
{
    use TraitDataSource;

    /**
     * Router\RouteStackInterface instance.
     *
     * @var RouteStackInterface | TreeRouteStack | SimpleRouteStack
     */
    protected $_router;

    /**
     * Router\RouteInterface match returned by the router.
     *
     * @var RouteMatch
     */
    protected $_routeMatch;

    public function __construct(RouteStackInterface $router, RouteMatch $routeMatch = null)
    {
        $this->_router     = $router;
        $this->_routeMatch = $routeMatch;
    }

    public function getRouter() : RouteStackInterface
    {
        return $this->_router;
    }

    public function getRouteMatch(): ? RouteMatch
    {
        return $this->_routeMatch;
    }

    public function isFrontend(string $routeName = null) : bool
    {
        return $this->_isRoute(Frontend\Module::NAME, $routeName);
    }

    public function isAdmin(string $routeName = null) : bool
    {
        return $this->_isRoute(Admin\Module::NAME, $routeName);
    }

    /**
     * Проверка, имеет ли представленный роут название модуля
     */
    public function hasModuleName(string $routeName = null) : bool
    {
        return $this->isAdmin($routeName) || $this->isFrontend($routeName);
    }

    /**
     * Название модуля
     */
    public function getModuleName() : string
    {
        return $this->isAdmin() ? Admin\Module::NAME : Frontend\Module::NAME;
    }

    /**
     * Возвращает название маршрута без модуля
     */
    public function getRouteName() : ? string
    {
        if ($this->_routeMatch !== null) {
            $matchedRoute = $this->_routeMatch->getMatchedRouteName();
            return substr($matchedRoute, strpos($matchedRoute, '/') + 1);
        }

        return null;
    }

    private function _isRoute(string $routeName, string $route = null) : bool
    {
        if ($this->_routeMatch !== null) {
            $checkedRoute = $route ?: $this->_routeMatch->getMatchedRouteName();
            return strpos($checkedRoute, $routeName) === 0;
        }

        return false;
    }

    /**
     * @copy of zf2 Url() View Helper
     * @see \Zend\View\Helper\Url
     *
     * Generates a url given the name of a route.
     *
     * @see RouteInterface::assemble()
     * @see RouteInterface::assemble()
     * @param  string $name Name of the route
     * @param  array $params Parameters for the link
     * @param  array|Traversable $options Options for the route
     * @param  bool $reuseMatchedParams Whether to reuse matched parameters
     * @return string Url For the link href attribute
     * @throws Exception\RuntimeException If no RouteStackInterface was
     *     provided
     * @throws Exception\RuntimeException If no RouteMatch was provided
     * @throws Exception\RuntimeException If RouteMatch didn't contain a
     *     matched route name
     * @throws Exception\InvalidArgumentException If the params object was not
     *     an array or Traversable object.
     */
    public function url($name = null, $params = [], $options = [], $reuseMatchedParams = false)
    {
        if (null === $this->_router) {
            throw new Exception\RuntimeException('No RouteStackInterface instance provided');
        }

        if (3 == func_num_args() && is_bool($options)) {
            $reuseMatchedParams = $options;
            $options = [];
        }

        if ($name === null) {
            if ($this->_routeMatch === null) {
                throw new Exception\RuntimeException('No RouteMatch instance provided');
            }

            $name = $this->_routeMatch->getMatchedRouteName();

            if ($name === null) {
                throw new Exception\RuntimeException('RouteMatch does not contain a matched route name');
            }
        }

        if (! is_array($params)) {
            if (! $params instanceof Traversable) {
                throw new Exception\InvalidArgumentException(
                    'Params is expected to be an array or a Traversable object'
                );
            }
            $params = iterator_to_array($params);
        }

        if ($reuseMatchedParams && $this->_routeMatch !== null) {
            $routeMatchParams = $this->_routeMatch->getParams();

            if (isset($routeMatchParams[ModuleRouteListener::ORIGINAL_CONTROLLER])) {
                $routeMatchParams['controller'] = $routeMatchParams[ModuleRouteListener::ORIGINAL_CONTROLLER];
                unset($routeMatchParams[ModuleRouteListener::ORIGINAL_CONTROLLER]);
            }

            if (isset($routeMatchParams[ModuleRouteListener::MODULE_NAMESPACE])) {
                unset($routeMatchParams[ModuleRouteListener::MODULE_NAMESPACE]);
            }

            $params = array_merge($routeMatchParams, $params);
        }

        $options['name'] = $name;

        return $this->_router->assemble($params, $options);
    }

    /**
     * @param string[] $params
     * @param array | bool $options
     * @param bool $reuseMatchedParams
     */
    public function frontend(
        string $controller = null,
        string $action = null,
        array $params = [],
        $options = [],
        bool $reuseMatchedParams = false
    ) : string
    {
        return $this->_url(Frontend\Module::NAME, $controller, $action, $params, $options, $reuseMatchedParams);
    }

    /**
     * @param string[] $params
     * @param array | bool $options
     * @param bool $reuseMatchedParams
     */
    public function admin(
        string $controller = null,
        string $action = null,
        array $params = [],
        $options = [],
        bool $reuseMatchedParams = false
    ) : string
    {
        return $this->_url(Admin\Module::NAME, $controller, $action, $params, $options, $reuseMatchedParams);
    }

    protected function _url(
        string $moduleName,
        string $controller = null,
        string $action = null,
        array $params = [],
        $options = [],
        bool $reuseMatchedParams = false
    ) : string
    {
        if ($controller) {
            $params['controller'] = $controller;
        }

        if ($action) {
            $params['action'] = $action;
        }

        return $this->url("$moduleName/" . ROUTE_WC, $params, $options, $reuseMatchedParams);
    }
}