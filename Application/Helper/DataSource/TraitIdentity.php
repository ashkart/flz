<?php

namespace Application\Helper\DataSource;

use Application\Helper\ServiceManager\Factory;
use Application\Model\Domain;

trait TraitIdentity
{
    use TraitDataSource;

    protected static function _getUser() : Domain\User
    {
        /** @var Domain\User $user */
        $user = self::$sm->get(Factory::AUTH_SERVICE)->getIdentity();

        return $user ?? Domain\User::createEmpty();
    }

    /**
     * По возможности используем этот нестатичный метод, т.к. статичный метод невозможно заткнуть в тестах
     */
    protected function _user() : Domain\User
    {
        return self::_getUser();
    }

    protected function _getUserIp() : ? string
    {
        return self::$ds->userAgentService()->getIp();
    }

    protected function _getUserSessionHash() : string
    {
        return self::$sm->get(Factory::SESSION_MANAGER)->getSessionHash();
    }
}