<?php
/**
 * Фабрика генерации мапперов и сервисов
 */

namespace Application\Helper\DataSource;

use Application\Model\Domain;
use Application\Model\Service;
use Atlas\Orm\Mapper\MapperLocator;

class MapperRegistry implements DataSourceInterface
{
    const MAPPERS = [
        Domain\City\CityMapper::class,
        Domain\Callback\CallbackMapper::class,
        Domain\Company\CompanyMapper::class,
        Domain\Contact\ContactMapper::class,
        Domain\ContactPerson\ContactPersonMapper::class,
        Domain\Conversation\ConversationMapper::class,
        Domain\ConversationMessage\ConversationMessageMapper::class,
        Domain\ConversationUser\ConversationUserMapper::class,
        Domain\Country\CountryMapper::class,
        Domain\Currency\CurrencyMapper::class,
        Domain\Education\EducationMapper::class,
        Domain\EntityTag\EntityTagMapper::class,
        Domain\LanguageProficiency\LanguageProficiencyMapper::class,
        Domain\Profarea\ProfareaMapper::class,
        Domain\Region\RegionMapper::class,
        Domain\Resume\ResumeMapper::class,
        Domain\Tag\TagMapper::class,
        Domain\User\UserMapper::class,
        Domain\UserCompany\UserCompanyMapper::class,
        Domain\Vacancy\VacancyMapper::class,
        Domain\VacancyLocation\VacancyLocationMapper::class,
        Domain\View\ViewMapper::class,
        Domain\WorkExperience\WorkExperienceMapper::class,
    ];

    /**
     * @var MapperLocator
     */
    private $_mapperLocator;

    /**
     * @var array
     */
    private $_services = [];

    public function __construct(MapperLocator $mapperLocator)
    {
        $this->_mapperLocator = $mapperLocator;
    }

    public function getMapper(string $type) : Domain\AbstractMapper
    {
        if (!$this->_mapperLocator->has($type)) {
            throw new \Exception("Mapper $type doesnt exist in MapperRegistry");
        }

        /** @var Domain\AbstractMapper $mapper */
        $mapper = $this->_mapperLocator->get($type);

        return $mapper;
    }

    public function getService(string $type) : Service\AbstractService
    {
        if (!isset($this->_services[$type])) {
            $this->_services[$type] = $this->loadService($type);
        }

        return $this->_services[$type];
    }

    private function loadService(string $type) : Service\AbstractService
    {
        return new $type();
    }

    /**
     * Mappers
     */

    /* @hint phpdoc далее только для спокойствия IDE */

    /** @return Domain\AbstractMapper */ public function cityMapper()                : Domain\City\CityMapper                               { return $this->getMapper(Domain\City\CityMapper::class); }
    /** @return Domain\AbstractMapper */ public function callbackMapper()            : Domain\Callback\CallbackMapper                       { return $this->getMapper(Domain\Callback\CallbackMapper::class); }
    /** @return Domain\AbstractMapper */ public function companyMapper()             : Domain\Company\CompanyMapper                         { return $this->getMapper(Domain\Company\CompanyMapper::class); }
    /** @return Domain\AbstractMapper */ public function contactMapper()             : Domain\Contact\ContactMapper                         { return $this->getMapper(Domain\Contact\ContactMapper::class); }
    /** @return Domain\AbstractMapper */ public function contactPersonMapper()       : Domain\ContactPerson\ContactPersonMapper             { return $this->getMapper(Domain\ContactPerson\ContactPersonMapper::class); }
    /** @return Domain\AbstractMapper */ public function conversationMapper()        : Domain\Conversation\ConversationMapper               { return $this->getMapper(Domain\Conversation\ConversationMapper::class); }
    /** @return Domain\AbstractMapper */ public function conversationMessageMapper() : Domain\ConversationMessage\ConversationMessageMapper { return $this->getMapper(Domain\ConversationMessage\ConversationMessageMapper::class); }
    /** @return Domain\AbstractMapper */ public function conversationUserMapper()    : Domain\ConversationUser\ConversationUserMapper       { return $this->getMapper(Domain\ConversationUser\ConversationUserMapper::class); }
    /** @return Domain\AbstractMapper */ public function countryMapper()             : Domain\Country\CountryMapper                         { return $this->getMapper(Domain\Country\CountryMapper::class); }
    /** @return Domain\AbstractMapper */ public function currencyMapper()            : Domain\Currency\CurrencyMapper                       { return $this->getMapper(Domain\Currency\CurrencyMapper::class); }
    /** @return Domain\AbstractMapper */ public function educationMapper()           : Domain\Education\EducationMapper                     { return $this->getMapper(Domain\Education\EducationMapper::class); }
    /** @return Domain\AbstractMapper */ public function entityTagMapper()           : Domain\EntityTag\EntityTagMapper                     { return $this->getMapper(Domain\EntityTag\EntityTagMapper::class); }
    /** @return Domain\AbstractMapper */ public function langProficiencyMapper()     : Domain\LanguageProficiency\LanguageProficiencyMapper { return $this->getMapper(Domain\LanguageProficiency\LanguageProficiencyMapper::class); }
    /** @return Domain\AbstractMapper */ public function profareaMapper()            : Domain\Profarea\ProfareaMapper                       { return $this->getMapper(Domain\Profarea\ProfareaMapper::class); }
    /** @return Domain\AbstractMapper */ public function regionMapper()              : Domain\Region\RegionMapper                           { return $this->getMapper(Domain\Region\RegionMapper::class); }
    /** @return Domain\AbstractMapper */ public function resumeMapper()              : Domain\Resume\ResumeMapper                           { return $this->getMapper(Domain\Resume\ResumeMapper::class); }
    /** @return Domain\AbstractMapper */ public function tagMapper()                 : Domain\Tag\TagMapper                                 { return $this->getMapper(Domain\Tag\TagMapper::class); }
    /** @return Domain\AbstractMapper */ public function userMapper()                : Domain\User\UserMapper                               { return $this->getMapper(Domain\User\UserMapper::class); }
    /** @return Domain\AbstractMapper */ public function userCompanyMapper()         : Domain\UserCompany\UserCompanyMapper                 { return $this->getMapper(Domain\UserCompany\UserCompanyMapper::class); }
    /** @return Domain\AbstractMapper */ public function vacancyMapper()             : Domain\Vacancy\VacancyMapper                         { return $this->getMapper(Domain\Vacancy\VacancyMapper::class); }
    /** @return Domain\AbstractMapper */ public function vacancyLocationMapper()     : Domain\VacancyLocation\VacancyLocationMapper         { return $this->getMapper(Domain\VacancyLocation\VacancyLocationMapper::class); }
    /** @return Domain\AbstractMapper */ public function viewMapper()                : Domain\View\ViewMapper                               { return $this->getMapper(Domain\View\ViewMapper::class); }
    /** @return Domain\AbstractMapper */ public function workExperienceMapper()      : Domain\WorkExperience\WorkExperienceMapper           { return $this->getMapper(Domain\WorkExperience\WorkExperienceMapper::class); }

    /**
     * Services
     */

    /** @return Service\AbstractService */ public function authService()            : Service\Auth               { return $this->getService(Service\Auth::class); }
    /** @return Service\AbstractService */ public function callbackService()        : Service\Callback           { return $this->getService(Service\Callback::class); }
    /** @return Service\AbstractService */ public function cityService()            : Service\City               { return $this->getService(Service\City::class); }
    /** @return Service\AbstractService */ public function companyService()         : Service\Company            { return $this->getService(Service\Company::class); }
    /** @return Service\AbstractService */ public function conversationService()    : Service\Conversation       { return $this->getService(Service\Conversation::class); }
    /** @return Service\AbstractService */ public function countryService()         : Service\Country            { return $this->getService(Service\Country::class); }
    /** @return Service\AbstractService */ public function educationService()       : Service\Education          { return $this->getService(Service\Education::class); }
    /** @return Service\AbstractService */ public function entityTagService()       : Service\EntityTag          { return $this->getService(Service\EntityTag::class); }
    /** @return Service\AbstractService */ public function elasticSearchService()   : Service\ElasticSearch      { return $this->getService(Service\ElasticSearch::class); }
    /** @return Service\AbstractService */ public function mailService()            : Service\Mail               { return $this->getService(Service\Mail::class); }
    /** @return Service\AbstractService */ public function photoService()           : Service\Photo              { return $this->getService(Service\Photo::class); }
    /** @return Service\AbstractService */ public function profareaService()        : Service\Profarea           { return $this->getService(Service\Profarea::class); }
    /** @return Service\AbstractService */ public function redisMQService()         : Service\RedisMQ            { return $this->getService(Service\RedisMQ::class); }
    /** @return Service\AbstractService */ public function regionService()          : Service\Region             { return $this->getService(Service\Region::class); }
    /** @return Service\AbstractService */ public function resumeService()          : Service\Resume             { return $this->getService(Service\Resume::class); }
    /** @return Service\AbstractService */ public function suggestService()         : Service\Suggest            { return $this->getService(Service\Suggest::class); }
    /** @return Service\AbstractService */ public function tagService()             : Service\Tag                { return $this->getService(Service\Tag::class); }
    /** @return Service\AbstractService */ public function vacancyService()         : Service\Vacancy            { return $this->getService(Service\Vacancy::class); }
    /** @return Service\AbstractService */ public function vacancyLocationService() : Service\VacancyLocation    { return $this->getService(Service\VacancyLocation::class); }
    /** @return Service\AbstractService */ public function viewService()            : Service\View               { return $this->getService(Service\View::class); }
    /** @return Service\AbstractService */ public function socnetService()          : Service\SocialNetwork      { return $this->getService(Service\SocialNetwork::class); }
    /** @return Service\AbstractService */ public function userAgentService()       : Service\UserAgent          { return $this->getService(Service\UserAgent::class); }
    /** @return Service\AbstractService */ public function userCompanyService()     : Service\UserCompany        { return $this->getService(Service\UserCompany::class); }
    /** @return Service\AbstractService */ public function userService()            : Service\User               { return $this->getService(Service\User::class); }
}