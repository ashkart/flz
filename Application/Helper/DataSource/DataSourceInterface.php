<?php

namespace Application\Helper\DataSource;

use Application\Model\Domain;
use Application\Model\Service;

interface DataSourceInterface
{
    public function getMapper(string $type) : Domain\AbstractMapper;

    public function getService(string $type) : Service\AbstractService;

    /**
     * Mappers
     */

    public function cityMapper()                : Domain\City\CityMapper;
    public function callbackMapper()            : Domain\Callback\CallbackMapper;
    public function companyMapper()             : Domain\Company\CompanyMapper;
    public function contactMapper()             : Domain\Contact\ContactMapper;
    public function contactPersonMapper()       : Domain\ContactPerson\ContactPersonMapper;
    public function conversationMapper()        : Domain\Conversation\ConversationMapper;
    public function conversationMessageMapper() : Domain\ConversationMessage\ConversationMessageMapper;
    public function conversationUserMapper()    : Domain\ConversationUser\ConversationUserMapper;
    public function countryMapper()             : Domain\Country\CountryMapper;
    public function currencyMapper()            : Domain\Currency\CurrencyMapper;
    public function educationMapper()           : Domain\Education\EducationMapper;
    public function entityTagMapper()           : Domain\EntityTag\EntityTagMapper;
    public function langProficiencyMapper()     : Domain\LanguageProficiency\LanguageProficiencyMapper;
    public function profareaMapper()            : Domain\Profarea\ProfareaMapper;
    public function regionMapper()              : Domain\Region\RegionMapper;
    public function resumeMapper()              : Domain\Resume\ResumeMapper;
    public function tagMapper()                 : Domain\Tag\TagMapper;
    public function userCompanyMapper()         : Domain\UserCompany\UserCompanyMapper;
    public function userMapper()                : Domain\User\UserMapper;
    public function vacancyMapper()             : Domain\Vacancy\VacancyMapper;
    public function vacancyLocationMapper()     : Domain\VacancyLocation\VacancyLocationMapper;
    public function viewMapper()                : Domain\View\ViewMapper;
    public function workExperienceMapper()      : Domain\WorkExperience\WorkExperienceMapper;

    /**
     * Services
     */

    public function authService()            : Service\Auth;
    public function callbackService()        : Service\Callback;
    public function cityService()            : Service\City;
    public function companyService()         : Service\Company;
    public function conversationService()    : Service\Conversation;
    public function countryService()         : Service\Country;
    public function educationService()       : Service\Education;
    public function entityTagService()       : Service\EntityTag;
    public function elasticSearchService()   : Service\ElasticSearch;
    public function mailService()            : Service\Mail;
    public function photoService()           : Service\Photo;
    public function profareaService()        : Service\Profarea;
    public function redisMQService()         : Service\RedisMQ;
    public function regionService()          : Service\Region;
    public function resumeService()          : Service\Resume;
    public function suggestService()         : Service\Suggest;
    public function tagService()             : Service\Tag;
    public function vacancyService()         : Service\Vacancy;
    public function vacancyLocationService() : Service\VacancyLocation;
    public function viewService()            : Service\View;
    public function socnetService()          : Service\SocialNetwork;
    public function userAgentService()       : Service\UserAgent;
    public function userCompanyService()     : Service\UserCompany;
    public function userService()            : Service\User;
}