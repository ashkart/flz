<?php

namespace Application\Helper\DataSource;

use Application\Helper\ServiceManager\Factory;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

trait TraitDataSource
{
    /**
     * @var ServiceManager | ServiceLocatorInterface | null
     */
    public static $sm = null;

    /**
     * @var DataSourceInterface | null
     */
    public static $ds = null;

    public static function setServiceManager(ServiceLocatorInterface $serviceLocator) : void
    {
        self::$sm = $serviceLocator;
        self::$ds = self::$sm->get(Factory::DATA_SOURCE);
    }
}