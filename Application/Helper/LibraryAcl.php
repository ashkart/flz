<?php

namespace Application\Helper;

use Application\Model\Domain\User;
use Library\Master\Acl\Acl;
use Frontend;
use Admin;
use Console;

class LibraryAcl
{
    public static function configure(Acl $acl)
    {
        $guest     = User::ROLE_GUEST;
        $employee  = User::ROLE_EMPLOYEE;
        $recruiter = User::ROLE_RECRUITER;
        $admin     = User::ROLE_ADMIN;

        $all = [
            $guest,
            $employee,
            $recruiter,
            $admin,
        ];

        $logedIn = [
            $employee,
            $recruiter,
            $admin
        ];

        /**
         * Добавляем роли
         */
        $acl
            ->addRole($guest)
            ->addRole($employee, $guest)
            ->addRole($recruiter, $guest)
            ->addRole($admin, [$employee, $recruiter])
        ;

        /**
         * Модули
         */
        $acl->addResources(
            [
                Frontend\Module::NAME,
                Admin\Module::NAME,
                Console\Module::NAME
            ]
        );

        /**
         * Маршруты админского модуля
         */
        $acl->addResources(
            [
                Admin\Module::NAME . ':city',
                Admin\Module::NAME . ':company',
                Admin\Module::NAME . ':education',
                Admin\Module::NAME . ':index',
                Admin\Module::NAME . ':profarea',
                Admin\Module::NAME . ':resume',
                Admin\Module::NAME . ':vacancy',
                Admin\Module::NAME . ':user',
                Admin\Module::NAME . ':work_experience',
            ],
            Admin\Module::NAME
        );

        /**
         * Маршруты CLI
         */
        $acl->addResources(
            [
                Console\Module::NAME . ':console',
                Console\Module::NAME . ':cron',
                Console\Module::NAME . ':elasticsearch'
            ],
            Console\Module::NAME
        );

        /**
         * Маршруты фронта
         */
        $acl->addResources(
            [
                Frontend\Module::NAME . ':auth',
                Frontend\Module::NAME . ':callback',
                Frontend\Module::NAME . ':city',
                Frontend\Module::NAME . ':dictionary',
                Frontend\Module::NAME . ':error',
                Frontend\Module::NAME . ':index',
                Frontend\Module::NAME . ':profile',
                Frontend\Module::NAME . ':resume',
                Frontend\Module::NAME . ':rule',
                Frontend\Module::NAME . ':suggest',
                Frontend\Module::NAME . ':vacancy',
                Frontend\Module::NAME . ':socnet',
            ],
            Frontend\Module::NAME
        );

        $acl->allow($all, Frontend\Module::NAME);

        $acl->deny($guest, [
            Frontend\Module::NAME . ':callback',
            Frontend\Module::NAME . ':profile'
        ]);

        $acl->deny($guest, Frontend\Module::NAME . ':resume', ['edit', 'save', 'add-callback', 'callbacks']);
        $acl->deny($guest, Frontend\Module::NAME . ':vacancy', ['edit', 'save', 'add-callback', 'callbacks']);

        $acl->deny($all, Frontend\Module::NAME . ':socnet', ['vk-catch-code']);

        $acl->allow($guest, [Frontend\Module::NAME . ':profile'], ['reset-password']);

        $acl->allow($all, [Frontend\Module::NAME . ':profile'], ['confirm-new-password', 'update-city']);

        $acl->deny($all, Admin\Module::NAME);

        $acl->deny($all, Console\Module::NAME);

//        $acl->allow($admin, Admin\Module::NAME);

        $acl->allow($admin, Console\Module::NAME);

        return $acl;
    }
}