<?php
/**
 * If you need an environment-specific system or frontend configuration,
 * there is an example in the documentation
 * @see https://docs.zendframework.com/tutorials/advanced-config/#environment-specific-system-configuration
 * @see https://docs.zendframework.com/tutorials/advanced-config/#environment-specific-application-configuration
 */

use Application\Application;
use Application\View\Factory\TranslatorDelegator;

return [
    // Retrieve list of modules used in this frontend.
    'modules' => require __DIR__ . '/modules.config.php',

    // These are various options for the listeners attached to the ModuleManager
    'module_listener_options' => [
        // This should be an array of paths in which modules reside.
        // If a string key is provided, the listener will consider that a Module
        // namespace, the value of that key the specific path to that Module's
        // Module class.
        'module_paths' => [
            './Module',
            './vendor',
        ],

        // An array of paths from which to glob configuration files after
        // modules are loaded. These effectively override configuration
        // provided by modules themselves. Paths may use GLOB_BRACE notation.
        'config_glob_paths' => [
            realpath(__DIR__) . '/autoload/{{,*.}global,{,*.}local}.php',
        ],

        // Whether or not to enable a configuration cache.
        // If enabled, the merged configuration will be cached and used in
        // subsequent requests.
        'config_cache_enabled' => false,

        // The key used to create the configuration cache file name.
        'config_cache_key' => 'frontend.config.cache',

        // Whether or not to enable a Module class map cache.
        // If enabled, creates a Module class map cache which will be used
        // by in future requests, to reduce the autoloading process.
        'module_map_cache_enabled' => false,

        // The key used to create the class map cache file name.
        'module_map_cache_key' => 'frontend.Module.cache',

        // The path in which to cache merged configuration.
        'cache_dir' => 'data/cache/',

        // Whether or not to enable modules dependency checking.
        // Enabled by default, prevents usage of modules that depend on other modules
        // that weren't loaded.
        // 'check_dependencies' => true,
    ],

    // Used to create an own service manager. May contain one or more child arrays.
    // 'service_listener_options' => [
    //     [
    //         'service_manager' => $stringServiceManagerName,
    //         'config_key'      => $stringConfigKey,
    //         'interface'       => $stringOptionalInterface,
    //         'method'          => $stringRequiredMethodName,
    //     ],
    // ],

    // Initial configuration with which to seed the ServiceManager.
    // Should be compatible with Zend\ServiceManager\Config.
    'service_manager' => [
        'abstract_factories' => [
            new \Application\Helper\ServiceManager\Factory()
        ],
        'factories' => [
            'Application' => function(\Zend\ServiceManager\ServiceManager $container) {
                return new Application(
                    $container,
                    $container->get('EventManager'),
                    $container->get('Request'),
                    $container->get('Response')
                );
            },

            'MvcTranslator' => \Zend\Mvc\I18n\TranslatorFactory::class,
            \Zend\I18n\Translator\TranslatorInterface::class => \Zend\I18n\Translator\TranslatorServiceFactory::class,
            Zend\View\Renderer\PhpRenderer::class => \Application\View\Factory\PhpRendererFactory::class,
            'Request' => \Library\Master\Mvc\RequestFactory::class,
            'ViewManager' => \Application\View\ViewManagerFactory::class
        ],
        'invokables' => [
            //'name' => ServiceClass::class - для каждого такого класса здесь назначится InvokableFactory
        ],
        'aliases' => [
            'url' => 'staffzroute_helper'
        ],
        'delegators' => [
            'MvcTranslator' => [
                TranslatorDelegator::class,
            ],
        ],
    ]
];
