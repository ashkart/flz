<?php

return [
    Console\Module::NAME => [
        'type' => \Zend\Mvc\Console\Router\Simple::class,
        'options' => [
            'route' => '<controller> <action>',
            'defaults' => [
                'module'     => Console\Module::NAME,
                'controller' => 'console',
                'action'     => 'test',
            ]
        ]
    ]
];