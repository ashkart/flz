<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * List of enabled modules for this frontend.
 *
 * This should be an array of Module namespaces used in the frontend.
 */

if (isset($_SERVER['HTTP_HOST'])) {
    $_SERVER['HTTP_HOST'] = explode(':', $_SERVER['HTTP_HOST'])[0]; // отрезаем порт
}

$configLocal = include 'autoload/local.php';

$modules = [
    'Zend\Log',
    'Zend\Router',
    'Zend\Form',
    'Zend\Validator'
];

$modules[] = ucfirst(Main\Module::NAME);

if (PHP_SAPI !== 'cli') {
    if (isset($_SERVER['HTTP_HOST'])) {
        foreach ($configLocal['router']['routes'] as $module => $data) {
            if ($_SERVER['HTTP_HOST'] === $data['options']['route']) {
                $modules[] = ucfirst($module);
                break;
            }
        }
    }
} else {
    $modules[] = 'Console';
}

return $modules;
