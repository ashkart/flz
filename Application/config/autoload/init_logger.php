<?php

namespace {

    use Zend\Log\Logger;
    use Zend\Log\Writer\Stream;

    function logError($log, array $configuration = [])
    {
        static $logger = null;

        if ($logger === null) {
            $logger = _logger($configuration);
        }

        $logger->err($log);
    }

    function _logger(array $configuration = []) : Logger
    {
        $fileName = null;

        if (PHP_SAPI === 'cli') {
            $fileName = "/../" . $configuration['logs']['cli'] ?? 'var/logs/cli.log';
        } else {
            $fileName = "/../" . $configuration['logs']['error'] ?? 'var/logs/error.log';
        }

        if (!file_exists($fileName)) {
            $handle = fopen($fileName, 'w');

            if ($handle !== false) {
                fclose($handle);
            } else {
                throw new \Exception("Cannot create file: '$fileName'");
            }
        }

        $writer = new Stream(APP_PATH . $fileName);

        return (new Logger())
            ->addWriter($writer)
        ;
    }
}