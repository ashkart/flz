<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'db' => [
        'server'           => 'mysql',
        'default_database' => 'flm',
        'host'             => '',
        'port'             => '3306',
        'username'         => '',
        'password'         => '',
        'charset'          => 'utf8'
    ],

    'router' => [
//        'router_class' => \Zend\Router\Http\TreeRouteStack::class,
        'routes' => [
            'frontend' => [
                'options' => [
                    'route' => 'staffz.ru'
                ]
            ],

            'admin' => [
                'options' => [
                    'route' => 'admin.staffz.ru'
                ]
            ]
        ]
    ],

    'console' => [
        'router' => [
            'routes' => [

            ]
        ]
    ],

    'scheme' => 'https',

    'session' => [
        'config' => [
            'class' => 'Zend\Session\Config\SessionConfig',
            'remember_me_seconds' => Application\Model\Domain\ClientData::TTL_DAY * 30,
            'options' => [
                'name'            => Application\Model\Domain\ClientData::COOKIE_SESSION_ID,
                'cookie_domain'   => PHP_SAPI != 'cli'
                    ? preg_replace('~^(www\.|admin\.)?([^:]*)(:\d+)?$~', '.$2', $_SERVER['HTTP_HOST'])
                    : '.staffz.ru'
                ,
                'cookie_lifetime' => Application\Model\Domain\ClientData::TTL_DAY * 30,
                'gc_maxlifetime'  => Application\Model\Domain\ClientData::TTL_DAY * 30
            ],
        ],
        'manager' => Library\Master\Session\SessionManager::class,
        'storage' => Zend\Session\Storage\SessionArrayStorage::class,
        'save_handler' => 'staffzredissessioncache',
        'save_handler_adapter' => [
            'name' => 'redis',
            'options' => [
                'resource_id' => 'sc',
                'resource_manager' => \Zend\Cache\Storage\Adapter\RedisResourceManager::class,
                'server' => [
                    'host'    => '127.0.0.1',
                    'port'    => 6379,
                    'timeout' => 1
                ],
                'ttl' => Application\Model\Domain\ClientData::TTL_DAY * 30
            ],
        ],
    ],

    'elasticsearch' => require_once 'elasticsearch.config.php',

    'staffzrediscache' => [
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => 6380,

        'read_write_timeout' => 0,
    ],

    'staffzredismq' => [
        'host'   => '127.0.0.1',
        'port'   => 6381,

        'view_stat' => [
            'interval' => 60,
            'max_bunch' => 5000
        ]
    ],

    "staffzredisstat" => [
        'host'   => '127.0.0.1',
        'port'   => 6382,
    ],

    'logs' => [
        'error'   => 'var/logs/error.log',
        'info'    => 'var/logs/info.log',
        'headers' => 'var/logs/headers.log',
        'cli'     => 'var/logs/cli.log'
    ],

    'phpSettings' => [],

    'translator' => [
        'locale' => [
            'ru',
            'en'
        ],

        'translation_file_patterns' => [
            [
                'type'      => \Zend\I18n\Translator\Loader\PhpArray::class,
                'base_dir' => APP_PATH . '/data/i18n/',
                'pattern'   => '%1$s/localization.php'
            ]
        ],

        'validator_translation_file_patterns' => [
            'default' => [
                'type'      => \Zend\I18n\Translator\Loader\PhpArray::class,
                'base_dir' => APP_PATH . '/data/i18n/',
                'pattern'   => '%1$s/validation.php'
            ]
        ]
    ],

    'staffzdadata' => [
        'token' => '',
        'url'   => 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/'
    ],

    'mail' => [
        'from'              => 'staffz',
        'sendToUsers'       => false,
        'developmentMail'   => '',
        'smtp' => [
            'name'              => 'staffz.ru',
            'host'              => 'smtp.yandex.ru',
            'port'              => 587,
            // Notice port change for TLS is 587
            'connection_class'  => 'login',
            'connection_config' => [
                'username' => '',
                'password' => '',
                'ssl'      => 'tls',
            ],
        ]
    ],

    'contacts' => [
        'brand' => 'Staffz',
        'support_email' => 'support@staffz.ru',
    ],

    'photo' => [
        'file_size' => [
            'width'  => 480,
            'height' => 640
        ],
        'quality' => 90,
        'formats' => [
            'default' => '.jpg',
            'optional' => [
                '.webp'
            ]
        ]
    ],

    'integrations' => [
        'vkApi' => [
            'publishVacancies' => true,
            'publishResumes' => false,

            'app_id' => '',

            /**
             * генерация токена:
             * идем по адресу https://oauth.vk.com/authorize?client_id={app_id}&response_type=code&display=popup&scope=wall,offline
             * полученный code вставляем в ссылку и переходим https://oauth.vk.com/access_token?code={code}&client_secret={app_secret}&client_id={app_id}
             */
            'access_token' => '',
            'owner_id' => '',
            'language' => 'ru'
        ],

        'facebookApi' => [
            'publishVacancies' => true,
            'publishResumes' => false,

            /**
             * Преобразование краткосрочного токена из https://developers.facebook.com/tools/explorer/?classic=0 в долгосрочный:
             * выполняем такой GET (всё можно в postman) https://graph.facebook.com/v2.10/oauth/access_token?grant_type=fb_exchange_token&client_id={app_id}&client_secret={app_secret}&fb_exchange_token={short_lived_token}
             * в результате получим 60-дневный токен.
             */

            'access_token' => '',
            'access_token_long_life' => '',
            'appId' => '',
            'appSecret' => '',
            'locale' => 'ru'
        ]
    ],

    'static_files_version' => '1.0'
];
