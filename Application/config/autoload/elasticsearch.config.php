<?php

$russianStopwords = [
    "и",
    "в",
    "во ",
    "не ",
    "что ",
    "он ",
    "на ",
    "я",
    "с",
    "со ",
    "как ",
    "а",
    "то ",
    "все ",
    "она ",
    "так ",
    "его ",
    "но ",
    "да ",
    "ты ",
    "к",
    "у",
    "же ",
    "вы ",
    "за ",
    "бы ",
    "по ",
    "только ",
    "ее ",
    "мне ",
    "было ",
    "вот ",
    "от ",
    "меня ",
    "еще ",
    "нет ",
    "о",
    "из ",
    "ему ",
    "теперь ",
    "когда ",
    "даже ",
    "ну ",
    "вдруг ",
    "ли ",
    "если ",
    "уже ",
    "или ",
    "ни ",
    "быть ",
    "был ",
    "него ",
    "до ",
    "вас ",
    "нибудь",
    "уж ",
    "вам ",
    "сказал ",
    "ведь ",
    "там ",
    "потом ",
    "себя ",
    "ничего ",
    "ей ",
    "может ",
    "они ",
    "тут ",
    "где ",
    "есть ",
    "надо ",
    "ней ",
    "для ",
    "мы ",
    "тебя ",
    "их ",
    "чем ",
    "была ",
    "сам ",
    "чтоб ",
    "без ",
    "будто ",
    "чего ",
    "раз ",
    "тоже ",
    "себе ",
    "под ",
    "будет ",
    "ж",
    "тогда ",
    "кто ",
    "этот ",
    "того ",
    "потому ",
    "этого ",
    "какой ",
    "совсем ",
    "ним ",
    "здесь ",
    "этом ",
    "один ",
    "почти ",
    "мой ",
    "тем ",
    "чтобы ",
    "нее ",
    "были ",
    "куда ",
    "зачем ",
    "сказать",
    "всех ",
    "можно ",
    "при ",
    "два ",
    "об ",
    "хоть ",
    "после ",
    "над ",
    "тот ",
    "через ",
    "эти ",
    "нас ",
    "про  ",
    "них ",
    "разве",
    "три ",
    "эту ",
    "моя ",
    "свою ",
    "этой ",
    "перед ",
    "иногда",
    "чуть",
    "том ",
    "нельзя",
    "такой",
    "им ",
    "более",
    "всегда",
    "конечно",
    "всю",
    "между",
];

return [
    'hosts' => [
        [
            'scheme' => 'http',
            'host'   => 'localhost',
            'port'   => 9200
        ]
    ],

    'default_settings' => [
        'number_of_shards' => 1,
        'number_of_replicas' => 0,
        'analysis' => [
            'filter' => [
                'edge_ngram' => [
                    'type' => 'edgeNGram',
                    'min_gram' => 2,
                    'max_gram' => 64,
                    'token_chars' => ['letter', 'digit']
                ],
                'stop_filter' => [
                    'type' => 'stop',
                    'stopwords' => ['_english_']
                ]
            ],
            'analyzer' => [
                'edge_ngram_analyzer' => [
                    'type' => 'custom',
                    'tokenizer' => 'standard',
                    'filter' => [
                        'lowercase',
                        'edge_ngram',
                        'stop_filter'
                    ]
                ],
                "keyword_analyzer" => [
                    "filter"    => ["lowercase"],
                    "tokenizer" => "standard"
                ]
            ]
        ]
    ],

    'indexes' => [
        'country' => [
            'mappings' => [
                'country' => [
                    "properties" => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'title_ru' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer'
                        ],
                        'title_en' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer'
                        ]
                    ]
                ]
            ]
        ],

        'region' => [
            'mappings' => [
                'region' => [
                    "properties" => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'country_id' => [
                            'type' => 'integer',
                        ],
                        'title' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer'
                        ],
                        'description' => [
                            "enabled" => false
                        ],
                    ]
                ],
            ]
        ],

        'city' => [
            'mappings' => [
                'city' => [
                    "properties" => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'region_id' => [
                            'type' => 'integer',
                            "null_value" => 0
                        ],
                        'country_id' => [
                            'type' => 'integer',
                        ],
                        'title_en' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer'
                        ],
                        'title_ru' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                            'boost' => 2.0
                        ],
                        'description' => [
                            "enabled" => false
                        ],
                    ]
                ]
            ]
        ],

        'company' => [
            'mappings' => [
                'company' => [
                    'properties' => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'user_id' => [
                            'type' => 'integer',
                        ],
                        'opf' => [
                            "enabled" => false
                        ],
                        'official_name' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer'
                        ]
                    ]
                ]
            ]
        ],

        'profarea' => [
            'mappings' => [
                'profarea' => [
                    "properties" => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'parent_id' => [
                            'type' => 'integer',
                            "null_value" => 0
                        ],
                        'title' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer'
                        ]
                    ]
                ],
            ]
        ],

        'tag' => [
            'settings' => [
                'number_of_shards' => 1,
                'number_of_replicas' => 0,
                'analysis' => [
                    'tokenizer' => [
                        'specials' => [
                            'type' => 'pattern',
                            'pattern' => "[\\s|\\.|,]"
                        ]
                    ],
                    'filter' => [
                        'edge_ngram' => [
                            'type' => 'edgeNGram',
                            'min_gram' => 2,
                            'max_gram' => 32,
                            'token_chars' => ['letter', 'digit', 'symbol']
                        ],
                        'stop_filter' => [
                            'type' => 'stop',
                            'stopwords' => ['_english_']
                        ]
                    ],
                    'analyzer' => [
                        'edge_ngram_analyzer' => [
                            'type' => 'custom',
                            'tokenizer' => 'specials',
                            'filter' => [
                                'lowercase',
                                'edge_ngram',
                                'stop_filter'
                            ]
                        ],
                        "keyword_analyzer" => [
                            "filter"    => ["lowercase"],
                            "tokenizer" => "whitespace"
                        ]
                    ]
                ]
            ],
            'mappings' => [
                'tag' => [
                    "properties" => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'name' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer'
                        ]
                    ]
                ]
            ]
        ],

        'vacancy' => [
            'settings' => [
                'number_of_shards' => 1,
                'number_of_replicas' => 0,
                'analysis' => [
                    'filter' => [
                        'edge_ngram' => [
                            'type' => 'edgeNGram',
                            'min_gram' => 2,
                            'max_gram' => 64,
                            'token_chars' => ['letter', 'digit']
                        ],
                        'stop_filter_eng' => [
                            'type' => 'stop',
                            'stopwords' => ['_english_']
                        ],
                        'stop_filter_rus' => [
                            'type' => 'stop',
                            'stopwords' => $russianStopwords
                        ]
                    ],
                    'analyzer' => [
                        'edge_ngram_analyzer' => [
                            'type' => 'custom',
                            'tokenizer' => 'standard',
                            'filter' => [
                                'lowercase',
                                'edge_ngram',
                                'stop_filter_eng',
                                'stop_filter_rus'
                            ]
                        ],
                        "keyword_analyzer" => [
                            "filter"    => ["lowercase"],
                            "tokenizer" => "standard"
                        ]
                    ]
                ],
            ],
            'mappings' => [
                'vacancy' => [
                    "properties" => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'user_id' => [
                            'type' => 'integer'
                        ],
                        'city_ids' => [],
                        'region_ids' => [],
                        'country_ids' => [],
                        'paycheck_const' => [
                            'type' => 'integer',
                        ],
                        'paycheck_max' => [
                            'type' => 'integer',
                        ],
                        'paycheck_min' => [
                            'type' => 'integer',
                        ],
                        'currency' => [
                            "enabled" => false
                        ],
                        'profarea_id' => [
                            'type' => 'integer',
                        ],
                        'position_name' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                ]
                            ]
                        ],
                        'required_experience' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                        ],
                        'is_remote' => [
                            'type' => 'boolean'
                        ],
                        'employment' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                        ],
                        'full_description' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'visibility' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'update_date' => [
                            "type" => "date",
                            "format" => "yyyy-MM-dd HH:mm:ss"
                        ]
                    ]
                ],
            ]
        ],

        'resume' => [
            'settings' => [
                'number_of_shards' => 1,
                'number_of_replicas' => 0,
                'analysis' => [
                    'filter' => [
                        'edge_ngram' => [
                            'type' => 'edgeNGram',
                            'min_gram' => 2,
                            'max_gram' => 64,
                            'token_chars' => ['letter', 'digit']
                        ],
                        'stop_filter_eng' => [
                            'type' => 'stop',
                            'stopwords' => ['_english_']
                        ],
                        'stop_filter_rus' => [
                            'type' => 'stop',
                            'stopwords' => $russianStopwords
                        ]
                    ],
                    'analyzer' => [
                        'edge_ngram_analyzer' => [
                            'type' => 'custom',
                            'tokenizer' => 'standard',
                            'filter' => [
                                'lowercase',
                                'edge_ngram',
                                'stop_filter_eng',
                                'stop_filter_rus'
                            ]
                        ],
                        "keyword_analyzer" => [
                            "filter"    => ["lowercase"],
                            "tokenizer" => "standard"
                        ]
                    ]
                ],
            ],
            'mappings' => [
                'resume' => [
                    "properties" => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'user_id' => [
                            'type' => 'integer'
                        ],
                        'city_id' => [
                            'type' => 'integer',
                        ],
                        'region_id' => [
                            'type' => 'integer',
                        ],
                        'country_id' => [
                            'type' => 'integer',
                        ],
                        'age' => [
                            'type' => 'integer',
                        ],
                        'gender' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'position_name' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                ]
                            ]
                        ],
                        'desired_paycheck' => [
                            'type' => 'integer'
                        ],
                        'currency_id' => [
                            'type' => 'integer'
                        ],
                        'education_level' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'is_remote' => [
                            'type' => 'boolean'
                        ],
                        'is_relocate_ready' => [
                            'type' => 'boolean'
                        ],
                        'employment' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'visibility' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'update_date' => [
                            "type" => "date",
                            "format" => "yyyy-MM-dd HH:mm:ss"
                        ],
                        'last_work_position' => [
                            'type' => 'text',
                            'analyzer' => 'edge_ngram_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'last_work_date_from' => [
                            "type" => "date",
                            "format" => "yyyy-MM-dd"
                        ],
                        'last_work_date_to' => [
                            "type" => "date",
                            "format" => "yyyy-MM-dd"
                        ],
                        'work_exp_years' => [
                            'type' => 'float',
                        ],
                        'work_exp_string' => [
                            'type' => 'text',
                            "index" => false
                        ],
                    ]
                ],
            ]
        ],

        'view' => [
            'mappings' => [
                'view' => [
                    'properties' => [
                        'user_id' => [
                            'type' => 'integer'
                        ],
                        'company_id' => [
                            'type' => 'integer'
                        ],
                        'entity_id' => [
                            'type' => 'integer'
                        ],
                        'entity_type' => [
                            'type' => 'text',
                            'analyzer' => 'keyword_analyzer',
                            'search_analyzer' => 'keyword_analyzer',
                        ],
                        'date_time' => [
                            "type" => "date",
                            "format" => "yyyy-MM-dd HH:mm:ss"
                        ]
                    ]
                ]
            ]
        ],
    ]
];