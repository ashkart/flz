<?php

use Zend\Router\Http\Segment;
use const Application\ROUTE_STANDARD;
use const Application\ROUTE_INDEX;
use Zend\Router\Http\Wildcard;

require_once 'routes.literal_names.php';

$standardRouteFront = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/[:controller[/:action]]',
        'constraints' => [
            ':id' => '\d+'
        ],
        'defaults' => [
            'module'     => Frontend\Module::NAME,
            'controller' => 'index',
            'action'     => 'index',
        ],
    ],
    'child_routes' => [
        'wildcard' => [
            'type' => Wildcard::class,
            'options' => [
                'key_value_delimiter' => '-',
                'param_delimiter' => '/'
            ]
        ]
    ],
    'may_terminate' => true
];

$indexRouteFront = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/:controller[/id-:id]',
        'constraints' => [
            ':id' => '\d+'
        ],
        'defaults' => [
            'module'     => Frontend\Module::NAME,
            'controller' => 'index',
            'action'     => 'index',
        ],
    ],
    'may_terminate' => true
];

$standardRouteAdmin = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/[:controller[/:action]]',
        'constraints' => [
            ':id' => '\d+'
        ],
        'defaults' => [
            'module'     => Admin\Module::NAME,
            'controller' => 'index',
            'action'     => 'index',
        ],
    ],
    'child_routes' => [
        'wildcard' => [
            'type' => Wildcard::class,
            'options' => [
                'key_value_delimiter' => '-',
                'param_delimiter' => '/'
            ]
        ]
    ],
    'may_terminate' => true
];

$indexRouteAdmin = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/:controller[/id-:id]',
        'constraints' => [
            ':id' => '\d+'
        ],
        'defaults' => [
            'module'     => Admin\Module::NAME,
            'controller' => 'index',
            'action'     => 'index',
        ],
    ],
    'may_terminate' => true
];

return [
    Frontend\Module::NAME => [
        'type' => \Zend\Router\Http\Hostname::class,
        'options' => [
            'route' => '', // Устанавливается в конфиге для каждого модуля
            'defaults' => [
//                \Zend\Mvc\ModuleRouteListener::MODULE_NAMESPACE => 'Frontend',
                'controller' => 'index',
                'action'     => 'index',
            ],
        ],
        'child_routes'  => [
            ROUTE_STANDARD  => $standardRouteFront,
            ROUTE_INDEX     => $indexRouteFront
        ],
        'may_terminate' => true,
    ],

    Admin\Module::NAME => [
        'type' => \Zend\Router\Http\Hostname::class,
        'options' => [
            'route' => '', // Устанавливается в конфиге для каждого модуля
            'defaults' => [
//                \Zend\Mvc\ModuleRouteListener::MODULE_NAMESPACE => 'Admin',
                'controller' => 'index',
                'action'     => 'index',
            ],
        ],
        'child_routes' => [
            ROUTE_STANDARD  => $standardRouteAdmin,
            ROUTE_INDEX     => $indexRouteAdmin
        ],
        'may_terminate' => true,
    ]
];