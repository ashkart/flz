<?php

namespace Application;

use Application\Controller\HttpException;
use Application\Controller\HttpRedirectException;
use Application\Helper\DataSource\TraitDataSource;
use Application\Helper\ErrorHandling;
use Application\Helper\ServiceManager\Factory;
use Zend\Config\Factory as ZendFactory;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

/**
 * @property Response $response
 * @property Request  $request
 */
class Application extends \Zend\Mvc\Application
{
    use TraitDataSource;

    public static function init($configuration = [])
    {
        // Prepare the service manager
        $smConfig = isset($configuration['service_manager'])
            ? $configuration['service_manager']
            : []
        ;

        $smConfig = new ServiceManagerConfig($smConfig);
        $sm = new ServiceManager();
        $smConfig->configureServiceManager($sm);

        $sm->setService('ApplicationConfig', $configuration);

        // Load modules
        $sm->get('ModuleManager')->loadModules();

        self::setServiceManager($sm);

        self::_initViewConfig($configuration);
        self::_initRouteConfig();

        // Prepare list of listeners to bootstrap
        $listenersFromAppConfig = isset($configuration['listeners'])
            ? $configuration['listeners']
            : []
        ;

        $config                     = self::$sm->get(Factory::CONFIG);
        $listenersFromConfigService = isset($config['listeners'])
            ? $config['listeners']
            : []
        ;

        $listeners = array_unique(array_merge(
            $listenersFromConfigService,
            $listenersFromAppConfig
        ));

        return self::$sm->get('Application')->bootstrap($listeners);
    }

    public static function getRouteConfig(bool $isCli) : array
    {
        $config = self::$sm->get('config');

        if ($isCli) {
            $routes = ZendFactory::fromFile(APP_PATH . '/config/cli.routes.config.php');
            $routesConfig = $config['console']['router']['routes'];
        } else {
            $routes = ZendFactory::fromFile(APP_PATH . '/config/routes.config.php');
            $routesConfig = $config['router']['routes'];
        }

        return array_replace_recursive($routes, $routesConfig);
    }

    protected static function _initRouteConfig() : void
    {
        $config = self::$sm->get('config');

        if (\Main\Module::isCli()) {
            $consoleRoutes       = ZendFactory::fromFile(APP_PATH . '/config/cli.routes.config.php');
            $consoleRoutesConfig = &$config['console']['router']['routes'];
            $consoleRoutesConfig = array_replace_recursive($consoleRoutes, $consoleRoutesConfig);
        }

        $routes = ZendFactory::fromFile(APP_PATH . '/config/routes.config.php');
        $routesConfig = & $config['router']['routes'];
        $routesConfig = array_replace_recursive($routes, $routesConfig);

        self::$sm->setAllowOverride(true);
        self::$sm->setService('config', $config);
        self::$sm->setAllowOverride(false);
    }

    protected static function _initViewConfig(array $configuration) : void
    {
        self::$sm->setAllowOverride(true);

        foreach ($configuration['service_manager']['factories'] as $name => $factory) {
            self::$sm->setFactory($name, $factory);
        }

        self::$sm->setAllowOverride(false);
    }

    public function run()
    {
        try {
            return parent::run();
        } catch (\Exception | \Throwable $exception) {
            if ($exception instanceof HttpRedirectException) {
                // Возможны редиректы и не из контроллеров, например при проверке прав доступа

                $query = $this->request->getQuery()->toArray();

                if (!$query || !preg_match('/(\?|\/)request=/', $query['request'] ?? '')) {
                    return $this->_redirectTo($exception->getUrl());
                } else {
                    // Рекурсивное перенаправление, не поддерживаем
                    $exception = new HttpException(500, "Redirect error: url='{$query['_url']}', request='{$query['request']}'", $exception);
                }
            }

            $this->_logException($exception);

            if ($exception instanceof HttpException) {
                $this->response->setStatusCode($exception->getCode());
            } else {
                $this->response->setStatusCode(500);
            }

            $this->response->setContent($exception);
            $this->response->send();

            return $this;
        }
    }

    protected function _redirectTo(string $newUrl)
    {
        $response = $this->response;
        $response->getHeaders()->addHeaderLine('Location', $newUrl);
        $response->setStatusCode(HttpRedirectException::SEE_OTHER_303);
        $response->sendHeaders();
        return $this;
    }

    protected function _logException(\Throwable $exception)
    {
        if (\Main\Module::isCli()) {
            /** @var \Application\Helper\Logger $logger */
            $logger = self::$sm->get(Factory::LOGGER)->getLogger();
            $logger->err($exception->getMessage());
        } else {
            /** @var ErrorHandling $errorHandler */
            $errorHandler = self::$sm->get(Factory::ERROR_HANDLER);
            $errorHandler->logException($exception, $this->request);
        }
    }
}