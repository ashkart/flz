<?php


namespace Library\Master\Session;

use \Zend\Session\SessionManager as ZendSessionManager;

class SessionManager extends ZendSessionManager
{
    /**
     * @var string
     */
    protected $_sessionIdSalt = 'k49fJre#';

    /**
     * Метод возвращает хешкод идентификатора сессии
     *
     * @return string
     */
    public function getSessionHash() : string
    {
        static $sessionHashArray = null;

        if ($sessionHashArray === null) {
            $sessionHashArray = md5($this->getId() . $this->_sessionIdSalt);
        }

        return $sessionHashArray;
    }

    /**
     * @deprecated Вместо данного метода необходимо использовать getSessionHash(),
     * возвращающий захешированный session_id.
     * Идентификатор сессии в чистом виде не хранится в базе данных и логах, тк это небезопасно.
     *
     * @return string
     */
    public function getId()
    {
        return parent::getId();
    }
}