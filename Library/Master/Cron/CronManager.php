<?php

namespace Library\Master\Cron;

use TiBeN\CrontabManager\CrontabAdapter;
use TiBeN\CrontabManager\CrontabJob;
use TiBeN\CrontabManager\CrontabRepository;

class CronManager
{
    public static function updateCronTable(array $cronTabLines, bool $purgeExiting = false) {
        $cronTabRepository = new CrontabRepository(new CrontabAdapter());

        if ($purgeExiting) {
            $currentJobs = $cronTabRepository->getJobs();

            foreach ($currentJobs as $currentJob) {
                $cronTabRepository->removeJob($currentJob);
            }
        }


        foreach ($cronTabLines as $cronTabLine) {
            $cronTabJob = CrontabJob::createFromCrontabLine($cronTabLine);
            $cronTabRepository->addJob($cronTabJob);
        }

        $cronTabRepository->persist();
    }
}