<?php

namespace Library\Master\Date;

class Date extends DateTime
{
    const MYSQL_FORMAT = 'Y-m-d';
    const DATE_FORMAT_RU   = 'd.m.Y';

    const NORMAL_FORMATS = [
        self::MYSQL_FORMAT,
        self::DATE_FORMAT_RU
    ];
}