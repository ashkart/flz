<?php

namespace Library\Master\Date;

use DateTimeZone;

class DateTime
{
    const MYSQL_FORMAT     = 'Y-m-d H:i:s';
    const UNIX_TIMESTAMP   = 'U';
    const DATE_FORMAT_RU   = 'd.m.Y H:i:s';

    const NORMAL_FORMATS = [
        self::MYSQL_FORMAT,
        self::DATE_FORMAT_RU
    ];

    /**
     * @var \DateTime
     */
    protected $_dateTime;

    /**
     * @param string $format
     * @param string $value
     * @param DateTimeZone|null $timezone
     * @return static
     * @throws \Exception
     */
    public static function createFromFormat(string $format, string $value, DateTimeZone $timezone = null) : self
    {
        $date = \DateTime::createFromFormat($format, $value);

        if ($date instanceof \DateTime) {
            $date->setTimezone($timezone ?? new DateTimeZone(date_default_timezone_get()));

            return new static($date);
        }

        throw new \Exception('Invalid value data (wrong format?)');
    }

    /**
     * @param string $value
     * @return static
     */
    public static function createFromMysqlFormat(string $value) : self
    {
        return static::createFromFormat(static::MYSQL_FORMAT, $value);
    }

    /**
     * @param int|null $timestamp
     * @param string|null $timezone
     * @return static
     */
    public static function createFromTimestamp(int $timestamp = null, string $timezone = null) : self
    {
        if ($timestamp === null) {
            $timestamp = time();
        }

        if ($timezone === null) {
            $timezone = new DateTimeZone(date_default_timezone_get());
        }

        return new static(\DateTime::createFromFormat(self::UNIX_TIMESTAMP, $timestamp)->setTimezone($timezone));
    }

    /**
     * @param string $value
     * @param DateTimeZone|null $timezone
     * @return static
     * @throws \Exception
     */
    public static function createFromNormalFormats(string $value, DateTimeZone $timezone = null) : self
    {
        foreach (static::NORMAL_FORMATS as $format) {
            try {
                return static::createFromFormat($format, $value, $timezone);
            } catch (\Exception $e) {
                // ожидаемо, ничего не предпринимаем
            }
        }

        throw new \Exception('Invalid value data (wrong format?)');
    }

    public static function reformat(string $normalFormattedDate, string $outFormat) : string
    {
        return static::createFromNormalFormats($normalFormattedDate)->format($outFormat);
    }

    public function __construct(\DateTime $value = null, $timezone = null)
    {
        if ($value instanceof \DateTime) {
            $this->_dateTime = $value;
        } else {
            $this->_dateTime = new \DateTime();
        }
    }

    public function mysqlFormat() : string
    {
        return $this->format(static::MYSQL_FORMAT);
    }

    public function format(string $format) : string
    {
        return $this->_dateTime->format($format);
    }

    public function getDate() : Date
    {
        return new Date($this->_dateTime);
    }
}