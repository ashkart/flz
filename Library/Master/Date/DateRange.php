<?php

namespace Library\Master\Date;

class DateRange
{
    /**
     * @var Date
     */
    protected $_dateStart;

    /**
     * @var Date
     */
    protected $_dateEnd = null;

    /**
     * DateRange constructor.
     * @param Date $_dateStart
     * @param Date $_dateEnd
     */
    public function __construct(Date $dateStart, ?Date $dateEnd)
    {
        if ($dateEnd && $dateEnd < $dateStart) {
            throw new \Exception('Date end cannot be less than date start');
        }

        $this->_dateStart = $dateStart;
        $this->_dateEnd   = $dateEnd;
    }

    public function getDateStart(): Date
    {
        return $this->_dateStart;
    }

    public function getDateEnd(): ?Date
    {
        return $this->_dateEnd;
    }

    public function interval() : \DateInterval
    {
        $dateEnd = $this->_dateEnd ? date_create($this->_dateEnd->mysqlFormat()) : date_create('now');
        $dateStart = date_create($this->_dateStart->mysqlFormat());

        return date_diff($dateStart, $dateEnd, true);
    }

    public function getFullYears() : int
    {
        return $this->interval()->y;
    }
}