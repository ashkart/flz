<?php

namespace Library\Master\Controller\Plugin;

use Zend\EventManager\EventInterface;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\RequestInterface;

abstract class AbstractPlugin extends \Zend\Mvc\Controller\Plugin\AbstractPlugin
{
    const METHOD_GET     = 'GET';
    const METHOD_HEAD    = 'HEAD';
    const METHOD_POST    = 'POST';
    const METHOD_PUT     = 'PUT';
    const METHOD_DELETE  = 'DELETE';
    const METHOD_CONNECT = 'CONNECT';
    const METHOD_OPTIONS = 'OPTIONS';
    const METHOD_TRACE   = 'TRACE';
    const METHOD_PATCH   = 'PATCH';

    const ALL_METHODS = [
        self::METHOD_GET,
        self::METHOD_HEAD,
        self::METHOD_POST,
        self::METHOD_PUT,
        self::METHOD_DELETE,
        self::METHOD_CONNECT,
        self::METHOD_OPTIONS,
        self::METHOD_TRACE,
        self::METHOD_PATCH,
    ];

    const COMMON_METHODS = [
        self::METHOD_GET,
        self::METHOD_HEAD,
        self::METHOD_POST,
    ];

    const EXCLUDED = [];

    const EXCLUDE_JS_REQUESTS = false;

    /**
     * @var ServiceLocatorInterface
     */
    protected $_sm;
    /**
     * @var MvcEvent | EventInterface
     */
    protected $_event;

    /**
     * @param MvcEvent | EventInterface $e
     */
    public function __invoke(EventInterface $e)
    {
        $this->_event = $e;
        return $this->process();
    }

    public function __construct(ServiceLocatorInterface $sm)
    {
        $this->_sm = $sm;
    }

    public abstract function process();

    protected function _setup(EventInterface $e) : bool
    {
        if ($e instanceof MvcEvent) {
            $this->_event = $e;
            return true;
        }

        return false;
    }
}