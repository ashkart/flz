<?php


namespace Library\Master\CrawlerDetector;


use Jaybizzle\CrawlerDetect\CrawlerDetect;

class CrawlerDetector
{
    /**
     * @var CrawlerDetect
     */
    protected $_crawlerDetect;

    public function __construct()
    {
        $this->_crawlerDetect = new CrawlerDetect();
    }

    public function isCrawler(string $userAgent) : bool
    {
        return $this->_crawlerDetect->isCrawler($userAgent);
    }
}