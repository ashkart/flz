<?php

namespace Library\Master\Form;

use Library\Master\Date\Date;
use Zend\Form\Element;
use Zend\Form\Element\Submit;

class Form extends \Zend\Form\Form
{
    use StandardInputFilterSpec;
    use TraitNormalizeData;

    const SUBMIT = 'submit';

    const CHECKED   = 1;
    const UNCHECKED = 0;

    const CHECKED_VALUES = [true, 1, '1', 'true'];

    public function __construct()
    {
        parent::__construct();
    }

    public function setAction(string $action) : self
    {
        $this->setAttribute('action', $action);
        return $this;
    }

    public function getAction() : string
    {
        return $this->getAttribute('action');
    }

    public function addSubmit()
    {
        $this->add((new Submit(self::SUBMIT))->setLabel('Сохранить'));
    }

    public function setData($data)
    {
        $formElements = $this->getElements();

        $this->_normalizeElementData($formElements, $data);

        return parent::setData($data);
    }
}