<?php

namespace Library\Master\Form;

class Fieldset extends \Zend\Form\Fieldset
{
    use StandardInputFilterSpec;
    use TraitNormalizeData;

    public function populateValues($data)
    {
        $formElements = $this->getElements();

        $this->_normalizeElementData($formElements, $data);

        parent::populateValues($data);
    }
}