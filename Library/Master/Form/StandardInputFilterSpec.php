<?php

namespace Library\Master\Form;

use Zend\Filter\StringTrim;
use Zend\Validator\StringLength;

trait StandardInputFilterSpec
{
    protected function _standardTextFilter(int $maxLength, int $minLength = 0, bool $required = false, bool $trimString = false) : array
    {
        $stringTrimValidator = [
            'name' => StringTrim::class,
        ];

        $inputFilter = [
            'required'    => $required,
            'allow_empty' => !$required,
            'validators'  => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => $minLength,
                        'max' => $maxLength
                    ]
                ]
            ],
            'filters' => []
        ];

        if ($trimString) {
            $inputFilter['filters'][] = $stringTrimValidator;
        }

        return $inputFilter;
    }
}