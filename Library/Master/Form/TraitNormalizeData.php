<?php

namespace Library\Master\Form;

use Library\Master\Date\Date;
use Zend\Escaper\Escaper;
use Zend\Form\Element;
use Zend\View\Helper\EscapeJs;

trait TraitNormalizeData
{
    protected function _normalizeElementData(array $elements, array & $data) : self
    {
        /** @var Element $element */
        foreach ($elements as $element) {
            // trying to prevent xss
            if (isset($data[$element->getName()])) {
                $data[$element->getName()] = strip_tags($data[$element->getName()], "<h1><h2><h3><h4><h5><h6><pre><p><ul><li><em><strong>");
            }

            if ($element instanceof Element\Collection || $element instanceof \Zend\Form\Fieldset) {
                if ($element->getFieldsets()) {
                    return $this->_normalizeElementData($element->getFieldsets(), $data);
                } else {
                    return $this->_normalizeElementData($element->getElements(), $data);
                }
            }

            if ($element instanceof Element\Checkbox) {
                $value = $data[$element->getName()];

                if (in_array($value, self::CHECKED_VALUES, true)) {
                    $data[$element->getName()] = $element->getCheckedValue();
                } else {
                    $data[$element->getName()] = $element->getUncheckedValue();
                }
            }
        }

        return $this;
    }
}