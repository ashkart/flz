<?php

namespace Library\Master\Autoload;

class Autoloader extends \Zend\Loader\StandardAutoloader
{
    protected function loadClass($class, $type)
    {
        try {
            $loadResult = parent::loadClass($class, $type);

            if (!$loadResult && in_array($class, $this->namespaces)) {
                throw new \Exception("Cant load class '$class'");
            }

            return $loadResult;
        } catch (\Throwable $exception) {
            echo $exception;
            return false;
        }
    }
}