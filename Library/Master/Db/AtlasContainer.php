<?php

namespace Library\Master\Db;

use Atlas\Orm\Table\TableEvents;
use Library\Master\Db\Mapper\IdentityMap;

class AtlasContainer extends \Atlas\Orm\AtlasContainer
{
    /**
     *
     * Sets a table into the table locator.
     *
     * @param string $tableClass The table class name.
     *
     */
    protected function setTable(string $tableClass) : void
    {
        $eventsClass = $tableClass . 'Events';

        $eventsClass = class_exists($eventsClass)
            ? $eventsClass
            : TableEvents::CLASS;

        $factory = function () use ($tableClass, $eventsClass) {
            return new $tableClass(
                $this->connectionManager,
                $this->queryFactory,
                new IdentityMap(),
                $this->newInstance($eventsClass)
            );
        };

        $this->tableLocator->set($tableClass, $factory);
    }

    protected function setConnectionManager(array $args): string
    {
        $driver = parent::setConnectionManager($args);

        $this->connectionManager = new ConnectionManager($this->getConnectionLocator());

        return $driver;
    }
}