<?php


namespace Library\Master\Db;

use Atlas\Orm\Table\ConnectionManager as AtlasConnectionManager;
use Aura\Sql\ExtendedPdo;
use Aura\Sql\ExtendedPdoInterface;
use PDOException;


class ConnectionManager extends AtlasConnectionManager
{
    const DEFAULT_DB_CHECK_INTERVAL = 60; // 1 minute

    /**
     * @var int
     */
    protected $_lastDbCheckTime = 0;

    protected function getConnection(string $type, string $tableClass): ExtendedPdoInterface
    {

        $conn = parent::getConnection($type, $tableClass);

        if (
            PHP_SAPI === 'cli' &&
            $conn instanceof ExtendedPdo &&
            time() - $this->_lastDbCheckTime >= self::DEFAULT_DB_CHECK_INTERVAL &&
            !$this->isAlive($conn)
        ) {
            $conn->disconnect();
            $conn->connect();
        }

        return $conn;
    }

    public function isAlive(ExtendedPdoInterface $connection) : bool
    {
        $this->_lastDbCheckTime = time();

        try {
            /** @var \PDO $pdo */
            $pdo = $connection->getPdo();
            $pdo->query('SELECT 1');
        } catch (PDOException $e) {
            return false;
        }

        return true;
    }
}