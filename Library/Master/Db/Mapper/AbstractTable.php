<?php

namespace Library\Master\Db\Mapper;

/**
 * Type-hinting class only.
 * @method IdentityMap getIdentityMap()
 */
abstract class AbstractTable extends \Atlas\Orm\Table\AbstractTable
{

}