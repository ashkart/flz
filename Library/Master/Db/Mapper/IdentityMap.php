<?php

namespace Library\Master\Db\Mapper;

use Atlas\Orm\Table\RowInterface;

class IdentityMap extends \Atlas\Orm\Table\IdentityMap
{
    public function deleteRow(RowInterface $row, array $primaryKey)
    {
        if (!$this->hasRow($row)) {
            return;
        }

        $primary = [];

        foreach ($primaryKey as $primaryCol) {
            $primary[$primaryCol] = $row->$primaryCol;
        }

        $serial = $this->getSerial($primary);
        unset($this->serialToRow[$serial]);
        unset($this->rowToSerial[$row]);
        unset($this->initial[$row]);
    }
}