<?php

namespace Library\Master\Db\Adapter;

use Library\Master\Db\AtlasContainer;
use Atlas\Orm\Mapper\MapperLocator;
use Aura\Sql\ConnectionLocator;

class Adapter
{
    /**
     * @var AtlasContainer
     */
    protected $_dbAdapter;

    /**
     * @var ConnectionLocator
     */
    protected $_connectionLocator;

    /**
     * @var MapperLocator
     */
    protected $_mapperLocator;

    public function __construct(string $dsn, string $userName, string $password)
    {
        $this->_dbAdapter         = new AtlasContainer($dsn, $userName, $password);
        $this->_connectionLocator = $this->_dbAdapter->getConnectionLocator();
        $this->_mapperLocator     = $this->_dbAdapter->getMapperLocator();
    }

    public function beginTransaction() : bool
    {
        return $this->_connectionLocator->getDefault()->beginTransaction();
    }

    public function commit() : bool
    {
        return $this->_connectionLocator->getDefault()->commit();
    }

    public function rollBack()
    {
        return $this->_connectionLocator->getDefault()->rollBack();
    }

    public function inTransaction() : bool
    {
        return $this->_connectionLocator->getDefault()->inTransaction();
    }

    public function setMappers(array $mappers)
    {
        $this->_dbAdapter->setMappers($mappers);
    }

    public function getMapperLocator() : MapperLocator
    {
        return $this->_mapperLocator;
    }
}