<?php

namespace Library\Master\Cache;

use Zend\Cache\Storage\Adapter\RedisResourceManager;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\StorageFactory;

abstract class RedisStorageFactory extends StorageFactory
{
    public static function adapterFactory($adapterName, $options = [])
    {
        if ($adapterName instanceof StorageInterface) {
            // $adapterName is already an adapter object
            $adapter = $adapterName;
        } else {
            $adapter = static::getAdapterPluginManager()->get($adapterName);
        }

        if ($options) {
            $redisResourceManager = & $options['resource_manager'];

            if (! $redisResourceManager instanceof RedisResourceManager) {
                $redisResourceManager = new $redisResourceManager();
            }

            $adapter->setOptions($options);
        }

        return $adapter;
    }
}