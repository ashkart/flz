<?php


namespace Library\Master\SocialNet\Vk;

use Library\Master\SocialNet\SocialNetApiInterface;
use VK\Client\VKApiClient;

class ApiClient implements SocialNetApiInterface
{
    /**
     * @var VKApiClient
     */
    protected $_client;

    /**
     * @var string
     */
    protected $_accessToken;

    /**
     * @var string
     */
    protected $_ownerId;

    public function __construct(string $ownerId, string $accessToken, string $apiVersion, string $language = 'ru')
    {
        $this->_client      = new VKApiClient($apiVersion, $language);
        $this->_accessToken = $accessToken;
        $this->_ownerId     = $ownerId;
    }

    public function wallPost(string $message, string $snippetUrl)
    {
        $result = $this->_client->wall()->post($this->_accessToken, [
            'from_group' => 1,
            'message'  => $message,
            'owner_id' => $this->_ownerId,
            'signed' => 0,
            'attachments' => [$snippetUrl]
        ]);

        return $result;
    }

    public function getUsersInfo(array $userIds, string $accessToken)
    {
        return $this->_client->users()->get($accessToken, [
            'user_ids' => $userIds,
        ]);
    }
}