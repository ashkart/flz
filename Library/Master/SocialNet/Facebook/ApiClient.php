<?php


namespace Library\Master\SocialNet\Facebook;

use Facebook\Facebook;
use Library\Master\SocialNet\SocialNetApiInterface;

class ApiClient implements SocialNetApiInterface
{
    /**
     * @var Facebook
     */
    protected $_client;

    /**
     * @var string
     */
    protected $_accessToken;

    /**
     * @var string
     */
    protected $_appSecret;

    /**
     * @var string
     */
    protected $_groupId;

    public function __construct(string $appId, string $groupId, string $appSecret, string $accessToken, string $locale = 'ru')
    {
        $config = [
            'app_id'  => $appId,
            'app_secret' => $appSecret,
            'locale' => $locale,
            'default_graph_version' => 'v3.2'
        ];

        $this->_client = new Facebook($config);
        $this->_client->setDefaultAccessToken($accessToken);

        $this->_accessToken = $accessToken;
        $this->_appSecret   = $appSecret;
        $this->_groupId     = $groupId;
    }

    public function wallPost(string $message, string $snippetUrl)
    {
        $result = $this->_client->post(
            "/{$this->_groupId}/feed",
            [
                "message" => $message,
                "link" => $snippetUrl,
            ],
            $this->_accessToken
        );

        return $result;
    }

    public function getUserInfo($accessToken) : array
    {
        $response = $this->_client->get('/me?fields=id,first_name,email', $accessToken);

        return $response->getGraphUser()->asArray();
    }

    public function codeAccessTokenExchange(string $code, string $redirectUri) : string
    {
        $accessToken = $this->_client->getOAuth2Client()->getAccessTokenFromCode($code, $redirectUri);

        return $accessToken->getValue();
    }
}