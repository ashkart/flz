<?php


namespace Library\Master\SocialNet;

interface SocialNetApiInterface
{
    public function wallPost(string $message, string $snippetUrl);
}