<?php

namespace Library\Master\Http\Header;

use Zend\Http\Header\Exception;
use Zend\Http\Header\GenericHeader;
use Zend\Http\Header\HeaderInterface;

class XTotalCount implements HeaderInterface
{
    const HEADER_NAME = 'X-Total-Count';
    /**
     * @var int
     */
    protected $_currentValue;

    /**
     * @var int
     */
    protected $_totalValue;

    public function __construct(int $currentValue, int $totalValue = null)
    {
        $this->_currentValue = $currentValue;
        $this->_totalValue   = $totalValue;
    }

    public static function fromString($headerLine)
    {
        list($name, $value) = GenericHeader::splitHeaderLine($headerLine);

        // check to ensure proper header type for this factory
        if (strtolower($name) !== self::HEADER_NAME) {
            throw new Exception\InvalidArgumentException(sprintf(
                'Invalid header line for ' . self::HEADER_NAME . ' string: "%s"',
                $name
            ));
        }

        list ($current, $total) = explode('/', $value, 2);

        $header = new static($current, $total);

        return $header;
    }

    /**
     * Retrieve header name
     *
     * @return string
     */
    public function getFieldName()
    {
        return self::HEADER_NAME;
    }

    /**
     * Retrieve header value
     *
     * @return string
     */
    public function getFieldValue()
    {
        return $this->_currentValue;
    }

    /**
     * Cast to string
     *
     * Returns in form of "NAME: VALUE"
     *
     * @return string
     */
    public function toString()
    {
        return self::HEADER_NAME . ": {$this->_currentValue}/{$this->_totalValue}";
    }
}