<?php

namespace Library\Master\Mvc;

use Zend\Http\PhpEnvironment\Request;
use Zend\Stdlib\RequestInterface;

class RequestFactory implements \Zend\ServiceManager\Factory\FactoryInterface
{
    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RequestInterface
     */
    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName, array $options = null)
    {
        $request = null;

        if (PHP_SAPI !== 'cli') {
            $request = new Request();
        } else {
            $request = new \Zend\Console\Request();
        }

        return $request;
    }
}