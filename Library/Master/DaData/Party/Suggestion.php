<?php

namespace Library\Master\DaData\Party;

class Suggestion
{
    protected $_suggestion;

    public function __construct(array $suggestion)
    {
        $this->_suggestion = $suggestion;
    }

    public function getOpfShort() : ?string
    {
        return $this->_suggestion['data']['opf']['short'];
    }

    public function getOfficialName() : ?string
    {
        return $this->_suggestion['data']['name']['full'];
    }

    public function getAddress() : ?string
    {
        return $this->_suggestion['data']['address']['data']['source'];
    }

    public function getNameWithFullOpf() : ?string
    {
        return $this->_suggestion['data']['name']['full_with_opf'];
    }

    public function getKpp() : ?string
    {
        return $this->_suggestion['data']['kpp'];
    }

    public function getOgrn() : ?string
    {
        return $this->_suggestion['data']['ogrn'];
    }

    public function getInn() : ?string
    {
        return $this->_suggestion['data']['inn'];
    }
}