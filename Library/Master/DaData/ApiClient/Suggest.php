<?php

namespace Library\Master\DaData\ApiClient;


use Library\Master\DaData\Party\Suggestion;

class Suggest
{
    /**
     * Ресурс - организации
     */
    const RESOURCE_PARTY = 'party';

    /**
     * @var string
     */
    private $_url;

    /**
     * @var string
     */
    private $_token;

    public function __construct(string $token, string $url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/')
    {
        $this->_token = $token;
        $this->_url   = $url;
    }

    public function suggestOneCompany(string $queryValue) : ? Suggestion
    {
        $data = [
            'query' => $queryValue,
            'count' => 1
        ];

        $responseData = $this->suggest($data, self::RESOURCE_PARTY);

        $suggestionData = $responseData['suggestions'][0] ?? [];

        if (!$suggestionData) {
            return null;
        }

        return new Suggestion($suggestionData);
    }

    public function suggest(array $data, string $resource) : array
    {
        $options = [
            'http' => [
                'method' => 'POST',
                'header' => [
                    'Content-type: application/json',
                    'Authorization: Token ' . $this->_token,
                ],
                'content' => json_encode($data),
            ],
        ];

        $context = stream_context_create($options);
        $result  = file_get_contents($this->_url . $resource, false, $context);

        return json_decode($result, JSON_OBJECT_AS_ARRAY);
    }
}