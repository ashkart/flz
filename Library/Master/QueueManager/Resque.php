<?php

namespace Library\Master\QueueManager;

use Psr\Log\LoggerInterface;

class Resque
{
    public function __construct(string $host, int $port)
    {
        \Resque::setBackend("$host:$port");
    }

    /**
     * @return string new job id
     */
    public function enqueue(string $queueName, string $jobClass, array $args) : string
    {
        return \Resque::enqueue($queueName, $jobClass, $args);
    }

    public function dequeue(string $queueName, array $items) : string
    {
        return \Resque::dequeue($queueName, $items);
    }

    public function runNewWorker(array $queues, LoggerInterface $logger)
    {
        $worker = new \Resque_Worker($queues);
        $this->runCustomWorker($worker, $logger);
    }

    public function runCustomWorker(\Resque_Worker $worker, LoggerInterface $logger, int $interval = \Resque::DEFAULT_INTERVAL)
    {
        $worker->setLogger($logger);
        $worker->work($interval, false);
    }
}