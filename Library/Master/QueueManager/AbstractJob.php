<?php

namespace Library\Master\QueueManager;

abstract class AbstractJob
{
    /**
     * @var array
     */
    public $args;

    abstract public function perform();
}