<?php

$config = require __DIR__ . '/../Application/config/autoload/local.php';

$dbConfig = $config['db'];

$server   = $dbConfig['server'];
$host     = $dbConfig['host'];
$user     = $dbConfig['username'];
$password = $dbConfig['password'];
$dbName   = $dbConfig['default_database'];

return [ "$server:dbname=$dbName;host=$host", $user, $password ];