#@maxim

CREATE TABLE `user` (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	email VARCHAR(512) NOT NULL,
	password VARCHAR(512) NOT NULL,
	role ENUM('employee','recruiter','admin') NOT NULL COMMENT 'Роль пользователя системы',
	first_name VARCHAR(512) NOT NULL COMMENT 'Имя',
	last_name VARCHAR(512) NOT NULL COMMENT 'Фамилия',
	middle_name VARCHAR(512) NOT NULL COMMENT 'Отчество',
	birth_date DATE NOT NULL COMMENT 'Дата рождения',
	last_visit_date DATETIME NULL DEFAULT NULL COMMENT 'Дата последнего входа в систему',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `uidx_user_email` (`email`),
	INDEX `idx_user_email_pass` (`email`, `password`)
);

ALTER TABLE `user`
ADD COLUMN create_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP()
COMMENT 'Дата создания пользователя'
AFTER `last_visit_date`;

CREATE TABLE `profarea` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`parent_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	`title` VARCHAR(1024) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_profarea_parent_id` (`parent_id`),
	CONSTRAINT `fk_profarea_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `profarea` (`id`) ON UPDATE CASCADE ON DELETE SET NULL
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

CREATE TABLE `vacancy` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL COMMENT 'Связанный ползователь',
	`city_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Город',
	`position_name` VARCHAR(1024) NOT NULL COMMENT 'Название должности',
	`profarea_id` INT(11) UNSIGNED NOT NULL COMMENT 'профобласть вакантной должности',
	`required_experience` ENUM('0','1','2','3','5') NULL DEFAULT '0' COMMENT 'требуемый опыт работы',
	`requirements` TEXT NOT NULL COMMENT 'Требования',
	`liabilities` TEXT NOT NULL COMMENT 'Должностные обязанности',
	`conditions` TEXT NULL COMMENT 'Условия',
	`description` TEXT NULL COMMENT 'Описание',
	`schedule_type` ENUM('full_day','flexible','shift','watch') NOT NULL DEFAULT 'full_day' COMMENT 'тип графика работы',
	`is_remote` INT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'удаленная работа',
	`paycheck_min` INT(11) NULL DEFAULT NULL COMMENT 'ЗП от',
	`paycheck_max` INT(11) NULL DEFAULT NULL COMMENT 'ЗП до',
	`paycheck_const` INT(11) NULL DEFAULT NULL COMMENT 'ЗП (стабильная)',
	`create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания вакансии',
	`update_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата последнего обновления',
	PRIMARY KEY (`id`),
	INDEX `fk_vacancy_user_id` (`user_id`),
	INDEX `fk_vacancy_profarea_id` (`profarea_id`),
	CONSTRAINT `fk_vacancy_profarea_id` FOREIGN KEY (`profarea_id`) REFERENCES `profarea` (`id`) ON UPDATE CASCADE,
	CONSTRAINT `fk_vacancy_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

CREATE TABLE `resume` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL COMMENT 'Связанный ползователь',
	`position_name` VARCHAR(1024) NOT NULL COMMENT 'Название должности',
	`education_level` ENUM('basic_general','secondary_general','secondary_vocational','higher_bachelor','higher_master','higher_hq') NULL DEFAULT NULL COMMENT 'Уровень образования',
	`autodescription` TEXT NULL COMMENT 'О себе',
	`desired_paycheck` INT(11) NULL DEFAULT NULL COMMENT 'Желаемая ЗП',
	PRIMARY KEY (`id`),
	INDEX `fk_resume_user_id` (`user_id`),
	CONSTRAINT `resume_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE `work_experience` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`resume_id` INT(11) UNSIGNED NOT NULL COMMENT 'Связанное резюме',
	`position_name` VARCHAR(1024) NOT NULL COMMENT 'Название должности',
	`description` VARCHAR(4096) NOT NULL COMMENT 'Описание опыта',
	`date_start` DATE NOT NULL COMMENT 'Дата начала работы',
	`date_end` DATE NULL DEFAULT NULL COMMENT 'Дата окончания работы',
	PRIMARY KEY (`id`),
	INDEX `fk_wokr_exp_resume_id` (`resume_id`),
	CONSTRAINT `working_experience_ibfk_1` FOREIGN KEY (`resume_id`) REFERENCES `resume` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE `education` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`resume_id` INT(11) UNSIGNED NOT NULL COMMENT 'Связанное резюме',
	`specialty` VARCHAR(1024) NOT NULL COMMENT 'Название специальности',
	`educational_organization` VARCHAR(1024) NOT NULL COMMENT 'Образовательное учереждение (наименование)',
	`date_start` DATE NOT NULL COMMENT 'Дата начала обучения',
	`date_end` DATE NULL DEFAULT NULL COMMENT 'Дата окончания обучения',
	PRIMARY KEY (`id`),
	INDEX `fk_education_resume_id` (`resume_id`),
	CONSTRAINT `education_ibfk_1` FOREIGN KEY (`resume_id`) REFERENCES `resume` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE `country` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title_ru` VARCHAR(60) NOT NULL,
	`title_en` VARCHAR(60) NOT NULL,
	PRIMARY KEY (`id`)
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

CREATE TABLE `region` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`country_id` INT(11) UNSIGNED NOT NULL,
	`title` VARCHAR(150) NOT NULL,
	`code` INT(4) NULL DEFAULT NULL COMMENT 'Код региона по федеральной адресной системе. (для РФ ФИАС)',
	PRIMARY KEY (`id`),
	INDEX `region_country_id` (`country_id`),
	CONSTRAINT `region_country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

CREATE TABLE `city` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`country_id` INT(11) UNSIGNED NOT NULL,
	`region_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	`title` VARCHAR(150) NOT NULL,
	`area` VARCHAR(150) NULL DEFAULT NULL,
	`region` VARCHAR(150) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `city_country_id` (`country_id`),
	INDEX `city_region_id` (`region_id`),
	INDEX `idx_key_title` (`title`),
	FULLTEXT INDEX `idx_title` (`title`),
	CONSTRAINT `city_country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
	CONSTRAINT `city_region_id` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

alter table `resume`
add column city_id int(11) unsigned null default null
comment 'Город'
after `user_id`;

alter table `resume`
add column create_date datetime not null default CURRENT_TIMESTAMP()
comment 'Дата создания резюме'
after `desired_paycheck`;

alter table `resume`
add column update_date datetime not null default CURRENT_TIMESTAMP()
comment 'Дата последнего обновления'
after `create_date`;

alter table `user`
add column city_id int(11) unsigned not null
comment 'Город пользователя'
after `birth_date`;

alter table region
drop column title_en;

alter table region
change column title_ru title VARCHAR(150) NOT NULL after `country_id`;

CREATE DATABASE `fias` COLLATE 'utf8_general_ci';

use fias;

CREATE TABLE `region_code` (
	`code` INT(4) UNSIGNED NOT NULL COMMENT 'Код региона',
	`name` VARCHAR(128) NOT NULL COMMENT 'Название региона',
	UNIQUE INDEX `uidx_code` (`code`)
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

CREATE TABLE `AddressObject` (
	`AOGUID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'Глобальный уникальный идентификатор адресного объекта ',
	`FORMALNAME` VARCHAR(120) NULL DEFAULT NULL COMMENT 'Формализованное наименование',
	`REGIONCODE` VARCHAR(2) NULL DEFAULT NULL COMMENT 'Код региона',
	`AUTOCODE` VARCHAR(1) NULL DEFAULT NULL COMMENT 'Код автономии',
	`AREACODE` VARCHAR(3) NULL DEFAULT NULL COMMENT 'Код района',
	`CITYCODE` VARCHAR(3) NULL DEFAULT NULL COMMENT 'Код города',
	`CTARCODE` VARCHAR(3) NULL DEFAULT NULL COMMENT 'Код внутригородского района',
	`PLACECODE` VARCHAR(3) NULL DEFAULT NULL COMMENT 'Код населенного пункта',
	`STREETCODE` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Код улицы',
	`EXTRCODE` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Код дополнительного адресообразующего элемента',
	`SEXTCODE` VARCHAR(3) NULL DEFAULT NULL COMMENT 'Код подчиненного дополнительного адресообразующего элемента',
	`OFFNAME` VARCHAR(120) NULL DEFAULT NULL COMMENT 'Официальное наименование',
	`POSTALCODE` VARCHAR(6) NULL DEFAULT NULL COMMENT 'Почтовый индекс',
	`IFNSFL` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
	`TERRIFNSFL` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
	`IFNSUL` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
	`TERRIFNSUL` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
	`OKATO` VARCHAR(11) NULL DEFAULT NULL COMMENT 'ОКАТО',
	`OKTMO` VARCHAR(11) NULL DEFAULT NULL COMMENT 'ОКТМО',
	`UPDATEDATE` DATE NULL DEFAULT NULL COMMENT 'Дата  внесения (обновления) записи',
	`SHORTNAME` VARCHAR(10) NULL DEFAULT NULL COMMENT 'Краткое наименование типа объекта',
	`AOLEVEL` INT(11) NULL DEFAULT NULL COMMENT 'Уровень адресного объекта ',
	`PARENTGUID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'Идентификатор объекта родительского объекта',
	`AOID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'Уникальный идентификатор записи. Ключевое поле',
	`PREVID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'Идентификатор записи связывания с предыдушей исторической записью',
	`NEXTID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'Идентификатор записи  связывания с последующей исторической записью',
	`CODE` VARCHAR(17) NULL DEFAULT NULL COMMENT 'Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0. ',
	`PLAINCODE` VARCHAR(15) NULL DEFAULT NULL COMMENT 'Код адресного объекта из КЛАДР 4.0 одной строкой без признака актуальности (последних двух цифр)',
	`ACTSTATUS` INT(11) NULL DEFAULT NULL COMMENT 'Статус актуальности адресного объекта ФИАС. Актуальный адрес на текущую дату. Обычно последняя запись об адресном объекте.',
	`CENTSTATUS` INT(11) NULL DEFAULT NULL COMMENT 'Статус центра',
	`OPERSTATUS` INT(11) NULL DEFAULT NULL COMMENT 'Статус действия над записью – причина появления записи (см. описание таблицы OperationStatus)',
	`CURRSTATUS` INT(11) NULL DEFAULT NULL COMMENT 'Статус актуальности КЛАДР 4 (последние две цифры в коде)',
	`STARTDATE` DATE NULL DEFAULT NULL COMMENT 'Начало действия записи',
	`ENDDATE` DATE NULL DEFAULT NULL COMMENT 'Окончание действия записи',
	`NORMDOC` VARCHAR(36) NULL DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
	`LIVESTATUS` ENUM('0','1') NULL DEFAULT NULL COMMENT 'Признак действующего адресного объекта',
	INDEX `idx_key_offname` (`OFFNAME`),
	FULLTEXT INDEX `idx_offname` (`OFFNAME`)
)
	COMMENT='Классификатор адресообразующих элементов'
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

CREATE TABLE `AddressObjectType` (
	`LEVEL` INT(11) NULL DEFAULT NULL COMMENT 'Тип адресного объекта',
	`SCNAME` VARCHAR(10) NULL DEFAULT NULL COMMENT 'Краткое наименование типа объекта',
	`SOCRNAME` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Полное наименование типа объекта',
	`KOD_T_ST` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Ключевое поле',
	UNIQUE INDEX `uidx_kod_t_st` (`KOD_T_ST`),
	INDEX `idx_scname` (`SCNAME`)
)
	COMMENT='Тип адресного объекта'
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

use flm;

alter table flm.user
	add column locale enum('en', 'ru') not null default 'en' comment 'язык интерфейса'
	after `city_id`;

alter table flm.vacancy
	add column is_active int(1) unsigned not null default 0
comment 'Активна ли вакансия'
	after `update_date`;


CREATE TABLE flm.`company` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`opf` VARCHAR(64) NOT NULL COMMENT 'Организационно-праввовая форма (аббревиатура)',
	`official_name` VARCHAR(512) NOT NULL COMMENT 'Официальное название компании без опф',
	`full_with_opf` VARCHAR(512) NOT NULL COMMENT 'Официальное название компании с несокращенной опф',
	`inn` INT(11) UNSIGNED NOT NULL COMMENT 'ИНН организации',
	`kpp` INT(11) UNSIGNED NOT NULL COMMENT 'КПП организации',
	`ogrn` VARCHAR(32) NOT NULL COMMENT 'ОГРН организации',
	`address` VARCHAR(1024) NOT NULL COMMENT 'Юридический адрес организации',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `uidx_company_inn` (`inn`)
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

CREATE TABLE flm.`user_company` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `company_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uidx_user_id_company_id` (`user_id`, `company_id`),
  INDEX `idx_user_company_user_id` (`user_id`),
  INDEX `idx_user_company_company_id` (`company_id`),
  CONSTRAINT `fk_user_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `fk_user_company_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB
;

alter table flm.vacancy
	add column company_id int(11) unsigned not null comment 'компания, в которую ищется работник'
	after `user_id`;

alter table flm.vacancy
	add constraint `fk_vacancy_company_id`
foreign key (`company_id`)
references flm.company(`id`)
	on update cascade on delete restrict;

CREATE TABLE flm.`currency` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`iso_code` VARCHAR(3) NOT NULL COMMENT 'ISO код валюты',
	`name` VARCHAR(512) NOT NULL COMMENT 'Название на русском языке',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `uidx_currency_iso_code` (`iso_code`)
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

alter table flm.vacancy
	add column currency_id int(11) unsigned null default null
comment 'Валюта расчета'
	after `paycheck_const`,
	add constraint `fk_vacancy_currency_id`
foreign key (`currency_id`)
references flm.currency(`id`)
	on update cascade on delete set null;

alter table flm.vacancy
add column `is_active` INT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Активна ли вакансия';

alter table flm.vacancy
	drop column schedule_type;

alter table flm.vacancy
	add COLUMN `employment` ENUM('full_day','part_time','shift','watch') NOT NULL DEFAULT 'full_day' COMMENT 'тип занятости и графика работы' AFTER `description`;

#---maxim
