ALTER TABLE flm.`company`
	DROP INDEX `uidx_company_inn`;

ALTER TABLE flm.`company`
	ALTER `opf` DROP DEFAULT,
	ALTER `inn` DROP DEFAULT,
	ALTER `kpp` DROP DEFAULT,
	ALTER `ogrn` DROP DEFAULT,
	ALTER `address` DROP DEFAULT;

ALTER TABLE flm.`company`
	CHANGE COLUMN `opf` `opf` VARCHAR(64) NULL COMMENT 'Организационно-праввовая форма (аббревиатура)' AFTER `id`,
	CHANGE COLUMN `inn` `inn` VARCHAR(32) NULL COMMENT 'ИНН организации' AFTER `full_with_opf`,
	CHANGE COLUMN `kpp` `kpp` VARCHAR(32) NULL COMMENT 'КПП организации' AFTER `inn`,
	CHANGE COLUMN `ogrn` `ogrn` VARCHAR(32) NULL COMMENT 'ОГРН организации' AFTER `kpp`,
	CHANGE COLUMN `address` `address` VARCHAR(1024) NULL COMMENT 'Юридический адрес организации' AFTER `ogrn`;

ALTER TABLE flm.`company`
	ALTER `official_name` DROP DEFAULT;

ALTER TABLE flm.`company`
	CHANGE COLUMN `official_name` `official_name` VARCHAR(512) NOT NULL COMMENT 'Официальное название компании' AFTER `opf`,
	DROP COLUMN `full_with_opf`;

ALTER TABLE flm.`company`
	ADD COLUMN `description` TEXT NULL DEFAULT NULL COMMENT 'Описание деятельности компании' AFTER `address`;

ALTER TABLE flm.`user`
	DROP COLUMN `first_name`,
	DROP COLUMN `last_name`,
	DROP COLUMN `middle_name`;

CREATE TABLE flm.`company_contact` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`company_id` INT(11) UNSIGNED NOT NULL,
	`first_name` VARCHAR(512) NOT NULL COMMENT 'Имя',
	`last_name` VARCHAR(512) NOT NULL COMMENT 'Фамилия',
	`middle_name` VARCHAR(512) NULL DEFAULT NULL COMMENT 'Отчество',
	`position` VARCHAR(512) NOT NULL COMMENT 'Должность',
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
;

alter table flm.company_contact
add constraint fk_contact_company_id foreign key (company_id) references flm.company(id) on update cascade on delete restrict;

ALTER TABLE flm.`user`
	ALTER `city_id` DROP DEFAULT;

ALTER TABLE flm.`user`
	ADD COLUMN `user_name` VARCHAR(512) NOT NULL AFTER `password`,
	CHANGE COLUMN `city_id` `city_id` INT(11) UNSIGNED NULL COMMENT 'Город пользователя' AFTER `role`,
	DROP COLUMN `birth_date`;

ALTER TABLE flm.`user`
	ADD COLUMN `is_confirmed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Подтвержден ли email' AFTER `role`;

# setup redis queue instance on port 6381

#sudo npm install

RENAME TABLE flm.`company_contact` TO `contact_person`;

create table flm.contact (
	id int(11) unsigned not null AUTO_INCREMENT,
	entity_id int(11) unsigned null default null comment 'Идентификатор сущности-владельца (резюме или вакансии и тд)',
	entity_type enum('user', 'resume', 'vacancy', 'contact_person') not null comment 'Тип сущности-владельца (резюме или вакансия и тд)',
	email varchar(512) null default null comment 'Контактный адрес электронной почты',
	phone varchar(128) null default null comment 'Контактный номер телефона',
	skype varchar (128) null default null comment 'Skypename',
	primary key (id),
	unique index (entity_id, entity_type, email),
	unique index (entity_id, entity_type, phone),
	unique index (entity_id, entity_type, skype)
)
COLLATE='utf8_general_ci';

ALTER TABLE flm.`resume`
	ADD COLUMN `birth_date` DATE NULL COMMENT 'Дата рождения' AFTER `user_id`;

ALTER TABLE flm.`resume`
	ADD COLUMN `gender` ENUM('male','female') NOT NULL DEFAULT 'male' COMMENT 'Пол' AFTER `birth_date`,
	CHANGE COLUMN `city_id` `city_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Город прживания' AFTER `gender`,
	ADD COLUMN `is_relocate_ready` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Готов к переезду' AFTER `position_name`,
	ADD COLUMN `skill_tags` TEXT NOT NULL COMMENT 'Навыки и умения' AFTER `desired_paycheck`;

update flm.resume set birth_date = DATE(now());

ALTER TABLE flm.`resume`
	ADD COLUMN `first_name` VARCHAR(512) NOT NULL AFTER `user_id`,
	ADD COLUMN `middle_name` VARCHAR(512) NULL AFTER `first_name`,
	ADD COLUMN `last_name` VARCHAR(512) NOT NULL AFTER `middle_name`,
	CHANGE COLUMN `birth_date` `birth_date` DATE NOT NULL COMMENT 'Дата рождения' AFTER `last_name`;

CREATE TABLE `language_proficiency` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`resume_id` INT(11) UNSIGNED NOT NULL,
	`language_code` varchar(10) NOT NULL comment 'html код языка',
	`level` ENUM('base', 'tech', 'speak', 'free') NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
;

ALTER TABLE `language_proficiency`
	ADD CONSTRAINT `lang_proficiency_resume_id` FOREIGN KEY (`resume_id`) REFERENCES `resume` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `resume`
	CHANGE COLUMN `city_id` `city_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Город прживания' AFTER `gender`,
	CHANGE COLUMN `is_relocate_ready` `is_relocate_ready` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Готов к переезду' AFTER `position_name`,
	ADD COLUMN `is_married` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Семейное положение' AFTER `is_relocate_ready`,
	ADD COLUMN `has_children` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Есть дети' AFTER `is_married`;

ALTER TABLE `resume`
	ADD COLUMN `currency_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Валюта' AFTER `desired_paycheck`;

ALTER TABLE `education`
	ALTER `specialty` DROP DEFAULT;
ALTER TABLE `education`
	CHANGE COLUMN `specialty` `speciality` VARCHAR(1024) NOT NULL COMMENT 'Название специальности' AFTER `resume_id`;

ALTER TABLE `resume`
	ADD COLUMN `is_remote` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Только удаленная работа' AFTER `is_relocate_ready`;

ALTER TABLE `resume`
	ADD COLUMN `employment` ENUM('full_day','part_time','shift','watch') NULL DEFAULT NULL COMMENT 'Тип занятости' AFTER `is_remote`;

ALTER TABLE `resume`
	CHANGE COLUMN `education_level` `education_level` ENUM('basic_general','secondary_general','secondary_vocational','higher_bachelor','higher_master','phd','doctor') NULL DEFAULT NULL COMMENT 'Уровень образования' AFTER `has_children`;

ALTER TABLE `resume`
	CHANGE COLUMN `skill_tags` `skill_tags` TEXT NULL COMMENT 'Навыки и умения' AFTER `currency_id`;

ALTER TABLE `resume`
	ADD COLUMN `profarea_id` INT(11) UNSIGNED NULL COMMENT 'Профобласть' AFTER `position_name`;

ALTER TABLE `resume`
	ADD CONSTRAINT `fk_resume_profarea_id` FOREIGN KEY (`profarea_id`) REFERENCES `profarea` (`id`);

ALTER TABLE `resume`
	CHANGE COLUMN `is_married` `is_married` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Семейное положение' AFTER `employment`,
	CHANGE COLUMN `has_children` `has_children` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Есть дети' AFTER `is_married`;

create table flm.tag (
	id int(11) unsigned not null AUTO_INCREMENT,
	name varchar(128) not null COMMENT 'собственно тег',
	PRIMARY KEY (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table entity_tag (
	id int(11) unsigned not null AUTO_INCREMENT,
	tag_id int(11) unsigned not null,
	entity_id int(11) unsigned not null,
	entity_type enum('vacancy', 'resume') not null,
	primary key (id),
	CONSTRAINT `fk_entity_tag_tag_id` foreign key (tag_id) references tag (id) on update restrict on delete restrict
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `resume`
	DROP COLUMN `skill_tags`;

ALTER TABLE `contact`
ADD COLUMN `type` ENUM('email','phone','skype') NOT NULL COMMENT 'тип контакта' AFTER `entity_type`,
CHANGE COLUMN `email` `value` VARCHAR(512) NULL DEFAULT NULL COMMENT 'контакт (адрес, номер и тд)' AFTER `type`,
DROP COLUMN `phone`,
DROP COLUMN `skype`,
DROP INDEX `entity_id_2`,
DROP INDEX `entity_id_3`,
DROP INDEX `entity_id`,
ADD UNIQUE INDEX `uidx_contact` (`entity_id`, `entity_type`, `type`, `value`);

CREATE TABLE `photo` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`resume_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	`path` VARCHAR(512) NOT NULL COMMENT 'Адрес к файлу в файловой системе',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `photo`
ADD CONSTRAINT `fk_photo_resume_id` FOREIGN KEY (`resume_id`) REFERENCES `resume` (`id`) ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE `resume`
	ADD COLUMN `photo_path` VARCHAR(512) NULL COMMENT 'Фото' AFTER `last_name`;

#ashkart
#stets