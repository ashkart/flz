CREATE TABLE flm.`conversation` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE flm.`callback` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL COMMENT 'Пользователь-создатель отклика',
	`entity_type` ENUM('vacancy','resume') NOT NULL COMMENT 'Отклик на какой тип документа',
	`entity_id` INT(11) UNSIGNED NOT NULL COMMENT 'Документ, получивший отклик',
	`from_entity_type` ENUM('vacancy','resume') NOT NULL COMMENT 'Тип документа в отклике',
	`from_entity_id` INT(11) UNSIGNED NOT NULL COMMENT 'Предложенный к рассмотрению документ ',
	`conversation_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_callback_user_id` (`user_id`),
	INDEX `fa_callback_conversation_id` (`conversation_id`),
	CONSTRAINT `fa_callback_conversation_id` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`),
	CONSTRAINT `fk_callback_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE flm.`conversation_message` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`text` VARCHAR(4096) NOT NULL,
	`conversation_id` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_conv_message_conversation_id` (`conversation_id`),
	CONSTRAINT `fk_conv_message_conversation_id` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE flm.`conversation_user` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL,
	`conversation_id` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_conversation_user_conv_id` (`conversation_id`),
	INDEX `fk_conversation_user_user_id` (`user_id`),
	CONSTRAINT `fk_conversation_user_conv_id` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`),
	CONSTRAINT `fk_conversation_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

# php Application/cli.php elasticsearch resume-index
# sudo npm install vanilla-modal --save

ALTER TABLE `conversation`
	ADD COLUMN `creator_user_id` INT(11) UNSIGNED NOT NULL AFTER `id`,
	ADD COLUMN `create_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `creator_user_id`,
	ADD CONSTRAINT `fk_conversation_creator_user_id` FOREIGN KEY (`creator_user_id`) REFERENCES `user` (`id`);

# sudo npm install react-infinite-scroller --save

ALTER TABLE `callback`
	ADD COLUMN `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата и время создания отклика' AFTER `conversation_id`;

ALTER TABLE `conversation_message`
	ADD COLUMN `from_user_id` INT(11) UNSIGNED NOT NULL COMMENT 'автор' AFTER `conversation_id`;

ALTER TABLE `conversation_message`
	ADD CONSTRAINT `fk_conv_message_from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`);

ALTER TABLE `conversation_message`
	ADD COLUMN `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `from_user_id`;

ALTER TABLE `callback`
	ADD COLUMN `state` ENUM('new', 'watched','accepted','rejected','revoked') NOT NULL COMMENT 'Статус отклика'
	AFTER `conversation_id`;

ALTER TABLE `callback`
	ADD COLUMN `update_date` TIMESTAMP NOT NULL COMMENT 'Дата и время смены статуса'
	AFTER `state`;

ALTER TABLE `callback`
	CHANGE COLUMN `update_date` `update_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата и время смены статуса'
	AFTER `state`;

CREATE TABLE `view` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`entity_id` INT(11) UNSIGNED NOT NULL,
	`entity_type` ENUM('callback', 'vacancy', 'resume') NOT NULL,
	`user_id` INT(11) UNSIGNED NULL,
	`company_id` INT(11) UNSIGNED NULL,
	`date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `idx_view_entity` (`entity_id`, `entity_type`)
)
COLLATE='utf8_general_ci'
;

ALTER TABLE `view`
	ADD CONSTRAINT `fk_view_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	ADD CONSTRAINT `fk_view_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

# php7 Application/cli.php elasticsearch view-index

# reverce geocoding procs

DELIMITER //

drop function if exists GCDist //
CREATE FUNCTION GCDist (
        _lat1 DOUBLE,  -- Scaled Degrees north for one point
        _lon1 DOUBLE,  -- Scaled Degrees west for one point
        _lat2 DOUBLE,  -- other point
        _lon2 DOUBLE
    ) RETURNS DOUBLE
    DETERMINISTIC
    CONTAINS SQL  -- SQL but does not read or write
    SQL SECURITY INVOKER  -- No special privileges granted
-- Input is a pair of latitudes/longitudes multiplied by 10000.
--    For example, the south pole has latitude -900000.
-- Multiply output by .0069172 to get miles between the two points
--    or by .0111325 to get kilometers
BEGIN
    -- Hardcoded constant:
    DECLARE _deg2rad DOUBLE DEFAULT PI()/180;  -- For scaled by 1e4 to MEDIUMINT
    DECLARE _rlat1 DOUBLE DEFAULT _deg2rad * _lat1;
    DECLARE _rlat2 DOUBLE DEFAULT _deg2rad * _lat2;
    -- compute as if earth's radius = 1.0
    DECLARE _rlond DOUBLE DEFAULT _deg2rad * (_lon1 - _lon2);
    DECLARE _m     DOUBLE DEFAULT COS(_rlat2);
    DECLARE _x     DOUBLE DEFAULT COS(_rlat1) - _m * COS(_rlond);
    DECLARE _y     DOUBLE DEFAULT               _m * SIN(_rlond);
    DECLARE _z     DOUBLE DEFAULT SIN(_rlat1) - SIN(_rlat2);
    DECLARE _n     DOUBLE DEFAULT SQRT(
                        _x * _x +
                        _y * _y +
                        _z * _z    );
    RETURN  2 * ASIN(_n / 2) / _deg2rad;   -- again--scaled degrees
END;
//
DELIMITER ;

DELIMITER //
-- FindNearest (about my 6th approach)
drop procedure if exists FindNearest6 //
CREATE
PROCEDURE FindNearest (
        IN _my_lat DOUBLE,  -- Latitude of me [-90..90] (not scaled)
        IN _my_lon DOUBLE,  -- Longitude [-180..180]
        IN _START_dist DOUBLE,  -- Starting estimate of how far to search: miles or km
        IN _max_dist DOUBLE,  -- Limit how far to search: miles or km
        IN _limit INT,     -- How many items to try to get
        IN _condition VARCHAR(1111)   -- will be ANDed in a WHERE clause
    )
    DETERMINISTIC
BEGIN
    -- lat and lng are in degrees -90..+90 and -180..+180
    -- All computations done in Latitude degrees.
    -- Thing to tailor
    --   *Locations* -- the table
    --   Scaling of lat, lon; here using *10000 in MEDIUMINT
    --   Table name
    --   miles versus km.

    -- Hardcoded constant:
    DECLARE _deg2rad DOUBLE DEFAULT PI()/180;  -- For scaled by 1e4 to MEDIUMINT

    -- Cannot use params in PREPARE, so switch to @variables:
    -- Hardcoded constant:
    SET @my_lat := _my_lat * 1,
        @my_lon := _my_lon * 1,
        @deg2dist := 111.325,  -- 69.172 for miles; 111.325 for km  *** (mi vs km)
        @start_deg := _start_dist / @deg2dist,  -- Start with this radius first (eg, 15 miles)
        @max_deg := _max_dist / @deg2dist,
        @cutoff := @max_deg / SQRT(2),  -- (slightly pessimistic)
        @dlat := @start_deg,  -- note: must stay positive
        @lon2lat := COS(_deg2rad * @my_lat),
        @iterations := 0;        -- just debugging

    -- Loop through, expanding search
    --   Search a 'square', repeat with bigger square until find enough rows
    --   If the inital probe found _limit rows, then probably the first
    --   iteration here will find the desired data.
    -- Hardcoded table name:
    -- This is the "first SELECT":
    SET @sql = CONCAT(
        "SELECT COUNT(*) INTO @near_ct
            FROM Locations
            WHERE lat    BETWEEN @my_lat - @dlat
                             AND @my_lat + @dlat   -- PARTITION Pruning and bounding box
              AND lon    BETWEEN @my_lon - @dlon
                             AND @my_lon + @dlon   -- first part of PK
              AND ", _condition);
    PREPARE _sql FROM @sql;
    MainLoop: LOOP
        SET @iterations := @iterations + 1;
        -- The main probe: Search a 'square'
        SET @dlon := ABS(@dlat / @lon2lat);  -- good enough for now  -- note: must stay positive
        -- Hardcoded constants:
        SET @dlon := IF(ABS(@my_lat) + @dlat >= 90, 361, @dlon);  -- near a Pole
        EXECUTE _sql;
        IF ( @near_ct >= _limit OR         -- Found enough
             @dlat >= @cutoff ) THEN       -- Give up (too far)
            LEAVE MainLoop;
        END IF;
        -- Expand 'square':
        SET @dlat := LEAST(2 * @dlat, @cutoff);   -- Double the radius to search
    END LOOP MainLoop;
    DEALLOCATE PREPARE _sql;

    -- Out of loop because found _limit items, or going too far.
    -- Expand range by about 1.4 (but not past _max_dist),
    -- then fetch details on nearest 10.

    -- Hardcoded constant:
    SET @dlat := IF( @dlat >= @max_deg OR @dlon >= 180,
                @max_deg,
                GCDist(ABS(@my_lat), @my_lon,
                       ABS(@my_lat) - @dlat, @my_lon - @dlon) );
            -- ABS: go toward equator to find farthest corner (also avoids poles)
            -- Dateline: not a problem (see GCDist code)

    -- Reach for longitude line at right angle:
    -- sin(dlon)*cos(lat) = sin(dlat)
    -- Hardcoded constant:
    SET @dlon := IFNULL(ASIN(SIN(_deg2rad * @dlat) /
                             COS(_deg2rad * @my_lat))
                            / _deg2rad -- precise
                        , 361);    -- must be too near a pole

    -- This is the "last SELECT":
    -- Hardcoded constants:
    IF (ABS(@my_lon) + @dlon < 180 OR    -- Usual case - not crossing dateline
        ABS(@my_lat) + @dlat <  90) THEN -- crossing pole, so dateline not an issue
        -- Hardcoded table name:
        SET @sql = CONCAT(
            "SELECT *,
                    @deg2dist * GCDist(@my_lat, @my_lon, lat, lon) AS dist
                FROM Locations
                WHERE lat BETWEEN @my_lat - @dlat
                              AND @my_lat + @dlat   -- PARTITION Pruning and bounding box
                  AND lon BETWEEN @my_lon - @dlon
                              AND @my_lon + @dlon   -- first part of PK
                  AND ", _condition, "
                HAVING dist <= ", _max_dist, "
                ORDER BY dist
                LIMIT ", _limit
                        );
    ELSE
        -- Hardcoded constants and table name:
        -- Circle crosses dateline, do two SELECTs, one for each side
        SET @west_lon := IF(@my_lon < 0, @my_lon, @my_lon - 360);
        SET @east_lon := @west_lon + 360;
        -- One of those will be beyond +/- 180; this gets points beyond the dateline
        SET @sql = CONCAT(
            "( SELECT *,
                    @deg2dist * GCDist(@my_lat, @west_lon, lat, lon) AS dist
                FROM Locations
                WHERE lat BETWEEN @my_lat - @dlat
                              AND @my_lat + @dlat   -- PARTITION Pruning and bounding box
                  AND lon BETWEEN @west_lon - @dlon
                              AND @west_lon + @dlon   -- first part of PK
                  AND ", _condition, "
                HAVING dist <= ", _max_dist, " )
            UNION ALL
            ( SELECT *,
                    @deg2dist * GCDist(@my_lat, @east_lon, lat, lon) AS dist
                FROM Locations
                WHERE lat BETWEEN @my_lat - @dlat
                              AND @my_lat + @dlat   -- PARTITION Pruning and bounding box
                  AND lon BETWEEN @east_lon - @dlon
                              AND @east_lon + @dlon   -- first part of PK
                  AND ", _condition, "
                HAVING dist <= ", _max_dist, " )
            ORDER BY dist
            LIMIT ", _limit
                        );
    END IF;

    PREPARE _sql FROM @sql;
    EXECUTE _sql;
    DEALLOCATE PREPARE _sql;
END;
//
DELIMITER ;

#sudo npm install react-geolocated --save


# import geo_compact db

delete from flm.city where country_id > 3;

ALTER TABLE flm.`city_coords`
	COMMENT='используется в функции FindNearest';

ALTER TABLE flm.`city`
	CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE flm.`city`
	ADD COLUMN `title_ru` VARCHAR(150) NULL DEFAULT NULL AFTER `title`,
	ADD COLUMN `geoname_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `longtitude`;

ALTER TABLE `city`
	CHANGE COLUMN `is_important` `is_important` TINYINT(1) NOT NULL DEFAULT '1' AFTER `region_id`;

insert into flm.city (country_id, region_id, title_ru, latitude, longtitude, geoname_id)
	select c.country_id, c.region_id, c.title, c.latitude, c.longtitude, c.geoname_id from geo_copmpact.city c;


update flm.city c
set c.title_ru = c.title where c.country_id <= 3;

ALTER TABLE `city`
	CHANGE COLUMN `title` `title_en` VARCHAR(150) NULL DEFAULT NULL AFTER `is_important`;



CREATE TABLE flm.`vacancy_location` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`vacancy_id` INT(11) UNSIGNED NOT NULL,
	`country_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	`city_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_vacancy_id` (`vacancy_id`),
	INDEX `fk_country_id` (`country_id`),
	INDEX `fk_city_id` (`city_id`),
	CONSTRAINT `fk_city_id` FOREIGN KEY (`city_id`) REFERENCES flm.`city` (`id`),
	CONSTRAINT `fk_country_id` FOREIGN KEY (`country_id`) REFERENCES flm.`country` (`id`),
	CONSTRAINT `fk_vacancy_id` FOREIGN KEY (`vacancy_id`) REFERENCES flm.`vacancy` (`id`)
)
ENGINE=InnoDB
;

insert into flm.vacancy_location (vacancy_id, country_id, city_id)
	select v.id, c.id, v.city_id
	from flm.vacancy v
	join flm.city ct on ct.id = v.city_id
	join flm.country c on c.id = ct.country_id
;

ALTER TABLE flm.`vacancy`
	DROP COLUMN `city_id`;

# php Application/cli.php elasticsearch resume-keyword
# php Application/cli.php elasticsearch vacancy-keyword-index
# php Application/cli.php elasticsearch vacancy-index
# php Application/cli.php elasticsearch resume-index


ALTER TABLE flm.`user`
	ADD COLUMN `vk_id` INT(11) NULL COMMENT 'Id юзера из Вк' AFTER `id`;


ALTER TABLE flm.`user`
	CHANGE COLUMN `vk_id` `vk_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Id юзера из Вк' AFTER `id`,
	ADD COLUMN `fb_id` varchar(32) NULL DEFAULT NULL COMMENT 'Id юзера из фейсбука' AFTER `vk_id`,
	ADD UNIQUE INDEX `fb_id` (`fb_id`);

#ashkart
#server